///*******************************************************************************
// * Copyright 2012 bmanuel
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// * http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
//package com.klolo.game
//
//import com.badlogic.gdx.Application.ApplicationType
//import com.badlogic.gdx.Gdx
//import com.badlogic.gdx.graphics.GL20
//import com.badlogic.gdx.utils.Disposable
//import com.bitfire.postprocessing.PostProcessor
//import com.bitfire.postprocessing.PostProcessorListener
//import com.bitfire.postprocessing.effects.*
//import com.bitfire.postprocessing.filters.Combine
//import com.bitfire.postprocessing.filters.CrtScreen.Effect
//import com.bitfire.postprocessing.filters.CrtScreen.RgbMode
//import com.bitfire.postprocessing.filters.RadialBlur
//import com.bitfire.postprocessing.effects.CrtMonitor
//
//
//
///**
// * Encapsulates postprocessing functionalities
// *
// * @author bmanuel
// */
//class PostProcessing : Disposable, PostProcessorListener {
//
//    var postProcessor: PostProcessor
//    var bloom: Bloom
//    var curvature: Curvature
//    var zoomer: Zoomer
//    var crt: CrtMonitor
//    var vignette: Vignette
//    var zoomRadialBlur: Boolean = false
//    var zoomAmount: Float = 0.toFloat()
//    var zoomFactor: Float = 0.toFloat()
//    var fxaa: Fxaa
//    var motionBlur: MotionBlur
//    private var blending: Boolean = false
//
//    var isEnabled: Boolean
//        get() = postProcessor.isEnabled
//        set(enabled) {
//            postProcessor.isEnabled = enabled
//        }
//
//    val isReady: Boolean
//        get() = postProcessor.isReady
//
//    init {
//        val isDesktop = Gdx.app.type == ApplicationType.Desktop
//        val vpW = Gdx.graphics.width
//        val vpH = Gdx.graphics.height
//        blending = false
//
//        // create the postprocessor
//        // ShaderLoader.Pedantic = false;
//        postProcessor = PostProcessor(false, true, isDesktop)
//        postProcessor.isEnabled = true
//        // optionally create a listener
//        postProcessor.setListener(this)
//        PostProcessor.EnableQueryStates = false
//
//        // create the effects you want
//        bloom = Bloom((Gdx.graphics.width * 0.25f).toInt(), (Gdx.graphics.height * 0.25f).toInt())
//        bloom.isEnabled = true
//
//        curvature = Curvature()
//        zoomer = Zoomer(vpW, vpH, if (isDesktop) RadialBlur.Quality.VeryHigh else RadialBlur.Quality.Low)
//        val effects = Effect.TweakContrast.v or Effect.PhosphorVibrance.v or Effect.Scanlines.v or Effect.Tint.v
//        crt = CrtMonitor(vpW, vpH, false, false, RgbMode.ChromaticAberrations, effects)
//        val combine = crt.combinePass
//        combine.source1Intensity = 0f
//        combine.source2Intensity = 1f
//        combine.source1Saturation = 0f
//        combine.source2Saturation = 1f
//
//        vignette = Vignette(vpW, vpH, false)
//
//        fxaa = Fxaa(Gdx.graphics.width, Gdx.graphics.height)
//
//        motionBlur = MotionBlur()
//        motionBlur.setBlurOpacity(0.3f)
//
//        // add them to the postprocessor
////        postProcessor.addEffect(curvature)
////        postProcessor.addEffect(zoomer)
////        postProcessor.addEffect(vignette)
////        postProcessor.addEffect(crt)
////        postProcessor.addEffect(bloom)
////        postProcessor.addEffect(fxaa)
//        postProcessor.addEffect(curvature)
//
//        initializeEffects()
//    }
//
//    fun switch() {
//        curvature.isEnabled = !curvature.isEnabled
//    }
//
//    private fun initializeEffects() {
//        // specify a negative value to blur inside-to-outside,
//        // so that to avoid artifacts at borders
//        zoomer.blurStrength = -0.1f
//        zoomer.setOrigin((Gdx.graphics.width / 2).toFloat(), (Gdx.graphics.height / 2).toFloat())
//        curvature.zoom = 1f
//        vignette.intensity = 1f
//
//        crt.isEnabled = false
//        vignette.isEnabled = false
//        curvature.isEnabled = false
//        zoomer.isEnabled = false
//    }
//
//    override fun dispose() {
//        postProcessor.dispose()
//    }
//
//    fun begin(): Boolean {
//        return postProcessor.capture()
//    }
//
//    fun end() {
//        postProcessor.render()
//    }
//
//    fun rebind() {
//        postProcessor.rebind()
//    }
//
//    fun enableBlending() {
//        bloom.enableBlending(GL20.GL_SRC_COLOR, GL20.GL_ONE_MINUS_SRC_COLOR)
//        blending = true
//    }
//
//    fun disableBlending() {
//        bloom.disableBlending()
//        blending = false
//    }
//
//    fun update(elapsedSecs: Float) {
//        // animate some effects
//        val smoothing = 1.5f
//        zoomFactor = lerp(zoomFactor * smoothing, zoomAmount / smoothing, 0.1f) * 0.5f
//        zoomer.zoom = 1f + 4.0f * zoomFactor
//        if (zoomRadialBlur) {
//            zoomer.blurStrength = -0.1f * zoomFactor
//        }
//
//        // this effect needs an external clock source to
//        // emulate scanlines properly
//        crt.setTime(elapsedSecs)
//    }
//
//    override fun beforeRenderToScreen() {
//        if (blending) {
//            Gdx.gl20.glEnable(GL20.GL_BLEND)
//            Gdx.gl20.glBlendFunc(GL20.GL_SRC_COLOR, GL20.GL_SRC_ALPHA)
//        }
//    }
//
//    private fun lerp(prev: Float, curr: Float, alpha: Float): Float {
//        return prev + alpha * (curr - prev)
//    }
//
//}
