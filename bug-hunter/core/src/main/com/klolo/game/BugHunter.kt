package com.klolo.game

import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Rectangle
import java.util.*

class BugHunter : ApplicationListener {
    private var screenWidth: Float = 0f
    private var screenHeight: Float = 0f
    private lateinit var sprite: Sprite
    private lateinit var spriteBatch: SpriteBatch
    private lateinit var bug: Texture
    private var bugs = emptyList<Rectangle>()
    private val speed = 200f

    override fun create() {
        sprite = Sprite(Texture(Gdx.files.internal("bucket.png")))
        bug = Texture(Gdx.files.internal("bug.png"))
        spriteBatch = SpriteBatch()
        screenWidth = Gdx.graphics.width.toFloat()
        screenHeight = Gdx.graphics.height.toFloat()
    }

    override fun resize(width: Int, height: Int) {

    }

    override fun render() {
        update()
        draw()
    }

    private fun update() {
        when {
            Gdx.input.isKeyPressed(Input.Keys.RIGHT) -> sprite.x += Gdx.graphics.deltaTime * (speed * 2)
            Gdx.input.isKeyPressed(Input.Keys.LEFT) -> sprite.x -= Gdx.graphics.deltaTime * (speed * 2)
        }

        if (bugs.size < 3) {
            bugs += Rectangle(Random().nextInt(screenHeight.toInt()).toFloat(), screenHeight + Random().nextInt(250).toFloat(), 25f, 25f)
        }

        bugs = bugs
                .asSequence()
                .onEach { it.y -= Gdx.graphics.deltaTime * speed }
                .filter { it.y > 0 }
                .filter { !it.overlaps(sprite.boundingRectangle) }
                .toList()
    }

    private fun draw() {
        Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        spriteBatch.begin()
        sprite.draw(spriteBatch)

        bugs.forEach {
            spriteBatch.draw(bug, it.x, it.y, it.width, it.height)
        }

        spriteBatch.end()
    }

    override fun pause() {

    }

    override fun resume() {

    }

    override fun dispose() {
        spriteBatch.dispose()
    }
}
