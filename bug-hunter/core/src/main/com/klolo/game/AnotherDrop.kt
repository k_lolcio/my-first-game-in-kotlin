package com.klolo.game

import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.*
import com.badlogic.gdx.math.Rectangle
import java.util.*


const val speed = 900f
const val bugSpeed = 300f
const val padding = 100f

class AnotherDrop : ApplicationListener {
    private lateinit var bucket: Sprite
    private lateinit var spriteBatch: Batch
    private lateinit var camera: OrthographicCamera
    private lateinit var bug: Texture

    private lateinit var particleAtlas: FileHandle
    private val effect = ParticleEffect()

    private var bugs = emptyList<Rectangle>()

    override fun create() {
        bucket = Sprite(Texture(Gdx.files.internal("bucket.png")))
        bucket.y = 100.0f
        spriteBatch = SpriteBatch()

        camera = OrthographicCamera()
        camera.setToOrtho(false, viewPortWith, viewPortHeight)

        bug = Texture(Gdx.files.internal("bug.png"))
        generateBug()
        initializeParticle()
    }

    private fun initializeParticle() {
        particleAtlas = Gdx.files.internal("")
        effect.load(Gdx.files.internal("superFire.p"), particleAtlas)
        effect.start()
    }

    private fun generateBug() {
        bugs += Rectangle(Random().nextInt(viewPortWith.toInt()) - padding, viewPortHeight, 64f, 64f)
    }

    override fun render() {
        update()

        Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        spriteBatch.begin()

        effect.setPosition(bucket.x + bucket.width / 2, bucket.y)
        effect.draw(spriteBatch, Gdx.graphics.deltaTime)
        bucket.draw(spriteBatch)

        bugs.forEach {
            spriteBatch.draw(bug, it.x, it.y)
            it.y -= bugSpeed * Gdx.graphics.deltaTime
        }

        bugs = bugs.filter { it.y + it.height > 0 }
        spriteBatch.end()
    }

    private fun update() {
        camera.update()
        spriteBatch.projectionMatrix = camera.combined
        processKeyboard()
        fixBucketPosition()
    }

    private fun fixBucketPosition() {
        if (bucket.x < 0) {
            bucket.x = 0f
        }

        if (bucket.x >= viewPortWith - padding) {
            bucket.x = viewPortWith - padding
        }
    }

    private fun processKeyboard() {
        when {
            Gdx.input.isKeyPressed(Input.Keys.LEFT) -> bucket.x -= speed * Gdx.graphics.deltaTime
            Gdx.input.isKeyPressed(Input.Keys.RIGHT) -> bucket.x += speed * Gdx.graphics.deltaTime
        }
    }

    override fun pause() {}

    override fun resume() {}

    override fun resize(width: Int, height: Int) {}

    override fun dispose() {}

}