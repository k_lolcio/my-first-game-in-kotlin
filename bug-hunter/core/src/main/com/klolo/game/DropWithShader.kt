package com.klolo.game

import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.ParticleEffect
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.utils.TimeUtils
import java.util.*

class DropWithShader : ApplicationListener {
    private lateinit var cam: OrthographicCamera
    private lateinit var bucket: Sprite
    private lateinit var spriteBatch: Batch
    private lateinit var camera: OrthographicCamera
    private lateinit var bug: Texture

    private lateinit var particleAtlas: FileHandle
    private val effect = ParticleEffect()

    private var bugs = emptyList<Rectangle>()

    override fun create() {
        cam = OrthographicCamera()
        //  ShaderProgram.pedantic = false;
        bucket = Sprite(Texture(Gdx.files.internal("bucket.png")))
        bucket.y = 100.0f

        spriteBatch = SpriteBatch()

        camera = OrthographicCamera()
        camera.setToOrtho(false, viewPortWith, viewPortHeight)

        bug = Texture(Gdx.files.internal("bug.png"))
        generateBug()
        initializeParticle()
    }

    private fun initializeParticle() {
        particleAtlas = Gdx.files.internal("")
        effect.load(Gdx.files.internal("superFire.p"), particleAtlas)
        effect.start()
    }

    private fun generateBug() {
        bugs += Rectangle(Random().nextInt(viewPortWith.toInt()) - padding, viewPortHeight, 64f, 64f)
    }

    override fun render() {
        update()
        cam.update();

        cam.setToOrtho(false, Gdx.graphics.getWidth().toFloat(), Gdx.graphics.getHeight().toFloat());

        Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        spriteBatch.setBlendFunction(GL20.GL_DST_COLOR, GL20.GL_SRC_ALPHA);
        spriteBatch.begin()
        spriteBatch.projectionMatrix = cam.combined

        effect.setPosition(bucket.x + bucket.width / 2, bucket.y)
        effect.draw(spriteBatch, Gdx.graphics.deltaTime)
        bucket.draw(spriteBatch)

        bugs.forEach {
            spriteBatch.draw(bug, it.x, it.y)
            it.y -= bugSpeed * Gdx.graphics.deltaTime
        }

        spriteBatch.shader = null

        bugs = bugs.filter { it.y + it.height > 0 }

        spriteBatch.end()

        spriteBatch.begin()
        bucket.draw(spriteBatch)
        spriteBatch.end()
    }

    val startMs = TimeUtils.millis()
    private fun update() {
        camera.update()
        spriteBatch.projectionMatrix = camera.combined
        processKeyboard()
        fixBucketPosition()
    }

    private fun fixBucketPosition() {
        if (bucket.x < 0) {
            bucket.x = 0f
        }

        if (bucket.x >= viewPortWith - padding) {
            bucket.x = viewPortWith - padding
        }
    }

    private fun processKeyboard() {
        when {
            Gdx.input.isKeyPressed(Input.Keys.LEFT) -> bucket.x -= speed * Gdx.graphics.deltaTime
            Gdx.input.isKeyPressed(Input.Keys.RIGHT) -> bucket.x += speed * Gdx.graphics.deltaTime
        }
    }

    override fun pause() {}

    override fun resume() {}

    override fun resize(width: Int, height: Int) {}

    override fun dispose() {}

}