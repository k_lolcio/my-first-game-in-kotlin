package com.klolo.game


import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.utils.TimeUtils

private const val dropSpeed = 300
private const val bucketSpeed = 900
private const val screenPadding = 64
const val viewPortWith = 1920f
const val viewPortHeight = 1080f

class Drop : ApplicationAdapter() {
    private lateinit var dropImage: Texture
    private lateinit var bucketImage: Texture
    private lateinit var camera: OrthographicCamera
    private lateinit var bucket: Rectangle
    private lateinit var batch: SpriteBatch

    private var score = 0
    private var raindrops: List<Rectangle> = mutableListOf()
    private var lastDropTime: Long = 0

    override fun create() {
        dropImage = Texture(Gdx.files.internal("bug.png"))
        bucketImage = Texture(Gdx.files.internal("bucket.png"))

        camera = OrthographicCamera()

        camera.setToOrtho(false, viewPortWith, viewPortHeight)
        batch = SpriteBatch()

        createBucket()
        spawnRaindrop()
    }

    private fun createBucket() {
        bucket = Rectangle()
                .apply {
                    x = (800 / 2 - 64 / 2).toFloat()
                    y = 20f
                    width = 64f
                    height = 64f
                }
    }

    private fun spawnRaindrop() {
        raindrops += Rectangle()
                .apply {
                    x = MathUtils.random(0, Gdx.graphics.width - 64).toFloat()
                    y = Gdx.graphics.height.toFloat() * 1.5f
                    width = 64f
                    height = 64f
                }

        lastDropTime = TimeUtils.nanoTime()
    }

    override fun render() {
        Gdx.gl.glClearColor(0f, 0f, 0.2f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        camera.update()

        // tell the SpriteBatch to render in the
        // coordinate system specified by the camera.
        batch.projectionMatrix = camera.combined

        batch.begin()
        batch.draw(bucketImage, bucket.x, bucket.y)
        raindrops.forEach { batch.draw(dropImage, it.x, it.y) }
        batch.end()

        processKeyboard()
        fixBucketPosition()

        // check if we need to create a new raindrop
        if (TimeUtils.nanoTime() - lastDropTime > 1000000000) spawnRaindrop()

        // move the raindrops, remove any that are beneath the bottom edge of
        // the screen or that hit the bucket. In the latter case we play back
        // a sound effect as well.
        raindrops.forEach { it.y -= dropSpeed * Gdx.graphics.deltaTime }

        raindrops
                .asSequence()
                .filter { it.overlaps(bucket) }
                .onEach { println("Score: ${score + 1}") }
                .forEach { _ -> score++ }

        raindrops = raindrops.filter { it.y + screenPadding > 0 && !it.overlaps(bucket) }
    }

    private fun fixBucketPosition() {
        bucket.x =
                when {
                    bucket.x < 0 -> 0f
                    bucket.x > (viewPortWith - screenPadding) -> (viewPortWith - screenPadding).toFloat()
                    else -> bucket.x
                }
    }

    private fun processKeyboard() {
        when {
            Gdx.input.isKeyPressed(Keys.LEFT) -> bucket.x -= bucketSpeed * Gdx.graphics.deltaTime
            Gdx.input.isKeyPressed(Keys.RIGHT) -> bucket.x += bucketSpeed * Gdx.graphics.deltaTime
        }
    }

    override fun dispose() {
        dropImage.dispose()
        bucketImage.dispose()
        batch.dispose()
    }
}