package com.klolo.game

import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Rectangle
import java.util.*

class Demo : ApplicationListener {

    private val bucketSprite: Sprite by lazy { Sprite(Texture(Gdx.files.internal("bucket.png"))) }

    private val bug: Texture by lazy { Texture(Gdx.files.internal("bug.png")) }

    private var bugPositions = emptyList<Rectangle>()

    private val batch: SpriteBatch by lazy { SpriteBatch() }

    override fun create() {
    }

    override fun resize(width: Int, height: Int) {
        createBug()
    }

    private fun createBug() {
        val rectangle = Rectangle()
        rectangle.x = Math.abs(Random().nextInt(Gdx.graphics.width).toFloat())
        rectangle.y = Gdx.graphics.height.toFloat() + Math.abs(Random().nextInt(Gdx.graphics.height).toFloat())
        bugPositions += rectangle
    }

    override fun render() {
        update()
        draw()
    }

    private fun update() {
        for (bug in bugPositions) {
            bug.y--

            if (bug.overlaps(bucketSprite.boundingRectangle)) {
                print("yeah!")
            }
        }

        bugPositions = bugPositions
                .filter { it.y > 0 }
                .filter { !it.overlaps(bucketSprite.boundingRectangle) }

        if (bugPositions.size < 10) {
            createBug()
        }

        val bucketSpeed = 600

        when {
            Gdx.input.isKeyPressed(Input.Keys.RIGHT) -> bucketSprite.x += Gdx.graphics.deltaTime * bucketSpeed
            Gdx.input.isKeyPressed(Input.Keys.LEFT) -> bucketSprite.x -= Gdx.graphics.deltaTime * bucketSpeed
        }

    }

    private fun draw() {
        Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        batch.begin()
        bucketSprite.draw(batch)

        bugPositions.forEach {
            batch.draw(bug, it.x, it.y)
        }

        batch.end()
    }

    override fun pause() {

    }

    override fun resume() {

    }

    override fun dispose() {

    }
}
