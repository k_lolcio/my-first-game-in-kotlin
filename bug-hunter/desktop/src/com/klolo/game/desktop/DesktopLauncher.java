package com.klolo.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.klolo.game.Demo;
import org.jetbrains.annotations.NotNull;

public class DesktopLauncher {
    public static void main(String[] arg) {
        new LwjglApplication(new Demo(), getApplicationConfiguration());
    }

    @NotNull
    private static LwjglApplicationConfiguration getApplicationConfiguration() {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.samples = 4;
        config.width = 1200;
        config.height = 800;
        return config;
    }
}
