! function(t, e) {
    "object" == typeof exports && "object" == typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define([], e) : "object" == typeof exports ? exports.KotlinPlayground = e() : t.KotlinPlayground = e()
}("undefined" != typeof self ? self : this, function() {
    return function(t) {
        function e(r) {
            if (n[r]) return n[r].exports;
            var i = n[r] = {
                i: r,
                l: !1,
                exports: {}
            };
            return t[r].call(i.exports, i, i.exports, e), i.l = !0, i.exports
        }
        var n = {};
        return e.m = t, e.c = n, e.d = function(t, n, r) {
            e.o(t, n) || Object.defineProperty(t, n, {
                configurable: !1,
                enumerable: !0,
                get: r
            })
        }, e.n = function(t) {
            var n = t && t.__esModule ? function() {
                return t.default
            } : function() {
                return t
            };
            return e.d(n, "a", n), n
        }, e.o = function(t, e) {
            return Object.prototype.hasOwnProperty.call(t, e)
        }, e.p = "", e(e.s = 32)
    }([function(t, e, n) {
        ! function(e, n) {
            t.exports = n()
        }(0, function() {
            "use strict";

            function t(t) {
                return new RegExp("(^|\\s)" + t + "(?:$|\\s)\\s*")
            }

            function e(t) {
                for (var e = t.childNodes.length; e > 0; --e) t.removeChild(t.firstChild);
                return t
            }

            function n(t, n) {
                return e(t).appendChild(n)
            }

            function r(t, e, n, r) {
                var i = document.createElement(t);
                if (n && (i.className = n), r && (i.style.cssText = r), "string" == typeof e) i.appendChild(document.createTextNode(e));
                else if (e)
                    for (var o = 0; o < e.length; ++o) i.appendChild(e[o]);
                return i
            }

            function i(t, e, n, i) {
                var o = r(t, e, n, i);
                return o.setAttribute("role", "presentation"), o
            }

            function o(t, e) {
                if (3 == e.nodeType && (e = e.parentNode), t.contains) return t.contains(e);
                do {
                    if (11 == e.nodeType && (e = e.host), e == t) return !0
                } while (e = e.parentNode)
            }

            function a() {
                var t;
                try {
                    t = document.activeElement
                } catch (e) {
                    t = document.body || null
                }
                for (; t && t.shadowRoot && t.shadowRoot.activeElement;) t = t.shadowRoot.activeElement;
                return t
            }

            function s(e, n) {
                var r = e.className;
                t(n).test(r) || (e.className += (r ? " " : "") + n)
            }

            function l(e, n) {
                for (var r = e.split(" "), i = 0; i < r.length; i++) r[i] && !t(r[i]).test(n) && (n += " " + r[i]);
                return n
            }

            function c(t) {
                var e = Array.prototype.slice.call(arguments, 1);
                return function() {
                    return t.apply(null, e)
                }
            }

            function u(t, e, n) {
                e || (e = {});
                for (var r in t) !t.hasOwnProperty(r) || !1 === n && e.hasOwnProperty(r) || (e[r] = t[r]);
                return e
            }

            function f(t, e, n, r, i) {
                null == e && -1 == (e = t.search(/[^\s\u00a0]/)) && (e = t.length);
                for (var o = r || 0, a = i || 0;;) {
                    var s = t.indexOf("\t", o);
                    if (s < 0 || s >= e) return a + (e - o);
                    a += s - o, a += n - a % n, o = s + 1
                }
            }

            function d(t, e) {
                for (var n = 0; n < t.length; ++n)
                    if (t[n] == e) return n;
                return -1
            }

            function h(t, e, n) {
                for (var r = 0, i = 0;;) {
                    var o = t.indexOf("\t", r); - 1 == o && (o = t.length);
                    var a = o - r;
                    if (o == t.length || i + a >= e) return r + Math.min(a, e - i);
                    if (i += o - r, i += n - i % n, r = o + 1, i >= e) return r
                }
            }

            function p(t) {
                for (; qa.length <= t;) qa.push(m(qa) + " ");
                return qa[t]
            }

            function m(t) {
                return t[t.length - 1]
            }

            function g(t, e) {
                for (var n = [], r = 0; r < t.length; r++) n[r] = e(t[r], r);
                return n
            }

            function v(t, e, n) {
                for (var r = 0, i = n(e); r < t.length && n(t[r]) <= i;) r++;
                t.splice(r, 0, e)
            }

            function y() {}

            function b(t, e) {
                var n;
                return Object.create ? n = Object.create(t) : (y.prototype = t, n = new y), e && u(e, n), n
            }

            function x(t) {
                return /\w/.test(t) || t > "" && (t.toUpperCase() != t.toLowerCase() || Ya.test(t))
            }

            function w(t, e) {
                return e ? !!(e.source.indexOf("\\w") > -1 && x(t)) || e.test(t) : x(t)
            }

            function C(t) {
                for (var e in t)
                    if (t.hasOwnProperty(e) && t[e]) return !1;
                return !0
            }

            function k(t) {
                return t.charCodeAt(0) >= 768 && Ja.test(t)
            }

            function _(t, e, n) {
                for (;
                    (n < 0 ? e > 0 : e < t.length) && k(t.charAt(e));) e += n;
                return e
            }

            function S(t, e, n) {
                for (var r = e > n ? -1 : 1;;) {
                    if (e == n) return e;
                    var i = (e + n) / 2,
                        o = r < 0 ? Math.ceil(i) : Math.floor(i);
                    if (o == e) return t(o) ? e : n;
                    t(o) ? n = o : e = o + r
                }
            }

            function E(t, e, n) {
                var o = this;
                this.input = n, o.scrollbarFiller = r("div", null, "CodeMirror-scrollbar-filler"), o.scrollbarFiller.setAttribute("cm-not-content", "true"), o.gutterFiller = r("div", null, "CodeMirror-gutter-filler"), o.gutterFiller.setAttribute("cm-not-content", "true"), o.lineDiv = i("div", null, "CodeMirror-code"), o.selectionDiv = r("div", null, null, "position: relative; z-index: 1"), o.cursorDiv = r("div", null, "CodeMirror-cursors"), o.measure = r("div", null, "CodeMirror-measure"), o.lineMeasure = r("div", null, "CodeMirror-measure"), o.lineSpace = i("div", [o.measure, o.lineMeasure, o.selectionDiv, o.cursorDiv, o.lineDiv], null, "position: relative; outline: none");
                var a = i("div", [o.lineSpace], "CodeMirror-lines");
                o.mover = r("div", [a], null, "position: relative"), o.sizer = r("div", [o.mover], "CodeMirror-sizer"), o.sizerWidth = null, o.heightForcer = r("div", null, null, "position: absolute; height: " + Ua + "px; width: 1px;"), o.gutters = r("div", null, "CodeMirror-gutters"), o.lineGutter = null, o.scroller = r("div", [o.sizer, o.heightForcer, o.gutters], "CodeMirror-scroll"), o.scroller.setAttribute("tabIndex", "-1"), o.wrapper = r("div", [o.scrollbarFiller, o.gutterFiller, o.scroller], "CodeMirror"), ba && xa < 8 && (o.gutters.style.zIndex = -1, o.scroller.style.paddingRight = 0), wa || ma && La || (o.scroller.draggable = !0), t && (t.appendChild ? t.appendChild(o.wrapper) : t(o.wrapper)), o.viewFrom = o.viewTo = e.first, o.reportedViewFrom = o.reportedViewTo = e.first, o.view = [], o.renderedView = null, o.externalMeasured = null, o.viewOffset = 0, o.lastWrapHeight = o.lastWrapWidth = 0, o.updateLineNumbers = null, o.nativeBarWidth = o.barHeight = o.barWidth = 0, o.scrollbarsClipped = !1, o.lineNumWidth = o.lineNumInnerWidth = o.lineNumChars = null, o.alignWidgets = !1, o.cachedCharWidth = o.cachedTextHeight = o.cachedPaddingH = null, o.maxLine = null, o.maxLineLength = 0, o.maxLineChanged = !1, o.wheelDX = o.wheelDY = o.wheelStartX = o.wheelStartY = null, o.shift = !1, o.selForContextMenu = null, o.activeTouch = null, n.init(o)
            }

            function T(t, e) {
                if ((e -= t.first) < 0 || e >= t.size) throw new Error("There is no line " + (e + t.first) + " in the document.");
                for (var n = t; !n.lines;)
                    for (var r = 0;; ++r) {
                        var i = n.children[r],
                            o = i.chunkSize();
                        if (e < o) {
                            n = i;
                            break
                        }
                        e -= o
                    }
                return n.lines[e]
            }

            function M(t, e, n) {
                var r = [],
                    i = e.line;
                return t.iter(e.line, n.line + 1, function(t) {
                    var o = t.text;
                    i == n.line && (o = o.slice(0, n.ch)), i == e.line && (o = o.slice(e.ch)), r.push(o), ++i
                }), r
            }

            function O(t, e, n) {
                var r = [];
                return t.iter(e, n, function(t) {
                    r.push(t.text)
                }), r
            }

            function L(t, e) {
                var n = e - t.height;
                if (n)
                    for (var r = t; r; r = r.parent) r.height += n
            }

            function A(t) {
                if (null == t.parent) return null;
                for (var e = t.parent, n = d(e.lines, t), r = e.parent; r; e = r, r = r.parent)
                    for (var i = 0; r.children[i] != e; ++i) n += r.children[i].chunkSize();
                return n + e.first
            }

            function N(t, e) {
                var n = t.first;
                t: do {
                    for (var r = 0; r < t.children.length; ++r) {
                        var i = t.children[r],
                            o = i.height;
                        if (e < o) {
                            t = i;
                            continue t
                        }
                        e -= o, n += i.chunkSize()
                    }
                    return n
                } while (!t.lines);
                for (var a = 0; a < t.lines.length; ++a) {
                    var s = t.lines[a],
                        l = s.height;
                    if (e < l) break;
                    e -= l
                }
                return n + a
            }

            function P(t, e) {
                return e >= t.first && e < t.first + t.size
            }

            function I(t, e) {
                return String(t.lineNumberFormatter(e + t.firstLineNumber))
            }

            function D(t, e, n) {
                if (void 0 === n && (n = null), !(this instanceof D)) return new D(t, e, n);
                this.line = t, this.ch = e, this.sticky = n
            }

            function F(t, e) {
                return t.line - e.line || t.ch - e.ch
            }

            function R(t, e) {
                return t.sticky == e.sticky && 0 == F(t, e)
            }

            function z(t) {
                return D(t.line, t.ch)
            }

            function j(t, e) {
                return F(t, e) < 0 ? e : t
            }

            function B(t, e) {
                return F(t, e) < 0 ? t : e
            }

            function H(t, e) {
                return Math.max(t.first, Math.min(e, t.first + t.size - 1))
            }

            function W(t, e) {
                if (e.line < t.first) return D(t.first, 0);
                var n = t.first + t.size - 1;
                return e.line > n ? D(n, T(t, n).text.length) : U(e, T(t, e.line).text.length)
            }

            function U(t, e) {
                var n = t.ch;
                return null == n || n > e ? D(t.line, e) : n < 0 ? D(t.line, 0) : t
            }

            function V(t, e) {
                for (var n = [], r = 0; r < e.length; r++) n[r] = W(t, e[r]);
                return n
            }

            function G() {
                Xa = !0
            }

            function K() {
                Za = !0
            }

            function $(t, e, n) {
                this.marker = t, this.from = e, this.to = n
            }

            function q(t, e) {
                if (t)
                    for (var n = 0; n < t.length; ++n) {
                        var r = t[n];
                        if (r.marker == e) return r
                    }
            }

            function Y(t, e) {
                for (var n, r = 0; r < t.length; ++r) t[r] != e && (n || (n = [])).push(t[r]);
                return n
            }

            function J(t, e) {
                t.markedSpans = t.markedSpans ? t.markedSpans.concat([e]) : [e], e.marker.attachLine(t)
            }

            function X(t, e, n) {
                var r;
                if (t)
                    for (var i = 0; i < t.length; ++i) {
                        var o = t[i],
                            a = o.marker,
                            s = null == o.from || (a.inclusiveLeft ? o.from <= e : o.from < e);
                        if (s || o.from == e && "bookmark" == a.type && (!n || !o.marker.insertLeft)) {
                            var l = null == o.to || (a.inclusiveRight ? o.to >= e : o.to > e);
                            (r || (r = [])).push(new $(a, o.from, l ? null : o.to))
                        }
                    }
                return r
            }

            function Z(t, e, n) {
                var r;
                if (t)
                    for (var i = 0; i < t.length; ++i) {
                        var o = t[i],
                            a = o.marker,
                            s = null == o.to || (a.inclusiveRight ? o.to >= e : o.to > e);
                        if (s || o.from == e && "bookmark" == a.type && (!n || o.marker.insertLeft)) {
                            var l = null == o.from || (a.inclusiveLeft ? o.from <= e : o.from < e);
                            (r || (r = [])).push(new $(a, l ? null : o.from - e, null == o.to ? null : o.to - e))
                        }
                    }
                return r
            }

            function Q(t, e) {
                if (e.full) return null;
                var n = P(t, e.from.line) && T(t, e.from.line).markedSpans,
                    r = P(t, e.to.line) && T(t, e.to.line).markedSpans;
                if (!n && !r) return null;
                var i = e.from.ch,
                    o = e.to.ch,
                    a = 0 == F(e.from, e.to),
                    s = X(n, i, a),
                    l = Z(r, o, a),
                    c = 1 == e.text.length,
                    u = m(e.text).length + (c ? i : 0);
                if (s)
                    for (var f = 0; f < s.length; ++f) {
                        var d = s[f];
                        if (null == d.to) {
                            var h = q(l, d.marker);
                            h ? c && (d.to = null == h.to ? null : h.to + u) : d.to = i
                        }
                    }
                if (l)
                    for (var p = 0; p < l.length; ++p) {
                        var g = l[p];
                        if (null != g.to && (g.to += u), null == g.from) {
                            var v = q(s, g.marker);
                            v || (g.from = u, c && (s || (s = [])).push(g))
                        } else g.from += u, c && (s || (s = [])).push(g)
                    }
                s && (s = tt(s)), l && l != s && (l = tt(l));
                var y = [s];
                if (!c) {
                    var b, x = e.text.length - 2;
                    if (x > 0 && s)
                        for (var w = 0; w < s.length; ++w) null == s[w].to && (b || (b = [])).push(new $(s[w].marker, null, null));
                    for (var C = 0; C < x; ++C) y.push(b);
                    y.push(l)
                }
                return y
            }

            function tt(t) {
                for (var e = 0; e < t.length; ++e) {
                    var n = t[e];
                    null != n.from && n.from == n.to && !1 !== n.marker.clearWhenEmpty && t.splice(e--, 1)
                }
                return t.length ? t : null
            }

            function et(t, e, n) {
                var r = null;
                if (t.iter(e.line, n.line + 1, function(t) {
                        if (t.markedSpans)
                            for (var e = 0; e < t.markedSpans.length; ++e) {
                                var n = t.markedSpans[e].marker;
                                !n.readOnly || r && -1 != d(r, n) || (r || (r = [])).push(n)
                            }
                    }), !r) return null;
                for (var i = [{
                        from: e,
                        to: n
                    }], o = 0; o < r.length; ++o)
                    for (var a = r[o], s = a.find(0), l = 0; l < i.length; ++l) {
                        var c = i[l];
                        if (!(F(c.to, s.from) < 0 || F(c.from, s.to) > 0)) {
                            var u = [l, 1],
                                f = F(c.from, s.from),
                                h = F(c.to, s.to);
                            (f < 0 || !a.inclusiveLeft && !f) && u.push({
                                from: c.from,
                                to: s.from
                            }), (h > 0 || !a.inclusiveRight && !h) && u.push({
                                from: s.to,
                                to: c.to
                            }), i.splice.apply(i, u), l += u.length - 3
                        }
                    }
                return i
            }

            function nt(t) {
                var e = t.markedSpans;
                if (e) {
                    for (var n = 0; n < e.length; ++n) e[n].marker.detachLine(t);
                    t.markedSpans = null
                }
            }

            function rt(t, e) {
                if (e) {
                    for (var n = 0; n < e.length; ++n) e[n].marker.attachLine(t);
                    t.markedSpans = e
                }
            }

            function it(t) {
                return t.inclusiveLeft ? -1 : 0
            }

            function ot(t) {
                return t.inclusiveRight ? 1 : 0
            }

            function at(t, e) {
                var n = t.lines.length - e.lines.length;
                if (0 != n) return n;
                var r = t.find(),
                    i = e.find(),
                    o = F(r.from, i.from) || it(t) - it(e);
                if (o) return -o;
                var a = F(r.to, i.to) || ot(t) - ot(e);
                return a || e.id - t.id
            }

            function st(t, e) {
                var n, r = Za && t.markedSpans;
                if (r)
                    for (var i = void 0, o = 0; o < r.length; ++o) i = r[o], i.marker.collapsed && null == (e ? i.from : i.to) && (!n || at(n, i.marker) < 0) && (n = i.marker);
                return n
            }

            function lt(t) {
                return st(t, !0)
            }

            function ct(t) {
                return st(t, !1)
            }

            function ut(t, e) {
                var n, r = Za && t.markedSpans;
                if (r)
                    for (var i = 0; i < r.length; ++i) {
                        var o = r[i];
                        o.marker.collapsed && (null == o.from || o.from < e) && (null == o.to || o.to > e) && (!n || at(n, o.marker) < 0) && (n = o.marker)
                    }
                return n
            }

            function ft(t, e, n, r, i) {
                var o = T(t, e),
                    a = Za && o.markedSpans;
                if (a)
                    for (var s = 0; s < a.length; ++s) {
                        var l = a[s];
                        if (l.marker.collapsed) {
                            var c = l.marker.find(0),
                                u = F(c.from, n) || it(l.marker) - it(i),
                                f = F(c.to, r) || ot(l.marker) - ot(i);
                            if (!(u >= 0 && f <= 0 || u <= 0 && f >= 0) && (u <= 0 && (l.marker.inclusiveRight && i.inclusiveLeft ? F(c.to, n) >= 0 : F(c.to, n) > 0) || u >= 0 && (l.marker.inclusiveRight && i.inclusiveLeft ? F(c.from, r) <= 0 : F(c.from, r) < 0))) return !0
                        }
                    }
            }

            function dt(t) {
                for (var e; e = lt(t);) t = e.find(-1, !0).line;
                return t
            }

            function ht(t) {
                for (var e; e = ct(t);) t = e.find(1, !0).line;
                return t
            }

            function pt(t) {
                for (var e, n; e = ct(t);) t = e.find(1, !0).line, (n || (n = [])).push(t);
                return n
            }

            function mt(t, e) {
                var n = T(t, e),
                    r = dt(n);
                return n == r ? e : A(r)
            }

            function gt(t, e) {
                if (e > t.lastLine()) return e;
                var n, r = T(t, e);
                if (!vt(t, r)) return e;
                for (; n = ct(r);) r = n.find(1, !0).line;
                return A(r) + 1
            }

            function vt(t, e) {
                var n = Za && e.markedSpans;
                if (n)
                    for (var r = void 0, i = 0; i < n.length; ++i)
                        if (r = n[i], r.marker.collapsed) {
                            if (null == r.from) return !0;
                            if (!r.marker.widgetNode && 0 == r.from && r.marker.inclusiveLeft && yt(t, e, r)) return !0
                        }
            }

            function yt(t, e, n) {
                if (null == n.to) {
                    var r = n.marker.find(1, !0);
                    return yt(t, r.line, q(r.line.markedSpans, n.marker))
                }
                if (n.marker.inclusiveRight && n.to == e.text.length) return !0;
                for (var i = void 0, o = 0; o < e.markedSpans.length; ++o)
                    if (i = e.markedSpans[o], i.marker.collapsed && !i.marker.widgetNode && i.from == n.to && (null == i.to || i.to != n.from) && (i.marker.inclusiveLeft || n.marker.inclusiveRight) && yt(t, e, i)) return !0
            }

            function bt(t) {
                t = dt(t);
                for (var e = 0, n = t.parent, r = 0; r < n.lines.length; ++r) {
                    var i = n.lines[r];
                    if (i == t) break;
                    e += i.height
                }
                for (var o = n.parent; o; n = o, o = n.parent)
                    for (var a = 0; a < o.children.length; ++a) {
                        var s = o.children[a];
                        if (s == n) break;
                        e += s.height
                    }
                return e
            }

            function xt(t) {
                if (0 == t.height) return 0;
                for (var e, n = t.text.length, r = t; e = lt(r);) {
                    var i = e.find(0, !0);
                    r = i.from.line, n += i.from.ch - i.to.ch
                }
                for (r = t; e = ct(r);) {
                    var o = e.find(0, !0);
                    n -= r.text.length - o.from.ch, r = o.to.line, n += r.text.length - o.to.ch
                }
                return n
            }

            function wt(t) {
                var e = t.display,
                    n = t.doc;
                e.maxLine = T(n, n.first), e.maxLineLength = xt(e.maxLine), e.maxLineChanged = !0, n.iter(function(t) {
                    var n = xt(t);
                    n > e.maxLineLength && (e.maxLineLength = n, e.maxLine = t)
                })
            }

            function Ct(t, e, n, r) {
                if (!t) return r(e, n, "ltr", 0);
                for (var i = !1, o = 0; o < t.length; ++o) {
                    var a = t[o];
                    (a.from < n && a.to > e || e == n && a.to == e) && (r(Math.max(a.from, e), Math.min(a.to, n), 1 == a.level ? "rtl" : "ltr", o), i = !0)
                }
                i || r(e, n, "ltr")
            }

            function kt(t, e, n) {
                var r;
                Qa = null;
                for (var i = 0; i < t.length; ++i) {
                    var o = t[i];
                    if (o.from < e && o.to > e) return i;
                    o.to == e && (o.from != o.to && "before" == n ? r = i : Qa = i), o.from == e && (o.from != o.to && "before" != n ? r = i : Qa = i)
                }
                return null != r ? r : Qa
            }

            function _t(t, e) {
                var n = t.order;
                return null == n && (n = t.order = ts(t.text, e)), n
            }

            function St(t, e) {
                return t._handlers && t._handlers[e] || es
            }

            function Et(t, e, n) {
                if (t.removeEventListener) t.removeEventListener(e, n, !1);
                else if (t.detachEvent) t.detachEvent("on" + e, n);
                else {
                    var r = t._handlers,
                        i = r && r[e];
                    if (i) {
                        var o = d(i, n);
                        o > -1 && (r[e] = i.slice(0, o).concat(i.slice(o + 1)))
                    }
                }
            }

            function Tt(t, e) {
                var n = St(t, e);
                if (n.length)
                    for (var r = Array.prototype.slice.call(arguments, 2), i = 0; i < n.length; ++i) n[i].apply(null, r)
            }

            function Mt(t, e, n) {
                return "string" == typeof e && (e = {
                    type: e,
                    preventDefault: function() {
                        this.defaultPrevented = !0
                    }
                }), Tt(t, n || e.type, t, e), It(e) || e.codemirrorIgnore
            }

            function Ot(t) {
                var e = t._handlers && t._handlers.cursorActivity;
                if (e)
                    for (var n = t.curOp.cursorActivityHandlers || (t.curOp.cursorActivityHandlers = []), r = 0; r < e.length; ++r) - 1 == d(n, e[r]) && n.push(e[r])
            }

            function Lt(t, e) {
                return St(t, e).length > 0
            }

            function At(t) {
                t.prototype.on = function(t, e) {
                    ns(this, t, e)
                }, t.prototype.off = function(t, e) {
                    Et(this, t, e)
                }
            }

            function Nt(t) {
                t.preventDefault ? t.preventDefault() : t.returnValue = !1
            }

            function Pt(t) {
                t.stopPropagation ? t.stopPropagation() : t.cancelBubble = !0
            }

            function It(t) {
                return null != t.defaultPrevented ? t.defaultPrevented : 0 == t.returnValue
            }

            function Dt(t) {
                Nt(t), Pt(t)
            }

            function Ft(t) {
                return t.target || t.srcElement
            }

            function Rt(t) {
                var e = t.which;
                return null == e && (1 & t.button ? e = 1 : 2 & t.button ? e = 3 : 4 & t.button && (e = 2)), Aa && t.ctrlKey && 1 == e && (e = 3), e
            }

            function zt(t) {
                if (null == Ha) {
                    var e = r("span", "​");
                    n(t, r("span", [e, document.createTextNode("x")])), 0 != t.firstChild.offsetHeight && (Ha = e.offsetWidth <= 1 && e.offsetHeight > 2 && !(ba && xa < 8))
                }
                var i = Ha ? r("span", "​") : r("span", " ", null, "display: inline-block; width: 1px; margin-right: -1px");
                return i.setAttribute("cm-text", ""), i
            }

            function jt(t) {
                if (null != Wa) return Wa;
                var r = n(t, document.createTextNode("AخA")),
                    i = Da(r, 0, 1).getBoundingClientRect(),
                    o = Da(r, 1, 2).getBoundingClientRect();
                return e(t), !(!i || i.left == i.right) && (Wa = o.right - i.right < 3)
            }

            function Bt(t) {
                if (null != ss) return ss;
                var e = n(t, r("span", "x")),
                    i = e.getBoundingClientRect(),
                    o = Da(e, 0, 1).getBoundingClientRect();
                return ss = Math.abs(i.left - o.left) > 1
            }

            function Ht(t, e) {
                arguments.length > 2 && (e.dependencies = Array.prototype.slice.call(arguments, 2)), ls[t] = e
            }

            function Wt(t, e) {
                cs[t] = e
            }

            function Ut(t) {
                if ("string" == typeof t && cs.hasOwnProperty(t)) t = cs[t];
                else if (t && "string" == typeof t.name && cs.hasOwnProperty(t.name)) {
                    var e = cs[t.name];
                    "string" == typeof e && (e = {
                        name: e
                    }), t = b(e, t), t.name = e.name
                } else {
                    if ("string" == typeof t && /^[\w\-]+\/[\w\-]+\+xml$/.test(t)) return Ut("application/xml");
                    if ("string" == typeof t && /^[\w\-]+\/[\w\-]+\+json$/.test(t)) return Ut("application/json")
                }
                return "string" == typeof t ? {
                    name: t
                } : t || {
                    name: "null"
                }
            }

            function Vt(t, e) {
                e = Ut(e);
                var n = ls[e.name];
                if (!n) return Vt(t, "text/plain");
                var r = n(t, e);
                if (us.hasOwnProperty(e.name)) {
                    var i = us[e.name];
                    for (var o in i) i.hasOwnProperty(o) && (r.hasOwnProperty(o) && (r["_" + o] = r[o]), r[o] = i[o])
                }
                if (r.name = e.name, e.helperType && (r.helperType = e.helperType), e.modeProps)
                    for (var a in e.modeProps) r[a] = e.modeProps[a];
                return r
            }

            function Gt(t, e) {
                u(e, us.hasOwnProperty(t) ? us[t] : us[t] = {})
            }

            function Kt(t, e) {
                if (!0 === e) return e;
                if (t.copyState) return t.copyState(e);
                var n = {};
                for (var r in e) {
                    var i = e[r];
                    i instanceof Array && (i = i.concat([])), n[r] = i
                }
                return n
            }

            function $t(t, e) {
                for (var n; t.innerMode && (n = t.innerMode(e)) && n.mode != t;) e = n.state, t = n.mode;
                return n || {
                    mode: t,
                    state: e
                }
            }

            function qt(t, e, n) {
                return !t.startState || t.startState(e, n)
            }

            function Yt(t, e, n, r) {
                var i = [t.state.modeGen],
                    o = {};
                re(t, e.text, t.doc.mode, n, function(t, e) {
                    return i.push(t, e)
                }, o, r);
                for (var a = n.state, s = 0; s < t.state.overlays.length; ++s) ! function(r) {
                    n.baseTokens = i;
                    var s = t.state.overlays[r],
                        l = 1,
                        c = 0;
                    n.state = !0, re(t, e.text, s.mode, n, function(t, e) {
                        for (var n = l; c < t;) {
                            var r = i[l];
                            r > t && i.splice(l, 1, t, i[l + 1], r), l += 2, c = Math.min(t, r)
                        }
                        if (e)
                            if (s.opaque) i.splice(n, l - n, t, "overlay " + e), l = n + 2;
                            else
                                for (; n < l; n += 2) {
                                    var o = i[n + 1];
                                    i[n + 1] = (o ? o + " " : "") + "overlay " + e
                                }
                    }, o), n.state = a, n.baseTokens = null, n.baseTokenPos = 1
                }(s);
                return {
                    styles: i,
                    classes: o.bgClass || o.textClass ? o : null
                }
            }

            function Jt(t, e, n) {
                if (!e.styles || e.styles[0] != t.state.modeGen) {
                    var r = Xt(t, A(e)),
                        i = e.text.length > t.options.maxHighlightLength && Kt(t.doc.mode, r.state),
                        o = Yt(t, e, r);
                    i && (r.state = i), e.stateAfter = r.save(!i), e.styles = o.styles, o.classes ? e.styleClasses = o.classes : e.styleClasses && (e.styleClasses = null), n === t.doc.highlightFrontier && (t.doc.modeFrontier = Math.max(t.doc.modeFrontier, ++t.doc.highlightFrontier))
                }
                return e.styles
            }

            function Xt(t, e, n) {
                var r = t.doc,
                    i = t.display;
                if (!r.mode.startState) return new hs(r, !0, e);
                var o = ie(t, e, n),
                    a = o > r.first && T(r, o - 1).stateAfter,
                    s = a ? hs.fromSaved(r, a, o) : new hs(r, qt(r.mode), o);
                return r.iter(o, e, function(n) {
                    Zt(t, n.text, s);
                    var r = s.line;
                    n.stateAfter = r == e - 1 || r % 5 == 0 || r >= i.viewFrom && r < i.viewTo ? s.save() : null, s.nextLine()
                }), n && (r.modeFrontier = s.line), s
            }

            function Zt(t, e, n, r) {
                var i = t.doc.mode,
                    o = new fs(e, t.options.tabSize, n);
                for (o.start = o.pos = r || 0, "" == e && Qt(i, n.state); !o.eol();) te(i, o, n.state), o.start = o.pos
            }

            function Qt(t, e) {
                if (t.blankLine) return t.blankLine(e);
                if (t.innerMode) {
                    var n = $t(t, e);
                    return n.mode.blankLine ? n.mode.blankLine(n.state) : void 0
                }
            }

            function te(t, e, n, r) {
                for (var i = 0; i < 10; i++) {
                    r && (r[0] = $t(t, n).mode);
                    var o = t.token(e, n);
                    if (e.pos > e.start) return o
                }
                throw new Error("Mode " + t.name + " failed to advance stream.")
            }

            function ee(t, e, n, r) {
                var i, o = t.doc,
                    a = o.mode;
                e = W(o, e);
                var s, l = T(o, e.line),
                    c = Xt(t, e.line, n),
                    u = new fs(l.text, t.options.tabSize, c);
                for (r && (s = []);
                    (r || u.pos < e.ch) && !u.eol();) u.start = u.pos, i = te(a, u, c.state), r && s.push(new ps(u, i, Kt(o.mode, c.state)));
                return r ? s : new ps(u, i, c.state)
            }

            function ne(t, e) {
                if (t)
                    for (;;) {
                        var n = t.match(/(?:^|\s+)line-(background-)?(\S+)/);
                        if (!n) break;
                        t = t.slice(0, n.index) + t.slice(n.index + n[0].length);
                        var r = n[1] ? "bgClass" : "textClass";
                        null == e[r] ? e[r] = n[2] : new RegExp("(?:^|s)" + n[2] + "(?:$|s)").test(e[r]) || (e[r] += " " + n[2])
                    }
                return t
            }

            function re(t, e, n, r, i, o, a) {
                var s = n.flattenSpans;
                null == s && (s = t.options.flattenSpans);
                var l, c = 0,
                    u = null,
                    f = new fs(e, t.options.tabSize, r),
                    d = t.options.addModeClass && [null];
                for ("" == e && ne(Qt(n, r.state), o); !f.eol();) {
                    if (f.pos > t.options.maxHighlightLength ? (s = !1, a && Zt(t, e, r, f.pos), f.pos = e.length, l = null) : l = ne(te(n, f, r.state, d), o), d) {
                        var h = d[0].name;
                        h && (l = "m-" + (l ? h + " " + l : h))
                    }
                    if (!s || u != l) {
                        for (; c < f.start;) c = Math.min(f.start, c + 5e3), i(c, u);
                        u = l
                    }
                    f.start = f.pos
                }
                for (; c < f.pos;) {
                    var p = Math.min(f.pos, c + 5e3);
                    i(p, u), c = p
                }
            }

            function ie(t, e, n) {
                for (var r, i, o = t.doc, a = n ? -1 : e - (t.doc.mode.innerMode ? 1e3 : 100), s = e; s > a; --s) {
                    if (s <= o.first) return o.first;
                    var l = T(o, s - 1),
                        c = l.stateAfter;
                    if (c && (!n || s + (c instanceof ds ? c.lookAhead : 0) <= o.modeFrontier)) return s;
                    var u = f(l.text, null, t.options.tabSize);
                    (null == i || r > u) && (i = s - 1, r = u)
                }
                return i
            }

            function oe(t, e) {
                if (t.modeFrontier = Math.min(t.modeFrontier, e), !(t.highlightFrontier < e - 10)) {
                    for (var n = t.first, r = e - 1; r > n; r--) {
                        var i = T(t, r).stateAfter;
                        if (i && (!(i instanceof ds) || r + i.lookAhead < e)) {
                            n = r + 1;
                            break
                        }
                    }
                    t.highlightFrontier = Math.min(t.highlightFrontier, n)
                }
            }

            function ae(t, e, n, r) {
                t.text = e, t.stateAfter && (t.stateAfter = null), t.styles && (t.styles = null), null != t.order && (t.order = null), nt(t), rt(t, n);
                var i = r ? r(t) : 1;
                i != t.height && L(t, i)
            }

            function se(t) {
                t.parent = null, nt(t)
            }

            function le(t, e) {
                if (!t || /^\s*$/.test(t)) return null;
                var n = e.addModeClass ? ys : vs;
                return n[t] || (n[t] = t.replace(/\S+/g, "cm-$&"))
            }

            function ce(t, e) {
                var n = i("span", null, null, wa ? "padding-right: .1px" : null),
                    r = {
                        pre: i("pre", [n], "CodeMirror-line"),
                        content: n,
                        col: 0,
                        pos: 0,
                        cm: t,
                        trailingSpace: !1,
                        splitSpaces: (ba || wa) && t.getOption("lineWrapping")
                    };
                e.measure = {};
                for (var o = 0; o <= (e.rest ? e.rest.length : 0); o++) {
                    var a = o ? e.rest[o - 1] : e.line,
                        s = void 0;
                    r.pos = 0, r.addToken = fe, jt(t.display.measure) && (s = _t(a, t.doc.direction)) && (r.addToken = he(r.addToken, s)), r.map = [];
                    me(a, r, Jt(t, a, e != t.display.externalMeasured && A(a))), a.styleClasses && (a.styleClasses.bgClass && (r.bgClass = l(a.styleClasses.bgClass, r.bgClass || "")), a.styleClasses.textClass && (r.textClass = l(a.styleClasses.textClass, r.textClass || ""))), 0 == r.map.length && r.map.push(0, 0, r.content.appendChild(zt(t.display.measure))), 0 == o ? (e.measure.map = r.map, e.measure.cache = {}) : ((e.measure.maps || (e.measure.maps = [])).push(r.map), (e.measure.caches || (e.measure.caches = [])).push({}))
                }
                if (wa) {
                    var c = r.content.lastChild;
                    (/\bcm-tab\b/.test(c.className) || c.querySelector && c.querySelector(".cm-tab")) && (r.content.className = "cm-tab-wrap-hack")
                }
                return Tt(t, "renderLine", t, e.line, r.pre), r.pre.className && (r.textClass = l(r.pre.className, r.textClass || "")), r
            }

            function ue(t) {
                var e = r("span", "•", "cm-invalidchar");
                return e.title = "\\u" + t.charCodeAt(0).toString(16), e.setAttribute("aria-label", e.title), e
            }

            function fe(t, e, n, i, o, a, s) {
                if (e) {
                    var l, c = t.splitSpaces ? de(e, t.trailingSpace) : e,
                        u = t.cm.state.specialChars,
                        f = !1;
                    if (u.test(e)) {
                        l = document.createDocumentFragment();
                        for (var d = 0;;) {
                            u.lastIndex = d;
                            var h = u.exec(e),
                                m = h ? h.index - d : e.length - d;
                            if (m) {
                                var g = document.createTextNode(c.slice(d, d + m));
                                ba && xa < 9 ? l.appendChild(r("span", [g])) : l.appendChild(g), t.map.push(t.pos, t.pos + m, g), t.col += m, t.pos += m
                            }
                            if (!h) break;
                            d += m + 1;
                            var v = void 0;
                            if ("\t" == h[0]) {
                                var y = t.cm.options.tabSize,
                                    b = y - t.col % y;
                                v = l.appendChild(r("span", p(b), "cm-tab")), v.setAttribute("role", "presentation"), v.setAttribute("cm-text", "\t"), t.col += b
                            } else "\r" == h[0] || "\n" == h[0] ? (v = l.appendChild(r("span", "\r" == h[0] ? "␍" : "␤", "cm-invalidchar")), v.setAttribute("cm-text", h[0]), t.col += 1) : (v = t.cm.options.specialCharPlaceholder(h[0]), v.setAttribute("cm-text", h[0]), ba && xa < 9 ? l.appendChild(r("span", [v])) : l.appendChild(v), t.col += 1);
                            t.map.push(t.pos, t.pos + 1, v), t.pos++
                        }
                    } else t.col += e.length, l = document.createTextNode(c), t.map.push(t.pos, t.pos + e.length, l), ba && xa < 9 && (f = !0), t.pos += e.length;
                    if (t.trailingSpace = 32 == c.charCodeAt(e.length - 1), n || i || o || f || s) {
                        var x = n || "";
                        i && (x += i), o && (x += o);
                        var w = r("span", [l], x, s);
                        return a && (w.title = a), t.content.appendChild(w)
                    }
                    t.content.appendChild(l)
                }
            }

            function de(t, e) {
                if (t.length > 1 && !/  /.test(t)) return t;
                for (var n = e, r = "", i = 0; i < t.length; i++) {
                    var o = t.charAt(i);
                    " " != o || !n || i != t.length - 1 && 32 != t.charCodeAt(i + 1) || (o = " "), r += o, n = " " == o
                }
                return r
            }

            function he(t, e) {
                return function(n, r, i, o, a, s, l) {
                    i = i ? i + " cm-force-border" : "cm-force-border";
                    for (var c = n.pos, u = c + r.length;;) {
                        for (var f = void 0, d = 0; d < e.length && (f = e[d], !(f.to > c && f.from <= c)); d++);
                        if (f.to >= u) return t(n, r, i, o, a, s, l);
                        t(n, r.slice(0, f.to - c), i, o, null, s, l), o = null, r = r.slice(f.to - c), c = f.to
                    }
                }
            }

            function pe(t, e, n, r) {
                var i = !r && n.widgetNode;
                i && t.map.push(t.pos, t.pos + e, i), !r && t.cm.display.input.needsContentAttribute && (i || (i = t.content.appendChild(document.createElement("span"))), i.setAttribute("cm-marker", n.id)), i && (t.cm.display.input.setUneditable(i), t.content.appendChild(i)), t.pos += e, t.trailingSpace = !1
            }

            function me(t, e, n) {
                var r = t.markedSpans,
                    i = t.text,
                    o = 0;
                if (r)
                    for (var a, s, l, c, u, f, d, h = i.length, p = 0, m = 1, g = "", v = 0;;) {
                        if (v == p) {
                            l = c = u = f = s = "", d = null, v = 1 / 0;
                            for (var y = [], b = void 0, x = 0; x < r.length; ++x) {
                                var w = r[x],
                                    C = w.marker;
                                "bookmark" == C.type && w.from == p && C.widgetNode ? y.push(C) : w.from <= p && (null == w.to || w.to > p || C.collapsed && w.to == p && w.from == p) ? (null != w.to && w.to != p && v > w.to && (v = w.to, c = ""), C.className && (l += " " + C.className), C.css && (s = (s ? s + ";" : "") + C.css), C.startStyle && w.from == p && (u += " " + C.startStyle), C.endStyle && w.to == v && (b || (b = [])).push(C.endStyle, w.to), C.title && !f && (f = C.title), C.collapsed && (!d || at(d.marker, C) < 0) && (d = w)) : w.from > p && v > w.from && (v = w.from)
                            }
                            if (b)
                                for (var k = 0; k < b.length; k += 2) b[k + 1] == v && (c += " " + b[k]);
                            if (!d || d.from == p)
                                for (var _ = 0; _ < y.length; ++_) pe(e, 0, y[_]);
                            if (d && (d.from || 0) == p) {
                                if (pe(e, (null == d.to ? h + 1 : d.to) - p, d.marker, null == d.from), null == d.to) return;
                                d.to == p && (d = !1)
                            }
                        }
                        if (p >= h) break;
                        for (var S = Math.min(h, v);;) {
                            if (g) {
                                var E = p + g.length;
                                if (!d) {
                                    var T = E > S ? g.slice(0, S - p) : g;
                                    e.addToken(e, T, a ? a + l : l, u, p + T.length == v ? c : "", f, s)
                                }
                                if (E >= S) {
                                    g = g.slice(S - p), p = S;
                                    break
                                }
                                p = E, u = ""
                            }
                            g = i.slice(o, o = n[m++]), a = le(n[m++], e.cm.options)
                        }
                    } else
                        for (var M = 1; M < n.length; M += 2) e.addToken(e, i.slice(o, o = n[M]), le(n[M + 1], e.cm.options))
            }

            function ge(t, e, n) {
                this.line = e, this.rest = pt(e), this.size = this.rest ? A(m(this.rest)) - n + 1 : 1, this.node = this.text = null, this.hidden = vt(t, e)
            }

            function ve(t, e, n) {
                for (var r, i = [], o = e; o < n; o = r) {
                    var a = new ge(t.doc, T(t.doc, o), o);
                    r = o + a.size, i.push(a)
                }
                return i
            }

            function ye(t) {
                bs ? bs.ops.push(t) : t.ownsGroup = bs = {
                    ops: [t],
                    delayedCallbacks: []
                }
            }

            function be(t) {
                var e = t.delayedCallbacks,
                    n = 0;
                do {
                    for (; n < e.length; n++) e[n].call(null);
                    for (var r = 0; r < t.ops.length; r++) {
                        var i = t.ops[r];
                        if (i.cursorActivityHandlers)
                            for (; i.cursorActivityCalled < i.cursorActivityHandlers.length;) i.cursorActivityHandlers[i.cursorActivityCalled++].call(null, i.cm)
                    }
                } while (n < e.length)
            }

            function xe(t, e) {
                var n = t.ownsGroup;
                if (n) try {
                    be(n)
                } finally {
                    bs = null, e(n)
                }
            }

            function we(t, e) {
                var n = St(t, e);
                if (n.length) {
                    var r, i = Array.prototype.slice.call(arguments, 2);
                    bs ? r = bs.delayedCallbacks : xs ? r = xs : (r = xs = [], setTimeout(Ce, 0));
                    for (var o = 0; o < n.length; ++o) ! function(t) {
                        r.push(function() {
                            return n[t].apply(null, i)
                        })
                    }(o)
                }
            }

            function Ce() {
                var t = xs;
                xs = null;
                for (var e = 0; e < t.length; ++e) t[e]()
            }

            function ke(t, e, n, r) {
                for (var i = 0; i < e.changes.length; i++) {
                    var o = e.changes[i];
                    "text" == o ? Te(t, e) : "gutter" == o ? Oe(t, e, n, r) : "class" == o ? Me(t, e) : "widget" == o && Le(t, e, r)
                }
                e.changes = null
            }

            function _e(t) {
                return t.node == t.text && (t.node = r("div", null, null, "position: relative"), t.text.parentNode && t.text.parentNode.replaceChild(t.node, t.text), t.node.appendChild(t.text), ba && xa < 8 && (t.node.style.zIndex = 2)), t.node
            }

            function Se(t, e) {
                var n = e.bgClass ? e.bgClass + " " + (e.line.bgClass || "") : e.line.bgClass;
                if (n && (n += " CodeMirror-linebackground"), e.background) n ? e.background.className = n : (e.background.parentNode.removeChild(e.background), e.background = null);
                else if (n) {
                    var i = _e(e);
                    e.background = i.insertBefore(r("div", null, n), i.firstChild), t.display.input.setUneditable(e.background)
                }
            }

            function Ee(t, e) {
                var n = t.display.externalMeasured;
                return n && n.line == e.line ? (t.display.externalMeasured = null, e.measure = n.measure, n.built) : ce(t, e)
            }

            function Te(t, e) {
                var n = e.text.className,
                    r = Ee(t, e);
                e.text == e.node && (e.node = r.pre), e.text.parentNode.replaceChild(r.pre, e.text), e.text = r.pre, r.bgClass != e.bgClass || r.textClass != e.textClass ? (e.bgClass = r.bgClass, e.textClass = r.textClass, Me(t, e)) : n && (e.text.className = n)
            }

            function Me(t, e) {
                Se(t, e), e.line.wrapClass ? _e(e).className = e.line.wrapClass : e.node != e.text && (e.node.className = "");
                var n = e.textClass ? e.textClass + " " + (e.line.textClass || "") : e.line.textClass;
                e.text.className = n || ""
            }

            function Oe(t, e, n, i) {
                if (e.gutter && (e.node.removeChild(e.gutter), e.gutter = null), e.gutterBackground && (e.node.removeChild(e.gutterBackground), e.gutterBackground = null), e.line.gutterClass) {
                    var o = _e(e);
                    e.gutterBackground = r("div", null, "CodeMirror-gutter-background " + e.line.gutterClass, "left: " + (t.options.fixedGutter ? i.fixedPos : -i.gutterTotalWidth) + "px; width: " + i.gutterTotalWidth + "px"), t.display.input.setUneditable(e.gutterBackground), o.insertBefore(e.gutterBackground, e.text)
                }
                var a = e.line.gutterMarkers;
                if (t.options.lineNumbers || a) {
                    var s = _e(e),
                        l = e.gutter = r("div", null, "CodeMirror-gutter-wrapper", "left: " + (t.options.fixedGutter ? i.fixedPos : -i.gutterTotalWidth) + "px");
                    if (t.display.input.setUneditable(l), s.insertBefore(l, e.text), e.line.gutterClass && (l.className += " " + e.line.gutterClass), !t.options.lineNumbers || a && a["CodeMirror-linenumbers"] || (e.lineNumber = l.appendChild(r("div", I(t.options, n), "CodeMirror-linenumber CodeMirror-gutter-elt", "left: " + i.gutterLeft["CodeMirror-linenumbers"] + "px; width: " + t.display.lineNumInnerWidth + "px"))), a)
                        for (var c = 0; c < t.options.gutters.length; ++c) {
                            var u = t.options.gutters[c],
                                f = a.hasOwnProperty(u) && a[u];
                            f && l.appendChild(r("div", [f], "CodeMirror-gutter-elt", "left: " + i.gutterLeft[u] + "px; width: " + i.gutterWidth[u] + "px"))
                        }
                }
            }

            function Le(t, e, n) {
                e.alignable && (e.alignable = null);
                for (var r = e.node.firstChild, i = void 0; r; r = i) i = r.nextSibling, "CodeMirror-linewidget" == r.className && e.node.removeChild(r);
                Ne(t, e, n)
            }

            function Ae(t, e, n, r) {
                var i = Ee(t, e);
                return e.text = e.node = i.pre, i.bgClass && (e.bgClass = i.bgClass), i.textClass && (e.textClass = i.textClass), Me(t, e), Oe(t, e, n, r), Ne(t, e, r), e.node
            }

            function Ne(t, e, n) {
                if (Pe(t, e.line, e, n, !0), e.rest)
                    for (var r = 0; r < e.rest.length; r++) Pe(t, e.rest[r], e, n, !1)
            }

            function Pe(t, e, n, i, o) {
                if (e.widgets)
                    for (var a = _e(n), s = 0, l = e.widgets; s < l.length; ++s) {
                        var c = l[s],
                            u = r("div", [c.node], "CodeMirror-linewidget");
                        c.handleMouseEvents || u.setAttribute("cm-ignore-events", "true"), Ie(c, u, n, i), t.display.input.setUneditable(u), o && c.above ? a.insertBefore(u, n.gutter || n.text) : a.appendChild(u), we(c, "redraw")
                    }
            }

            function Ie(t, e, n, r) {
                if (t.noHScroll) {
                    (n.alignable || (n.alignable = [])).push(e);
                    var i = r.wrapperWidth;
                    e.style.left = r.fixedPos + "px", t.coverGutter || (i -= r.gutterTotalWidth, e.style.paddingLeft = r.gutterTotalWidth + "px"), e.style.width = i + "px"
                }
                t.coverGutter && (e.style.zIndex = 5, e.style.position = "relative", t.noHScroll || (e.style.marginLeft = -r.gutterTotalWidth + "px"))
            }

            function De(t) {
                if (null != t.height) return t.height;
                var e = t.doc.cm;
                if (!e) return 0;
                if (!o(document.body, t.node)) {
                    var i = "position: relative;";
                    t.coverGutter && (i += "margin-left: -" + e.display.gutters.offsetWidth + "px;"), t.noHScroll && (i += "width: " + e.display.wrapper.clientWidth + "px;"), n(e.display.measure, r("div", [t.node], null, i))
                }
                return t.height = t.node.parentNode.offsetHeight
            }

            function Fe(t, e) {
                for (var n = Ft(e); n != t.wrapper; n = n.parentNode)
                    if (!n || 1 == n.nodeType && "true" == n.getAttribute("cm-ignore-events") || n.parentNode == t.sizer && n != t.mover) return !0
            }

            function Re(t) {
                return t.lineSpace.offsetTop
            }

            function ze(t) {
                return t.mover.offsetHeight - t.lineSpace.offsetHeight
            }

            function je(t) {
                if (t.cachedPaddingH) return t.cachedPaddingH;
                var e = n(t.measure, r("pre", "x")),
                    i = window.getComputedStyle ? window.getComputedStyle(e) : e.currentStyle,
                    o = {
                        left: parseInt(i.paddingLeft),
                        right: parseInt(i.paddingRight)
                    };
                return isNaN(o.left) || isNaN(o.right) || (t.cachedPaddingH = o), o
            }

            function Be(t) {
                return Ua - t.display.nativeBarWidth
            }

            function He(t) {
                return t.display.scroller.clientWidth - Be(t) - t.display.barWidth
            }

            function We(t) {
                return t.display.scroller.clientHeight - Be(t) - t.display.barHeight
            }

            function Ue(t, e, n) {
                var r = t.options.lineWrapping,
                    i = r && He(t);
                if (!e.measure.heights || r && e.measure.width != i) {
                    var o = e.measure.heights = [];
                    if (r) {
                        e.measure.width = i;
                        for (var a = e.text.firstChild.getClientRects(), s = 0; s < a.length - 1; s++) {
                            var l = a[s],
                                c = a[s + 1];
                            Math.abs(l.bottom - c.bottom) > 2 && o.push((l.bottom + c.top) / 2 - n.top)
                        }
                    }
                    o.push(n.bottom - n.top)
                }
            }

            function Ve(t, e, n) {
                if (t.line == e) return {
                    map: t.measure.map,
                    cache: t.measure.cache
                };
                for (var r = 0; r < t.rest.length; r++)
                    if (t.rest[r] == e) return {
                        map: t.measure.maps[r],
                        cache: t.measure.caches[r]
                    };
                for (var i = 0; i < t.rest.length; i++)
                    if (A(t.rest[i]) > n) return {
                        map: t.measure.maps[i],
                        cache: t.measure.caches[i],
                        before: !0
                    }
            }

            function Ge(t, e) {
                e = dt(e);
                var r = A(e),
                    i = t.display.externalMeasured = new ge(t.doc, e, r);
                i.lineN = r;
                var o = i.built = ce(t, i);
                return i.text = o.pre, n(t.display.lineMeasure, o.pre), i
            }

            function Ke(t, e, n, r) {
                return Ye(t, qe(t, e), n, r)
            }

            function $e(t, e) {
                if (e >= t.display.viewFrom && e < t.display.viewTo) return t.display.view[Tn(t, e)];
                var n = t.display.externalMeasured;
                return n && e >= n.lineN && e < n.lineN + n.size ? n : void 0
            }

            function qe(t, e) {
                var n = A(e),
                    r = $e(t, n);
                r && !r.text ? r = null : r && r.changes && (ke(t, r, n, Cn(t)), t.curOp.forceUpdate = !0), r || (r = Ge(t, e));
                var i = Ve(r, e, n);
                return {
                    line: e,
                    view: r,
                    rect: null,
                    map: i.map,
                    cache: i.cache,
                    before: i.before,
                    hasHeights: !1
                }
            }

            function Ye(t, e, n, r, i) {
                e.before && (n = -1);
                var o, a = n + (r || "");
                return e.cache.hasOwnProperty(a) ? o = e.cache[a] : (e.rect || (e.rect = e.view.text.getBoundingClientRect()), e.hasHeights || (Ue(t, e.view, e.rect), e.hasHeights = !0), o = Ze(t, e, n, r), o.bogus || (e.cache[a] = o)), {
                    left: o.left,
                    right: o.right,
                    top: i ? o.rtop : o.top,
                    bottom: i ? o.rbottom : o.bottom
                }
            }

            function Je(t, e, n) {
                for (var r, i, o, a, s, l, c = 0; c < t.length; c += 3)
                    if (s = t[c], l = t[c + 1], e < s ? (i = 0, o = 1, a = "left") : e < l ? (i = e - s, o = i + 1) : (c == t.length - 3 || e == l && t[c + 3] > e) && (o = l - s, i = o - 1, e >= l && (a = "right")), null != i) {
                        if (r = t[c + 2], s == l && n == (r.insertLeft ? "left" : "right") && (a = n), "left" == n && 0 == i)
                            for (; c && t[c - 2] == t[c - 3] && t[c - 1].insertLeft;) r = t[2 + (c -= 3)], a = "left";
                        if ("right" == n && i == l - s)
                            for (; c < t.length - 3 && t[c + 3] == t[c + 4] && !t[c + 5].insertLeft;) r = t[(c += 3) + 2], a = "right";
                        break
                    }
                return {
                    node: r,
                    start: i,
                    end: o,
                    collapse: a,
                    coverStart: s,
                    coverEnd: l
                }
            }

            function Xe(t, e) {
                var n = ws;
                if ("left" == e)
                    for (var r = 0; r < t.length && (n = t[r]).left == n.right; r++);
                else
                    for (var i = t.length - 1; i >= 0 && (n = t[i]).left == n.right; i--);
                return n
            }

            function Ze(t, e, n, r) {
                var i, o = Je(e.map, n, r),
                    a = o.node,
                    s = o.start,
                    l = o.end,
                    c = o.collapse;
                if (3 == a.nodeType) {
                    for (var u = 0; u < 4; u++) {
                        for (; s && k(e.line.text.charAt(o.coverStart + s));) --s;
                        for (; o.coverStart + l < o.coverEnd && k(e.line.text.charAt(o.coverStart + l));) ++l;
                        if (i = ba && xa < 9 && 0 == s && l == o.coverEnd - o.coverStart ? a.parentNode.getBoundingClientRect() : Xe(Da(a, s, l).getClientRects(), r), i.left || i.right || 0 == s) break;
                        l = s, s -= 1, c = "right"
                    }
                    ba && xa < 11 && (i = Qe(t.display.measure, i))
                } else {
                    s > 0 && (c = r = "right");
                    var f;
                    i = t.options.lineWrapping && (f = a.getClientRects()).length > 1 ? f["right" == r ? f.length - 1 : 0] : a.getBoundingClientRect()
                }
                if (ba && xa < 9 && !s && (!i || !i.left && !i.right)) {
                    var d = a.parentNode.getClientRects()[0];
                    i = d ? {
                        left: d.left,
                        right: d.left + wn(t.display),
                        top: d.top,
                        bottom: d.bottom
                    } : ws
                }
                for (var h = i.top - e.rect.top, p = i.bottom - e.rect.top, m = (h + p) / 2, g = e.view.measure.heights, v = 0; v < g.length - 1 && !(m < g[v]); v++);
                var y = v ? g[v - 1] : 0,
                    b = g[v],
                    x = {
                        left: ("right" == c ? i.right : i.left) - e.rect.left,
                        right: ("left" == c ? i.left : i.right) - e.rect.left,
                        top: y,
                        bottom: b
                    };
                return i.left || i.right || (x.bogus = !0), t.options.singleCursorHeightPerLine || (x.rtop = h, x.rbottom = p), x
            }

            function Qe(t, e) {
                if (!window.screen || null == screen.logicalXDPI || screen.logicalXDPI == screen.deviceXDPI || !Bt(t)) return e;
                var n = screen.logicalXDPI / screen.deviceXDPI,
                    r = screen.logicalYDPI / screen.deviceYDPI;
                return {
                    left: e.left * n,
                    right: e.right * n,
                    top: e.top * r,
                    bottom: e.bottom * r
                }
            }

            function tn(t) {
                if (t.measure && (t.measure.cache = {}, t.measure.heights = null, t.rest))
                    for (var e = 0; e < t.rest.length; e++) t.measure.caches[e] = {}
            }

            function en(t) {
                t.display.externalMeasure = null, e(t.display.lineMeasure);
                for (var n = 0; n < t.display.view.length; n++) tn(t.display.view[n])
            }

            function nn(t) {
                en(t), t.display.cachedCharWidth = t.display.cachedTextHeight = t.display.cachedPaddingH = null, t.options.lineWrapping || (t.display.maxLineChanged = !0), t.display.lineNumChars = null
            }

            function rn() {
                return ka && Oa ? -(document.body.getBoundingClientRect().left - parseInt(getComputedStyle(document.body).marginLeft)) : window.pageXOffset || (document.documentElement || document.body).scrollLeft
            }

            function on() {
                return ka && Oa ? -(document.body.getBoundingClientRect().top - parseInt(getComputedStyle(document.body).marginTop)) : window.pageYOffset || (document.documentElement || document.body).scrollTop
            }

            function an(t) {
                var e = 0;
                if (t.widgets)
                    for (var n = 0; n < t.widgets.length; ++n) t.widgets[n].above && (e += De(t.widgets[n]));
                return e
            }

            function sn(t, e, n, r, i) {
                if (!i) {
                    var o = an(e);
                    n.top += o, n.bottom += o
                }
                if ("line" == r) return n;
                r || (r = "local");
                var a = bt(e);
                if ("local" == r ? a += Re(t.display) : a -= t.display.viewOffset, "page" == r || "window" == r) {
                    var s = t.display.lineSpace.getBoundingClientRect();
                    a += s.top + ("window" == r ? 0 : on());
                    var l = s.left + ("window" == r ? 0 : rn());
                    n.left += l, n.right += l
                }
                return n.top += a, n.bottom += a, n
            }

            function ln(t, e, n) {
                if ("div" == n) return e;
                var r = e.left,
                    i = e.top;
                if ("page" == n) r -= rn(), i -= on();
                else if ("local" == n || !n) {
                    var o = t.display.sizer.getBoundingClientRect();
                    r += o.left, i += o.top
                }
                var a = t.display.lineSpace.getBoundingClientRect();
                return {
                    left: r - a.left,
                    top: i - a.top
                }
            }

            function cn(t, e, n, r, i) {
                return r || (r = T(t.doc, e.line)), sn(t, r, Ke(t, r, e.ch, i), n)
            }

            function un(t, e, n, r, i, o) {
                function a(e, a) {
                    var s = Ye(t, i, e, a ? "right" : "left", o);
                    return a ? s.left = s.right : s.right = s.left, sn(t, r, s, n)
                }

                function s(t, e, n) {
                    var r = l[e],
                        i = 1 == r.level;
                    return a(n ? t - 1 : t, i != n)
                }
                r = r || T(t.doc, e.line), i || (i = qe(t, r));
                var l = _t(r, t.doc.direction),
                    c = e.ch,
                    u = e.sticky;
                if (c >= r.text.length ? (c = r.text.length, u = "before") : c <= 0 && (c = 0, u = "after"), !l) return a("before" == u ? c - 1 : c, "before" == u);
                var f = kt(l, c, u),
                    d = Qa,
                    h = s(c, f, "before" == u);
                return null != d && (h.other = s(c, d, "before" != u)), h
            }

            function fn(t, e) {
                var n = 0;
                e = W(t.doc, e), t.options.lineWrapping || (n = wn(t.display) * e.ch);
                var r = T(t.doc, e.line),
                    i = bt(r) + Re(t.display);
                return {
                    left: n,
                    right: n,
                    top: i,
                    bottom: i + r.height
                }
            }

            function dn(t, e, n, r, i) {
                var o = D(t, e, n);
                return o.xRel = i, r && (o.outside = !0), o
            }

            function hn(t, e, n) {
                var r = t.doc;
                if ((n += t.display.viewOffset) < 0) return dn(r.first, 0, null, !0, -1);
                var i = N(r, n),
                    o = r.first + r.size - 1;
                if (i > o) return dn(r.first + r.size - 1, T(r, o).text.length, null, !0, 1);
                e < 0 && (e = 0);
                for (var a = T(r, i);;) {
                    var s = vn(t, a, i, e, n),
                        l = ut(a, s.ch + (s.xRel > 0 ? 1 : 0));
                    if (!l) return s;
                    var c = l.find(1);
                    if (c.line == i) return c;
                    a = T(r, i = c.line)
                }
            }

            function pn(t, e, n, r) {
                r -= an(e);
                var i = e.text.length,
                    o = S(function(e) {
                        return Ye(t, n, e - 1).bottom <= r
                    }, i, 0);
                return i = S(function(e) {
                    return Ye(t, n, e).top > r
                }, o, i), {
                    begin: o,
                    end: i
                }
            }

            function mn(t, e, n, r) {
                return n || (n = qe(t, e)), pn(t, e, n, sn(t, e, Ye(t, n, r), "line").top)
            }

            function gn(t, e, n, r) {
                return !(t.bottom <= n) && (t.top > n || (r ? t.left : t.right) > e)
            }

            function vn(t, e, n, r, i) {
                i -= bt(e);
                var o = qe(t, e),
                    a = an(e),
                    s = 0,
                    l = e.text.length,
                    c = !0,
                    u = _t(e, t.doc.direction);
                if (u) {
                    var f = (t.options.lineWrapping ? bn : yn)(t, e, n, o, u, r, i);
                    c = 1 != f.level, s = c ? f.from : f.to - 1, l = c ? f.to : f.from - 1
                }
                var d, h, p = null,
                    m = null,
                    g = S(function(e) {
                        var n = Ye(t, o, e);
                        return n.top += a, n.bottom += a, !!gn(n, r, i, !1) && (n.top <= i && n.left <= r && (p = e, m = n), !0)
                    }, s, l),
                    v = !1;
                if (m) {
                    var y = r - m.left < m.right - r,
                        b = y == c;
                    g = p + (b ? 0 : 1), h = b ? "after" : "before", d = y ? m.left : m.right
                } else {
                    c || g != l && g != s || g++, h = 0 == g ? "after" : g == e.text.length ? "before" : Ye(t, o, g - (c ? 1 : 0)).bottom + a <= i == c ? "after" : "before";
                    var x = un(t, D(n, g, h), "line", e, o);
                    d = x.left, v = i < x.top || i >= x.bottom
                }
                return g = _(e.text, g, 1), dn(n, g, h, v, r - d)
            }

            function yn(t, e, n, r, i, o, a) {
                var s = S(function(s) {
                        var l = i[s],
                            c = 1 != l.level;
                        return gn(un(t, D(n, c ? l.to : l.from, c ? "before" : "after"), "line", e, r), o, a, !0)
                    }, 0, i.length - 1),
                    l = i[s];
                if (s > 0) {
                    var c = 1 != l.level,
                        u = un(t, D(n, c ? l.from : l.to, c ? "after" : "before"), "line", e, r);
                    gn(u, o, a, !0) && u.top > a && (l = i[s - 1])
                }
                return l
            }

            function bn(t, e, n, r, i, o, a) {
                var s = pn(t, e, r, a),
                    l = s.begin,
                    c = s.end;
                /\s/.test(e.text.charAt(c - 1)) && c--;
                for (var u = null, f = null, d = 0; d < i.length; d++) {
                    var h = i[d];
                    if (!(h.from >= c || h.to <= l)) {
                        var p = 1 != h.level,
                            m = Ye(t, r, p ? Math.min(c, h.to) - 1 : Math.max(l, h.from)).right,
                            g = m < o ? o - m + 1e9 : m - o;
                        (!u || f > g) && (u = h, f = g)
                    }
                }
                return u || (u = i[i.length - 1]), u.from < l && (u = {
                    from: l,
                    to: u.to,
                    level: u.level
                }), u.to > c && (u = {
                    from: u.from,
                    to: c,
                    level: u.level
                }), u
            }

            function xn(t) {
                if (null != t.cachedTextHeight) return t.cachedTextHeight;
                if (null == gs) {
                    gs = r("pre");
                    for (var i = 0; i < 49; ++i) gs.appendChild(document.createTextNode("x")), gs.appendChild(r("br"));
                    gs.appendChild(document.createTextNode("x"))
                }
                n(t.measure, gs);
                var o = gs.offsetHeight / 50;
                return o > 3 && (t.cachedTextHeight = o), e(t.measure), o || 1
            }

            function wn(t) {
                if (null != t.cachedCharWidth) return t.cachedCharWidth;
                var e = r("span", "xxxxxxxxxx"),
                    i = r("pre", [e]);
                n(t.measure, i);
                var o = e.getBoundingClientRect(),
                    a = (o.right - o.left) / 10;
                return a > 2 && (t.cachedCharWidth = a), a || 10
            }

            function Cn(t) {
                for (var e = t.display, n = {}, r = {}, i = e.gutters.clientLeft, o = e.gutters.firstChild, a = 0; o; o = o.nextSibling, ++a) n[t.options.gutters[a]] = o.offsetLeft + o.clientLeft + i, r[t.options.gutters[a]] = o.clientWidth;
                return {
                    fixedPos: kn(e),
                    gutterTotalWidth: e.gutters.offsetWidth,
                    gutterLeft: n,
                    gutterWidth: r,
                    wrapperWidth: e.wrapper.clientWidth
                }
            }

            function kn(t) {
                return t.scroller.getBoundingClientRect().left - t.sizer.getBoundingClientRect().left
            }

            function _n(t) {
                var e = xn(t.display),
                    n = t.options.lineWrapping,
                    r = n && Math.max(5, t.display.scroller.clientWidth / wn(t.display) - 3);
                return function(i) {
                    if (vt(t.doc, i)) return 0;
                    var o = 0;
                    if (i.widgets)
                        for (var a = 0; a < i.widgets.length; a++) i.widgets[a].height && (o += i.widgets[a].height);
                    return n ? o + (Math.ceil(i.text.length / r) || 1) * e : o + e
                }
            }

            function Sn(t) {
                var e = t.doc,
                    n = _n(t);
                e.iter(function(t) {
                    var e = n(t);
                    e != t.height && L(t, e)
                })
            }

            function En(t, e, n, r) {
                var i = t.display;
                if (!n && "true" == Ft(e).getAttribute("cm-not-content")) return null;
                var o, a, s = i.lineSpace.getBoundingClientRect();
                try {
                    o = e.clientX - s.left, a = e.clientY - s.top
                } catch (e) {
                    return null
                }
                var l, c = hn(t, o, a);
                if (r && 1 == c.xRel && (l = T(t.doc, c.line).text).length == c.ch) {
                    var u = f(l, l.length, t.options.tabSize) - l.length;
                    c = D(c.line, Math.max(0, Math.round((o - je(t.display).left) / wn(t.display)) - u))
                }
                return c
            }

            function Tn(t, e) {
                if (e >= t.display.viewTo) return null;
                if ((e -= t.display.viewFrom) < 0) return null;
                for (var n = t.display.view, r = 0; r < n.length; r++)
                    if ((e -= n[r].size) < 0) return r
            }

            function Mn(t) {
                t.display.input.showSelection(t.display.input.prepareSelection())
            }

            function On(t, e) {
                void 0 === e && (e = !0);
                for (var n = t.doc, r = {}, i = r.cursors = document.createDocumentFragment(), o = r.selection = document.createDocumentFragment(), a = 0; a < n.sel.ranges.length; a++)
                    if (e || a != n.sel.primIndex) {
                        var s = n.sel.ranges[a];
                        if (!(s.from().line >= t.display.viewTo || s.to().line < t.display.viewFrom)) {
                            var l = s.empty();
                            (l || t.options.showCursorWhenSelecting) && Ln(t, s.head, i), l || Nn(t, s, o)
                        }
                    }
                return r
            }

            function Ln(t, e, n) {
                var i = un(t, e, "div", null, null, !t.options.singleCursorHeightPerLine),
                    o = n.appendChild(r("div", " ", "CodeMirror-cursor"));
                if (o.style.left = i.left + "px", o.style.top = i.top + "px", o.style.height = Math.max(0, i.bottom - i.top) * t.options.cursorHeight + "px", i.other) {
                    var a = n.appendChild(r("div", " ", "CodeMirror-cursor CodeMirror-secondarycursor"));
                    a.style.display = "", a.style.left = i.other.left + "px", a.style.top = i.other.top + "px", a.style.height = .85 * (i.other.bottom - i.other.top) + "px"
                }
            }

            function An(t, e) {
                return t.top - e.top || t.left - e.left
            }

            function Nn(t, e, n) {
                function i(t, e, n, i) {
                    e < 0 && (e = 0), e = Math.round(e), i = Math.round(i), l.appendChild(r("div", null, "CodeMirror-selected", "position: absolute; left: " + t + "px;\n                             top: " + e + "px; width: " + (null == n ? f - t : n) + "px;\n                             height: " + (i - e) + "px"))
                }

                function o(e, n, r) {
                    function o(n, r) {
                        return cn(t, D(e, n), "div", h, r)
                    }

                    function a(e, n, r) {
                        var i = mn(t, h, null, e),
                            a = "ltr" == n == ("after" == r) ? "left" : "right";
                        return o("after" == r ? i.begin : i.end - (/\s/.test(h.text.charAt(i.end - 1)) ? 2 : 1), a)[a]
                    }
                    var l, c, h = T(s, e),
                        p = h.text.length,
                        m = _t(h, s.direction);
                    return Ct(m, n || 0, null == r ? p : r, function(t, e, s, h) {
                        var g = "ltr" == s,
                            v = o(t, g ? "left" : "right"),
                            y = o(e - 1, g ? "right" : "left"),
                            b = null == n && 0 == t,
                            x = null == r && e == p,
                            w = 0 == h,
                            C = !m || h == m.length - 1;
                        if (y.top - v.top <= 3) {
                            var k = (d ? b : x) && w,
                                _ = (d ? x : b) && C,
                                S = k ? u : (g ? v : y).left,
                                E = _ ? f : (g ? y : v).right;
                            i(S, v.top, E - S, v.bottom)
                        } else {
                            var T, M, O, L;
                            g ? (T = d && b && w ? u : v.left, M = d ? f : a(t, s, "before"), O = d ? u : a(e, s, "after"), L = d && x && C ? f : y.right) : (T = d ? a(t, s, "before") : u, M = !d && b && w ? f : v.right, O = !d && x && C ? u : y.left, L = d ? a(e, s, "after") : f), i(T, v.top, M - T, v.bottom), v.bottom < y.top && i(u, v.bottom, null, y.top), i(O, y.top, L - O, y.bottom)
                        }(!l || An(v, l) < 0) && (l = v), An(y, l) < 0 && (l = y), (!c || An(v, c) < 0) && (c = v), An(y, c) < 0 && (c = y)
                    }), {
                        start: l,
                        end: c
                    }
                }
                var a = t.display,
                    s = t.doc,
                    l = document.createDocumentFragment(),
                    c = je(t.display),
                    u = c.left,
                    f = Math.max(a.sizerWidth, He(t) - a.sizer.offsetLeft) - c.right,
                    d = "ltr" == s.direction,
                    h = e.from(),
                    p = e.to();
                if (h.line == p.line) o(h.line, h.ch, p.ch);
                else {
                    var m = T(s, h.line),
                        g = T(s, p.line),
                        v = dt(m) == dt(g),
                        y = o(h.line, h.ch, v ? m.text.length + 1 : null).end,
                        b = o(p.line, v ? 0 : null, p.ch).start;
                    v && (y.top < b.top - 2 ? (i(y.right, y.top, null, y.bottom), i(u, b.top, b.left, b.bottom)) : i(y.right, y.top, b.left - y.right, y.bottom)), y.bottom < b.top && i(u, y.bottom, null, b.top)
                }
                n.appendChild(l)
            }

            function Pn(t) {
                if (t.state.focused) {
                    var e = t.display;
                    clearInterval(e.blinker);
                    var n = !0;
                    e.cursorDiv.style.visibility = "", t.options.cursorBlinkRate > 0 ? e.blinker = setInterval(function() {
                        return e.cursorDiv.style.visibility = (n = !n) ? "" : "hidden"
                    }, t.options.cursorBlinkRate) : t.options.cursorBlinkRate < 0 && (e.cursorDiv.style.visibility = "hidden")
                }
            }

            function In(t) {
                t.state.focused || (t.display.input.focus(), Fn(t))
            }

            function Dn(t) {
                t.state.delayingBlurEvent = !0, setTimeout(function() {
                    t.state.delayingBlurEvent && (t.state.delayingBlurEvent = !1, Rn(t))
                }, 100)
            }

            function Fn(t, e) {
                t.state.delayingBlurEvent && (t.state.delayingBlurEvent = !1), "nocursor" != t.options.readOnly && (t.state.focused || (Tt(t, "focus", t, e), t.state.focused = !0, s(t.display.wrapper, "CodeMirror-focused"), t.curOp || t.display.selForContextMenu == t.doc.sel || (t.display.input.reset(), wa && setTimeout(function() {
                    return t.display.input.reset(!0)
                }, 20)), t.display.input.receivedFocus()), Pn(t))
            }

            function Rn(t, e) {
                t.state.delayingBlurEvent || (t.state.focused && (Tt(t, "blur", t, e), t.state.focused = !1, za(t.display.wrapper, "CodeMirror-focused")), clearInterval(t.display.blinker), setTimeout(function() {
                    t.state.focused || (t.display.shift = !1)
                }, 150))
            }

            function zn(t) {
                for (var e = t.display, n = e.lineDiv.offsetTop, r = 0; r < e.view.length; r++) {
                    var i = e.view[r],
                        o = void 0;
                    if (!i.hidden) {
                        if (ba && xa < 8) {
                            var a = i.node.offsetTop + i.node.offsetHeight;
                            o = a - n, n = a
                        } else {
                            var s = i.node.getBoundingClientRect();
                            o = s.bottom - s.top
                        }
                        var l = i.line.height - o;
                        if (o < 2 && (o = xn(e)), (l > .005 || l < -.005) && (L(i.line, o), jn(i.line), i.rest))
                            for (var c = 0; c < i.rest.length; c++) jn(i.rest[c])
                    }
                }
            }

            function jn(t) {
                if (t.widgets)
                    for (var e = 0; e < t.widgets.length; ++e) {
                        var n = t.widgets[e],
                            r = n.node.parentNode;
                        r && (n.height = r.offsetHeight)
                    }
            }

            function Bn(t, e, n) {
                var r = n && null != n.top ? Math.max(0, n.top) : t.scroller.scrollTop;
                r = Math.floor(r - Re(t));
                var i = n && null != n.bottom ? n.bottom : r + t.wrapper.clientHeight,
                    o = N(e, r),
                    a = N(e, i);
                if (n && n.ensure) {
                    var s = n.ensure.from.line,
                        l = n.ensure.to.line;
                    s < o ? (o = s, a = N(e, bt(T(e, s)) + t.wrapper.clientHeight)) : Math.min(l, e.lastLine()) >= a && (o = N(e, bt(T(e, l)) - t.wrapper.clientHeight), a = l)
                }
                return {
                    from: o,
                    to: Math.max(a, o + 1)
                }
            }

            function Hn(t) {
                var e = t.display,
                    n = e.view;
                if (e.alignWidgets || e.gutters.firstChild && t.options.fixedGutter) {
                    for (var r = kn(e) - e.scroller.scrollLeft + t.doc.scrollLeft, i = e.gutters.offsetWidth, o = r + "px", a = 0; a < n.length; a++)
                        if (!n[a].hidden) {
                            t.options.fixedGutter && (n[a].gutter && (n[a].gutter.style.left = o), n[a].gutterBackground && (n[a].gutterBackground.style.left = o));
                            var s = n[a].alignable;
                            if (s)
                                for (var l = 0; l < s.length; l++) s[l].style.left = o
                        }
                    t.options.fixedGutter && (e.gutters.style.left = r + i + "px")
                }
            }

            function Wn(t) {
                if (!t.options.lineNumbers) return !1;
                var e = t.doc,
                    n = I(t.options, e.first + e.size - 1),
                    i = t.display;
                if (n.length != i.lineNumChars) {
                    var o = i.measure.appendChild(r("div", [r("div", n)], "CodeMirror-linenumber CodeMirror-gutter-elt")),
                        a = o.firstChild.offsetWidth,
                        s = o.offsetWidth - a;
                    return i.lineGutter.style.width = "", i.lineNumInnerWidth = Math.max(a, i.lineGutter.offsetWidth - s) + 1, i.lineNumWidth = i.lineNumInnerWidth + s, i.lineNumChars = i.lineNumInnerWidth ? n.length : -1, i.lineGutter.style.width = i.lineNumWidth + "px", Pr(t), !0
                }
                return !1
            }

            function Un(t, e) {
                if (!Mt(t, "scrollCursorIntoView")) {
                    var n = t.display,
                        i = n.sizer.getBoundingClientRect(),
                        o = null;
                    if (e.top + i.top < 0 ? o = !0 : e.bottom + i.top > (window.innerHeight || document.documentElement.clientHeight) && (o = !1), null != o && !Ta) {
                        var a = r("div", "​", null, "position: absolute;\n                         top: " + (e.top - n.viewOffset - Re(t.display)) + "px;\n                         height: " + (e.bottom - e.top + Be(t) + n.barHeight) + "px;\n                         left: " + e.left + "px; width: " + Math.max(2, e.right - e.left) + "px;");
                        t.display.lineSpace.appendChild(a), a.scrollIntoView(o), t.display.lineSpace.removeChild(a)
                    }
                }
            }

            function Vn(t, e, n, r) {
                null == r && (r = 0);
                var i;
                t.options.lineWrapping || e != n || (e = e.ch ? D(e.line, "before" == e.sticky ? e.ch - 1 : e.ch, "after") : e, n = "before" == e.sticky ? D(e.line, e.ch + 1, "before") : e);
                for (var o = 0; o < 5; o++) {
                    var a = !1,
                        s = un(t, e),
                        l = n && n != e ? un(t, n) : s;
                    i = {
                        left: Math.min(s.left, l.left),
                        top: Math.min(s.top, l.top) - r,
                        right: Math.max(s.left, l.left),
                        bottom: Math.max(s.bottom, l.bottom) + r
                    };
                    var c = Kn(t, i),
                        u = t.doc.scrollTop,
                        f = t.doc.scrollLeft;
                    if (null != c.scrollTop && (Qn(t, c.scrollTop), Math.abs(t.doc.scrollTop - u) > 1 && (a = !0)), null != c.scrollLeft && (er(t, c.scrollLeft), Math.abs(t.doc.scrollLeft - f) > 1 && (a = !0)), !a) break
                }
                return i
            }

            function Gn(t, e) {
                var n = Kn(t, e);
                null != n.scrollTop && Qn(t, n.scrollTop), null != n.scrollLeft && er(t, n.scrollLeft)
            }

            function Kn(t, e) {
                var n = t.display,
                    r = xn(t.display);
                e.top < 0 && (e.top = 0);
                var i = t.curOp && null != t.curOp.scrollTop ? t.curOp.scrollTop : n.scroller.scrollTop,
                    o = We(t),
                    a = {};
                e.bottom - e.top > o && (e.bottom = e.top + o);
                var s = t.doc.height + ze(n),
                    l = e.top < r,
                    c = e.bottom > s - r;
                if (e.top < i) a.scrollTop = l ? 0 : e.top;
                else if (e.bottom > i + o) {
                    var u = Math.min(e.top, (c ? s : e.bottom) - o);
                    u != i && (a.scrollTop = u)
                }
                var f = t.curOp && null != t.curOp.scrollLeft ? t.curOp.scrollLeft : n.scroller.scrollLeft,
                    d = He(t) - (t.options.fixedGutter ? n.gutters.offsetWidth : 0),
                    h = e.right - e.left > d;
                return h && (e.right = e.left + d), e.left < 10 ? a.scrollLeft = 0 : e.left < f ? a.scrollLeft = Math.max(0, e.left - (h ? 0 : 10)) : e.right > d + f - 3 && (a.scrollLeft = e.right + (h ? 0 : 10) - d), a
            }

            function $n(t, e) {
                null != e && (Xn(t), t.curOp.scrollTop = (null == t.curOp.scrollTop ? t.doc.scrollTop : t.curOp.scrollTop) + e)
            }

            function qn(t) {
                Xn(t);
                var e = t.getCursor();
                t.curOp.scrollToPos = {
                    from: e,
                    to: e,
                    margin: t.options.cursorScrollMargin
                }
            }

            function Yn(t, e, n) {
                null == e && null == n || Xn(t), null != e && (t.curOp.scrollLeft = e), null != n && (t.curOp.scrollTop = n)
            }

            function Jn(t, e) {
                Xn(t), t.curOp.scrollToPos = e
            }

            function Xn(t) {
                var e = t.curOp.scrollToPos;
                if (e) {
                    t.curOp.scrollToPos = null;
                    Zn(t, fn(t, e.from), fn(t, e.to), e.margin)
                }
            }

            function Zn(t, e, n, r) {
                var i = Kn(t, {
                    left: Math.min(e.left, n.left),
                    top: Math.min(e.top, n.top) - r,
                    right: Math.max(e.right, n.right),
                    bottom: Math.max(e.bottom, n.bottom) + r
                });
                Yn(t, i.scrollLeft, i.scrollTop)
            }

            function Qn(t, e) {
                Math.abs(t.doc.scrollTop - e) < 2 || (ma || Ar(t, {
                    top: e
                }), tr(t, e, !0), ma && Ar(t), _r(t, 100))
            }

            function tr(t, e, n) {
                e = Math.min(t.display.scroller.scrollHeight - t.display.scroller.clientHeight, e), (t.display.scroller.scrollTop != e || n) && (t.doc.scrollTop = e, t.display.scrollbars.setScrollTop(e), t.display.scroller.scrollTop != e && (t.display.scroller.scrollTop = e))
            }

            function er(t, e, n, r) {
                e = Math.min(e, t.display.scroller.scrollWidth - t.display.scroller.clientWidth), (n ? e == t.doc.scrollLeft : Math.abs(t.doc.scrollLeft - e) < 2) && !r || (t.doc.scrollLeft = e, Hn(t), t.display.scroller.scrollLeft != e && (t.display.scroller.scrollLeft = e), t.display.scrollbars.setScrollLeft(e))
            }

            function nr(t) {
                var e = t.display,
                    n = e.gutters.offsetWidth,
                    r = Math.round(t.doc.height + ze(t.display));
                return {
                    clientHeight: e.scroller.clientHeight,
                    viewHeight: e.wrapper.clientHeight,
                    scrollWidth: e.scroller.scrollWidth,
                    clientWidth: e.scroller.clientWidth,
                    viewWidth: e.wrapper.clientWidth,
                    barLeft: t.options.fixedGutter ? n : 0,
                    docHeight: r,
                    scrollHeight: r + Be(t) + e.barHeight,
                    nativeBarWidth: e.nativeBarWidth,
                    gutterWidth: n
                }
            }

            function rr(t, e) {
                e || (e = nr(t));
                var n = t.display.barWidth,
                    r = t.display.barHeight;
                ir(t, e);
                for (var i = 0; i < 4 && n != t.display.barWidth || r != t.display.barHeight; i++) n != t.display.barWidth && t.options.lineWrapping && zn(t), ir(t, nr(t)), n = t.display.barWidth, r = t.display.barHeight
            }

            function ir(t, e) {
                var n = t.display,
                    r = n.scrollbars.update(e);
                n.sizer.style.paddingRight = (n.barWidth = r.right) + "px", n.sizer.style.paddingBottom = (n.barHeight = r.bottom) + "px", n.heightForcer.style.borderBottom = r.bottom + "px solid transparent", r.right && r.bottom ? (n.scrollbarFiller.style.display = "block", n.scrollbarFiller.style.height = r.bottom + "px", n.scrollbarFiller.style.width = r.right + "px") : n.scrollbarFiller.style.display = "", r.bottom && t.options.coverGutterNextToScrollbar && t.options.fixedGutter ? (n.gutterFiller.style.display = "block", n.gutterFiller.style.height = r.bottom + "px", n.gutterFiller.style.width = e.gutterWidth + "px") : n.gutterFiller.style.display = ""
            }

            function or(t) {
                t.display.scrollbars && (t.display.scrollbars.clear(), t.display.scrollbars.addClass && za(t.display.wrapper, t.display.scrollbars.addClass)), t.display.scrollbars = new _s[t.options.scrollbarStyle](function(e) {
                    t.display.wrapper.insertBefore(e, t.display.scrollbarFiller), ns(e, "mousedown", function() {
                        t.state.focused && setTimeout(function() {
                            return t.display.input.focus()
                        }, 0)
                    }), e.setAttribute("cm-not-content", "true")
                }, function(e, n) {
                    "horizontal" == n ? er(t, e) : Qn(t, e)
                }, t), t.display.scrollbars.addClass && s(t.display.wrapper, t.display.scrollbars.addClass)
            }

            function ar(t) {
                t.curOp = {
                    cm: t,
                    viewChanged: !1,
                    startHeight: t.doc.height,
                    forceUpdate: !1,
                    updateInput: null,
                    typing: !1,
                    changeObjs: null,
                    cursorActivityHandlers: null,
                    cursorActivityCalled: 0,
                    selectionChanged: !1,
                    updateMaxLine: !1,
                    scrollLeft: null,
                    scrollTop: null,
                    scrollToPos: null,
                    focus: !1,
                    id: ++Ss
                }, ye(t.curOp)
            }

            function sr(t) {
                xe(t.curOp, function(t) {
                    for (var e = 0; e < t.ops.length; e++) t.ops[e].cm.curOp = null;
                    lr(t)
                })
            }

            function lr(t) {
                for (var e = t.ops, n = 0; n < e.length; n++) cr(e[n]);
                for (var r = 0; r < e.length; r++) ur(e[r]);
                for (var i = 0; i < e.length; i++) fr(e[i]);
                for (var o = 0; o < e.length; o++) dr(e[o]);
                for (var a = 0; a < e.length; a++) hr(e[a])
            }

            function cr(t) {
                var e = t.cm,
                    n = e.display;
                Er(e), t.updateMaxLine && wt(e), t.mustUpdate = t.viewChanged || t.forceUpdate || null != t.scrollTop || t.scrollToPos && (t.scrollToPos.from.line < n.viewFrom || t.scrollToPos.to.line >= n.viewTo) || n.maxLineChanged && e.options.lineWrapping, t.update = t.mustUpdate && new Es(e, t.mustUpdate && {
                    top: t.scrollTop,
                    ensure: t.scrollToPos
                }, t.forceUpdate)
            }

            function ur(t) {
                t.updatedDisplay = t.mustUpdate && Or(t.cm, t.update)
            }

            function fr(t) {
                var e = t.cm,
                    n = e.display;
                t.updatedDisplay && zn(e), t.barMeasure = nr(e), n.maxLineChanged && !e.options.lineWrapping && (t.adjustWidthTo = Ke(e, n.maxLine, n.maxLine.text.length).left + 3, e.display.sizerWidth = t.adjustWidthTo, t.barMeasure.scrollWidth = Math.max(n.scroller.clientWidth, n.sizer.offsetLeft + t.adjustWidthTo + Be(e) + e.display.barWidth), t.maxScrollLeft = Math.max(0, n.sizer.offsetLeft + t.adjustWidthTo - He(e))), (t.updatedDisplay || t.selectionChanged) && (t.preparedSelection = n.input.prepareSelection())
            }

            function dr(t) {
                var e = t.cm;
                null != t.adjustWidthTo && (e.display.sizer.style.minWidth = t.adjustWidthTo + "px", t.maxScrollLeft < e.doc.scrollLeft && er(e, Math.min(e.display.scroller.scrollLeft, t.maxScrollLeft), !0), e.display.maxLineChanged = !1);
                var n = t.focus && t.focus == a();
                t.preparedSelection && e.display.input.showSelection(t.preparedSelection, n), (t.updatedDisplay || t.startHeight != e.doc.height) && rr(e, t.barMeasure), t.updatedDisplay && Ir(e, t.barMeasure), t.selectionChanged && Pn(e), e.state.focused && t.updateInput && e.display.input.reset(t.typing), n && In(t.cm)
            }

            function hr(t) {
                var e = t.cm,
                    n = e.display,
                    r = e.doc;
                if (t.updatedDisplay && Lr(e, t.update), null == n.wheelStartX || null == t.scrollTop && null == t.scrollLeft && !t.scrollToPos || (n.wheelStartX = n.wheelStartY = null), null != t.scrollTop && tr(e, t.scrollTop, t.forceScroll), null != t.scrollLeft && er(e, t.scrollLeft, !0, !0), t.scrollToPos) {
                    Un(e, Vn(e, W(r, t.scrollToPos.from), W(r, t.scrollToPos.to), t.scrollToPos.margin))
                }
                var i = t.maybeHiddenMarkers,
                    o = t.maybeUnhiddenMarkers;
                if (i)
                    for (var a = 0; a < i.length; ++a) i[a].lines.length || Tt(i[a], "hide");
                if (o)
                    for (var s = 0; s < o.length; ++s) o[s].lines.length && Tt(o[s], "unhide");
                n.wrapper.offsetHeight && (r.scrollTop = e.display.scroller.scrollTop), t.changeObjs && Tt(e, "changes", e, t.changeObjs), t.update && t.update.finish()
            }

            function pr(t, e) {
                if (t.curOp) return e();
                ar(t);
                try {
                    return e()
                } finally {
                    sr(t)
                }
            }

            function mr(t, e) {
                return function() {
                    if (t.curOp) return e.apply(t, arguments);
                    ar(t);
                    try {
                        return e.apply(t, arguments)
                    } finally {
                        sr(t)
                    }
                }
            }

            function gr(t) {
                return function() {
                    if (this.curOp) return t.apply(this, arguments);
                    ar(this);
                    try {
                        return t.apply(this, arguments)
                    } finally {
                        sr(this)
                    }
                }
            }

            function vr(t) {
                return function() {
                    var e = this.cm;
                    if (!e || e.curOp) return t.apply(this, arguments);
                    ar(e);
                    try {
                        return t.apply(this, arguments)
                    } finally {
                        sr(e)
                    }
                }
            }

            function yr(t, e, n, r) {
                null == e && (e = t.doc.first), null == n && (n = t.doc.first + t.doc.size), r || (r = 0);
                var i = t.display;
                if (r && n < i.viewTo && (null == i.updateLineNumbers || i.updateLineNumbers > e) && (i.updateLineNumbers = e), t.curOp.viewChanged = !0, e >= i.viewTo) Za && mt(t.doc, e) < i.viewTo && xr(t);
                else if (n <= i.viewFrom) Za && gt(t.doc, n + r) > i.viewFrom ? xr(t) : (i.viewFrom += r, i.viewTo += r);
                else if (e <= i.viewFrom && n >= i.viewTo) xr(t);
                else if (e <= i.viewFrom) {
                    var o = wr(t, n, n + r, 1);
                    o ? (i.view = i.view.slice(o.index), i.viewFrom = o.lineN, i.viewTo += r) : xr(t)
                } else if (n >= i.viewTo) {
                    var a = wr(t, e, e, -1);
                    a ? (i.view = i.view.slice(0, a.index), i.viewTo = a.lineN) : xr(t)
                } else {
                    var s = wr(t, e, e, -1),
                        l = wr(t, n, n + r, 1);
                    s && l ? (i.view = i.view.slice(0, s.index).concat(ve(t, s.lineN, l.lineN)).concat(i.view.slice(l.index)), i.viewTo += r) : xr(t)
                }
                var c = i.externalMeasured;
                c && (n < c.lineN ? c.lineN += r : e < c.lineN + c.size && (i.externalMeasured = null))
            }

            function br(t, e, n) {
                t.curOp.viewChanged = !0;
                var r = t.display,
                    i = t.display.externalMeasured;
                if (i && e >= i.lineN && e < i.lineN + i.size && (r.externalMeasured = null), !(e < r.viewFrom || e >= r.viewTo)) {
                    var o = r.view[Tn(t, e)];
                    if (null != o.node) {
                        var a = o.changes || (o.changes = []); - 1 == d(a, n) && a.push(n)
                    }
                }
            }

            function xr(t) {
                t.display.viewFrom = t.display.viewTo = t.doc.first, t.display.view = [], t.display.viewOffset = 0
            }

            function wr(t, e, n, r) {
                var i, o = Tn(t, e),
                    a = t.display.view;
                if (!Za || n == t.doc.first + t.doc.size) return {
                    index: o,
                    lineN: n
                };
                for (var s = t.display.viewFrom, l = 0; l < o; l++) s += a[l].size;
                if (s != e) {
                    if (r > 0) {
                        if (o == a.length - 1) return null;
                        i = s + a[o].size - e, o++
                    } else i = s - e;
                    e += i, n += i
                }
                for (; mt(t.doc, n) != n;) {
                    if (o == (r < 0 ? 0 : a.length - 1)) return null;
                    n += r * a[o - (r < 0 ? 1 : 0)].size, o += r
                }
                return {
                    index: o,
                    lineN: n
                }
            }

            function Cr(t, e, n) {
                var r = t.display;
                0 == r.view.length || e >= r.viewTo || n <= r.viewFrom ? (r.view = ve(t, e, n), r.viewFrom = e) : (r.viewFrom > e ? r.view = ve(t, e, r.viewFrom).concat(r.view) : r.viewFrom < e && (r.view = r.view.slice(Tn(t, e))), r.viewFrom = e, r.viewTo < n ? r.view = r.view.concat(ve(t, r.viewTo, n)) : r.viewTo > n && (r.view = r.view.slice(0, Tn(t, n)))), r.viewTo = n
            }

            function kr(t) {
                for (var e = t.display.view, n = 0, r = 0; r < e.length; r++) {
                    var i = e[r];
                    i.hidden || i.node && !i.changes || ++n
                }
                return n
            }

            function _r(t, e) {
                t.doc.highlightFrontier < t.display.viewTo && t.state.highlight.set(e, c(Sr, t))
            }

            function Sr(t) {
                var e = t.doc;
                if (!(e.highlightFrontier >= t.display.viewTo)) {
                    var n = +new Date + t.options.workTime,
                        r = Xt(t, e.highlightFrontier),
                        i = [];
                    e.iter(r.line, Math.min(e.first + e.size, t.display.viewTo + 500), function(o) {
                        if (r.line >= t.display.viewFrom) {
                            var a = o.styles,
                                s = o.text.length > t.options.maxHighlightLength ? Kt(e.mode, r.state) : null,
                                l = Yt(t, o, r, !0);
                            s && (r.state = s), o.styles = l.styles;
                            var c = o.styleClasses,
                                u = l.classes;
                            u ? o.styleClasses = u : c && (o.styleClasses = null);
                            for (var f = !a || a.length != o.styles.length || c != u && (!c || !u || c.bgClass != u.bgClass || c.textClass != u.textClass), d = 0; !f && d < a.length; ++d) f = a[d] != o.styles[d];
                            f && i.push(r.line), o.stateAfter = r.save(), r.nextLine()
                        } else o.text.length <= t.options.maxHighlightLength && Zt(t, o.text, r), o.stateAfter = r.line % 5 == 0 ? r.save() : null, r.nextLine();
                        if (+new Date > n) return _r(t, t.options.workDelay), !0
                    }), e.highlightFrontier = r.line, e.modeFrontier = Math.max(e.modeFrontier, r.line), i.length && pr(t, function() {
                        for (var e = 0; e < i.length; e++) br(t, i[e], "text")
                    })
                }
            }

            function Er(t) {
                var e = t.display;
                !e.scrollbarsClipped && e.scroller.offsetWidth && (e.nativeBarWidth = e.scroller.offsetWidth - e.scroller.clientWidth, e.heightForcer.style.height = Be(t) + "px", e.sizer.style.marginBottom = -e.nativeBarWidth + "px", e.sizer.style.borderRightWidth = Be(t) + "px", e.scrollbarsClipped = !0)
            }

            function Tr(t) {
                if (t.hasFocus()) return null;
                var e = a();
                if (!e || !o(t.display.lineDiv, e)) return null;
                var n = {
                    activeElt: e
                };
                if (window.getSelection) {
                    var r = window.getSelection();
                    r.anchorNode && r.extend && o(t.display.lineDiv, r.anchorNode) && (n.anchorNode = r.anchorNode, n.anchorOffset = r.anchorOffset, n.focusNode = r.focusNode, n.focusOffset = r.focusOffset)
                }
                return n
            }

            function Mr(t) {
                if (t && t.activeElt && t.activeElt != a() && (t.activeElt.focus(), t.anchorNode && o(document.body, t.anchorNode) && o(document.body, t.focusNode))) {
                    var e = window.getSelection(),
                        n = document.createRange();
                    n.setEnd(t.anchorNode, t.anchorOffset), n.collapse(!1), e.removeAllRanges(), e.addRange(n), e.extend(t.focusNode, t.focusOffset)
                }
            }

            function Or(t, n) {
                var r = t.display,
                    i = t.doc;
                if (n.editorIsHidden) return xr(t), !1;
                if (!n.force && n.visible.from >= r.viewFrom && n.visible.to <= r.viewTo && (null == r.updateLineNumbers || r.updateLineNumbers >= r.viewTo) && r.renderedView == r.view && 0 == kr(t)) return !1;
                Wn(t) && (xr(t), n.dims = Cn(t));
                var o = i.first + i.size,
                    a = Math.max(n.visible.from - t.options.viewportMargin, i.first),
                    s = Math.min(o, n.visible.to + t.options.viewportMargin);
                r.viewFrom < a && a - r.viewFrom < 20 && (a = Math.max(i.first, r.viewFrom)), r.viewTo > s && r.viewTo - s < 20 && (s = Math.min(o, r.viewTo)), Za && (a = mt(t.doc, a), s = gt(t.doc, s));
                var l = a != r.viewFrom || s != r.viewTo || r.lastWrapHeight != n.wrapperHeight || r.lastWrapWidth != n.wrapperWidth;
                Cr(t, a, s), r.viewOffset = bt(T(t.doc, r.viewFrom)), t.display.mover.style.top = r.viewOffset + "px";
                var c = kr(t);
                if (!l && 0 == c && !n.force && r.renderedView == r.view && (null == r.updateLineNumbers || r.updateLineNumbers >= r.viewTo)) return !1;
                var u = Tr(t);
                return c > 4 && (r.lineDiv.style.display = "none"), Nr(t, r.updateLineNumbers, n.dims), c > 4 && (r.lineDiv.style.display = ""), r.renderedView = r.view, Mr(u), e(r.cursorDiv), e(r.selectionDiv), r.gutters.style.height = r.sizer.style.minHeight = 0, l && (r.lastWrapHeight = n.wrapperHeight, r.lastWrapWidth = n.wrapperWidth, _r(t, 400)), r.updateLineNumbers = null, !0
            }

            function Lr(t, e) {
                for (var n = e.viewport, r = !0;
                    (r && t.options.lineWrapping && e.oldDisplayWidth != He(t) || (n && null != n.top && (n = {
                        top: Math.min(t.doc.height + ze(t.display) - We(t), n.top)
                    }), e.visible = Bn(t.display, t.doc, n), !(e.visible.from >= t.display.viewFrom && e.visible.to <= t.display.viewTo))) && Or(t, e); r = !1) {
                    zn(t);
                    var i = nr(t);
                    Mn(t), rr(t, i), Ir(t, i), e.force = !1
                }
                e.signal(t, "update", t), t.display.viewFrom == t.display.reportedViewFrom && t.display.viewTo == t.display.reportedViewTo || (e.signal(t, "viewportChange", t, t.display.viewFrom, t.display.viewTo), t.display.reportedViewFrom = t.display.viewFrom, t.display.reportedViewTo = t.display.viewTo)
            }

            function Ar(t, e) {
                var n = new Es(t, e);
                if (Or(t, n)) {
                    zn(t), Lr(t, n);
                    var r = nr(t);
                    Mn(t), rr(t, r), Ir(t, r), n.finish()
                }
            }

            function Nr(t, n, r) {
                function i(e) {
                    var n = e.nextSibling;
                    return wa && Aa && t.display.currentWheelTarget == e ? e.style.display = "none" : e.parentNode.removeChild(e), n
                }
                for (var o = t.display, a = t.options.lineNumbers, s = o.lineDiv, l = s.firstChild, c = o.view, u = o.viewFrom, f = 0; f < c.length; f++) {
                    var h = c[f];
                    if (h.hidden);
                    else if (h.node && h.node.parentNode == s) {
                        for (; l != h.node;) l = i(l);
                        var p = a && null != n && n <= u && h.lineNumber;
                        h.changes && (d(h.changes, "gutter") > -1 && (p = !1), ke(t, h, u, r)), p && (e(h.lineNumber), h.lineNumber.appendChild(document.createTextNode(I(t.options, u)))), l = h.node.nextSibling
                    } else {
                        var m = Ae(t, h, u, r);
                        s.insertBefore(m, l)
                    }
                    u += h.size
                }
                for (; l;) l = i(l)
            }

            function Pr(t) {
                var e = t.display.gutters.offsetWidth;
                t.display.sizer.style.marginLeft = e + "px"
            }

            function Ir(t, e) {
                t.display.sizer.style.minHeight = e.docHeight + "px", t.display.heightForcer.style.top = e.docHeight + "px", t.display.gutters.style.height = e.docHeight + t.display.barHeight + Be(t) + "px"
            }

            function Dr(t) {
                var n = t.display.gutters,
                    i = t.options.gutters;
                e(n);
                for (var o = 0; o < i.length; ++o) {
                    var a = i[o],
                        s = n.appendChild(r("div", null, "CodeMirror-gutter " + a));
                    "CodeMirror-linenumbers" == a && (t.display.lineGutter = s, s.style.width = (t.display.lineNumWidth || 1) + "px")
                }
                n.style.display = o ? "" : "none", Pr(t)
            }

            function Fr(t) {
                var e = d(t.gutters, "CodeMirror-linenumbers"); - 1 == e && t.lineNumbers ? t.gutters = t.gutters.concat(["CodeMirror-linenumbers"]) : e > -1 && !t.lineNumbers && (t.gutters = t.gutters.slice(0), t.gutters.splice(e, 1))
            }

            function Rr(t) {
                var e = t.wheelDeltaX,
                    n = t.wheelDeltaY;
                return null == e && t.detail && t.axis == t.HORIZONTAL_AXIS && (e = t.detail), null == n && t.detail && t.axis == t.VERTICAL_AXIS ? n = t.detail : null == n && (n = t.wheelDelta), {
                    x: e,
                    y: n
                }
            }

            function zr(t) {
                var e = Rr(t);
                return e.x *= Ms, e.y *= Ms, e
            }

            function jr(t, e) {
                var n = Rr(e),
                    r = n.x,
                    i = n.y,
                    o = t.display,
                    a = o.scroller,
                    s = a.scrollWidth > a.clientWidth,
                    l = a.scrollHeight > a.clientHeight;
                if (r && s || i && l) {
                    if (i && Aa && wa) t: for (var c = e.target, u = o.view; c != a; c = c.parentNode)
                        for (var f = 0; f < u.length; f++)
                            if (u[f].node == c) {
                                t.display.currentWheelTarget = c;
                                break t
                            }
                    if (r && !ma && !_a && null != Ms) return i && l && Qn(t, Math.max(0, a.scrollTop + i * Ms)), er(t, Math.max(0, a.scrollLeft + r * Ms)), (!i || i && l) && Nt(e), void(o.wheelStartX = null);
                    if (i && null != Ms) {
                        var d = i * Ms,
                            h = t.doc.scrollTop,
                            p = h + o.wrapper.clientHeight;
                        d < 0 ? h = Math.max(0, h + d - 50) : p = Math.min(t.doc.height, p + d + 50), Ar(t, {
                            top: h,
                            bottom: p
                        })
                    }
                    Ts < 20 && (null == o.wheelStartX ? (o.wheelStartX = a.scrollLeft, o.wheelStartY = a.scrollTop, o.wheelDX = r, o.wheelDY = i, setTimeout(function() {
                        if (null != o.wheelStartX) {
                            var t = a.scrollLeft - o.wheelStartX,
                                e = a.scrollTop - o.wheelStartY,
                                n = e && o.wheelDY && e / o.wheelDY || t && o.wheelDX && t / o.wheelDX;
                            o.wheelStartX = o.wheelStartY = null, n && (Ms = (Ms * Ts + n) / (Ts + 1), ++Ts)
                        }
                    }, 200)) : (o.wheelDX += r, o.wheelDY += i))
                }
            }

            function Br(t, e) {
                var n = t[e];
                t.sort(function(t, e) {
                    return F(t.from(), e.from())
                }), e = d(t, n);
                for (var r = 1; r < t.length; r++) {
                    var i = t[r],
                        o = t[r - 1];
                    if (F(o.to(), i.from()) >= 0) {
                        var a = B(o.from(), i.from()),
                            s = j(o.to(), i.to()),
                            l = o.empty() ? i.from() == i.head : o.from() == o.head;
                        r <= e && --e, t.splice(--r, 2, new Ls(l ? s : a, l ? a : s))
                    }
                }
                return new Os(t, e)
            }

            function Hr(t, e) {
                return new Os([new Ls(t, e || t)], 0)
            }

            function Wr(t) {
                return t.text ? D(t.from.line + t.text.length - 1, m(t.text).length + (1 == t.text.length ? t.from.ch : 0)) : t.to
            }

            function Ur(t, e) {
                if (F(t, e.from) < 0) return t;
                if (F(t, e.to) <= 0) return Wr(e);
                var n = t.line + e.text.length - (e.to.line - e.from.line) - 1,
                    r = t.ch;
                return t.line == e.to.line && (r += Wr(e).ch - e.to.ch), D(n, r)
            }

            function Vr(t, e) {
                for (var n = [], r = 0; r < t.sel.ranges.length; r++) {
                    var i = t.sel.ranges[r];
                    n.push(new Ls(Ur(i.anchor, e), Ur(i.head, e)))
                }
                return Br(n, t.sel.primIndex)
            }

            function Gr(t, e, n) {
                return t.line == e.line ? D(n.line, t.ch - e.ch + n.ch) : D(n.line + (t.line - e.line), t.ch)
            }

            function Kr(t, e, n) {
                for (var r = [], i = D(t.first, 0), o = i, a = 0; a < e.length; a++) {
                    var s = e[a],
                        l = Gr(s.from, i, o),
                        c = Gr(Wr(s), i, o);
                    if (i = s.to, o = c, "around" == n) {
                        var u = t.sel.ranges[a],
                            f = F(u.head, u.anchor) < 0;
                        r[a] = new Ls(f ? c : l, f ? l : c)
                    } else r[a] = new Ls(l, l)
                }
                return new Os(r, t.sel.primIndex)
            }

            function $r(t) {
                t.doc.mode = Vt(t.options, t.doc.modeOption), qr(t)
            }

            function qr(t) {
                t.doc.iter(function(t) {
                    t.stateAfter && (t.stateAfter = null), t.styles && (t.styles = null)
                }), t.doc.modeFrontier = t.doc.highlightFrontier = t.doc.first, _r(t, 100), t.state.modeGen++, t.curOp && yr(t)
            }

            function Yr(t, e) {
                return 0 == e.from.ch && 0 == e.to.ch && "" == m(e.text) && (!t.cm || t.cm.options.wholeLineUpdateBefore)
            }

            function Jr(t, e, n, r) {
                function i(t) {
                    return n ? n[t] : null
                }

                function o(t, n, i) {
                    ae(t, n, i, r), we(t, "change", t, e)
                }

                function a(t, e) {
                    for (var n = [], o = t; o < e; ++o) n.push(new ms(c[o], i(o), r));
                    return n
                }
                var s = e.from,
                    l = e.to,
                    c = e.text,
                    u = T(t, s.line),
                    f = T(t, l.line),
                    d = m(c),
                    h = i(c.length - 1),
                    p = l.line - s.line;
                if (e.full) t.insert(0, a(0, c.length)), t.remove(c.length, t.size - c.length);
                else if (Yr(t, e)) {
                    var g = a(0, c.length - 1);
                    o(f, f.text, h), p && t.remove(s.line, p), g.length && t.insert(s.line, g)
                } else if (u == f)
                    if (1 == c.length) o(u, u.text.slice(0, s.ch) + d + u.text.slice(l.ch), h);
                    else {
                        var v = a(1, c.length - 1);
                        v.push(new ms(d + u.text.slice(l.ch), h, r)), o(u, u.text.slice(0, s.ch) + c[0], i(0)), t.insert(s.line + 1, v)
                    } else if (1 == c.length) o(u, u.text.slice(0, s.ch) + c[0] + f.text.slice(l.ch), i(0)), t.remove(s.line + 1, p);
                else {
                    o(u, u.text.slice(0, s.ch) + c[0], i(0)), o(f, d + f.text.slice(l.ch), h);
                    var y = a(1, c.length - 1);
                    p > 1 && t.remove(s.line + 1, p - 1), t.insert(s.line + 1, y)
                }
                we(t, "change", t, e)
            }

            function Xr(t, e, n) {
                function r(t, i, o) {
                    if (t.linked)
                        for (var a = 0; a < t.linked.length; ++a) {
                            var s = t.linked[a];
                            if (s.doc != i) {
                                var l = o && s.sharedHist;
                                n && !l || (e(s.doc, l), r(s.doc, t, l))
                            }
                        }
                }
                r(t, null, !0)
            }

            function Zr(t, e) {
                if (e.cm) throw new Error("This document is already in use.");
                t.doc = e, e.cm = t, Sn(t), $r(t), Qr(t), t.options.lineWrapping || wt(t), t.options.mode = e.modeOption, yr(t)
            }

            function Qr(t) {
                ("rtl" == t.doc.direction ? s : za)(t.display.lineDiv, "CodeMirror-rtl")
            }

            function ti(t) {
                pr(t, function() {
                    Qr(t), yr(t)
                })
            }

            function ei(t) {
                this.done = [], this.undone = [], this.undoDepth = 1 / 0, this.lastModTime = this.lastSelTime = 0, this.lastOp = this.lastSelOp = null, this.lastOrigin = this.lastSelOrigin = null, this.generation = this.maxGeneration = t || 1
            }

            function ni(t, e) {
                var n = {
                    from: z(e.from),
                    to: Wr(e),
                    text: M(t, e.from, e.to)
                };
                return ci(t, n, e.from.line, e.to.line + 1), Xr(t, function(t) {
                    return ci(t, n, e.from.line, e.to.line + 1)
                }, !0), n
            }

            function ri(t) {
                for (; t.length;) {
                    if (!m(t).ranges) break;
                    t.pop()
                }
            }

            function ii(t, e) {
                return e ? (ri(t.done), m(t.done)) : t.done.length && !m(t.done).ranges ? m(t.done) : t.done.length > 1 && !t.done[t.done.length - 2].ranges ? (t.done.pop(), m(t.done)) : void 0
            }

            function oi(t, e, n, r) {
                var i = t.history;
                i.undone.length = 0;
                var o, a, s = +new Date;
                if ((i.lastOp == r || i.lastOrigin == e.origin && e.origin && ("+" == e.origin.charAt(0) && i.lastModTime > s - (t.cm ? t.cm.options.historyEventDelay : 500) || "*" == e.origin.charAt(0))) && (o = ii(i, i.lastOp == r))) a = m(o.changes), 0 == F(e.from, e.to) && 0 == F(e.from, a.to) ? a.to = Wr(e) : o.changes.push(ni(t, e));
                else {
                    var l = m(i.done);
                    for (l && l.ranges || li(t.sel, i.done), o = {
                            changes: [ni(t, e)],
                            generation: i.generation
                        }, i.done.push(o); i.done.length > i.undoDepth;) i.done.shift(), i.done[0].ranges || i.done.shift()
                }
                i.done.push(n), i.generation = ++i.maxGeneration, i.lastModTime = i.lastSelTime = s, i.lastOp = i.lastSelOp = r, i.lastOrigin = i.lastSelOrigin = e.origin, a || Tt(t, "historyAdded")
            }

            function ai(t, e, n, r) {
                var i = e.charAt(0);
                return "*" == i || "+" == i && n.ranges.length == r.ranges.length && n.somethingSelected() == r.somethingSelected() && new Date - t.history.lastSelTime <= (t.cm ? t.cm.options.historyEventDelay : 500)
            }

            function si(t, e, n, r) {
                var i = t.history,
                    o = r && r.origin;
                n == i.lastSelOp || o && i.lastSelOrigin == o && (i.lastModTime == i.lastSelTime && i.lastOrigin == o || ai(t, o, m(i.done), e)) ? i.done[i.done.length - 1] = e : li(e, i.done), i.lastSelTime = +new Date, i.lastSelOrigin = o, i.lastSelOp = n, r && !1 !== r.clearRedo && ri(i.undone)
            }

            function li(t, e) {
                var n = m(e);
                n && n.ranges && n.equals(t) || e.push(t)
            }

            function ci(t, e, n, r) {
                var i = e["spans_" + t.id],
                    o = 0;
                t.iter(Math.max(t.first, n), Math.min(t.first + t.size, r), function(n) {
                    n.markedSpans && ((i || (i = e["spans_" + t.id] = {}))[o] = n.markedSpans), ++o
                })
            }

            function ui(t) {
                if (!t) return null;
                for (var e, n = 0; n < t.length; ++n) t[n].marker.explicitlyCleared ? e || (e = t.slice(0, n)) : e && e.push(t[n]);
                return e ? e.length ? e : null : t
            }

            function fi(t, e) {
                var n = e["spans_" + t.id];
                if (!n) return null;
                for (var r = [], i = 0; i < e.text.length; ++i) r.push(ui(n[i]));
                return r
            }

            function di(t, e) {
                var n = fi(t, e),
                    r = Q(t, e);
                if (!n) return r;
                if (!r) return n;
                for (var i = 0; i < n.length; ++i) {
                    var o = n[i],
                        a = r[i];
                    if (o && a) t: for (var s = 0; s < a.length; ++s) {
                        for (var l = a[s], c = 0; c < o.length; ++c)
                            if (o[c].marker == l.marker) continue t;
                        o.push(l)
                    } else a && (n[i] = a)
                }
                return n
            }

            function hi(t, e, n) {
                for (var r = [], i = 0; i < t.length; ++i) {
                    var o = t[i];
                    if (o.ranges) r.push(n ? Os.prototype.deepCopy.call(o) : o);
                    else {
                        var a = o.changes,
                            s = [];
                        r.push({
                            changes: s
                        });
                        for (var l = 0; l < a.length; ++l) {
                            var c = a[l],
                                u = void 0;
                            if (s.push({
                                    from: c.from,
                                    to: c.to,
                                    text: c.text
                                }), e)
                                for (var f in c)(u = f.match(/^spans_(\d+)$/)) && d(e, Number(u[1])) > -1 && (m(s)[f] = c[f], delete c[f])
                        }
                    }
                }
                return r
            }

            function pi(t, e, n, r) {
                if (r) {
                    var i = t.anchor;
                    if (n) {
                        var o = F(e, i) < 0;
                        o != F(n, i) < 0 ? (i = e, e = n) : o != F(e, n) < 0 && (e = n)
                    }
                    return new Ls(i, e)
                }
                return new Ls(n || e, e)
            }

            function mi(t, e, n, r, i) {
                null == i && (i = t.cm && (t.cm.display.shift || t.extend)), wi(t, new Os([pi(t.sel.primary(), e, n, i)], 0), r)
            }

            function gi(t, e, n) {
                for (var r = [], i = t.cm && (t.cm.display.shift || t.extend), o = 0; o < t.sel.ranges.length; o++) r[o] = pi(t.sel.ranges[o], e[o], null, i);
                wi(t, Br(r, t.sel.primIndex), n)
            }

            function vi(t, e, n, r) {
                var i = t.sel.ranges.slice(0);
                i[e] = n, wi(t, Br(i, t.sel.primIndex), r)
            }

            function yi(t, e, n, r) {
                wi(t, Hr(e, n), r)
            }

            function bi(t, e, n) {
                var r = {
                    ranges: e.ranges,
                    update: function(e) {
                        var n = this;
                        this.ranges = [];
                        for (var r = 0; r < e.length; r++) n.ranges[r] = new Ls(W(t, e[r].anchor), W(t, e[r].head))
                    },
                    origin: n && n.origin
                };
                return Tt(t, "beforeSelectionChange", t, r), t.cm && Tt(t.cm, "beforeSelectionChange", t.cm, r), r.ranges != e.ranges ? Br(r.ranges, r.ranges.length - 1) : e
            }

            function xi(t, e, n) {
                var r = t.history.done,
                    i = m(r);
                i && i.ranges ? (r[r.length - 1] = e, Ci(t, e, n)) : wi(t, e, n)
            }

            function wi(t, e, n) {
                Ci(t, e, n), si(t, t.sel, t.cm ? t.cm.curOp.id : NaN, n)
            }

            function Ci(t, e, n) {
                (Lt(t, "beforeSelectionChange") || t.cm && Lt(t.cm, "beforeSelectionChange")) && (e = bi(t, e, n)), ki(t, Si(t, e, n && n.bias || (F(e.primary().head, t.sel.primary().head) < 0 ? -1 : 1), !0)), n && !1 === n.scroll || !t.cm || qn(t.cm)
            }

            function ki(t, e) {
                e.equals(t.sel) || (t.sel = e, t.cm && (t.cm.curOp.updateInput = t.cm.curOp.selectionChanged = !0, Ot(t.cm)), we(t, "cursorActivity", t))
            }

            function _i(t) {
                ki(t, Si(t, t.sel, null, !1))
            }

            function Si(t, e, n, r) {
                for (var i, o = 0; o < e.ranges.length; o++) {
                    var a = e.ranges[o],
                        s = e.ranges.length == t.sel.ranges.length && t.sel.ranges[o],
                        l = Ti(t, a.anchor, s && s.anchor, n, r),
                        c = Ti(t, a.head, s && s.head, n, r);
                    (i || l != a.anchor || c != a.head) && (i || (i = e.ranges.slice(0, o)), i[o] = new Ls(l, c))
                }
                return i ? Br(i, e.primIndex) : e
            }

            function Ei(t, e, n, r, i) {
                var o = T(t, e.line);
                if (o.markedSpans)
                    for (var a = 0; a < o.markedSpans.length; ++a) {
                        var s = o.markedSpans[a],
                            l = s.marker;
                        if ((null == s.from || (l.inclusiveLeft ? s.from <= e.ch : s.from < e.ch)) && (null == s.to || (l.inclusiveRight ? s.to >= e.ch : s.to > e.ch))) {
                            if (i && (Tt(l, "beforeCursorEnter"), l.explicitlyCleared)) {
                                if (o.markedSpans) {
                                    --a;
                                    continue
                                }
                                break
                            }
                            if (!l.atomic) continue;
                            if (n) {
                                var c = l.find(r < 0 ? 1 : -1),
                                    u = void 0;
                                if ((r < 0 ? l.inclusiveRight : l.inclusiveLeft) && (c = Mi(t, c, -r, c && c.line == e.line ? o : null)), c && c.line == e.line && (u = F(c, n)) && (r < 0 ? u < 0 : u > 0)) return Ei(t, c, e, r, i)
                            }
                            var f = l.find(r < 0 ? -1 : 1);
                            return (r < 0 ? l.inclusiveLeft : l.inclusiveRight) && (f = Mi(t, f, r, f.line == e.line ? o : null)), f ? Ei(t, f, e, r, i) : null
                        }
                    }
                return e
            }

            function Ti(t, e, n, r, i) {
                var o = r || 1,
                    a = Ei(t, e, n, o, i) || !i && Ei(t, e, n, o, !0) || Ei(t, e, n, -o, i) || !i && Ei(t, e, n, -o, !0);
                return a || (t.cantEdit = !0, D(t.first, 0))
            }

            function Mi(t, e, n, r) {
                return n < 0 && 0 == e.ch ? e.line > t.first ? W(t, D(e.line - 1)) : null : n > 0 && e.ch == (r || T(t, e.line)).text.length ? e.line < t.first + t.size - 1 ? D(e.line + 1, 0) : null : new D(e.line, e.ch + n)
            }

            function Oi(t) {
                t.setSelection(D(t.firstLine(), 0), D(t.lastLine()), Ga)
            }

            function Li(t, e, n) {
                var r = {
                    canceled: !1,
                    from: e.from,
                    to: e.to,
                    text: e.text,
                    origin: e.origin,
                    cancel: function() {
                        return r.canceled = !0
                    }
                };
                return n && (r.update = function(e, n, i, o) {
                    e && (r.from = W(t, e)), n && (r.to = W(t, n)), i && (r.text = i), void 0 !== o && (r.origin = o)
                }), Tt(t, "beforeChange", t, r), t.cm && Tt(t.cm, "beforeChange", t.cm, r), r.canceled ? null : {
                    from: r.from,
                    to: r.to,
                    text: r.text,
                    origin: r.origin
                }
            }

            function Ai(t, e, n) {
                if (t.cm) {
                    if (!t.cm.curOp) return mr(t.cm, Ai)(t, e, n);
                    if (t.cm.state.suppressEdits) return
                }
                if (!(Lt(t, "beforeChange") || t.cm && Lt(t.cm, "beforeChange")) || (e = Li(t, e, !0))) {
                    var r = Xa && !n && et(t, e.from, e.to);
                    if (r)
                        for (var i = r.length - 1; i >= 0; --i) Ni(t, {
                            from: r[i].from,
                            to: r[i].to,
                            text: i ? [""] : e.text,
                            origin: e.origin
                        });
                    else Ni(t, e)
                }
            }

            function Ni(t, e) {
                if (1 != e.text.length || "" != e.text[0] || 0 != F(e.from, e.to)) {
                    var n = Vr(t, e);
                    oi(t, e, n, t.cm ? t.cm.curOp.id : NaN), Di(t, e, n, Q(t, e));
                    var r = [];
                    Xr(t, function(t, n) {
                        n || -1 != d(r, t.history) || (Bi(t.history, e), r.push(t.history)), Di(t, e, null, Q(t, e))
                    })
                }
            }

            function Pi(t, e, n) {
                var r = t.cm && t.cm.state.suppressEdits;
                if (!r || n) {
                    for (var i, o = t.history, a = t.sel, s = "undo" == e ? o.done : o.undone, l = "undo" == e ? o.undone : o.done, c = 0; c < s.length && (i = s[c], n ? !i.ranges || i.equals(t.sel) : i.ranges); c++);
                    if (c != s.length) {
                        for (o.lastOrigin = o.lastSelOrigin = null;;) {
                            if (i = s.pop(), !i.ranges) {
                                if (r) return void s.push(i);
                                break
                            }
                            if (li(i, l), n && !i.equals(t.sel)) return void wi(t, i, {
                                clearRedo: !1
                            });
                            a = i
                        }
                        var u = [];
                        li(a, l), l.push({
                            changes: u,
                            generation: o.generation
                        }), o.generation = i.generation || ++o.maxGeneration;
                        for (var f = Lt(t, "beforeChange") || t.cm && Lt(t.cm, "beforeChange"), h = i.changes.length - 1; h >= 0; --h) {
                            var p = function(n) {
                                var r = i.changes[n];
                                if (r.origin = e, f && !Li(t, r, !1)) return s.length = 0, {};
                                u.push(ni(t, r));
                                var o = n ? Vr(t, r) : m(s);
                                Di(t, r, o, di(t, r)), !n && t.cm && t.cm.scrollIntoView({
                                    from: r.from,
                                    to: Wr(r)
                                });
                                var a = [];
                                Xr(t, function(t, e) {
                                    e || -1 != d(a, t.history) || (Bi(t.history, r), a.push(t.history)), Di(t, r, null, di(t, r))
                                })
                            }(h);
                            if (p) return p.v
                        }
                    }
                }
            }

            function Ii(t, e) {
                if (0 != e && (t.first += e, t.sel = new Os(g(t.sel.ranges, function(t) {
                        return new Ls(D(t.anchor.line + e, t.anchor.ch), D(t.head.line + e, t.head.ch))
                    }), t.sel.primIndex), t.cm)) {
                    yr(t.cm, t.first, t.first - e, e);
                    for (var n = t.cm.display, r = n.viewFrom; r < n.viewTo; r++) br(t.cm, r, "gutter")
                }
            }

            function Di(t, e, n, r) {
                if (t.cm && !t.cm.curOp) return mr(t.cm, Di)(t, e, n, r);
                if (e.to.line < t.first) return void Ii(t, e.text.length - 1 - (e.to.line - e.from.line));
                if (!(e.from.line > t.lastLine())) {
                    if (e.from.line < t.first) {
                        var i = e.text.length - 1 - (t.first - e.from.line);
                        Ii(t, i), e = {
                            from: D(t.first, 0),
                            to: D(e.to.line + i, e.to.ch),
                            text: [m(e.text)],
                            origin: e.origin
                        }
                    }
                    var o = t.lastLine();
                    e.to.line > o && (e = {
                        from: e.from,
                        to: D(o, T(t, o).text.length),
                        text: [e.text[0]],
                        origin: e.origin
                    }), e.removed = M(t, e.from, e.to), n || (n = Vr(t, e)), t.cm ? Fi(t.cm, e, r) : Jr(t, e, r), Ci(t, n, Ga)
                }
            }

            function Fi(t, e, n) {
                var r = t.doc,
                    i = t.display,
                    o = e.from,
                    a = e.to,
                    s = !1,
                    l = o.line;
                t.options.lineWrapping || (l = A(dt(T(r, o.line))), r.iter(l, a.line + 1, function(t) {
                    if (t == i.maxLine) return s = !0, !0
                })), r.sel.contains(e.from, e.to) > -1 && Ot(t), Jr(r, e, n, _n(t)), t.options.lineWrapping || (r.iter(l, o.line + e.text.length, function(t) {
                    var e = xt(t);
                    e > i.maxLineLength && (i.maxLine = t, i.maxLineLength = e, i.maxLineChanged = !0, s = !1)
                }), s && (t.curOp.updateMaxLine = !0)), oe(r, o.line), _r(t, 400);
                var c = e.text.length - (a.line - o.line) - 1;
                e.full ? yr(t) : o.line != a.line || 1 != e.text.length || Yr(t.doc, e) ? yr(t, o.line, a.line + 1, c) : br(t, o.line, "text");
                var u = Lt(t, "changes"),
                    f = Lt(t, "change");
                if (f || u) {
                    var d = {
                        from: o,
                        to: a,
                        text: e.text,
                        removed: e.removed,
                        origin: e.origin
                    };
                    f && we(t, "change", t, d), u && (t.curOp.changeObjs || (t.curOp.changeObjs = [])).push(d)
                }
                t.display.selForContextMenu = null
            }

            function Ri(t, e, n, r, i) {
                if (r || (r = n), F(r, n) < 0) {
                    var o;
                    o = [r, n], n = o[0], r = o[1]
                }
                "string" == typeof e && (e = t.splitLines(e)), Ai(t, {
                    from: n,
                    to: r,
                    text: e,
                    origin: i
                })
            }

            function zi(t, e, n, r) {
                n < t.line ? t.line += r : e < t.line && (t.line = e, t.ch = 0)
            }

            function ji(t, e, n, r) {
                for (var i = 0; i < t.length; ++i) {
                    var o = t[i],
                        a = !0;
                    if (o.ranges) {
                        o.copied || (o = t[i] = o.deepCopy(), o.copied = !0);
                        for (var s = 0; s < o.ranges.length; s++) zi(o.ranges[s].anchor, e, n, r), zi(o.ranges[s].head, e, n, r)
                    } else {
                        for (var l = 0; l < o.changes.length; ++l) {
                            var c = o.changes[l];
                            if (n < c.from.line) c.from = D(c.from.line + r, c.from.ch), c.to = D(c.to.line + r, c.to.ch);
                            else if (e <= c.to.line) {
                                a = !1;
                                break
                            }
                        }
                        a || (t.splice(0, i + 1), i = 0)
                    }
                }
            }

            function Bi(t, e) {
                var n = e.from.line,
                    r = e.to.line,
                    i = e.text.length - (r - n) - 1;
                ji(t.done, n, r, i), ji(t.undone, n, r, i)
            }

            function Hi(t, e, n, r) {
                var i = e,
                    o = e;
                return "number" == typeof e ? o = T(t, H(t, e)) : i = A(e), null == i ? null : (r(o, i) && t.cm && br(t.cm, i, n), o)
            }

            function Wi(t) {
                var e = this;
                this.lines = t, this.parent = null;
                for (var n = 0, r = 0; r < t.length; ++r) t[r].parent = e, n += t[r].height;
                this.height = n
            }

            function Ui(t) {
                var e = this;
                this.children = t;
                for (var n = 0, r = 0, i = 0; i < t.length; ++i) {
                    var o = t[i];
                    n += o.chunkSize(), r += o.height, o.parent = e
                }
                this.size = n, this.height = r, this.parent = null
            }

            function Vi(t, e, n) {
                bt(e) < (t.curOp && t.curOp.scrollTop || t.doc.scrollTop) && $n(t, n)
            }

            function Gi(t, e, n, r) {
                var i = new As(t, n, r),
                    o = t.cm;
                return o && i.noHScroll && (o.display.alignWidgets = !0), Hi(t, e, "widget", function(e) {
                    var n = e.widgets || (e.widgets = []);
                    if (null == i.insertAt ? n.push(i) : n.splice(Math.min(n.length - 1, Math.max(0, i.insertAt)), 0, i), i.line = e, o && !vt(t, e)) {
                        var r = bt(e) < t.scrollTop;
                        L(e, e.height + De(i)), r && $n(o, i.height), o.curOp.forceUpdate = !0
                    }
                    return !0
                }), o && we(o, "lineWidgetAdded", o, i, "number" == typeof e ? e : A(e)), i
            }

            function Ki(t, e, n, r, o) {
                if (r && r.shared) return $i(t, e, n, r, o);
                if (t.cm && !t.cm.curOp) return mr(t.cm, Ki)(t, e, n, r, o);
                var a = new Ps(t, o),
                    s = F(e, n);
                if (r && u(r, a, !1), s > 0 || 0 == s && !1 !== a.clearWhenEmpty) return a;
                if (a.replacedWith && (a.collapsed = !0, a.widgetNode = i("span", [a.replacedWith], "CodeMirror-widget"), r.handleMouseEvents || a.widgetNode.setAttribute("cm-ignore-events", "true"), r.insertLeft && (a.widgetNode.insertLeft = !0)), a.collapsed) {
                    if (ft(t, e.line, e, n, a) || e.line != n.line && ft(t, n.line, e, n, a)) throw new Error("Inserting collapsed marker partially overlapping an existing one");
                    K()
                }
                a.addToHistory && oi(t, {
                    from: e,
                    to: n,
                    origin: "markText"
                }, t.sel, NaN);
                var l, c = e.line,
                    f = t.cm;
                if (t.iter(c, n.line + 1, function(t) {
                        f && a.collapsed && !f.options.lineWrapping && dt(t) == f.display.maxLine && (l = !0), a.collapsed && c != e.line && L(t, 0), J(t, new $(a, c == e.line ? e.ch : null, c == n.line ? n.ch : null)), ++c
                    }), a.collapsed && t.iter(e.line, n.line + 1, function(e) {
                        vt(t, e) && L(e, 0)
                    }), a.clearOnEnter && ns(a, "beforeCursorEnter", function() {
                        return a.clear()
                    }), a.readOnly && (G(), (t.history.done.length || t.history.undone.length) && t.clearHistory()), a.collapsed && (a.id = ++Ns, a.atomic = !0), f) {
                    if (l && (f.curOp.updateMaxLine = !0), a.collapsed) yr(f, e.line, n.line + 1);
                    else if (a.className || a.title || a.startStyle || a.endStyle || a.css)
                        for (var d = e.line; d <= n.line; d++) br(f, d, "text");
                    a.atomic && _i(f.doc), we(f, "markerAdded", f, a)
                }
                return a
            }

            function $i(t, e, n, r, i) {
                r = u(r), r.shared = !1;
                var o = [Ki(t, e, n, r, i)],
                    a = o[0],
                    s = r.widgetNode;
                return Xr(t, function(t) {
                    s && (r.widgetNode = s.cloneNode(!0)), o.push(Ki(t, W(t, e), W(t, n), r, i));
                    for (var l = 0; l < t.linked.length; ++l)
                        if (t.linked[l].isParent) return;
                    a = m(o)
                }), new Is(o, a)
            }

            function qi(t) {
                return t.findMarks(D(t.first, 0), t.clipPos(D(t.lastLine())), function(t) {
                    return t.parent
                })
            }

            function Yi(t, e) {
                for (var n = 0; n < e.length; n++) {
                    var r = e[n],
                        i = r.find(),
                        o = t.clipPos(i.from),
                        a = t.clipPos(i.to);
                    if (F(o, a)) {
                        var s = Ki(t, o, a, r.primary, r.primary.type);
                        r.markers.push(s), s.parent = r
                    }
                }
            }

            function Ji(t) {
                for (var e = 0; e < t.length; e++) ! function(e) {
                    var n = t[e],
                        r = [n.primary.doc];
                    Xr(n.primary.doc, function(t) {
                        return r.push(t)
                    });
                    for (var i = 0; i < n.markers.length; i++) {
                        var o = n.markers[i]; - 1 == d(r, o.doc) && (o.parent = null, n.markers.splice(i--, 1))
                    }
                }(e)
            }

            function Xi(t) {
                var e = this;
                if (to(e), !Mt(e, t) && !Fe(e.display, t)) {
                    Nt(t), ba && (Rs = +new Date);
                    var n = En(e, t, !0),
                        r = t.dataTransfer.files;
                    if (n && !e.isReadOnly())
                        if (r && r.length && window.FileReader && window.File)
                            for (var i = r.length, o = Array(i), a = 0, s = 0; s < i; ++s) ! function(t, r) {
                                if (!e.options.allowDropFileTypes || -1 != d(e.options.allowDropFileTypes, t.type)) {
                                    var s = new FileReader;
                                    s.onload = mr(e, function() {
                                        var t = s.result;
                                        if (/[\x00-\x08\x0e-\x1f]{2}/.test(t) && (t = ""), o[r] = t, ++a == i) {
                                            n = W(e.doc, n);
                                            var l = {
                                                from: n,
                                                to: n,
                                                text: e.doc.splitLines(o.join(e.doc.lineSeparator())),
                                                origin: "paste"
                                            };
                                            Ai(e.doc, l), xi(e.doc, Hr(n, Wr(l)))
                                        }
                                    }), s.readAsText(t)
                                }
                            }(r[s], s);
                        else {
                            if (e.state.draggingText && e.doc.sel.contains(n) > -1) return e.state.draggingText(t), void setTimeout(function() {
                                return e.display.input.focus()
                            }, 20);
                            try {
                                var l = t.dataTransfer.getData("Text");
                                if (l) {
                                    var c;
                                    if (e.state.draggingText && !e.state.draggingText.copy && (c = e.listSelections()), Ci(e.doc, Hr(n, n)), c)
                                        for (var u = 0; u < c.length; ++u) Ri(e.doc, "", c[u].anchor, c[u].head, "drag");
                                    e.replaceSelection(l, "around", "paste"), e.display.input.focus()
                                }
                            } catch (t) {}
                        }
                }
            }

            function Zi(t, e) {
                if (ba && (!t.state.draggingText || +new Date - Rs < 100)) return void Dt(e);
                if (!Mt(t, e) && !Fe(t.display, e) && (e.dataTransfer.setData("Text", t.getSelection()), e.dataTransfer.effectAllowed = "copyMove", e.dataTransfer.setDragImage && !Sa)) {
                    var n = r("img", null, null, "position: fixed; left: 0; top: 0;");
                    n.src = "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==", _a && (n.width = n.height = 1, t.display.wrapper.appendChild(n), n._top = n.offsetTop), e.dataTransfer.setDragImage(n, 0, 0), _a && n.parentNode.removeChild(n)
                }
            }

            function Qi(t, e) {
                var i = En(t, e);
                if (i) {
                    var o = document.createDocumentFragment();
                    Ln(t, i, o), t.display.dragCursor || (t.display.dragCursor = r("div", null, "CodeMirror-cursors CodeMirror-dragcursors"), t.display.lineSpace.insertBefore(t.display.dragCursor, t.display.cursorDiv)), n(t.display.dragCursor, o)
                }
            }

            function to(t) {
                t.display.dragCursor && (t.display.lineSpace.removeChild(t.display.dragCursor), t.display.dragCursor = null)
            }

            function eo(t) {
                if (document.getElementsByClassName)
                    for (var e = document.getElementsByClassName("CodeMirror"), n = 0; n < e.length; n++) {
                        var r = e[n].CodeMirror;
                        r && t(r)
                    }
            }

            function no() {
                zs || (ro(), zs = !0)
            }

            function ro() {
                var t;
                ns(window, "resize", function() {
                    null == t && (t = setTimeout(function() {
                        t = null, eo(io)
                    }, 100))
                }), ns(window, "blur", function() {
                    return eo(Rn)
                })
            }

            function io(t) {
                var e = t.display;
                e.cachedCharWidth = e.cachedTextHeight = e.cachedPaddingH = null, e.scrollbarsClipped = !1, t.setSize()
            }

            function oo(t) {
                var e = t.split(/-(?!$)/);
                t = e[e.length - 1];
                for (var n, r, i, o, a = 0; a < e.length - 1; a++) {
                    var s = e[a];
                    if (/^(cmd|meta|m)$/i.test(s)) o = !0;
                    else if (/^a(lt)?$/i.test(s)) n = !0;
                    else if (/^(c|ctrl|control)$/i.test(s)) r = !0;
                    else {
                        if (!/^s(hift)?$/i.test(s)) throw new Error("Unrecognized modifier name: " + s);
                        i = !0
                    }
                }
                return n && (t = "Alt-" + t), r && (t = "Ctrl-" + t), o && (t = "Cmd-" + t), i && (t = "Shift-" + t), t
            }

            function ao(t) {
                var e = {};
                for (var n in t)
                    if (t.hasOwnProperty(n)) {
                        var r = t[n];
                        if (/^(name|fallthrough|(de|at)tach)$/.test(n)) continue;
                        if ("..." == r) {
                            delete t[n];
                            continue
                        }
                        for (var i = g(n.split(" "), oo), o = 0; o < i.length; o++) {
                            var a = void 0,
                                s = void 0;
                            o == i.length - 1 ? (s = i.join(" "), a = r) : (s = i.slice(0, o + 1).join(" "), a = "...");
                            var l = e[s];
                            if (l) {
                                if (l != a) throw new Error("Inconsistent bindings for " + s)
                            } else e[s] = a
                        }
                        delete t[n]
                    }
                for (var c in e) t[c] = e[c];
                return t
            }

            function so(t, e, n, r) {
                e = fo(e);
                var i = e.call ? e.call(t, r) : e[t];
                if (!1 === i) return "nothing";
                if ("..." === i) return "multi";
                if (null != i && n(i)) return "handled";
                if (e.fallthrough) {
                    if ("[object Array]" != Object.prototype.toString.call(e.fallthrough)) return so(t, e.fallthrough, n, r);
                    for (var o = 0; o < e.fallthrough.length; o++) {
                        var a = so(t, e.fallthrough[o], n, r);
                        if (a) return a
                    }
                }
            }

            function lo(t) {
                var e = "string" == typeof t ? t : js[t.keyCode];
                return "Ctrl" == e || "Alt" == e || "Shift" == e || "Mod" == e
            }

            function co(t, e, n) {
                var r = t;
                return e.altKey && "Alt" != r && (t = "Alt-" + t), (Fa ? e.metaKey : e.ctrlKey) && "Ctrl" != r && (t = "Ctrl-" + t), (Fa ? e.ctrlKey : e.metaKey) && "Cmd" != r && (t = "Cmd-" + t), !n && e.shiftKey && "Shift" != r && (t = "Shift-" + t), t
            }

            function uo(t, e) {
                if (_a && 34 == t.keyCode && t.char) return !1;
                var n = js[t.keyCode];
                return null != n && !t.altGraphKey && (3 == t.keyCode && t.code && (n = t.code), co(n, t, e))
            }

            function fo(t) {
                return "string" == typeof t ? Us[t] : t
            }

            function ho(t, e) {
                for (var n = t.doc.sel.ranges, r = [], i = 0; i < n.length; i++) {
                    for (var o = e(n[i]); r.length && F(o.from, m(r).to) <= 0;) {
                        var a = r.pop();
                        if (F(a.from, o.from) < 0) {
                            o.from = a.from;
                            break
                        }
                    }
                    r.push(o)
                }
                pr(t, function() {
                    for (var e = r.length - 1; e >= 0; e--) Ri(t.doc, "", r[e].from, r[e].to, "+delete");
                    qn(t)
                })
            }

            function po(t, e, n) {
                var r = _(t.text, e + n, n);
                return r < 0 || r > t.text.length ? null : r
            }

            function mo(t, e, n) {
                var r = po(t, e.ch, n);
                return null == r ? null : new D(e.line, r, n < 0 ? "after" : "before")
            }

            function go(t, e, n, r, i) {
                if (t) {
                    var o = _t(n, e.doc.direction);
                    if (o) {
                        var a, s = i < 0 ? m(o) : o[0],
                            l = i < 0 == (1 == s.level),
                            c = l ? "after" : "before";
                        if (s.level > 0 || "rtl" == e.doc.direction) {
                            var u = qe(e, n);
                            a = i < 0 ? n.text.length - 1 : 0;
                            var f = Ye(e, u, a).top;
                            a = S(function(t) {
                                return Ye(e, u, t).top == f
                            }, i < 0 == (1 == s.level) ? s.from : s.to - 1, a), "before" == c && (a = po(n, a, 1))
                        } else a = i < 0 ? s.to : s.from;
                        return new D(r, a, c)
                    }
                }
                return new D(r, i < 0 ? n.text.length : 0, i < 0 ? "before" : "after")
            }

            function vo(t, e, n, r) {
                var i = _t(e, t.doc.direction);
                if (!i) return mo(e, n, r);
                n.ch >= e.text.length ? (n.ch = e.text.length, n.sticky = "before") : n.ch <= 0 && (n.ch = 0, n.sticky = "after");
                var o = kt(i, n.ch, n.sticky),
                    a = i[o];
                if ("ltr" == t.doc.direction && a.level % 2 == 0 && (r > 0 ? a.to > n.ch : a.from < n.ch)) return mo(e, n, r);
                var s, l = function(t, n) {
                        return po(e, t instanceof D ? t.ch : t, n)
                    },
                    c = function(n) {
                        return t.options.lineWrapping ? (s = s || qe(t, e), mn(t, e, s, n)) : {
                            begin: 0,
                            end: e.text.length
                        }
                    },
                    u = c("before" == n.sticky ? l(n, -1) : n.ch);
                if ("rtl" == t.doc.direction || 1 == a.level) {
                    var f = 1 == a.level == r < 0,
                        d = l(n, f ? 1 : -1);
                    if (null != d && (f ? d <= a.to && d <= u.end : d >= a.from && d >= u.begin)) {
                        var h = f ? "before" : "after";
                        return new D(n.line, d, h)
                    }
                }
                var p = function(t, e, r) {
                        for (var o = function(t, e) {
                                return e ? new D(n.line, l(t, 1), "before") : new D(n.line, t, "after")
                            }; t >= 0 && t < i.length; t += e) {
                            var a = i[t],
                                s = e > 0 == (1 != a.level),
                                c = s ? r.begin : l(r.end, -1);
                            if (a.from <= c && c < a.to) return o(c, s);
                            if (c = s ? a.from : l(a.to, -1), r.begin <= c && c < r.end) return o(c, s)
                        }
                    },
                    m = p(o + r, r, u);
                if (m) return m;
                var g = r > 0 ? u.end : l(u.begin, -1);
                return null == g || r > 0 && g == e.text.length || !(m = p(r > 0 ? 0 : i.length - 1, r, c(g))) ? null : m
            }

            function yo(t, e) {
                var n = T(t.doc, e),
                    r = dt(n);
                return r != n && (e = A(r)), go(!0, t, r, e, 1)
            }

            function bo(t, e) {
                var n = T(t.doc, e),
                    r = ht(n);
                return r != n && (e = A(r)), go(!0, t, n, e, -1)
            }

            function xo(t, e) {
                var n = yo(t, e.line),
                    r = T(t.doc, n.line),
                    i = _t(r, t.doc.direction);
                if (!i || 0 == i[0].level) {
                    var o = Math.max(0, r.text.search(/\S/)),
                        a = e.line == n.line && e.ch <= o && e.ch;
                    return D(n.line, a ? 0 : o, n.sticky)
                }
                return n
            }

            function wo(t, e, n) {
                if ("string" == typeof e && !(e = Vs[e])) return !1;
                t.display.input.ensurePolled();
                var r = t.display.shift,
                    i = !1;
                try {
                    t.isReadOnly() && (t.state.suppressEdits = !0), n && (t.display.shift = !1), i = e(t) != Va
                } finally {
                    t.display.shift = r, t.state.suppressEdits = !1
                }
                return i
            }

            function Co(t, e, n) {
                for (var r = 0; r < t.state.keyMaps.length; r++) {
                    var i = so(e, t.state.keyMaps[r], n, t);
                    if (i) return i
                }
                return t.options.extraKeys && so(e, t.options.extraKeys, n, t) || so(e, t.options.keyMap, n, t)
            }

            function ko(t, e, n, r) {
                var i = t.state.keySeq;
                if (i) {
                    if (lo(e)) return "handled";
                    if (/\'$/.test(e) ? t.state.keySeq = null : Gs.set(50, function() {
                            t.state.keySeq == i && (t.state.keySeq = null, t.display.input.reset())
                        }), _o(t, i + " " + e, n, r)) return !0
                }
                return _o(t, e, n, r)
            }

            function _o(t, e, n, r) {
                var i = Co(t, e, r);
                return "multi" == i && (t.state.keySeq = e), "handled" == i && we(t, "keyHandled", t, e, n), "handled" != i && "multi" != i || (Nt(n), Pn(t)), !!i
            }

            function So(t, e) {
                var n = uo(e, !0);
                return !!n && (e.shiftKey && !t.state.keySeq ? ko(t, "Shift-" + n, e, function(e) {
                    return wo(t, e, !0)
                }) || ko(t, n, e, function(e) {
                    if ("string" == typeof e ? /^go[A-Z]/.test(e) : e.motion) return wo(t, e)
                }) : ko(t, n, e, function(e) {
                    return wo(t, e)
                }))
            }

            function Eo(t, e, n) {
                return ko(t, "'" + n + "'", e, function(e) {
                    return wo(t, e, !0)
                })
            }

            function To(t) {
                var e = this;
                if (e.curOp.focus = a(), !Mt(e, t)) {
                    ba && xa < 11 && 27 == t.keyCode && (t.returnValue = !1);
                    var n = t.keyCode;
                    e.display.shift = 16 == n || t.shiftKey;
                    var r = So(e, t);
                    _a && (Ks = r ? n : null, !r && 88 == n && !as && (Aa ? t.metaKey : t.ctrlKey) && e.replaceSelection("", null, "cut")), 18 != n || /\bCodeMirror-crosshair\b/.test(e.display.lineDiv.className) || Mo(e)
                }
            }

            function Mo(t) {
                function e(t) {
                    18 != t.keyCode && t.altKey || (za(n, "CodeMirror-crosshair"), Et(document, "keyup", e), Et(document, "mouseover", e))
                }
                var n = t.display.lineDiv;
                s(n, "CodeMirror-crosshair"), ns(document, "keyup", e), ns(document, "mouseover", e)
            }

            function Oo(t) {
                16 == t.keyCode && (this.doc.sel.shift = !1), Mt(this, t)
            }

            function Lo(t) {
                var e = this;
                if (!(Fe(e.display, t) || Mt(e, t) || t.ctrlKey && !t.altKey || Aa && t.metaKey)) {
                    var n = t.keyCode,
                        r = t.charCode;
                    if (_a && n == Ks) return Ks = null, void Nt(t);
                    if (!_a || t.which && !(t.which < 10) || !So(e, t)) {
                        var i = String.fromCharCode(null == r ? n : r);
                        "\b" != i && (Eo(e, t, i) || e.display.input.onKeyPress(t))
                    }
                }
            }

            function Ao(t, e) {
                var n = +new Date;
                return Ys && Ys.compare(n, t, e) ? (qs = Ys = null, "triple") : qs && qs.compare(n, t, e) ? (Ys = new $s(n, t, e), qs = null, "double") : (qs = new $s(n, t, e), Ys = null, "single")
            }

            function No(t) {
                var e = this,
                    n = e.display;
                if (!(Mt(e, t) || n.activeTouch && n.input.supportsTouch())) {
                    if (n.input.ensurePolled(), n.shift = t.shiftKey, Fe(n, t)) return void(wa || (n.scroller.draggable = !1, setTimeout(function() {
                        return n.scroller.draggable = !0
                    }, 100)));
                    if (!Ho(e, t)) {
                        var r = En(e, t),
                            i = Rt(t),
                            o = r ? Ao(r, i) : "single";
                        window.focus(), 1 == i && e.state.selectingText && e.state.selectingText(t), r && Po(e, i, r, o, t) || (1 == i ? r ? Do(e, r, o, t) : Ft(t) == n.scroller && Nt(t) : 2 == i ? (r && mi(e.doc, r), setTimeout(function() {
                            return n.input.focus()
                        }, 20)) : 3 == i && (Ra ? Wo(e, t) : Dn(e)))
                    }
                }
            }

            function Po(t, e, n, r, i) {
                var o = "Click";
                return "double" == r ? o = "Double" + o : "triple" == r && (o = "Triple" + o), o = (1 == e ? "Left" : 2 == e ? "Middle" : "Right") + o, ko(t, co(o, i), i, function(e) {
                    if ("string" == typeof e && (e = Vs[e]), !e) return !1;
                    var r = !1;
                    try {
                        t.isReadOnly() && (t.state.suppressEdits = !0), r = e(t, n) != Va
                    } finally {
                        t.state.suppressEdits = !1
                    }
                    return r
                })
            }

            function Io(t, e, n) {
                var r = t.getOption("configureMouse"),
                    i = r ? r(t, e, n) : {};
                if (null == i.unit) {
                    var o = Na ? n.shiftKey && n.metaKey : n.altKey;
                    i.unit = o ? "rectangle" : "single" == e ? "char" : "double" == e ? "word" : "line"
                }
                return (null == i.extend || t.doc.extend) && (i.extend = t.doc.extend || n.shiftKey), null == i.addNew && (i.addNew = Aa ? n.metaKey : n.ctrlKey), null == i.moveOnDrag && (i.moveOnDrag = !(Aa ? n.altKey : n.ctrlKey)), i
            }

            function Do(t, e, n, r) {
                ba ? setTimeout(c(In, t), 0) : t.curOp.focus = a();
                var i, o = Io(t, n, r),
                    s = t.doc.sel;
                t.options.dragDrop && rs && !t.isReadOnly() && "single" == n && (i = s.contains(e)) > -1 && (F((i = s.ranges[i]).from(), e) < 0 || e.xRel > 0) && (F(i.to(), e) > 0 || e.xRel < 0) ? Fo(t, r, e, o) : zo(t, r, e, o)
            }

            function Fo(t, e, n, r) {
                var i = t.display,
                    o = !1,
                    a = mr(t, function(e) {
                        wa && (i.scroller.draggable = !1), t.state.draggingText = !1, Et(i.wrapper.ownerDocument, "mouseup", a), Et(i.wrapper.ownerDocument, "mousemove", s), Et(i.scroller, "dragstart", l), Et(i.scroller, "drop", a), o || (Nt(e), r.addNew || mi(t.doc, n, null, null, r.extend), wa || ba && 9 == xa ? setTimeout(function() {
                            i.wrapper.ownerDocument.body.focus(), i.input.focus()
                        }, 20) : i.input.focus())
                    }),
                    s = function(t) {
                        o = o || Math.abs(e.clientX - t.clientX) + Math.abs(e.clientY - t.clientY) >= 10
                    },
                    l = function() {
                        return o = !0
                    };
                wa && (i.scroller.draggable = !0), t.state.draggingText = a, a.copy = !r.moveOnDrag, i.scroller.dragDrop && i.scroller.dragDrop(), ns(i.wrapper.ownerDocument, "mouseup", a), ns(i.wrapper.ownerDocument, "mousemove", s), ns(i.scroller, "dragstart", l), ns(i.scroller, "drop", a), Dn(t), setTimeout(function() {
                    return i.input.focus()
                }, 20)
            }

            function Ro(t, e, n) {
                if ("char" == n) return new Ls(e, e);
                if ("word" == n) return t.findWordAt(e);
                if ("line" == n) return new Ls(D(e.line, 0), W(t.doc, D(e.line + 1, 0)));
                var r = n(t, e);
                return new Ls(r.from, r.to)
            }

            function zo(t, e, n, r) {
                function i(e) {
                    if (0 != F(v, e))
                        if (v = e, "rectangle" == r.unit) {
                            for (var i = [], o = t.options.tabSize, a = f(T(c, n.line).text, n.ch, o), s = f(T(c, e.line).text, e.ch, o), l = Math.min(a, s), m = Math.max(a, s), g = Math.min(n.line, e.line), y = Math.min(t.lastLine(), Math.max(n.line, e.line)); g <= y; g++) {
                                var b = T(c, g).text,
                                    x = h(b, l, o);
                                l == m ? i.push(new Ls(D(g, x), D(g, x))) : b.length > x && i.push(new Ls(D(g, x), D(g, h(b, m, o))))
                            }
                            i.length || i.push(new Ls(n, n)), wi(c, Br(p.ranges.slice(0, d).concat(i), d), {
                                origin: "*mouse",
                                scroll: !1
                            }), t.scrollIntoView(e)
                        } else {
                            var w, C = u,
                                k = Ro(t, e, r.unit),
                                _ = C.anchor;
                            F(k.anchor, _) > 0 ? (w = k.head, _ = B(C.from(), k.anchor)) : (w = k.anchor, _ = j(C.to(), k.head));
                            var S = p.ranges.slice(0);
                            S[d] = jo(t, new Ls(W(c, _), w)), wi(c, Br(S, d), Ka)
                        }
                }

                function o(e) {
                    var n = ++b,
                        s = En(t, e, !0, "rectangle" == r.unit);
                    if (s)
                        if (0 != F(s, v)) {
                            t.curOp.focus = a(), i(s);
                            var u = Bn(l, c);
                            (s.line >= u.to || s.line < u.from) && setTimeout(mr(t, function() {
                                b == n && o(e)
                            }), 150)
                        } else {
                            var f = e.clientY < y.top ? -20 : e.clientY > y.bottom ? 20 : 0;
                            f && setTimeout(mr(t, function() {
                                b == n && (l.scroller.scrollTop += f, o(e))
                            }), 50)
                        }
                }

                function s(e) {
                    t.state.selectingText = !1, b = 1 / 0, Nt(e), l.input.focus(), Et(l.wrapper.ownerDocument, "mousemove", x), Et(l.wrapper.ownerDocument, "mouseup", w), c.history.lastSelOrigin = null
                }
                var l = t.display,
                    c = t.doc;
                Nt(e);
                var u, d, p = c.sel,
                    m = p.ranges;
                if (r.addNew && !r.extend ? (d = c.sel.contains(n), u = d > -1 ? m[d] : new Ls(n, n)) : (u = c.sel.primary(), d = c.sel.primIndex), "rectangle" == r.unit) r.addNew || (u = new Ls(n, n)), n = En(t, e, !0, !0), d = -1;
                else {
                    var g = Ro(t, n, r.unit);
                    u = r.extend ? pi(u, g.anchor, g.head, r.extend) : g
                }
                r.addNew ? -1 == d ? (d = m.length, wi(c, Br(m.concat([u]), d), {
                    scroll: !1,
                    origin: "*mouse"
                })) : m.length > 1 && m[d].empty() && "char" == r.unit && !r.extend ? (wi(c, Br(m.slice(0, d).concat(m.slice(d + 1)), 0), {
                    scroll: !1,
                    origin: "*mouse"
                }), p = c.sel) : vi(c, d, u, Ka) : (d = 0, wi(c, new Os([u], 0), Ka), p = c.sel);
                var v = n,
                    y = l.wrapper.getBoundingClientRect(),
                    b = 0,
                    x = mr(t, function(t) {
                        0 !== t.buttons && Rt(t) ? o(t) : s(t)
                    }),
                    w = mr(t, s);
                t.state.selectingText = w, ns(l.wrapper.ownerDocument, "mousemove", x), ns(l.wrapper.ownerDocument, "mouseup", w)
            }

            function jo(t, e) {
                var n = e.anchor,
                    r = e.head,
                    i = T(t.doc, n.line);
                if (0 == F(n, r) && n.sticky == r.sticky) return e;
                var o = _t(i);
                if (!o) return e;
                var a = kt(o, n.ch, n.sticky),
                    s = o[a];
                if (s.from != n.ch && s.to != n.ch) return e;
                var l = a + (s.from == n.ch == (1 != s.level) ? 0 : 1);
                if (0 == l || l == o.length) return e;
                var c;
                if (r.line != n.line) c = (r.line - n.line) * ("ltr" == t.doc.direction ? 1 : -1) > 0;
                else {
                    var u = kt(o, r.ch, r.sticky),
                        f = u - a || (r.ch - n.ch) * (1 == s.level ? -1 : 1);
                    c = u == l - 1 || u == l ? f < 0 : f > 0
                }
                var d = o[l + (c ? -1 : 0)],
                    h = c == (1 == d.level),
                    p = h ? d.from : d.to,
                    m = h ? "after" : "before";
                return n.ch == p && n.sticky == m ? e : new Ls(new D(n.line, p, m), r)
            }

            function Bo(t, e, n, r) {
                var i, o;
                if (e.touches) i = e.touches[0].clientX, o = e.touches[0].clientY;
                else try {
                    i = e.clientX, o = e.clientY
                } catch (e) {
                    return !1
                }
                if (i >= Math.floor(t.display.gutters.getBoundingClientRect().right)) return !1;
                r && Nt(e);
                var a = t.display,
                    s = a.lineDiv.getBoundingClientRect();
                if (o > s.bottom || !Lt(t, n)) return It(e);
                o -= s.top - a.viewOffset;
                for (var l = 0; l < t.options.gutters.length; ++l) {
                    var c = a.gutters.childNodes[l];
                    if (c && c.getBoundingClientRect().right >= i) {
                        return Tt(t, n, t, N(t.doc, o), t.options.gutters[l], e), It(e)
                    }
                }
            }

            function Ho(t, e) {
                return Bo(t, e, "gutterClick", !0)
            }

            function Wo(t, e) {
                Fe(t.display, e) || Uo(t, e) || Mt(t, e, "contextmenu") || t.display.input.onContextMenu(e)
            }

            function Uo(t, e) {
                return !!Lt(t, "gutterContextMenu") && Bo(t, e, "gutterContextMenu", !1)
            }

            function Vo(t) {
                t.display.wrapper.className = t.display.wrapper.className.replace(/\s*cm-s-\S+/g, "") + t.options.theme.replace(/(^|\s)\s*/g, " cm-s-"), nn(t)
            }

            function Go(t) {
                Dr(t), yr(t), Hn(t)
            }

            function Ko(t, e, n) {
                if (!e != !(n && n != Js)) {
                    var r = t.display.dragFunctions,
                        i = e ? ns : Et;
                    i(t.display.scroller, "dragstart", r.start), i(t.display.scroller, "dragenter", r.enter), i(t.display.scroller, "dragover", r.over), i(t.display.scroller, "dragleave", r.leave), i(t.display.scroller, "drop", r.drop)
                }
            }

            function $o(t) {
                t.options.lineWrapping ? (s(t.display.wrapper, "CodeMirror-wrap"), t.display.sizer.style.minWidth = "", t.display.sizerWidth = null) : (za(t.display.wrapper, "CodeMirror-wrap"), wt(t)), Sn(t), yr(t), nn(t), setTimeout(function() {
                    return rr(t)
                }, 100)
            }

            function qo(t, e) {
                var n = this;
                if (!(this instanceof qo)) return new qo(t, e);
                this.options = e = e ? u(e) : {}, u(Xs, e, !1), Fr(e);
                var r = e.value;
                "string" == typeof r ? r = new Fs(r, e.mode, null, e.lineSeparator, e.direction) : e.mode && (r.modeOption = e.mode), this.doc = r;
                var i = new qo.inputStyles[e.inputStyle](this),
                    o = this.display = new E(t, r, i);
                o.wrapper.CodeMirror = this, Dr(this), Vo(this), e.lineWrapping && (this.display.wrapper.className += " CodeMirror-wrap"), or(this), this.state = {
                    keyMaps: [],
                    overlays: [],
                    modeGen: 0,
                    overwrite: !1,
                    delayingBlurEvent: !1,
                    focused: !1,
                    suppressEdits: !1,
                    pasteIncoming: !1,
                    cutIncoming: !1,
                    selectingText: !1,
                    draggingText: !1,
                    highlight: new Ba,
                    keySeq: null,
                    specialChars: null
                }, e.autofocus && !La && o.input.focus(), ba && xa < 11 && setTimeout(function() {
                    return n.display.input.reset(!0)
                }, 20), Yo(this), no(), ar(this), this.curOp.forceUpdate = !0, Zr(this, r), e.autofocus && !La || this.hasFocus() ? setTimeout(c(Fn, this), 20) : Rn(this);
                for (var a in Zs) Zs.hasOwnProperty(a) && Zs[a](n, e[a], Js);
                Wn(this), e.finishInit && e.finishInit(this);
                for (var s = 0; s < Qs.length; ++s) Qs[s](n);
                sr(this), wa && e.lineWrapping && "optimizelegibility" == getComputedStyle(o.lineDiv).textRendering && (o.lineDiv.style.textRendering = "auto")
            }

            function Yo(t) {
                function e() {
                    i.activeTouch && (o = setTimeout(function() {
                        return i.activeTouch = null
                    }, 1e3), a = i.activeTouch, a.end = +new Date)
                }

                function n(t) {
                    if (1 != t.touches.length) return !1;
                    var e = t.touches[0];
                    return e.radiusX <= 1 && e.radiusY <= 1
                }

                function r(t, e) {
                    if (null == e.left) return !0;
                    var n = e.left - t.left,
                        r = e.top - t.top;
                    return n * n + r * r > 400
                }
                var i = t.display;
                ns(i.scroller, "mousedown", mr(t, No)), ba && xa < 11 ? ns(i.scroller, "dblclick", mr(t, function(e) {
                    if (!Mt(t, e)) {
                        var n = En(t, e);
                        if (n && !Ho(t, e) && !Fe(t.display, e)) {
                            Nt(e);
                            var r = t.findWordAt(n);
                            mi(t.doc, r.anchor, r.head)
                        }
                    }
                })) : ns(i.scroller, "dblclick", function(e) {
                    return Mt(t, e) || Nt(e)
                }), Ra || ns(i.scroller, "contextmenu", function(e) {
                    return Wo(t, e)
                });
                var o, a = {
                    end: 0
                };
                ns(i.scroller, "touchstart", function(e) {
                    if (!Mt(t, e) && !n(e) && !Ho(t, e)) {
                        i.input.ensurePolled(), clearTimeout(o);
                        var r = +new Date;
                        i.activeTouch = {
                            start: r,
                            moved: !1,
                            prev: r - a.end <= 300 ? a : null
                        }, 1 == e.touches.length && (i.activeTouch.left = e.touches[0].pageX, i.activeTouch.top = e.touches[0].pageY)
                    }
                }), ns(i.scroller, "touchmove", function() {
                    i.activeTouch && (i.activeTouch.moved = !0)
                }), ns(i.scroller, "touchend", function(n) {
                    var o = i.activeTouch;
                    if (o && !Fe(i, n) && null != o.left && !o.moved && new Date - o.start < 300) {
                        var a, s = t.coordsChar(i.activeTouch, "page");
                        a = !o.prev || r(o, o.prev) ? new Ls(s, s) : !o.prev.prev || r(o, o.prev.prev) ? t.findWordAt(s) : new Ls(D(s.line, 0), W(t.doc, D(s.line + 1, 0))), t.setSelection(a.anchor, a.head), t.focus(), Nt(n)
                    }
                    e()
                }), ns(i.scroller, "touchcancel", e), ns(i.scroller, "scroll", function() {
                    i.scroller.clientHeight && (Qn(t, i.scroller.scrollTop), er(t, i.scroller.scrollLeft, !0), Tt(t, "scroll", t))
                }), ns(i.scroller, "mousewheel", function(e) {
                    return jr(t, e)
                }), ns(i.scroller, "DOMMouseScroll", function(e) {
                    return jr(t, e)
                }), ns(i.wrapper, "scroll", function() {
                    return i.wrapper.scrollTop = i.wrapper.scrollLeft = 0
                }), i.dragFunctions = {
                    enter: function(e) {
                        Mt(t, e) || Dt(e)
                    },
                    over: function(e) {
                        Mt(t, e) || (Qi(t, e), Dt(e))
                    },
                    start: function(e) {
                        return Zi(t, e)
                    },
                    drop: mr(t, Xi),
                    leave: function(e) {
                        Mt(t, e) || to(t)
                    }
                };
                var s = i.input.getField();
                ns(s, "keyup", function(e) {
                    return Oo.call(t, e)
                }), ns(s, "keydown", mr(t, To)), ns(s, "keypress", mr(t, Lo)), ns(s, "focus", function(e) {
                    return Fn(t, e)
                }), ns(s, "blur", function(e) {
                    return Rn(t, e)
                })
            }

            function Jo(t, e, n, r) {
                var i, o = t.doc;
                null == n && (n = "add"), "smart" == n && (o.mode.indent ? i = Xt(t, e).state : n = "prev");
                var a = t.options.tabSize,
                    s = T(o, e),
                    l = f(s.text, null, a);
                s.stateAfter && (s.stateAfter = null);
                var c, u = s.text.match(/^\s*/)[0];
                if (r || /\S/.test(s.text)) {
                    if ("smart" == n && ((c = o.mode.indent(i, s.text.slice(u.length), s.text)) == Va || c > 150)) {
                        if (!r) return;
                        n = "prev"
                    }
                } else c = 0, n = "not";
                "prev" == n ? c = e > o.first ? f(T(o, e - 1).text, null, a) : 0 : "add" == n ? c = l + t.options.indentUnit : "subtract" == n ? c = l - t.options.indentUnit : "number" == typeof n && (c = l + n), c = Math.max(0, c);
                var d = "",
                    h = 0;
                if (t.options.indentWithTabs)
                    for (var m = Math.floor(c / a); m; --m) h += a, d += "\t";
                if (h < c && (d += p(c - h)), d != u) return Ri(o, d, D(e, 0), D(e, u.length), "+input"), s.stateAfter = null, !0;
                for (var g = 0; g < o.sel.ranges.length; g++) {
                    var v = o.sel.ranges[g];
                    if (v.head.line == e && v.head.ch < u.length) {
                        var y = D(e, u.length);
                        vi(o, g, new Ls(y, y));
                        break
                    }
                }
            }

            function Xo(t) {
                tl = t
            }

            function Zo(t, e, n, r, i) {
                var o = t.doc;
                t.display.shift = !1, r || (r = o.sel);
                var a = t.state.pasteIncoming || "paste" == i,
                    s = is(e),
                    l = null;
                if (a && r.ranges.length > 1)
                    if (tl && tl.text.join("\n") == e) {
                        if (r.ranges.length % tl.text.length == 0) {
                            l = [];
                            for (var c = 0; c < tl.text.length; c++) l.push(o.splitLines(tl.text[c]))
                        }
                    } else s.length == r.ranges.length && t.options.pasteLinesPerSelection && (l = g(s, function(t) {
                        return [t]
                    }));
                for (var u, f = r.ranges.length - 1; f >= 0; f--) {
                    var d = r.ranges[f],
                        h = d.from(),
                        p = d.to();
                    d.empty() && (n && n > 0 ? h = D(h.line, h.ch - n) : t.state.overwrite && !a ? p = D(p.line, Math.min(T(o, p.line).text.length, p.ch + m(s).length)) : tl && tl.lineWise && tl.text.join("\n") == e && (h = p = D(h.line, 0))), u = t.curOp.updateInput;
                    var v = {
                        from: h,
                        to: p,
                        text: l ? l[f % l.length] : s,
                        origin: i || (a ? "paste" : t.state.cutIncoming ? "cut" : "+input")
                    };
                    Ai(t.doc, v), we(t, "inputRead", t, v)
                }
                e && !a && ta(t, e), qn(t), t.curOp.updateInput = u, t.curOp.typing = !0, t.state.pasteIncoming = t.state.cutIncoming = !1
            }

            function Qo(t, e) {
                var n = t.clipboardData && t.clipboardData.getData("Text");
                if (n) return t.preventDefault(), e.isReadOnly() || e.options.disableInput || pr(e, function() {
                    return Zo(e, n, 0, null, "paste")
                }), !0
            }

            function ta(t, e) {
                if (t.options.electricChars && t.options.smartIndent)
                    for (var n = t.doc.sel, r = n.ranges.length - 1; r >= 0; r--) {
                        var i = n.ranges[r];
                        if (!(i.head.ch > 100 || r && n.ranges[r - 1].head.line == i.head.line)) {
                            var o = t.getModeAt(i.head),
                                a = !1;
                            if (o.electricChars) {
                                for (var s = 0; s < o.electricChars.length; s++)
                                    if (e.indexOf(o.electricChars.charAt(s)) > -1) {
                                        a = Jo(t, i.head.line, "smart");
                                        break
                                    }
                            } else o.electricInput && o.electricInput.test(T(t.doc, i.head.line).text.slice(0, i.head.ch)) && (a = Jo(t, i.head.line, "smart"));
                            a && we(t, "electricInput", t, i.head.line)
                        }
                    }
            }

            function ea(t) {
                for (var e = [], n = [], r = 0; r < t.doc.sel.ranges.length; r++) {
                    var i = t.doc.sel.ranges[r].head.line,
                        o = {
                            anchor: D(i, 0),
                            head: D(i + 1, 0)
                        };
                    n.push(o), e.push(t.getRange(o.anchor, o.head))
                }
                return {
                    text: e,
                    ranges: n
                }
            }

            function na(t, e) {
                t.setAttribute("autocorrect", "off"), t.setAttribute("autocapitalize", "off"), t.setAttribute("spellcheck", !!e)
            }

            function ra() {
                var t = r("textarea", null, null, "position: absolute; bottom: -1em; padding: 0; width: 1px; height: 1em; outline: none"),
                    e = r("div", [t], null, "overflow: hidden; position: relative; width: 3px; height: 0px;");
                return wa ? t.style.width = "1000px" : t.setAttribute("wrap", "off"), Ma && (t.style.border = "1px solid black"), na(t), e
            }

            function ia(t, e, n, r, i) {
                function o() {
                    var r = e.line + n;
                    return !(r < t.first || r >= t.first + t.size) && (e = new D(r, e.ch, e.sticky), c = T(t, r))
                }

                function a(r) {
                    var a;
                    if (null == (a = i ? vo(t.cm, c, e, n) : mo(c, e, n))) {
                        if (r || !o()) return !1;
                        e = go(i, t.cm, c, e.line, n)
                    } else e = a;
                    return !0
                }
                var s = e,
                    l = n,
                    c = T(t, e.line);
                if ("char" == r) a();
                else if ("column" == r) a(!0);
                else if ("word" == r || "group" == r)
                    for (var u = null, f = "group" == r, d = t.cm && t.cm.getHelper(e, "wordChars"), h = !0; !(n < 0) || a(!h); h = !1) {
                        var p = c.text.charAt(e.ch) || "\n",
                            m = w(p, d) ? "w" : f && "\n" == p ? "n" : !f || /\s/.test(p) ? null : "p";
                        if (!f || h || m || (m = "s"), u && u != m) {
                            n < 0 && (n = 1, a(), e.sticky = "after");
                            break
                        }
                        if (m && (u = m), n > 0 && !a(!h)) break
                    }
                var g = Ti(t, e, s, l, !0);
                return R(s, g) && (g.hitSide = !0), g
            }

            function oa(t, e, n, r) {
                var i, o = t.doc,
                    a = e.left;
                if ("page" == r) {
                    var s = Math.min(t.display.wrapper.clientHeight, window.innerHeight || document.documentElement.clientHeight),
                        l = Math.max(s - .5 * xn(t.display), 3);
                    i = (n > 0 ? e.bottom : e.top) + n * l
                } else "line" == r && (i = n > 0 ? e.bottom + 3 : e.top - 3);
                for (var c; c = hn(t, a, i), c.outside;) {
                    if (n < 0 ? i <= 0 : i >= o.height) {
                        c.hitSide = !0;
                        break
                    }
                    i += 5 * n
                }
                return c
            }

            function aa(t, e) {
                var n = $e(t, e.line);
                if (!n || n.hidden) return null;
                var r = T(t.doc, e.line),
                    i = Ve(n, r, e.line),
                    o = _t(r, t.doc.direction),
                    a = "left";
                if (o) {
                    a = kt(o, e.ch) % 2 ? "right" : "left"
                }
                var s = Je(i.map, e.ch, a);
                return s.offset = "right" == s.collapse ? s.end : s.start, s
            }

            function sa(t) {
                for (var e = t; e; e = e.parentNode)
                    if (/CodeMirror-gutter-wrapper/.test(e.className)) return !0;
                return !1
            }

            function la(t, e) {
                return e && (t.bad = !0), t
            }

            function ca(t, e, n, r, i) {
                function o(t) {
                    return function(e) {
                        return e.id == t
                    }
                }

                function a() {
                    u && (c += f, d && (c += f), u = d = !1)
                }

                function s(t) {
                    t && (a(), c += t)
                }

                function l(e) {
                    if (1 == e.nodeType) {
                        var n = e.getAttribute("cm-text");
                        if (n) return void s(n);
                        var c, h = e.getAttribute("cm-marker");
                        if (h) {
                            var p = t.findMarks(D(r, 0), D(i + 1, 0), o(+h));
                            return void(p.length && (c = p[0].find(0)) && s(M(t.doc, c.from, c.to).join(f)))
                        }
                        if ("false" == e.getAttribute("contenteditable")) return;
                        var m = /^(pre|div|p|li|table|br)$/i.test(e.nodeName);
                        if (!/^br$/i.test(e.nodeName) && 0 == e.textContent.length) return;
                        m && a();
                        for (var g = 0; g < e.childNodes.length; g++) l(e.childNodes[g]);
                        /^(pre|p)$/i.test(e.nodeName) && (d = !0), m && (u = !0)
                    } else 3 == e.nodeType && s(e.nodeValue.replace(/\u200b/g, "").replace(/\u00a0/g, " "))
                }
                for (var c = "", u = !1, f = t.doc.lineSeparator(), d = !1; l(e), e != n;) e = e.nextSibling, d = !1;
                return c
            }

            function ua(t, e, n) {
                var r;
                if (e == t.display.lineDiv) {
                    if (!(r = t.display.lineDiv.childNodes[n])) return la(t.clipPos(D(t.display.viewTo - 1)), !0);
                    e = null, n = 0
                } else
                    for (r = e;; r = r.parentNode) {
                        if (!r || r == t.display.lineDiv) return null;
                        if (r.parentNode && r.parentNode == t.display.lineDiv) break
                    }
                for (var i = 0; i < t.display.view.length; i++) {
                    var o = t.display.view[i];
                    if (o.node == r) return fa(o, e, n)
                }
            }

            function fa(t, e, n) {
                function r(e, n, r) {
                    for (var i = -1; i < (f ? f.length : 0); i++)
                        for (var o = i < 0 ? u.map : f[i], a = 0; a < o.length; a += 3) {
                            var s = o[a + 2];
                            if (s == e || s == n) {
                                var l = A(i < 0 ? t.line : t.rest[i]),
                                    c = o[a] + r;
                                return (r < 0 || s != e) && (c = o[a + (r ? 1 : 0)]), D(l, c)
                            }
                        }
                }
                var i = t.text.firstChild,
                    a = !1;
                if (!e || !o(i, e)) return la(D(A(t.line), 0), !0);
                if (e == i && (a = !0, e = i.childNodes[n], n = 0, !e)) {
                    var s = t.rest ? m(t.rest) : t.line;
                    return la(D(A(s), s.text.length), a)
                }
                var l = 3 == e.nodeType ? e : null,
                    c = e;
                for (l || 1 != e.childNodes.length || 3 != e.firstChild.nodeType || (l = e.firstChild, n && (n = l.nodeValue.length)); c.parentNode != i;) c = c.parentNode;
                var u = t.measure,
                    f = u.maps,
                    d = r(l, c, n);
                if (d) return la(d, a);
                for (var h = c.nextSibling, p = l ? l.nodeValue.length - n : 0; h; h = h.nextSibling) {
                    if (d = r(h, h.firstChild, 0)) return la(D(d.line, d.ch - p), a);
                    p += h.textContent.length
                }
                for (var g = c.previousSibling, v = n; g; g = g.previousSibling) {
                    if (d = r(g, g.firstChild, -1)) return la(D(d.line, d.ch + v), a);
                    v += g.textContent.length
                }
            }

            function da(t, e) {
                function n() {
                    t.value = l.getValue()
                }
                if (e = e ? u(e) : {}, e.value = t.value, !e.tabindex && t.tabIndex && (e.tabindex = t.tabIndex), !e.placeholder && t.placeholder && (e.placeholder = t.placeholder), null == e.autofocus) {
                    var r = a();
                    e.autofocus = r == t || null != t.getAttribute("autofocus") && r == document.body
                }
                var i;
                if (t.form && (ns(t.form, "submit", n), !e.leaveSubmitMethodAlone)) {
                    var o = t.form;
                    i = o.submit;
                    try {
                        var s = o.submit = function() {
                            n(), o.submit = i, o.submit(), o.submit = s
                        }
                    } catch (t) {}
                }
                e.finishInit = function(e) {
                    e.save = n, e.getTextArea = function() {
                        return t
                    }, e.toTextArea = function() {
                        e.toTextArea = isNaN, n(), t.parentNode.removeChild(e.getWrapperElement()), t.style.display = "", t.form && (Et(t.form, "submit", n), "function" == typeof t.form.submit && (t.form.submit = i))
                    }
                }, t.style.display = "none";
                var l = qo(function(e) {
                    return t.parentNode.insertBefore(e, t.nextSibling)
                }, e);
                return l
            }
            var ha = navigator.userAgent,
                pa = navigator.platform,
                ma = /gecko\/\d/i.test(ha),
                ga = /MSIE \d/.test(ha),
                va = /Trident\/(?:[7-9]|\d{2,})\..*rv:(\d+)/.exec(ha),
                ya = /Edge\/(\d+)/.exec(ha),
                ba = ga || va || ya,
                xa = ba && (ga ? document.documentMode || 6 : +(ya || va)[1]),
                wa = !ya && /WebKit\//.test(ha),
                Ca = wa && /Qt\/\d+\.\d+/.test(ha),
                ka = !ya && /Chrome\//.test(ha),
                _a = /Opera\//.test(ha),
                Sa = /Apple Computer/.test(navigator.vendor),
                Ea = /Mac OS X 1\d\D([8-9]|\d\d)\D/.test(ha),
                Ta = /PhantomJS/.test(ha),
                Ma = !ya && /AppleWebKit/.test(ha) && /Mobile\/\w+/.test(ha),
                Oa = /Android/.test(ha),
                La = Ma || Oa || /webOS|BlackBerry|Opera Mini|Opera Mobi|IEMobile/i.test(ha),
                Aa = Ma || /Mac/.test(pa),
                Na = /\bCrOS\b/.test(ha),
                Pa = /win/i.test(pa),
                Ia = _a && ha.match(/Version\/(\d*\.\d*)/);
            Ia && (Ia = Number(Ia[1])), Ia && Ia >= 15 && (_a = !1, wa = !0);
            var Da, Fa = Aa && (Ca || _a && (null == Ia || Ia < 12.11)),
                Ra = ma || ba && xa >= 9,
                za = function(e, n) {
                    var r = e.className,
                        i = t(n).exec(r);
                    if (i) {
                        var o = r.slice(i.index + i[0].length);
                        e.className = r.slice(0, i.index) + (o ? i[1] + o : "")
                    }
                };
            Da = document.createRange ? function(t, e, n, r) {
                var i = document.createRange();
                return i.setEnd(r || t, n), i.setStart(t, e), i
            } : function(t, e, n) {
                var r = document.body.createTextRange();
                try {
                    r.moveToElementText(t.parentNode)
                } catch (t) {
                    return r
                }
                return r.collapse(!0), r.moveEnd("character", n), r.moveStart("character", e), r
            };
            var ja = function(t) {
                t.select()
            };
            Ma ? ja = function(t) {
                t.selectionStart = 0, t.selectionEnd = t.value.length
            } : ba && (ja = function(t) {
                try {
                    t.select()
                } catch (t) {}
            });
            var Ba = function() {
                this.id = null
            };
            Ba.prototype.set = function(t, e) {
                clearTimeout(this.id), this.id = setTimeout(e, t)
            };
            var Ha, Wa, Ua = 30,
                Va = {
                    toString: function() {
                        return "CodeMirror.Pass"
                    }
                },
                Ga = {
                    scroll: !1
                },
                Ka = {
                    origin: "*mouse"
                },
                $a = {
                    origin: "+move"
                },
                qa = [""],
                Ya = /[\u00df\u0587\u0590-\u05f4\u0600-\u06ff\u3040-\u309f\u30a0-\u30ff\u3400-\u4db5\u4e00-\u9fcc\uac00-\ud7af]/,
                Ja = /[\u0300-\u036f\u0483-\u0489\u0591-\u05bd\u05bf\u05c1\u05c2\u05c4\u05c5\u05c7\u0610-\u061a\u064b-\u065e\u0670\u06d6-\u06dc\u06de-\u06e4\u06e7\u06e8\u06ea-\u06ed\u0711\u0730-\u074a\u07a6-\u07b0\u07eb-\u07f3\u0816-\u0819\u081b-\u0823\u0825-\u0827\u0829-\u082d\u0900-\u0902\u093c\u0941-\u0948\u094d\u0951-\u0955\u0962\u0963\u0981\u09bc\u09be\u09c1-\u09c4\u09cd\u09d7\u09e2\u09e3\u0a01\u0a02\u0a3c\u0a41\u0a42\u0a47\u0a48\u0a4b-\u0a4d\u0a51\u0a70\u0a71\u0a75\u0a81\u0a82\u0abc\u0ac1-\u0ac5\u0ac7\u0ac8\u0acd\u0ae2\u0ae3\u0b01\u0b3c\u0b3e\u0b3f\u0b41-\u0b44\u0b4d\u0b56\u0b57\u0b62\u0b63\u0b82\u0bbe\u0bc0\u0bcd\u0bd7\u0c3e-\u0c40\u0c46-\u0c48\u0c4a-\u0c4d\u0c55\u0c56\u0c62\u0c63\u0cbc\u0cbf\u0cc2\u0cc6\u0ccc\u0ccd\u0cd5\u0cd6\u0ce2\u0ce3\u0d3e\u0d41-\u0d44\u0d4d\u0d57\u0d62\u0d63\u0dca\u0dcf\u0dd2-\u0dd4\u0dd6\u0ddf\u0e31\u0e34-\u0e3a\u0e47-\u0e4e\u0eb1\u0eb4-\u0eb9\u0ebb\u0ebc\u0ec8-\u0ecd\u0f18\u0f19\u0f35\u0f37\u0f39\u0f71-\u0f7e\u0f80-\u0f84\u0f86\u0f87\u0f90-\u0f97\u0f99-\u0fbc\u0fc6\u102d-\u1030\u1032-\u1037\u1039\u103a\u103d\u103e\u1058\u1059\u105e-\u1060\u1071-\u1074\u1082\u1085\u1086\u108d\u109d\u135f\u1712-\u1714\u1732-\u1734\u1752\u1753\u1772\u1773\u17b7-\u17bd\u17c6\u17c9-\u17d3\u17dd\u180b-\u180d\u18a9\u1920-\u1922\u1927\u1928\u1932\u1939-\u193b\u1a17\u1a18\u1a56\u1a58-\u1a5e\u1a60\u1a62\u1a65-\u1a6c\u1a73-\u1a7c\u1a7f\u1b00-\u1b03\u1b34\u1b36-\u1b3a\u1b3c\u1b42\u1b6b-\u1b73\u1b80\u1b81\u1ba2-\u1ba5\u1ba8\u1ba9\u1c2c-\u1c33\u1c36\u1c37\u1cd0-\u1cd2\u1cd4-\u1ce0\u1ce2-\u1ce8\u1ced\u1dc0-\u1de6\u1dfd-\u1dff\u200c\u200d\u20d0-\u20f0\u2cef-\u2cf1\u2de0-\u2dff\u302a-\u302f\u3099\u309a\ua66f-\ua672\ua67c\ua67d\ua6f0\ua6f1\ua802\ua806\ua80b\ua825\ua826\ua8c4\ua8e0-\ua8f1\ua926-\ua92d\ua947-\ua951\ua980-\ua982\ua9b3\ua9b6-\ua9b9\ua9bc\uaa29-\uaa2e\uaa31\uaa32\uaa35\uaa36\uaa43\uaa4c\uaab0\uaab2-\uaab4\uaab7\uaab8\uaabe\uaabf\uaac1\uabe5\uabe8\uabed\udc00-\udfff\ufb1e\ufe00-\ufe0f\ufe20-\ufe26\uff9e\uff9f]/,
                Xa = !1,
                Za = !1,
                Qa = null,
                ts = function() {
                    function t(t) {
                        return t <= 247 ? n.charAt(t) : 1424 <= t && t <= 1524 ? "R" : 1536 <= t && t <= 1785 ? r.charAt(t - 1536) : 1774 <= t && t <= 2220 ? "r" : 8192 <= t && t <= 8203 ? "w" : 8204 == t ? "b" : "L"
                    }

                    function e(t, e, n) {
                        this.level = t, this.from = e, this.to = n
                    }
                    var n = "bbbbbbbbbtstwsbbbbbbbbbbbbbbssstwNN%%%NNNNNN,N,N1111111111NNNNNNNLLLLLLLLLLLLLLLLLLLLLLLLLLNNNNNNLLLLLLLLLLLLLLLLLLLLLLLLLLNNNNbbbbbbsbbbbbbbbbbbbbbbbbbbbbbbbbb,N%%%%NNNNLNNNNN%%11NLNNN1LNNNNNLLLLLLLLLLLLLLLLLLLLLLLNLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLN",
                        r = "nnnnnnNNr%%r,rNNmmmmmmmmmmmrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrmmmmmmmmmmmmmmmmmmmmmnnnnnnnnnn%nnrrrmrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrmmmmmmmnNmmmmmmrrmmNmmmmrr1111111111",
                        i = /[\u0590-\u05f4\u0600-\u06ff\u0700-\u08ac]/,
                        o = /[stwN]/,
                        a = /[LRr]/,
                        s = /[Lb1n]/,
                        l = /[1n]/;
                    return function(n, r) {
                        var c = "ltr" == r ? "L" : "R";
                        if (0 == n.length || "ltr" == r && !i.test(n)) return !1;
                        for (var u = n.length, f = [], d = 0; d < u; ++d) f.push(t(n.charCodeAt(d)));
                        for (var h = 0, p = c; h < u; ++h) {
                            var g = f[h];
                            "m" == g ? f[h] = p : p = g
                        }
                        for (var v = 0, y = c; v < u; ++v) {
                            var b = f[v];
                            "1" == b && "r" == y ? f[v] = "n" : a.test(b) && (y = b, "r" == b && (f[v] = "R"))
                        }
                        for (var x = 1, w = f[0]; x < u - 1; ++x) {
                            var C = f[x];
                            "+" == C && "1" == w && "1" == f[x + 1] ? f[x] = "1" : "," != C || w != f[x + 1] || "1" != w && "n" != w || (f[x] = w), w = C
                        }
                        for (var k = 0; k < u; ++k) {
                            var _ = f[k];
                            if ("," == _) f[k] = "N";
                            else if ("%" == _) {
                                var S = void 0;
                                for (S = k + 1; S < u && "%" == f[S]; ++S);
                                for (var E = k && "!" == f[k - 1] || S < u && "1" == f[S] ? "1" : "N", T = k; T < S; ++T) f[T] = E;
                                k = S - 1
                            }
                        }
                        for (var M = 0, O = c; M < u; ++M) {
                            var L = f[M];
                            "L" == O && "1" == L ? f[M] = "L" : a.test(L) && (O = L)
                        }
                        for (var A = 0; A < u; ++A)
                            if (o.test(f[A])) {
                                var N = void 0;
                                for (N = A + 1; N < u && o.test(f[N]); ++N);
                                for (var P = "L" == (A ? f[A - 1] : c), I = "L" == (N < u ? f[N] : c), D = P == I ? P ? "L" : "R" : c, F = A; F < N; ++F) f[F] = D;
                                A = N - 1
                            }
                        for (var R, z = [], j = 0; j < u;)
                            if (s.test(f[j])) {
                                var B = j;
                                for (++j; j < u && s.test(f[j]); ++j);
                                z.push(new e(0, B, j))
                            } else {
                                var H = j,
                                    W = z.length;
                                for (++j; j < u && "L" != f[j]; ++j);
                                for (var U = H; U < j;)
                                    if (l.test(f[U])) {
                                        H < U && z.splice(W, 0, new e(1, H, U));
                                        var V = U;
                                        for (++U; U < j && l.test(f[U]); ++U);
                                        z.splice(W, 0, new e(2, V, U)), H = U
                                    } else ++U;
                                H < j && z.splice(W, 0, new e(1, H, j))
                            }
                        return "ltr" == r && (1 == z[0].level && (R = n.match(/^\s+/)) && (z[0].from = R[0].length, z.unshift(new e(0, 0, R[0].length))), 1 == m(z).level && (R = n.match(/\s+$/)) && (m(z).to -= R[0].length, z.push(new e(0, u - R[0].length, u)))), "rtl" == r ? z.reverse() : z
                    }
                }(),
                es = [],
                ns = function(t, e, n) {
                    if (t.addEventListener) t.addEventListener(e, n, !1);
                    else if (t.attachEvent) t.attachEvent("on" + e, n);
                    else {
                        var r = t._handlers || (t._handlers = {});
                        r[e] = (r[e] || es).concat(n)
                    }
                },
                rs = function() {
                    if (ba && xa < 9) return !1;
                    var t = r("div");
                    return "draggable" in t || "dragDrop" in t
                }(),
                is = 3 != "\n\nb".split(/\n/).length ? function(t) {
                    for (var e = 0, n = [], r = t.length; e <= r;) {
                        var i = t.indexOf("\n", e); - 1 == i && (i = t.length);
                        var o = t.slice(e, "\r" == t.charAt(i - 1) ? i - 1 : i),
                            a = o.indexOf("\r"); - 1 != a ? (n.push(o.slice(0, a)), e += a + 1) : (n.push(o), e = i + 1)
                    }
                    return n
                } : function(t) {
                    return t.split(/\r\n?|\n/)
                },
                os = window.getSelection ? function(t) {
                    try {
                        return t.selectionStart != t.selectionEnd
                    } catch (t) {
                        return !1
                    }
                } : function(t) {
                    var e;
                    try {
                        e = t.ownerDocument.selection.createRange()
                    } catch (t) {}
                    return !(!e || e.parentElement() != t) && 0 != e.compareEndPoints("StartToEnd", e)
                },
                as = function() {
                    var t = r("div");
                    return "oncopy" in t || (t.setAttribute("oncopy", "return;"), "function" == typeof t.oncopy)
                }(),
                ss = null,
                ls = {},
                cs = {},
                us = {},
                fs = function(t, e, n) {
                    this.pos = this.start = 0, this.string = t, this.tabSize = e || 8, this.lastColumnPos = this.lastColumnValue = 0, this.lineStart = 0, this.lineOracle = n
                };
            fs.prototype.eol = function() {
                return this.pos >= this.string.length
            }, fs.prototype.sol = function() {
                return this.pos == this.lineStart
            }, fs.prototype.peek = function() {
                return this.string.charAt(this.pos) || void 0
            }, fs.prototype.next = function() {
                if (this.pos < this.string.length) return this.string.charAt(this.pos++)
            }, fs.prototype.eat = function(t) {
                var e = this.string.charAt(this.pos);
                if ("string" == typeof t ? e == t : e && (t.test ? t.test(e) : t(e))) return ++this.pos, e
            }, fs.prototype.eatWhile = function(t) {
                for (var e = this.pos; this.eat(t););
                return this.pos > e
            }, fs.prototype.eatSpace = function() {
                for (var t = this, e = this.pos;
                    /[\s\u00a0]/.test(this.string.charAt(this.pos));) ++t.pos;
                return this.pos > e
            }, fs.prototype.skipToEnd = function() {
                this.pos = this.string.length
            }, fs.prototype.skipTo = function(t) {
                var e = this.string.indexOf(t, this.pos);
                if (e > -1) return this.pos = e, !0
            }, fs.prototype.backUp = function(t) {
                this.pos -= t
            }, fs.prototype.column = function() {
                return this.lastColumnPos < this.start && (this.lastColumnValue = f(this.string, this.start, this.tabSize, this.lastColumnPos, this.lastColumnValue), this.lastColumnPos = this.start), this.lastColumnValue - (this.lineStart ? f(this.string, this.lineStart, this.tabSize) : 0)
            }, fs.prototype.indentation = function() {
                return f(this.string, null, this.tabSize) - (this.lineStart ? f(this.string, this.lineStart, this.tabSize) : 0)
            }, fs.prototype.match = function(t, e, n) {
                if ("string" != typeof t) {
                    var r = this.string.slice(this.pos).match(t);
                    return r && r.index > 0 ? null : (r && !1 !== e && (this.pos += r[0].length), r)
                }
                var i = function(t) {
                    return n ? t.toLowerCase() : t
                };
                if (i(this.string.substr(this.pos, t.length)) == i(t)) return !1 !== e && (this.pos += t.length), !0
            }, fs.prototype.current = function() {
                return this.string.slice(this.start, this.pos)
            }, fs.prototype.hideFirstChars = function(t, e) {
                this.lineStart += t;
                try {
                    return e()
                } finally {
                    this.lineStart -= t
                }
            }, fs.prototype.lookAhead = function(t) {
                var e = this.lineOracle;
                return e && e.lookAhead(t)
            }, fs.prototype.baseToken = function() {
                var t = this.lineOracle;
                return t && t.baseToken(this.pos)
            };
            var ds = function(t, e) {
                    this.state = t, this.lookAhead = e
                },
                hs = function(t, e, n, r) {
                    this.state = e, this.doc = t, this.line = n, this.maxLookAhead = r || 0, this.baseTokens = null, this.baseTokenPos = 1
                };
            hs.prototype.lookAhead = function(t) {
                var e = this.doc.getLine(this.line + t);
                return null != e && t > this.maxLookAhead && (this.maxLookAhead = t), e
            }, hs.prototype.baseToken = function(t) {
                var e = this;
                if (!this.baseTokens) return null;
                for (; this.baseTokens[this.baseTokenPos] <= t;) e.baseTokenPos += 2;
                var n = this.baseTokens[this.baseTokenPos + 1];
                return {
                    type: n && n.replace(/( |^)overlay .*/, ""),
                    size: this.baseTokens[this.baseTokenPos] - t
                }
            }, hs.prototype.nextLine = function() {
                this.line++, this.maxLookAhead > 0 && this.maxLookAhead--
            }, hs.fromSaved = function(t, e, n) {
                return e instanceof ds ? new hs(t, Kt(t.mode, e.state), n, e.lookAhead) : new hs(t, Kt(t.mode, e), n)
            }, hs.prototype.save = function(t) {
                var e = !1 !== t ? Kt(this.doc.mode, this.state) : this.state;
                return this.maxLookAhead > 0 ? new ds(e, this.maxLookAhead) : e
            };
            var ps = function(t, e, n) {
                    this.start = t.start, this.end = t.pos, this.string = t.current(), this.type = e || null, this.state = n
                },
                ms = function(t, e, n) {
                    this.text = t, rt(this, e), this.height = n ? n(this) : 1
                };
            ms.prototype.lineNo = function() {
                return A(this)
            }, At(ms);
            var gs, vs = {},
                ys = {},
                bs = null,
                xs = null,
                ws = {
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0
                },
                Cs = function(t, e, n) {
                    this.cm = n;
                    var i = this.vert = r("div", [r("div", null, null, "min-width: 1px")], "CodeMirror-vscrollbar"),
                        o = this.horiz = r("div", [r("div", null, null, "height: 100%; min-height: 1px")], "CodeMirror-hscrollbar");
                    i.tabIndex = o.tabIndex = -1, t(i), t(o), ns(i, "scroll", function() {
                        i.clientHeight && e(i.scrollTop, "vertical")
                    }), ns(o, "scroll", function() {
                        o.clientWidth && e(o.scrollLeft, "horizontal")
                    }), this.checkedZeroWidth = !1, ba && xa < 8 && (this.horiz.style.minHeight = this.vert.style.minWidth = "18px")
                };
            Cs.prototype.update = function(t) {
                var e = t.scrollWidth > t.clientWidth + 1,
                    n = t.scrollHeight > t.clientHeight + 1,
                    r = t.nativeBarWidth;
                if (n) {
                    this.vert.style.display = "block", this.vert.style.bottom = e ? r + "px" : "0";
                    var i = t.viewHeight - (e ? r : 0);
                    this.vert.firstChild.style.height = Math.max(0, t.scrollHeight - t.clientHeight + i) + "px"
                } else this.vert.style.display = "", this.vert.firstChild.style.height = "0";
                if (e) {
                    this.horiz.style.display = "block", this.horiz.style.right = n ? r + "px" : "0", this.horiz.style.left = t.barLeft + "px";
                    var o = t.viewWidth - t.barLeft - (n ? r : 0);
                    this.horiz.firstChild.style.width = Math.max(0, t.scrollWidth - t.clientWidth + o) + "px"
                } else this.horiz.style.display = "", this.horiz.firstChild.style.width = "0";
                return !this.checkedZeroWidth && t.clientHeight > 0 && (0 == r && this.zeroWidthHack(), this.checkedZeroWidth = !0), {
                    right: n ? r : 0,
                    bottom: e ? r : 0
                }
            }, Cs.prototype.setScrollLeft = function(t) {
                this.horiz.scrollLeft != t && (this.horiz.scrollLeft = t), this.disableHoriz && this.enableZeroWidthBar(this.horiz, this.disableHoriz, "horiz")
            }, Cs.prototype.setScrollTop = function(t) {
                this.vert.scrollTop != t && (this.vert.scrollTop = t), this.disableVert && this.enableZeroWidthBar(this.vert, this.disableVert, "vert")
            }, Cs.prototype.zeroWidthHack = function() {
                var t = Aa && !Ea ? "12px" : "18px";
                this.horiz.style.height = this.vert.style.width = t, this.horiz.style.pointerEvents = this.vert.style.pointerEvents = "none", this.disableHoriz = new Ba, this.disableVert = new Ba
            }, Cs.prototype.enableZeroWidthBar = function(t, e, n) {
                function r() {
                    var i = t.getBoundingClientRect();
                    ("vert" == n ? document.elementFromPoint(i.right - 1, (i.top + i.bottom) / 2) : document.elementFromPoint((i.right + i.left) / 2, i.bottom - 1)) != t ? t.style.pointerEvents = "none" : e.set(1e3, r)
                }
                t.style.pointerEvents = "auto", e.set(1e3, r)
            }, Cs.prototype.clear = function() {
                var t = this.horiz.parentNode;
                t.removeChild(this.horiz), t.removeChild(this.vert)
            };
            var ks = function() {};
            ks.prototype.update = function() {
                return {
                    bottom: 0,
                    right: 0
                }
            }, ks.prototype.setScrollLeft = function() {}, ks.prototype.setScrollTop = function() {}, ks.prototype.clear = function() {};
            var _s = {
                    native: Cs,
                    null: ks
                },
                Ss = 0,
                Es = function(t, e, n) {
                    var r = t.display;
                    this.viewport = e, this.visible = Bn(r, t.doc, e), this.editorIsHidden = !r.wrapper.offsetWidth, this.wrapperHeight = r.wrapper.clientHeight, this.wrapperWidth = r.wrapper.clientWidth, this.oldDisplayWidth = He(t), this.force = n, this.dims = Cn(t), this.events = []
                };
            Es.prototype.signal = function(t, e) {
                Lt(t, e) && this.events.push(arguments)
            }, Es.prototype.finish = function() {
                for (var t = this, e = 0; e < this.events.length; e++) Tt.apply(null, t.events[e])
            };
            var Ts = 0,
                Ms = null;
            ba ? Ms = -.53 : ma ? Ms = 15 : ka ? Ms = -.7 : Sa && (Ms = -1 / 3);
            var Os = function(t, e) {
                this.ranges = t, this.primIndex = e
            };
            Os.prototype.primary = function() {
                return this.ranges[this.primIndex]
            }, Os.prototype.equals = function(t) {
                var e = this;
                if (t == this) return !0;
                if (t.primIndex != this.primIndex || t.ranges.length != this.ranges.length) return !1;
                for (var n = 0; n < this.ranges.length; n++) {
                    var r = e.ranges[n],
                        i = t.ranges[n];
                    if (!R(r.anchor, i.anchor) || !R(r.head, i.head)) return !1
                }
                return !0
            }, Os.prototype.deepCopy = function() {
                for (var t = this, e = [], n = 0; n < this.ranges.length; n++) e[n] = new Ls(z(t.ranges[n].anchor), z(t.ranges[n].head));
                return new Os(e, this.primIndex)
            }, Os.prototype.somethingSelected = function() {
                for (var t = this, e = 0; e < this.ranges.length; e++)
                    if (!t.ranges[e].empty()) return !0;
                return !1
            }, Os.prototype.contains = function(t, e) {
                var n = this;
                e || (e = t);
                for (var r = 0; r < this.ranges.length; r++) {
                    var i = n.ranges[r];
                    if (F(e, i.from()) >= 0 && F(t, i.to()) <= 0) return r
                }
                return -1
            };
            var Ls = function(t, e) {
                this.anchor = t, this.head = e
            };
            Ls.prototype.from = function() {
                return B(this.anchor, this.head)
            }, Ls.prototype.to = function() {
                return j(this.anchor, this.head)
            }, Ls.prototype.empty = function() {
                return this.head.line == this.anchor.line && this.head.ch == this.anchor.ch
            }, Wi.prototype = {
                chunkSize: function() {
                    return this.lines.length
                },
                removeInner: function(t, e) {
                    for (var n = this, r = t, i = t + e; r < i; ++r) {
                        var o = n.lines[r];
                        n.height -= o.height, se(o), we(o, "delete")
                    }
                    this.lines.splice(t, e)
                },
                collapse: function(t) {
                    t.push.apply(t, this.lines)
                },
                insertInner: function(t, e, n) {
                    var r = this;
                    this.height += n, this.lines = this.lines.slice(0, t).concat(e).concat(this.lines.slice(t));
                    for (var i = 0; i < e.length; ++i) e[i].parent = r
                },
                iterN: function(t, e, n) {
                    for (var r = this, i = t + e; t < i; ++t)
                        if (n(r.lines[t])) return !0
                }
            }, Ui.prototype = {
                chunkSize: function() {
                    return this.size
                },
                removeInner: function(t, e) {
                    var n = this;
                    this.size -= e;
                    for (var r = 0; r < this.children.length; ++r) {
                        var i = n.children[r],
                            o = i.chunkSize();
                        if (t < o) {
                            var a = Math.min(e, o - t),
                                s = i.height;
                            if (i.removeInner(t, a), n.height -= s - i.height, o == a && (n.children.splice(r--, 1), i.parent = null), 0 == (e -= a)) break;
                            t = 0
                        } else t -= o
                    }
                    if (this.size - e < 25 && (this.children.length > 1 || !(this.children[0] instanceof Wi))) {
                        var l = [];
                        this.collapse(l), this.children = [new Wi(l)], this.children[0].parent = this
                    }
                },
                collapse: function(t) {
                    for (var e = this, n = 0; n < this.children.length; ++n) e.children[n].collapse(t)
                },
                insertInner: function(t, e, n) {
                    var r = this;
                    this.size += e.length, this.height += n;
                    for (var i = 0; i < this.children.length; ++i) {
                        var o = r.children[i],
                            a = o.chunkSize();
                        if (t <= a) {
                            if (o.insertInner(t, e, n), o.lines && o.lines.length > 50) {
                                for (var s = o.lines.length % 25 + 25, l = s; l < o.lines.length;) {
                                    var c = new Wi(o.lines.slice(l, l += 25));
                                    o.height -= c.height, r.children.splice(++i, 0, c), c.parent = r
                                }
                                o.lines = o.lines.slice(0, s), r.maybeSpill()
                            }
                            break
                        }
                        t -= a
                    }
                },
                maybeSpill: function() {
                    if (!(this.children.length <= 10)) {
                        var t = this;
                        do {
                            var e = t.children.splice(t.children.length - 5, 5),
                                n = new Ui(e);
                            if (t.parent) {
                                t.size -= n.size, t.height -= n.height;
                                var r = d(t.parent.children, t);
                                t.parent.children.splice(r + 1, 0, n)
                            } else {
                                var i = new Ui(t.children);
                                i.parent = t, t.children = [i, n], t = i
                            }
                            n.parent = t.parent
                        } while (t.children.length > 10);
                        t.parent.maybeSpill()
                    }
                },
                iterN: function(t, e, n) {
                    for (var r = this, i = 0; i < this.children.length; ++i) {
                        var o = r.children[i],
                            a = o.chunkSize();
                        if (t < a) {
                            var s = Math.min(e, a - t);
                            if (o.iterN(t, s, n)) return !0;
                            if (0 == (e -= s)) break;
                            t = 0
                        } else t -= a
                    }
                }
            };
            var As = function(t, e, n) {
                var r = this;
                if (n)
                    for (var i in n) n.hasOwnProperty(i) && (r[i] = n[i]);
                this.doc = t, this.node = e
            };
            As.prototype.clear = function() {
                var t = this,
                    e = this.doc.cm,
                    n = this.line.widgets,
                    r = this.line,
                    i = A(r);
                if (null != i && n) {
                    for (var o = 0; o < n.length; ++o) n[o] == t && n.splice(o--, 1);
                    n.length || (r.widgets = null);
                    var a = De(this);
                    L(r, Math.max(0, r.height - a)), e && (pr(e, function() {
                        Vi(e, r, -a), br(e, i, "widget")
                    }), we(e, "lineWidgetCleared", e, this, i))
                }
            }, As.prototype.changed = function() {
                var t = this,
                    e = this.height,
                    n = this.doc.cm,
                    r = this.line;
                this.height = null;
                var i = De(this) - e;
                i && (vt(this.doc, r) || L(r, r.height + i), n && pr(n, function() {
                    n.curOp.forceUpdate = !0, Vi(n, r, i), we(n, "lineWidgetChanged", n, t, A(r))
                }))
            }, At(As);
            var Ns = 0,
                Ps = function(t, e) {
                    this.lines = [], this.type = e, this.doc = t, this.id = ++Ns
                };
            Ps.prototype.clear = function() {
                var t = this;
                if (!this.explicitlyCleared) {
                    var e = this.doc.cm,
                        n = e && !e.curOp;
                    if (n && ar(e), Lt(this, "clear")) {
                        var r = this.find();
                        r && we(this, "clear", r.from, r.to)
                    }
                    for (var i = null, o = null, a = 0; a < this.lines.length; ++a) {
                        var s = t.lines[a],
                            l = q(s.markedSpans, t);
                        e && !t.collapsed ? br(e, A(s), "text") : e && (null != l.to && (o = A(s)), null != l.from && (i = A(s))), s.markedSpans = Y(s.markedSpans, l), null == l.from && t.collapsed && !vt(t.doc, s) && e && L(s, xn(e.display))
                    }
                    if (e && this.collapsed && !e.options.lineWrapping)
                        for (var c = 0; c < this.lines.length; ++c) {
                            var u = dt(t.lines[c]),
                                f = xt(u);
                            f > e.display.maxLineLength && (e.display.maxLine = u, e.display.maxLineLength = f, e.display.maxLineChanged = !0)
                        }
                    null != i && e && this.collapsed && yr(e, i, o + 1), this.lines.length = 0, this.explicitlyCleared = !0, this.atomic && this.doc.cantEdit && (this.doc.cantEdit = !1, e && _i(e.doc)), e && we(e, "markerCleared", e, this, i, o), n && sr(e), this.parent && this.parent.clear()
                }
            }, Ps.prototype.find = function(t, e) {
                var n = this;
                null == t && "bookmark" == this.type && (t = 1);
                for (var r, i, o = 0; o < this.lines.length; ++o) {
                    var a = n.lines[o],
                        s = q(a.markedSpans, n);
                    if (null != s.from && (r = D(e ? a : A(a), s.from), -1 == t)) return r;
                    if (null != s.to && (i = D(e ? a : A(a), s.to), 1 == t)) return i
                }
                return r && {
                    from: r,
                    to: i
                }
            }, Ps.prototype.changed = function() {
                var t = this,
                    e = this.find(-1, !0),
                    n = this,
                    r = this.doc.cm;
                e && r && pr(r, function() {
                    var i = e.line,
                        o = A(e.line),
                        a = $e(r, o);
                    if (a && (tn(a), r.curOp.selectionChanged = r.curOp.forceUpdate = !0), r.curOp.updateMaxLine = !0, !vt(n.doc, i) && null != n.height) {
                        var s = n.height;
                        n.height = null;
                        var l = De(n) - s;
                        l && L(i, i.height + l)
                    }
                    we(r, "markerChanged", r, t)
                })
            }, Ps.prototype.attachLine = function(t) {
                if (!this.lines.length && this.doc.cm) {
                    var e = this.doc.cm.curOp;
                    e.maybeHiddenMarkers && -1 != d(e.maybeHiddenMarkers, this) || (e.maybeUnhiddenMarkers || (e.maybeUnhiddenMarkers = [])).push(this)
                }
                this.lines.push(t)
            }, Ps.prototype.detachLine = function(t) {
                if (this.lines.splice(d(this.lines, t), 1), !this.lines.length && this.doc.cm) {
                    var e = this.doc.cm.curOp;
                    (e.maybeHiddenMarkers || (e.maybeHiddenMarkers = [])).push(this)
                }
            }, At(Ps);
            var Is = function(t, e) {
                var n = this;
                this.markers = t, this.primary = e;
                for (var r = 0; r < t.length; ++r) t[r].parent = n
            };
            Is.prototype.clear = function() {
                var t = this;
                if (!this.explicitlyCleared) {
                    this.explicitlyCleared = !0;
                    for (var e = 0; e < this.markers.length; ++e) t.markers[e].clear();
                    we(this, "clear")
                }
            }, Is.prototype.find = function(t, e) {
                return this.primary.find(t, e)
            }, At(Is);
            var Ds = 0,
                Fs = function(t, e, n, r, i) {
                    if (!(this instanceof Fs)) return new Fs(t, e, n, r, i);
                    null == n && (n = 0), Ui.call(this, [new Wi([new ms("", null)])]), this.first = n, this.scrollTop = this.scrollLeft = 0, this.cantEdit = !1, this.cleanGeneration = 1, this.modeFrontier = this.highlightFrontier = n;
                    var o = D(n, 0);
                    this.sel = Hr(o), this.history = new ei(null), this.id = ++Ds, this.modeOption = e, this.lineSep = r, this.direction = "rtl" == i ? "rtl" : "ltr", this.extend = !1, "string" == typeof t && (t = this.splitLines(t)), Jr(this, {
                        from: o,
                        to: o,
                        text: t
                    }), wi(this, Hr(o), Ga)
                };
            Fs.prototype = b(Ui.prototype, {
                constructor: Fs,
                iter: function(t, e, n) {
                    n ? this.iterN(t - this.first, e - t, n) : this.iterN(this.first, this.first + this.size, t)
                },
                insert: function(t, e) {
                    for (var n = 0, r = 0; r < e.length; ++r) n += e[r].height;
                    this.insertInner(t - this.first, e, n)
                },
                remove: function(t, e) {
                    this.removeInner(t - this.first, e)
                },
                getValue: function(t) {
                    var e = O(this, this.first, this.first + this.size);
                    return !1 === t ? e : e.join(t || this.lineSeparator())
                },
                setValue: vr(function(t) {
                    var e = D(this.first, 0),
                        n = this.first + this.size - 1;
                    Ai(this, {
                        from: e,
                        to: D(n, T(this, n).text.length),
                        text: this.splitLines(t),
                        origin: "setValue",
                        full: !0
                    }, !0), this.cm && Yn(this.cm, 0, 0), wi(this, Hr(e), Ga)
                }),
                replaceRange: function(t, e, n, r) {
                    e = W(this, e), n = n ? W(this, n) : e, Ri(this, t, e, n, r)
                },
                getRange: function(t, e, n) {
                    var r = M(this, W(this, t), W(this, e));
                    return !1 === n ? r : r.join(n || this.lineSeparator())
                },
                getLine: function(t) {
                    var e = this.getLineHandle(t);
                    return e && e.text
                },
                getLineHandle: function(t) {
                    if (P(this, t)) return T(this, t)
                },
                getLineNumber: function(t) {
                    return A(t)
                },
                getLineHandleVisualStart: function(t) {
                    return "number" == typeof t && (t = T(this, t)), dt(t)
                },
                lineCount: function() {
                    return this.size
                },
                firstLine: function() {
                    return this.first
                },
                lastLine: function() {
                    return this.first + this.size - 1
                },
                clipPos: function(t) {
                    return W(this, t)
                },
                getCursor: function(t) {
                    var e = this.sel.primary();
                    return null == t || "head" == t ? e.head : "anchor" == t ? e.anchor : "end" == t || "to" == t || !1 === t ? e.to() : e.from()
                },
                listSelections: function() {
                    return this.sel.ranges
                },
                somethingSelected: function() {
                    return this.sel.somethingSelected()
                },
                setCursor: vr(function(t, e, n) {
                    yi(this, W(this, "number" == typeof t ? D(t, e || 0) : t), null, n)
                }),
                setSelection: vr(function(t, e, n) {
                    yi(this, W(this, t), W(this, e || t), n)
                }),
                extendSelection: vr(function(t, e, n) {
                    mi(this, W(this, t), e && W(this, e), n)
                }),
                extendSelections: vr(function(t, e) {
                    gi(this, V(this, t), e)
                }),
                extendSelectionsBy: vr(function(t, e) {
                    gi(this, V(this, g(this.sel.ranges, t)), e)
                }),
                setSelections: vr(function(t, e, n) {
                    var r = this;
                    if (t.length) {
                        for (var i = [], o = 0; o < t.length; o++) i[o] = new Ls(W(r, t[o].anchor), W(r, t[o].head));
                        null == e && (e = Math.min(t.length - 1, this.sel.primIndex)), wi(this, Br(i, e), n)
                    }
                }),
                addSelection: vr(function(t, e, n) {
                    var r = this.sel.ranges.slice(0);
                    r.push(new Ls(W(this, t), W(this, e || t))), wi(this, Br(r, r.length - 1), n)
                }),
                getSelection: function(t) {
                    for (var e, n = this, r = this.sel.ranges, i = 0; i < r.length; i++) {
                        var o = M(n, r[i].from(), r[i].to());
                        e = e ? e.concat(o) : o
                    }
                    return !1 === t ? e : e.join(t || this.lineSeparator())
                },
                getSelections: function(t) {
                    for (var e = this, n = [], r = this.sel.ranges, i = 0; i < r.length; i++) {
                        var o = M(e, r[i].from(), r[i].to());
                        !1 !== t && (o = o.join(t || e.lineSeparator())), n[i] = o
                    }
                    return n
                },
                replaceSelection: function(t, e, n) {
                    for (var r = [], i = 0; i < this.sel.ranges.length; i++) r[i] = t;
                    this.replaceSelections(r, e, n || "+input")
                },
                replaceSelections: vr(function(t, e, n) {
                    for (var r = this, i = [], o = this.sel, a = 0; a < o.ranges.length; a++) {
                        var s = o.ranges[a];
                        i[a] = {
                            from: s.from(),
                            to: s.to(),
                            text: r.splitLines(t[a]),
                            origin: n
                        }
                    }
                    for (var l = e && "end" != e && Kr(this, i, e), c = i.length - 1; c >= 0; c--) Ai(r, i[c]);
                    l ? xi(this, l) : this.cm && qn(this.cm)
                }),
                undo: vr(function() {
                    Pi(this, "undo")
                }),
                redo: vr(function() {
                    Pi(this, "redo")
                }),
                undoSelection: vr(function() {
                    Pi(this, "undo", !0)
                }),
                redoSelection: vr(function() {
                    Pi(this, "redo", !0)
                }),
                setExtending: function(t) {
                    this.extend = t
                },
                getExtending: function() {
                    return this.extend
                },
                historySize: function() {
                    for (var t = this.history, e = 0, n = 0, r = 0; r < t.done.length; r++) t.done[r].ranges || ++e;
                    for (var i = 0; i < t.undone.length; i++) t.undone[i].ranges || ++n;
                    return {
                        undo: e,
                        redo: n
                    }
                },
                clearHistory: function() {
                    this.history = new ei(this.history.maxGeneration)
                },
                markClean: function() {
                    this.cleanGeneration = this.changeGeneration(!0)
                },
                changeGeneration: function(t) {
                    return t && (this.history.lastOp = this.history.lastSelOp = this.history.lastOrigin = null), this.history.generation
                },
                isClean: function(t) {
                    return this.history.generation == (t || this.cleanGeneration)
                },
                getHistory: function() {
                    return {
                        done: hi(this.history.done),
                        undone: hi(this.history.undone)
                    }
                },
                setHistory: function(t) {
                    var e = this.history = new ei(this.history.maxGeneration);
                    e.done = hi(t.done.slice(0), null, !0), e.undone = hi(t.undone.slice(0), null, !0)
                },
                setGutterMarker: vr(function(t, e, n) {
                    return Hi(this, t, "gutter", function(t) {
                        var r = t.gutterMarkers || (t.gutterMarkers = {});
                        return r[e] = n, !n && C(r) && (t.gutterMarkers = null), !0
                    })
                }),
                clearGutter: vr(function(t) {
                    var e = this;
                    this.iter(function(n) {
                        n.gutterMarkers && n.gutterMarkers[t] && Hi(e, n, "gutter", function() {
                            return n.gutterMarkers[t] = null, C(n.gutterMarkers) && (n.gutterMarkers = null), !0
                        })
                    })
                }),
                lineInfo: function(t) {
                    var e;
                    if ("number" == typeof t) {
                        if (!P(this, t)) return null;
                        if (e = t, !(t = T(this, t))) return null
                    } else if (null == (e = A(t))) return null;
                    return {
                        line: e,
                        handle: t,
                        text: t.text,
                        gutterMarkers: t.gutterMarkers,
                        textClass: t.textClass,
                        bgClass: t.bgClass,
                        wrapClass: t.wrapClass,
                        widgets: t.widgets
                    }
                },
                addLineClass: vr(function(e, n, r) {
                    return Hi(this, e, "gutter" == n ? "gutter" : "class", function(e) {
                        var i = "text" == n ? "textClass" : "background" == n ? "bgClass" : "gutter" == n ? "gutterClass" : "wrapClass";
                        if (e[i]) {
                            if (t(r).test(e[i])) return !1;
                            e[i] += " " + r
                        } else e[i] = r;
                        return !0
                    })
                }),
                removeLineClass: vr(function(e, n, r) {
                    return Hi(this, e, "gutter" == n ? "gutter" : "class", function(e) {
                        var i = "text" == n ? "textClass" : "background" == n ? "bgClass" : "gutter" == n ? "gutterClass" : "wrapClass",
                            o = e[i];
                        if (!o) return !1;
                        if (null == r) e[i] = null;
                        else {
                            var a = o.match(t(r));
                            if (!a) return !1;
                            var s = a.index + a[0].length;
                            e[i] = o.slice(0, a.index) + (a.index && s != o.length ? " " : "") + o.slice(s) || null
                        }
                        return !0
                    })
                }),
                addLineWidget: vr(function(t, e, n) {
                    return Gi(this, t, e, n)
                }),
                removeLineWidget: function(t) {
                    t.clear()
                },
                markText: function(t, e, n) {
                    return Ki(this, W(this, t), W(this, e), n, n && n.type || "range")
                },
                setBookmark: function(t, e) {
                    var n = {
                        replacedWith: e && (null == e.nodeType ? e.widget : e),
                        insertLeft: e && e.insertLeft,
                        clearWhenEmpty: !1,
                        shared: e && e.shared,
                        handleMouseEvents: e && e.handleMouseEvents
                    };
                    return t = W(this, t), Ki(this, t, t, n, "bookmark")
                },
                findMarksAt: function(t) {
                    t = W(this, t);
                    var e = [],
                        n = T(this, t.line).markedSpans;
                    if (n)
                        for (var r = 0; r < n.length; ++r) {
                            var i = n[r];
                            (null == i.from || i.from <= t.ch) && (null == i.to || i.to >= t.ch) && e.push(i.marker.parent || i.marker)
                        }
                    return e
                },
                findMarks: function(t, e, n) {
                    t = W(this, t), e = W(this, e);
                    var r = [],
                        i = t.line;
                    return this.iter(t.line, e.line + 1, function(o) {
                        var a = o.markedSpans;
                        if (a)
                            for (var s = 0; s < a.length; s++) {
                                var l = a[s];
                                null != l.to && i == t.line && t.ch >= l.to || null == l.from && i != t.line || null != l.from && i == e.line && l.from >= e.ch || n && !n(l.marker) || r.push(l.marker.parent || l.marker)
                            }++i
                    }), r
                },
                getAllMarks: function() {
                    var t = [];
                    return this.iter(function(e) {
                        var n = e.markedSpans;
                        if (n)
                            for (var r = 0; r < n.length; ++r) null != n[r].from && t.push(n[r].marker)
                    }), t
                },
                posFromIndex: function(t) {
                    var e, n = this.first,
                        r = this.lineSeparator().length;
                    return this.iter(function(i) {
                        var o = i.text.length + r;
                        if (o > t) return e = t, !0;
                        t -= o, ++n
                    }), W(this, D(n, e))
                },
                indexFromPos: function(t) {
                    t = W(this, t);
                    var e = t.ch;
                    if (t.line < this.first || t.ch < 0) return 0;
                    var n = this.lineSeparator().length;
                    return this.iter(this.first, t.line, function(t) {
                        e += t.text.length + n
                    }), e
                },
                copy: function(t) {
                    var e = new Fs(O(this, this.first, this.first + this.size), this.modeOption, this.first, this.lineSep, this.direction);
                    return e.scrollTop = this.scrollTop, e.scrollLeft = this.scrollLeft, e.sel = this.sel, e.extend = !1, t && (e.history.undoDepth = this.history.undoDepth, e.setHistory(this.getHistory())), e
                },
                linkedDoc: function(t) {
                    t || (t = {});
                    var e = this.first,
                        n = this.first + this.size;
                    null != t.from && t.from > e && (e = t.from), null != t.to && t.to < n && (n = t.to);
                    var r = new Fs(O(this, e, n), t.mode || this.modeOption, e, this.lineSep, this.direction);
                    return t.sharedHist && (r.history = this.history), (this.linked || (this.linked = [])).push({
                        doc: r,
                        sharedHist: t.sharedHist
                    }), r.linked = [{
                        doc: this,
                        isParent: !0,
                        sharedHist: t.sharedHist
                    }], Yi(r, qi(this)), r
                },
                unlinkDoc: function(t) {
                    var e = this;
                    if (t instanceof qo && (t = t.doc), this.linked)
                        for (var n = 0; n < this.linked.length; ++n) {
                            var r = e.linked[n];
                            if (r.doc == t) {
                                e.linked.splice(n, 1), t.unlinkDoc(e), Ji(qi(e));
                                break
                            }
                        }
                    if (t.history == this.history) {
                        var i = [t.id];
                        Xr(t, function(t) {
                            return i.push(t.id)
                        }, !0), t.history = new ei(null), t.history.done = hi(this.history.done, i), t.history.undone = hi(this.history.undone, i)
                    }
                },
                iterLinkedDocs: function(t) {
                    Xr(this, t)
                },
                getMode: function() {
                    return this.mode
                },
                getEditor: function() {
                    return this.cm
                },
                splitLines: function(t) {
                    return this.lineSep ? t.split(this.lineSep) : is(t)
                },
                lineSeparator: function() {
                    return this.lineSep || "\n"
                },
                setDirection: vr(function(t) {
                    "rtl" != t && (t = "ltr"), t != this.direction && (this.direction = t, this.iter(function(t) {
                        return t.order = null
                    }), this.cm && ti(this.cm))
                })
            }), Fs.prototype.eachLine = Fs.prototype.iter;
            for (var Rs = 0, zs = !1, js = {
                    3: "Pause",
                    8: "Backspace",
                    9: "Tab",
                    13: "Enter",
                    16: "Shift",
                    17: "Ctrl",
                    18: "Alt",
                    19: "Pause",
                    20: "CapsLock",
                    27: "Esc",
                    32: "Space",
                    33: "PageUp",
                    34: "PageDown",
                    35: "End",
                    36: "Home",
                    37: "Left",
                    38: "Up",
                    39: "Right",
                    40: "Down",
                    44: "PrintScrn",
                    45: "Insert",
                    46: "Delete",
                    59: ";",
                    61: "=",
                    91: "Mod",
                    92: "Mod",
                    93: "Mod",
                    106: "*",
                    107: "=",
                    109: "-",
                    110: ".",
                    111: "/",
                    127: "Delete",
                    145: "ScrollLock",
                    173: "-",
                    186: ";",
                    187: "=",
                    188: ",",
                    189: "-",
                    190: ".",
                    191: "/",
                    192: "`",
                    219: "[",
                    220: "\\",
                    221: "]",
                    222: "'",
                    63232: "Up",
                    63233: "Down",
                    63234: "Left",
                    63235: "Right",
                    63272: "Delete",
                    63273: "Home",
                    63275: "End",
                    63276: "PageUp",
                    63277: "PageDown",
                    63302: "Insert"
                }, Bs = 0; Bs < 10; Bs++) js[Bs + 48] = js[Bs + 96] = String(Bs);
            for (var Hs = 65; Hs <= 90; Hs++) js[Hs] = String.fromCharCode(Hs);
            for (var Ws = 1; Ws <= 12; Ws++) js[Ws + 111] = js[Ws + 63235] = "F" + Ws;
            var Us = {};
            Us.basic = {
                Left: "goCharLeft",
                Right: "goCharRight",
                Up: "goLineUp",
                Down: "goLineDown",
                End: "goLineEnd",
                Home: "goLineStartSmart",
                PageUp: "goPageUp",
                PageDown: "goPageDown",
                Delete: "delCharAfter",
                Backspace: "delCharBefore",
                "Shift-Backspace": "delCharBefore",
                Tab: "defaultTab",
                "Shift-Tab": "indentAuto",
                Enter: "newlineAndIndent",
                Insert: "toggleOverwrite",
                Esc: "singleSelection"
            }, Us.pcDefault = {
                "Ctrl-A": "selectAll",
                "Ctrl-D": "deleteLine",
                "Ctrl-Z": "undo",
                "Shift-Ctrl-Z": "redo",
                "Ctrl-Y": "redo",
                "Ctrl-Home": "goDocStart",
                "Ctrl-End": "goDocEnd",
                "Ctrl-Up": "goLineUp",
                "Ctrl-Down": "goLineDown",
                "Ctrl-Left": "goGroupLeft",
                "Ctrl-Right": "goGroupRight",
                "Alt-Left": "goLineStart",
                "Alt-Right": "goLineEnd",
                "Ctrl-Backspace": "delGroupBefore",
                "Ctrl-Delete": "delGroupAfter",
                "Ctrl-S": "save",
                "Ctrl-F": "find",
                "Ctrl-G": "findNext",
                "Shift-Ctrl-G": "findPrev",
                "Shift-Ctrl-F": "replace",
                "Shift-Ctrl-R": "replaceAll",
                "Ctrl-[": "indentLess",
                "Ctrl-]": "indentMore",
                "Ctrl-U": "undoSelection",
                "Shift-Ctrl-U": "redoSelection",
                "Alt-U": "redoSelection",
                fallthrough: "basic"
            }, Us.emacsy = {
                "Ctrl-F": "goCharRight",
                "Ctrl-B": "goCharLeft",
                "Ctrl-P": "goLineUp",
                "Ctrl-N": "goLineDown",
                "Alt-F": "goWordRight",
                "Alt-B": "goWordLeft",
                "Ctrl-A": "goLineStart",
                "Ctrl-E": "goLineEnd",
                "Ctrl-V": "goPageDown",
                "Shift-Ctrl-V": "goPageUp",
                "Ctrl-D": "delCharAfter",
                "Ctrl-H": "delCharBefore",
                "Alt-D": "delWordAfter",
                "Alt-Backspace": "delWordBefore",
                "Ctrl-K": "killLine",
                "Ctrl-T": "transposeChars",
                "Ctrl-O": "openLine"
            }, Us.macDefault = {
                "Cmd-A": "selectAll",
                "Cmd-D": "deleteLine",
                "Cmd-Z": "undo",
                "Shift-Cmd-Z": "redo",
                "Cmd-Y": "redo",
                "Cmd-Home": "goDocStart",
                "Cmd-Up": "goDocStart",
                "Cmd-End": "goDocEnd",
                "Cmd-Down": "goDocEnd",
                "Alt-Left": "goGroupLeft",
                "Alt-Right": "goGroupRight",
                "Cmd-Left": "goLineLeft",
                "Cmd-Right": "goLineRight",
                "Alt-Backspace": "delGroupBefore",
                "Ctrl-Alt-Backspace": "delGroupAfter",
                "Alt-Delete": "delGroupAfter",
                "Cmd-S": "save",
                "Cmd-F": "find",
                "Cmd-G": "findNext",
                "Shift-Cmd-G": "findPrev",
                "Cmd-Alt-F": "replace",
                "Shift-Cmd-Alt-F": "replaceAll",
                "Cmd-[": "indentLess",
                "Cmd-]": "indentMore",
                "Cmd-Backspace": "delWrappedLineLeft",
                "Cmd-Delete": "delWrappedLineRight",
                "Cmd-U": "undoSelection",
                "Shift-Cmd-U": "redoSelection",
                "Ctrl-Up": "goDocStart",
                "Ctrl-Down": "goDocEnd",
                fallthrough: ["basic", "emacsy"]
            }, Us.default = Aa ? Us.macDefault : Us.pcDefault;
            var Vs = {
                    selectAll: Oi,
                    singleSelection: function(t) {
                        return t.setSelection(t.getCursor("anchor"), t.getCursor("head"), Ga)
                    },
                    killLine: function(t) {
                        return ho(t, function(e) {
                            if (e.empty()) {
                                var n = T(t.doc, e.head.line).text.length;
                                return e.head.ch == n && e.head.line < t.lastLine() ? {
                                    from: e.head,
                                    to: D(e.head.line + 1, 0)
                                } : {
                                    from: e.head,
                                    to: D(e.head.line, n)
                                }
                            }
                            return {
                                from: e.from(),
                                to: e.to()
                            }
                        })
                    },
                    deleteLine: function(t) {
                        return ho(t, function(e) {
                            return {
                                from: D(e.from().line, 0),
                                to: W(t.doc, D(e.to().line + 1, 0))
                            }
                        })
                    },
                    delLineLeft: function(t) {
                        return ho(t, function(t) {
                            return {
                                from: D(t.from().line, 0),
                                to: t.from()
                            }
                        })
                    },
                    delWrappedLineLeft: function(t) {
                        return ho(t, function(e) {
                            var n = t.charCoords(e.head, "div").top + 5;
                            return {
                                from: t.coordsChar({
                                    left: 0,
                                    top: n
                                }, "div"),
                                to: e.from()
                            }
                        })
                    },
                    delWrappedLineRight: function(t) {
                        return ho(t, function(e) {
                            var n = t.charCoords(e.head, "div").top + 5,
                                r = t.coordsChar({
                                    left: t.display.lineDiv.offsetWidth + 100,
                                    top: n
                                }, "div");
                            return {
                                from: e.from(),
                                to: r
                            }
                        })
                    },
                    undo: function(t) {
                        return t.undo()
                    },
                    redo: function(t) {
                        return t.redo()
                    },
                    undoSelection: function(t) {
                        return t.undoSelection()
                    },
                    redoSelection: function(t) {
                        return t.redoSelection()
                    },
                    goDocStart: function(t) {
                        return t.extendSelection(D(t.firstLine(), 0))
                    },
                    goDocEnd: function(t) {
                        return t.extendSelection(D(t.lastLine()))
                    },
                    goLineStart: function(t) {
                        return t.extendSelectionsBy(function(e) {
                            return yo(t, e.head.line)
                        }, {
                            origin: "+move",
                            bias: 1
                        })
                    },
                    goLineStartSmart: function(t) {
                        return t.extendSelectionsBy(function(e) {
                            return xo(t, e.head)
                        }, {
                            origin: "+move",
                            bias: 1
                        })
                    },
                    goLineEnd: function(t) {
                        return t.extendSelectionsBy(function(e) {
                            return bo(t, e.head.line)
                        }, {
                            origin: "+move",
                            bias: -1
                        })
                    },
                    goLineRight: function(t) {
                        return t.extendSelectionsBy(function(e) {
                            var n = t.cursorCoords(e.head, "div").top + 5;
                            return t.coordsChar({
                                left: t.display.lineDiv.offsetWidth + 100,
                                top: n
                            }, "div")
                        }, $a)
                    },
                    goLineLeft: function(t) {
                        return t.extendSelectionsBy(function(e) {
                            var n = t.cursorCoords(e.head, "div").top + 5;
                            return t.coordsChar({
                                left: 0,
                                top: n
                            }, "div")
                        }, $a)
                    },
                    goLineLeftSmart: function(t) {
                        return t.extendSelectionsBy(function(e) {
                            var n = t.cursorCoords(e.head, "div").top + 5,
                                r = t.coordsChar({
                                    left: 0,
                                    top: n
                                }, "div");
                            return r.ch < t.getLine(r.line).search(/\S/) ? xo(t, e.head) : r
                        }, $a)
                    },
                    goLineUp: function(t) {
                        return t.moveV(-1, "line")
                    },
                    goLineDown: function(t) {
                        return t.moveV(1, "line")
                    },
                    goPageUp: function(t) {
                        return t.moveV(-1, "page")
                    },
                    goPageDown: function(t) {
                        return t.moveV(1, "page")
                    },
                    goCharLeft: function(t) {
                        return t.moveH(-1, "char")
                    },
                    goCharRight: function(t) {
                        return t.moveH(1, "char")
                    },
                    goColumnLeft: function(t) {
                        return t.moveH(-1, "column")
                    },
                    goColumnRight: function(t) {
                        return t.moveH(1, "column")
                    },
                    goWordLeft: function(t) {
                        return t.moveH(-1, "word")
                    },
                    goGroupRight: function(t) {
                        return t.moveH(1, "group")
                    },
                    goGroupLeft: function(t) {
                        return t.moveH(-1, "group")
                    },
                    goWordRight: function(t) {
                        return t.moveH(1, "word")
                    },
                    delCharBefore: function(t) {
                        return t.deleteH(-1, "char")
                    },
                    delCharAfter: function(t) {
                        return t.deleteH(1, "char")
                    },
                    delWordBefore: function(t) {
                        return t.deleteH(-1, "word")
                    },
                    delWordAfter: function(t) {
                        return t.deleteH(1, "word")
                    },
                    delGroupBefore: function(t) {
                        return t.deleteH(-1, "group")
                    },
                    delGroupAfter: function(t) {
                        return t.deleteH(1, "group")
                    },
                    indentAuto: function(t) {
                        return t.indentSelection("smart")
                    },
                    indentMore: function(t) {
                        return t.indentSelection("add")
                    },
                    indentLess: function(t) {
                        return t.indentSelection("subtract")
                    },
                    insertTab: function(t) {
                        return t.replaceSelection("\t")
                    },
                    insertSoftTab: function(t) {
                        for (var e = [], n = t.listSelections(), r = t.options.tabSize, i = 0; i < n.length; i++) {
                            var o = n[i].from(),
                                a = f(t.getLine(o.line), o.ch, r);
                            e.push(p(r - a % r))
                        }
                        t.replaceSelections(e)
                    },
                    defaultTab: function(t) {
                        t.somethingSelected() ? t.indentSelection("add") : t.execCommand("insertTab")
                    },
                    transposeChars: function(t) {
                        return pr(t, function() {
                            for (var e = t.listSelections(), n = [], r = 0; r < e.length; r++)
                                if (e[r].empty()) {
                                    var i = e[r].head,
                                        o = T(t.doc, i.line).text;
                                    if (o)
                                        if (i.ch == o.length && (i = new D(i.line, i.ch - 1)), i.ch > 0) i = new D(i.line, i.ch + 1), t.replaceRange(o.charAt(i.ch - 1) + o.charAt(i.ch - 2), D(i.line, i.ch - 2), i, "+transpose");
                                        else if (i.line > t.doc.first) {
                                        var a = T(t.doc, i.line - 1).text;
                                        a && (i = new D(i.line, 1), t.replaceRange(o.charAt(0) + t.doc.lineSeparator() + a.charAt(a.length - 1), D(i.line - 1, a.length - 1), i, "+transpose"))
                                    }
                                    n.push(new Ls(i, i))
                                }
                            t.setSelections(n)
                        })
                    },
                    newlineAndIndent: function(t) {
                        return pr(t, function() {
                            for (var e = t.listSelections(), n = e.length - 1; n >= 0; n--) t.replaceRange(t.doc.lineSeparator(), e[n].anchor, e[n].head, "+input");
                            e = t.listSelections();
                            for (var r = 0; r < e.length; r++) t.indentLine(e[r].from().line, null, !0);
                            qn(t)
                        })
                    },
                    openLine: function(t) {
                        return t.replaceSelection("\n", "start")
                    },
                    toggleOverwrite: function(t) {
                        return t.toggleOverwrite()
                    }
                },
                Gs = new Ba,
                Ks = null,
                $s = function(t, e, n) {
                    this.time = t, this.pos = e, this.button = n
                };
            $s.prototype.compare = function(t, e, n) {
                return this.time + 400 > t && 0 == F(e, this.pos) && n == this.button
            };
            var qs, Ys, Js = {
                    toString: function() {
                        return "CodeMirror.Init"
                    }
                },
                Xs = {},
                Zs = {};
            qo.defaults = Xs, qo.optionHandlers = Zs;
            var Qs = [];
            qo.defineInitHook = function(t) {
                return Qs.push(t)
            };
            var tl = null,
                el = function(t) {
                    this.cm = t, this.lastAnchorNode = this.lastAnchorOffset = this.lastFocusNode = this.lastFocusOffset = null, this.polling = new Ba, this.composing = null, this.gracePeriod = !1, this.readDOMTimeout = null
                };
            el.prototype.init = function(t) {
                function e(t) {
                    if (!Mt(i, t)) {
                        if (i.somethingSelected()) Xo({
                            lineWise: !1,
                            text: i.getSelections()
                        }), "cut" == t.type && i.replaceSelection("", null, "cut");
                        else {
                            if (!i.options.lineWiseCopyCut) return;
                            var e = ea(i);
                            Xo({
                                lineWise: !0,
                                text: e.text
                            }), "cut" == t.type && i.operation(function() {
                                i.setSelections(e.ranges, 0, Ga), i.replaceSelection("", null, "cut")
                            })
                        }
                        if (t.clipboardData) {
                            t.clipboardData.clearData();
                            var n = tl.text.join("\n");
                            if (t.clipboardData.setData("Text", n), t.clipboardData.getData("Text") == n) return void t.preventDefault()
                        }
                        var a = ra(),
                            s = a.firstChild;
                        i.display.lineSpace.insertBefore(a, i.display.lineSpace.firstChild), s.value = tl.text.join("\n");
                        var l = document.activeElement;
                        ja(s), setTimeout(function() {
                            i.display.lineSpace.removeChild(a), l.focus(), l == o && r.showPrimarySelection()
                        }, 50)
                    }
                }
                var n = this,
                    r = this,
                    i = r.cm,
                    o = r.div = t.lineDiv;
                na(o, i.options.spellcheck), ns(o, "paste", function(t) {
                    Mt(i, t) || Qo(t, i) || xa <= 11 && setTimeout(mr(i, function() {
                        return n.updateFromDOM()
                    }), 20)
                }), ns(o, "compositionstart", function(t) {
                    n.composing = {
                        data: t.data,
                        done: !1
                    }
                }), ns(o, "compositionupdate", function(t) {
                    n.composing || (n.composing = {
                        data: t.data,
                        done: !1
                    })
                }), ns(o, "compositionend", function(t) {
                    n.composing && (t.data != n.composing.data && n.readFromDOMSoon(), n.composing.done = !0)
                }), ns(o, "touchstart", function() {
                    return r.forceCompositionEnd()
                }), ns(o, "input", function() {
                    n.composing || n.readFromDOMSoon()
                }), ns(o, "copy", e), ns(o, "cut", e)
            }, el.prototype.prepareSelection = function() {
                var t = On(this.cm, !1);
                return t.focus = this.cm.state.focused, t
            }, el.prototype.showSelection = function(t, e) {
                t && this.cm.display.view.length && ((t.focus || e) && this.showPrimarySelection(), this.showMultipleSelections(t))
            }, el.prototype.getSelection = function() {
                return this.cm.display.wrapper.ownerDocument.getSelection()
            }, el.prototype.showPrimarySelection = function() {
                var t = this.getSelection(),
                    e = this.cm,
                    n = e.doc.sel.primary(),
                    r = n.from(),
                    i = n.to();
                if (e.display.viewTo == e.display.viewFrom || r.line >= e.display.viewTo || i.line < e.display.viewFrom) return void t.removeAllRanges();
                var o = ua(e, t.anchorNode, t.anchorOffset),
                    a = ua(e, t.focusNode, t.focusOffset);
                if (!o || o.bad || !a || a.bad || 0 != F(B(o, a), r) || 0 != F(j(o, a), i)) {
                    var s = e.display.view,
                        l = r.line >= e.display.viewFrom && aa(e, r) || {
                            node: s[0].measure.map[2],
                            offset: 0
                        },
                        c = i.line < e.display.viewTo && aa(e, i);
                    if (!c) {
                        var u = s[s.length - 1].measure,
                            f = u.maps ? u.maps[u.maps.length - 1] : u.map;
                        c = {
                            node: f[f.length - 1],
                            offset: f[f.length - 2] - f[f.length - 3]
                        }
                    }
                    if (!l || !c) return void t.removeAllRanges();
                    var d, h = t.rangeCount && t.getRangeAt(0);
                    try {
                        d = Da(l.node, l.offset, c.offset, c.node)
                    } catch (t) {}
                    d && (!ma && e.state.focused ? (t.collapse(l.node, l.offset), d.collapsed || (t.removeAllRanges(), t.addRange(d))) : (t.removeAllRanges(), t.addRange(d)), h && null == t.anchorNode ? t.addRange(h) : ma && this.startGracePeriod()), this.rememberSelection()
                }
            }, el.prototype.startGracePeriod = function() {
                var t = this;
                clearTimeout(this.gracePeriod), this.gracePeriod = setTimeout(function() {
                    t.gracePeriod = !1, t.selectionChanged() && t.cm.operation(function() {
                        return t.cm.curOp.selectionChanged = !0
                    })
                }, 20)
            }, el.prototype.showMultipleSelections = function(t) {
                n(this.cm.display.cursorDiv, t.cursors), n(this.cm.display.selectionDiv, t.selection)
            }, el.prototype.rememberSelection = function() {
                var t = this.getSelection();
                this.lastAnchorNode = t.anchorNode, this.lastAnchorOffset = t.anchorOffset, this.lastFocusNode = t.focusNode, this.lastFocusOffset = t.focusOffset
            }, el.prototype.selectionInEditor = function() {
                var t = this.getSelection();
                if (!t.rangeCount) return !1;
                var e = t.getRangeAt(0).commonAncestorContainer;
                return o(this.div, e)
            }, el.prototype.focus = function() {
                "nocursor" != this.cm.options.readOnly && (this.selectionInEditor() || this.showSelection(this.prepareSelection(), !0), this.div.focus())
            }, el.prototype.blur = function() {
                this.div.blur()
            }, el.prototype.getField = function() {
                return this.div
            }, el.prototype.supportsTouch = function() {
                return !0
            }, el.prototype.receivedFocus = function() {
                function t() {
                    e.cm.state.focused && (e.pollSelection(), e.polling.set(e.cm.options.pollInterval, t))
                }
                var e = this;
                this.selectionInEditor() ? this.pollSelection() : pr(this.cm, function() {
                    return e.cm.curOp.selectionChanged = !0
                }), this.polling.set(this.cm.options.pollInterval, t)
            }, el.prototype.selectionChanged = function() {
                var t = this.getSelection();
                return t.anchorNode != this.lastAnchorNode || t.anchorOffset != this.lastAnchorOffset || t.focusNode != this.lastFocusNode || t.focusOffset != this.lastFocusOffset
            }, el.prototype.pollSelection = function() {
                if (null == this.readDOMTimeout && !this.gracePeriod && this.selectionChanged()) {
                    var t = this.getSelection(),
                        e = this.cm;
                    if (Oa && ka && this.cm.options.gutters.length && sa(t.anchorNode)) return this.cm.triggerOnKeyDown({
                        type: "keydown",
                        keyCode: 8,
                        preventDefault: Math.abs
                    }), this.blur(), void this.focus();
                    if (!this.composing) {
                        this.rememberSelection();
                        var n = ua(e, t.anchorNode, t.anchorOffset),
                            r = ua(e, t.focusNode, t.focusOffset);
                        n && r && pr(e, function() {
                            wi(e.doc, Hr(n, r), Ga), (n.bad || r.bad) && (e.curOp.selectionChanged = !0)
                        })
                    }
                }
            }, el.prototype.pollContent = function() {
                null != this.readDOMTimeout && (clearTimeout(this.readDOMTimeout), this.readDOMTimeout = null);
                var t = this.cm,
                    e = t.display,
                    n = t.doc.sel.primary(),
                    r = n.from(),
                    i = n.to();
                if (0 == r.ch && r.line > t.firstLine() && (r = D(r.line - 1, T(t.doc, r.line - 1).length)), i.ch == T(t.doc, i.line).text.length && i.line < t.lastLine() && (i = D(i.line + 1, 0)), r.line < e.viewFrom || i.line > e.viewTo - 1) return !1;
                var o, a, s;
                r.line == e.viewFrom || 0 == (o = Tn(t, r.line)) ? (a = A(e.view[0].line), s = e.view[0].node) : (a = A(e.view[o].line), s = e.view[o - 1].node.nextSibling);
                var l, c, u = Tn(t, i.line);
                if (u == e.view.length - 1 ? (l = e.viewTo - 1, c = e.lineDiv.lastChild) : (l = A(e.view[u + 1].line) - 1, c = e.view[u + 1].node.previousSibling), !s) return !1;
                for (var f = t.doc.splitLines(ca(t, s, c, a, l)), d = M(t.doc, D(a, 0), D(l, T(t.doc, l).text.length)); f.length > 1 && d.length > 1;)
                    if (m(f) == m(d)) f.pop(), d.pop(), l--;
                    else {
                        if (f[0] != d[0]) break;
                        f.shift(), d.shift(), a++
                    }
                for (var h = 0, p = 0, g = f[0], v = d[0], y = Math.min(g.length, v.length); h < y && g.charCodeAt(h) == v.charCodeAt(h);) ++h;
                for (var b = m(f), x = m(d), w = Math.min(b.length - (1 == f.length ? h : 0), x.length - (1 == d.length ? h : 0)); p < w && b.charCodeAt(b.length - p - 1) == x.charCodeAt(x.length - p - 1);) ++p;
                if (1 == f.length && 1 == d.length && a == r.line)
                    for (; h && h > r.ch && b.charCodeAt(b.length - p - 1) == x.charCodeAt(x.length - p - 1);) h--, p++;
                f[f.length - 1] = b.slice(0, b.length - p).replace(/^\u200b+/, ""), f[0] = f[0].slice(h).replace(/\u200b+$/, "");
                var C = D(a, h),
                    k = D(l, d.length ? m(d).length - p : 0);
                return f.length > 1 || f[0] || F(C, k) ? (Ri(t.doc, f, C, k, "+input"), !0) : void 0
            }, el.prototype.ensurePolled = function() {
                this.forceCompositionEnd()
            }, el.prototype.reset = function() {
                this.forceCompositionEnd()
            }, el.prototype.forceCompositionEnd = function() {
                this.composing && (clearTimeout(this.readDOMTimeout), this.composing = null, this.updateFromDOM(), this.div.blur(), this.div.focus())
            }, el.prototype.readFromDOMSoon = function() {
                var t = this;
                null == this.readDOMTimeout && (this.readDOMTimeout = setTimeout(function() {
                    if (t.readDOMTimeout = null, t.composing) {
                        if (!t.composing.done) return;
                        t.composing = null
                    }
                    t.updateFromDOM()
                }, 80))
            }, el.prototype.updateFromDOM = function() {
                var t = this;
                !this.cm.isReadOnly() && this.pollContent() || pr(this.cm, function() {
                    return yr(t.cm)
                })
            }, el.prototype.setUneditable = function(t) {
                t.contentEditable = "false"
            }, el.prototype.onKeyPress = function(t) {
                0 == t.charCode || this.composing || (t.preventDefault(), this.cm.isReadOnly() || mr(this.cm, Zo)(this.cm, String.fromCharCode(null == t.charCode ? t.keyCode : t.charCode), 0))
            }, el.prototype.readOnlyChanged = function(t) {
                this.div.contentEditable = String("nocursor" != t)
            }, el.prototype.onContextMenu = function() {}, el.prototype.resetPosition = function() {}, el.prototype.needsContentAttribute = !0;
            var nl = function(t) {
                this.cm = t, this.prevInput = "", this.pollingFast = !1, this.polling = new Ba, this.hasSelection = !1, this.composing = null
            };
            nl.prototype.init = function(t) {
                    function e(t) {
                        if (!Mt(i, t)) {
                            if (i.somethingSelected()) Xo({
                                lineWise: !1,
                                text: i.getSelections()
                            });
                            else {
                                if (!i.options.lineWiseCopyCut) return;
                                var e = ea(i);
                                Xo({
                                    lineWise: !0,
                                    text: e.text
                                }), "cut" == t.type ? i.setSelections(e.ranges, null, Ga) : (r.prevInput = "", o.value = e.text.join("\n"), ja(o))
                            }
                            "cut" == t.type && (i.state.cutIncoming = !0)
                        }
                    }
                    var n = this,
                        r = this,
                        i = this.cm;
                    this.createField(t);
                    var o = this.textarea;
                    t.wrapper.insertBefore(this.wrapper, t.wrapper.firstChild), Ma && (o.style.width = "0px"), ns(o, "input", function() {
                        ba && xa >= 9 && n.hasSelection && (n.hasSelection = null), r.poll()
                    }), ns(o, "paste", function(t) {
                        Mt(i, t) || Qo(t, i) || (i.state.pasteIncoming = !0, r.fastPoll())
                    }), ns(o, "cut", e), ns(o, "copy", e), ns(t.scroller, "paste", function(e) {
                        Fe(t, e) || Mt(i, e) || (i.state.pasteIncoming = !0, r.focus())
                    }), ns(t.lineSpace, "selectstart", function(e) {
                        Fe(t, e) || Nt(e)
                    }), ns(o, "compositionstart", function() {
                        var t = i.getCursor("from");
                        r.composing && r.composing.range.clear(), r.composing = {
                            start: t,
                            range: i.markText(t, i.getCursor("to"), {
                                className: "CodeMirror-composing"
                            })
                        }
                    }), ns(o, "compositionend", function() {
                        r.composing && (r.poll(), r.composing.range.clear(), r.composing = null)
                    })
                }, nl.prototype.createField = function(t) {
                    this.wrapper = ra(), this.textarea = this.wrapper.firstChild
                }, nl.prototype.prepareSelection = function() {
                    var t = this.cm,
                        e = t.display,
                        n = t.doc,
                        r = On(t);
                    if (t.options.moveInputWithCursor) {
                        var i = un(t, n.sel.primary().head, "div"),
                            o = e.wrapper.getBoundingClientRect(),
                            a = e.lineDiv.getBoundingClientRect();
                        r.teTop = Math.max(0, Math.min(e.wrapper.clientHeight - 10, i.top + a.top - o.top)), r.teLeft = Math.max(0, Math.min(e.wrapper.clientWidth - 10, i.left + a.left - o.left))
                    }
                    return r
                }, nl.prototype.showSelection = function(t) {
                    var e = this.cm,
                        r = e.display;
                    n(r.cursorDiv, t.cursors), n(r.selectionDiv, t.selection), null != t.teTop && (this.wrapper.style.top = t.teTop + "px", this.wrapper.style.left = t.teLeft + "px")
                }, nl.prototype.reset = function(t) {
                    if (!this.contextMenuPending && !this.composing) {
                        var e = this.cm;
                        if (e.somethingSelected()) {
                            this.prevInput = "";
                            var n = e.getSelection();
                            this.textarea.value = n, e.state.focused && ja(this.textarea), ba && xa >= 9 && (this.hasSelection = n)
                        } else t || (this.prevInput = this.textarea.value = "", ba && xa >= 9 && (this.hasSelection = null))
                    }
                }, nl.prototype.getField = function() {
                    return this.textarea
                }, nl.prototype.supportsTouch = function() {
                    return !1
                }, nl.prototype.focus = function() {
                    if ("nocursor" != this.cm.options.readOnly && (!La || a() != this.textarea)) try {
                        this.textarea.focus()
                    } catch (t) {}
                }, nl.prototype.blur = function() {
                    this.textarea.blur()
                }, nl.prototype.resetPosition = function() {
                    this.wrapper.style.top = this.wrapper.style.left = 0
                }, nl.prototype.receivedFocus = function() {
                    this.slowPoll()
                }, nl.prototype.slowPoll = function() {
                    var t = this;
                    this.pollingFast || this.polling.set(this.cm.options.pollInterval, function() {
                        t.poll(), t.cm.state.focused && t.slowPoll()
                    })
                }, nl.prototype.fastPoll = function() {
                    function t() {
                        n.poll() || e ? (n.pollingFast = !1, n.slowPoll()) : (e = !0, n.polling.set(60, t))
                    }
                    var e = !1,
                        n = this;
                    n.pollingFast = !0, n.polling.set(20, t)
                }, nl.prototype.poll = function() {
                    var t = this,
                        e = this.cm,
                        n = this.textarea,
                        r = this.prevInput;
                    if (this.contextMenuPending || !e.state.focused || os(n) && !r && !this.composing || e.isReadOnly() || e.options.disableInput || e.state.keySeq) return !1;
                    var i = n.value;
                    if (i == r && !e.somethingSelected()) return !1;
                    if (ba && xa >= 9 && this.hasSelection === i || Aa && /[\uf700-\uf7ff]/.test(i)) return e.display.input.reset(), !1;
                    if (e.doc.sel == e.display.selForContextMenu) {
                        var o = i.charCodeAt(0);
                        if (8203 != o || r || (r = "​"), 8666 == o) return this.reset(), this.cm.execCommand("undo")
                    }
                    for (var a = 0, s = Math.min(r.length, i.length); a < s && r.charCodeAt(a) == i.charCodeAt(a);) ++a;
                    return pr(e, function() {
                        Zo(e, i.slice(a), r.length - a, null, t.composing ? "*compose" : null), i.length > 1e3 || i.indexOf("\n") > -1 ? n.value = t.prevInput = "" : t.prevInput = i, t.composing && (t.composing.range.clear(), t.composing.range = e.markText(t.composing.start, e.getCursor("to"), {
                            className: "CodeMirror-composing"
                        }))
                    }), !0
                }, nl.prototype.ensurePolled = function() {
                    this.pollingFast && this.poll() && (this.pollingFast = !1)
                }, nl.prototype.onKeyPress = function() {
                    ba && xa >= 9 && (this.hasSelection = null), this.fastPoll()
                }, nl.prototype.onContextMenu = function(t) {
                    function e() {
                        if (null != a.selectionStart) {
                            var t = i.somethingSelected(),
                                e = "​" + (t ? a.value : "");
                            a.value = "⇚", a.value = e, r.prevInput = t ? "" : "​", a.selectionStart = 1, a.selectionEnd = e.length, o.selForContextMenu = i.doc.sel
                        }
                    }

                    function n() {
                        if (r.contextMenuPending = !1, r.wrapper.style.cssText = u, a.style.cssText = c, ba && xa < 9 && o.scrollbars.setScrollTop(o.scroller.scrollTop = l), null != a.selectionStart) {
                            (!ba || ba && xa < 9) && e();
                            var t = 0,
                                n = function() {
                                    o.selForContextMenu == i.doc.sel && 0 == a.selectionStart && a.selectionEnd > 0 && "​" == r.prevInput ? mr(i, Oi)(i) : t++ < 10 ? o.detectingSelectAll = setTimeout(n, 500) : (o.selForContextMenu = null, o.input.reset())
                                };
                            o.detectingSelectAll = setTimeout(n, 200)
                        }
                    }
                    var r = this,
                        i = r.cm,
                        o = i.display,
                        a = r.textarea,
                        s = En(i, t),
                        l = o.scroller.scrollTop;
                    if (s && !_a) {
                        i.options.resetSelectionOnContextMenu && -1 == i.doc.sel.contains(s) && mr(i, wi)(i.doc, Hr(s), Ga);
                        var c = a.style.cssText,
                            u = r.wrapper.style.cssText;
                        r.wrapper.style.cssText = "position: absolute";
                        var f = r.wrapper.getBoundingClientRect();
                        a.style.cssText = "position: absolute; width: 30px; height: 30px;\n      top: " + (t.clientY - f.top - 5) + "px; left: " + (t.clientX - f.left - 5) + "px;\n      z-index: 1000; background: " + (ba ? "rgba(255, 255, 255, .05)" : "transparent") + ";\n      outline: none; border-width: 0; outline: none; overflow: hidden; opacity: .05; filter: alpha(opacity=5);";
                        var d;
                        if (wa && (d = window.scrollY), o.input.focus(), wa && window.scrollTo(null, d), o.input.reset(), i.somethingSelected() || (a.value = r.prevInput = " "), r.contextMenuPending = !0, o.selForContextMenu = i.doc.sel, clearTimeout(o.detectingSelectAll), ba && xa >= 9 && e(), Ra) {
                            Dt(t);
                            var h = function() {
                                Et(window, "mouseup", h), setTimeout(n, 20)
                            };
                            ns(window, "mouseup", h)
                        } else setTimeout(n, 50)
                    }
                }, nl.prototype.readOnlyChanged = function(t) {
                    t || this.reset(), this.textarea.disabled = "nocursor" == t
                }, nl.prototype.setUneditable = function() {}, nl.prototype.needsContentAttribute = !1,
                function(t) {
                    function e(e, r, i, o) {
                        t.defaults[e] = r, i && (n[e] = o ? function(t, e, n) {
                            n != Js && i(t, e, n)
                        } : i)
                    }
                    var n = t.optionHandlers;
                    t.defineOption = e, t.Init = Js, e("value", "", function(t, e) {
                        return t.setValue(e)
                    }, !0), e("mode", null, function(t, e) {
                        t.doc.modeOption = e, $r(t)
                    }, !0), e("indentUnit", 2, $r, !0), e("indentWithTabs", !1), e("smartIndent", !0), e("tabSize", 4, function(t) {
                        qr(t), nn(t), yr(t)
                    }, !0), e("lineSeparator", null, function(t, e) {
                        if (t.doc.lineSep = e, e) {
                            var n = [],
                                r = t.doc.first;
                            t.doc.iter(function(t) {
                                for (var i = 0;;) {
                                    var o = t.text.indexOf(e, i);
                                    if (-1 == o) break;
                                    i = o + e.length, n.push(D(r, o))
                                }
                                r++
                            });
                            for (var i = n.length - 1; i >= 0; i--) Ri(t.doc, e, n[i], D(n[i].line, n[i].ch + e.length))
                        }
                    }), e("specialChars", /[\u0000-\u001f\u007f-\u009f\u00ad\u061c\u200b-\u200f\u2028\u2029\ufeff]/g, function(t, e, n) {
                        t.state.specialChars = new RegExp(e.source + (e.test("\t") ? "" : "|\t"), "g"), n != Js && t.refresh()
                    }), e("specialCharPlaceholder", ue, function(t) {
                        return t.refresh()
                    }, !0), e("electricChars", !0), e("inputStyle", La ? "contenteditable" : "textarea", function() {
                        throw new Error("inputStyle can not (yet) be changed in a running editor")
                    }, !0), e("spellcheck", !1, function(t, e) {
                        return t.getInputField().spellcheck = e
                    }, !0), e("rtlMoveVisually", !Pa), e("wholeLineUpdateBefore", !0), e("theme", "default", function(t) {
                        Vo(t), Go(t)
                    }, !0), e("keyMap", "default", function(t, e, n) {
                        var r = fo(e),
                            i = n != Js && fo(n);
                        i && i.detach && i.detach(t, r), r.attach && r.attach(t, i || null)
                    }), e("extraKeys", null), e("configureMouse", null), e("lineWrapping", !1, $o, !0), e("gutters", [], function(t) {
                        Fr(t.options), Go(t)
                    }, !0), e("fixedGutter", !0, function(t, e) {
                        t.display.gutters.style.left = e ? kn(t.display) + "px" : "0", t.refresh()
                    }, !0), e("coverGutterNextToScrollbar", !1, function(t) {
                        return rr(t)
                    }, !0), e("scrollbarStyle", "native", function(t) {
                        or(t), rr(t), t.display.scrollbars.setScrollTop(t.doc.scrollTop), t.display.scrollbars.setScrollLeft(t.doc.scrollLeft)
                    }, !0), e("lineNumbers", !1, function(t) {
                        Fr(t.options), Go(t)
                    }, !0), e("firstLineNumber", 1, Go, !0), e("lineNumberFormatter", function(t) {
                        return t
                    }, Go, !0), e("showCursorWhenSelecting", !1, Mn, !0), e("resetSelectionOnContextMenu", !0), e("lineWiseCopyCut", !0), e("pasteLinesPerSelection", !0), e("readOnly", !1, function(t, e) {
                        "nocursor" == e && (Rn(t), t.display.input.blur()), t.display.input.readOnlyChanged(e)
                    }), e("disableInput", !1, function(t, e) {
                        e || t.display.input.reset()
                    }, !0), e("dragDrop", !0, Ko), e("allowDropFileTypes", null), e("cursorBlinkRate", 530), e("cursorScrollMargin", 0), e("cursorHeight", 1, Mn, !0), e("singleCursorHeightPerLine", !0, Mn, !0), e("workTime", 100), e("workDelay", 100), e("flattenSpans", !0, qr, !0), e("addModeClass", !1, qr, !0), e("pollInterval", 100), e("undoDepth", 200, function(t, e) {
                        return t.doc.history.undoDepth = e
                    }), e("historyEventDelay", 1250), e("viewportMargin", 10, function(t) {
                        return t.refresh()
                    }, !0), e("maxHighlightLength", 1e4, qr, !0), e("moveInputWithCursor", !0, function(t, e) {
                        e || t.display.input.resetPosition()
                    }), e("tabindex", null, function(t, e) {
                        return t.display.input.getField().tabIndex = e || ""
                    }), e("autofocus", null), e("direction", "ltr", function(t, e) {
                        return t.doc.setDirection(e)
                    }, !0), e("phrases", null)
                }(qo),
                function(t) {
                    var e = t.optionHandlers,
                        n = t.helpers = {};
                    t.prototype = {
                        constructor: t,
                        focus: function() {
                            window.focus(), this.display.input.focus()
                        },
                        setOption: function(t, n) {
                            var r = this.options,
                                i = r[t];
                            r[t] == n && "mode" != t || (r[t] = n, e.hasOwnProperty(t) && mr(this, e[t])(this, n, i), Tt(this, "optionChange", this, t))
                        },
                        getOption: function(t) {
                            return this.options[t]
                        },
                        getDoc: function() {
                            return this.doc
                        },
                        addKeyMap: function(t, e) {
                            this.state.keyMaps[e ? "push" : "unshift"](fo(t))
                        },
                        removeKeyMap: function(t) {
                            for (var e = this.state.keyMaps, n = 0; n < e.length; ++n)
                                if (e[n] == t || e[n].name == t) return e.splice(n, 1), !0
                        },
                        addOverlay: gr(function(e, n) {
                            var r = e.token ? e : t.getMode(this.options, e);
                            if (r.startState) throw new Error("Overlays may not be stateful.");
                            v(this.state.overlays, {
                                mode: r,
                                modeSpec: e,
                                opaque: n && n.opaque,
                                priority: n && n.priority || 0
                            }, function(t) {
                                return t.priority
                            }), this.state.modeGen++, yr(this)
                        }),
                        removeOverlay: gr(function(t) {
                            for (var e = this, n = this.state.overlays, r = 0; r < n.length; ++r) {
                                var i = n[r].modeSpec;
                                if (i == t || "string" == typeof t && i.name == t) return n.splice(r, 1), e.state.modeGen++, void yr(e)
                            }
                        }),
                        indentLine: gr(function(t, e, n) {
                            "string" != typeof e && "number" != typeof e && (e = null == e ? this.options.smartIndent ? "smart" : "prev" : e ? "add" : "subtract"), P(this.doc, t) && Jo(this, t, e, n)
                        }),
                        indentSelection: gr(function(t) {
                            for (var e = this, n = this.doc.sel.ranges, r = -1, i = 0; i < n.length; i++) {
                                var o = n[i];
                                if (o.empty()) o.head.line > r && (Jo(e, o.head.line, t, !0), r = o.head.line, i == e.doc.sel.primIndex && qn(e));
                                else {
                                    var a = o.from(),
                                        s = o.to(),
                                        l = Math.max(r, a.line);
                                    r = Math.min(e.lastLine(), s.line - (s.ch ? 0 : 1)) + 1;
                                    for (var c = l; c < r; ++c) Jo(e, c, t);
                                    var u = e.doc.sel.ranges;
                                    0 == a.ch && n.length == u.length && u[i].from().ch > 0 && vi(e.doc, i, new Ls(a, u[i].to()), Ga)
                                }
                            }
                        }),
                        getTokenAt: function(t, e) {
                            return ee(this, t, e)
                        },
                        getLineTokens: function(t, e) {
                            return ee(this, D(t), e, !0)
                        },
                        getTokenTypeAt: function(t) {
                            t = W(this.doc, t);
                            var e, n = Jt(this, T(this.doc, t.line)),
                                r = 0,
                                i = (n.length - 1) / 2,
                                o = t.ch;
                            if (0 == o) e = n[2];
                            else
                                for (;;) {
                                    var a = r + i >> 1;
                                    if ((a ? n[2 * a - 1] : 0) >= o) i = a;
                                    else {
                                        if (!(n[2 * a + 1] < o)) {
                                            e = n[2 * a + 2];
                                            break
                                        }
                                        r = a + 1
                                    }
                                }
                            var s = e ? e.indexOf("overlay ") : -1;
                            return s < 0 ? e : 0 == s ? null : e.slice(0, s - 1)
                        },
                        getModeAt: function(e) {
                            var n = this.doc.mode;
                            return n.innerMode ? t.innerMode(n, this.getTokenAt(e).state).mode : n
                        },
                        getHelper: function(t, e) {
                            return this.getHelpers(t, e)[0]
                        },
                        getHelpers: function(t, e) {
                            var r = this,
                                i = [];
                            if (!n.hasOwnProperty(e)) return i;
                            var o = n[e],
                                a = this.getModeAt(t);
                            if ("string" == typeof a[e]) o[a[e]] && i.push(o[a[e]]);
                            else if (a[e])
                                for (var s = 0; s < a[e].length; s++) {
                                    var l = o[a[e][s]];
                                    l && i.push(l)
                                } else a.helperType && o[a.helperType] ? i.push(o[a.helperType]) : o[a.name] && i.push(o[a.name]);
                            for (var c = 0; c < o._global.length; c++) {
                                var u = o._global[c];
                                u.pred(a, r) && -1 == d(i, u.val) && i.push(u.val)
                            }
                            return i
                        },
                        getStateAfter: function(t, e) {
                            var n = this.doc;
                            return t = H(n, null == t ? n.first + n.size - 1 : t), Xt(this, t + 1, e).state
                        },
                        cursorCoords: function(t, e) {
                            var n, r = this.doc.sel.primary();
                            return n = null == t ? r.head : "object" == typeof t ? W(this.doc, t) : t ? r.from() : r.to(), un(this, n, e || "page")
                        },
                        charCoords: function(t, e) {
                            return cn(this, W(this.doc, t), e || "page")
                        },
                        coordsChar: function(t, e) {
                            return t = ln(this, t, e || "page"), hn(this, t.left, t.top)
                        },
                        lineAtHeight: function(t, e) {
                            return t = ln(this, {
                                top: t,
                                left: 0
                            }, e || "page").top, N(this.doc, t + this.display.viewOffset)
                        },
                        heightAtLine: function(t, e, n) {
                            var r, i = !1;
                            if ("number" == typeof t) {
                                var o = this.doc.first + this.doc.size - 1;
                                t < this.doc.first ? t = this.doc.first : t > o && (t = o, i = !0), r = T(this.doc, t)
                            } else r = t;
                            return sn(this, r, {
                                top: 0,
                                left: 0
                            }, e || "page", n || i).top + (i ? this.doc.height - bt(r) : 0)
                        },
                        defaultTextHeight: function() {
                            return xn(this.display)
                        },
                        defaultCharWidth: function() {
                            return wn(this.display)
                        },
                        getViewport: function() {
                            return {
                                from: this.display.viewFrom,
                                to: this.display.viewTo
                            }
                        },
                        addWidget: function(t, e, n, r, i) {
                            var o = this.display;
                            t = un(this, W(this.doc, t));
                            var a = t.bottom,
                                s = t.left;
                            if (e.style.position = "absolute", e.setAttribute("cm-ignore-events", "true"), this.display.input.setUneditable(e), o.sizer.appendChild(e), "over" == r) a = t.top;
                            else if ("above" == r || "near" == r) {
                                var l = Math.max(o.wrapper.clientHeight, this.doc.height),
                                    c = Math.max(o.sizer.clientWidth, o.lineSpace.clientWidth);
                                ("above" == r || t.bottom + e.offsetHeight > l) && t.top > e.offsetHeight ? a = t.top - e.offsetHeight : t.bottom + e.offsetHeight <= l && (a = t.bottom), s + e.offsetWidth > c && (s = c - e.offsetWidth)
                            }
                            e.style.top = a + "px", e.style.left = e.style.right = "", "right" == i ? (s = o.sizer.clientWidth - e.offsetWidth, e.style.right = "0px") : ("left" == i ? s = 0 : "middle" == i && (s = (o.sizer.clientWidth - e.offsetWidth) / 2), e.style.left = s + "px"), n && Gn(this, {
                                left: s,
                                top: a,
                                right: s + e.offsetWidth,
                                bottom: a + e.offsetHeight
                            })
                        },
                        triggerOnKeyDown: gr(To),
                        triggerOnKeyPress: gr(Lo),
                        triggerOnKeyUp: Oo,
                        triggerOnMouseDown: gr(No),
                        execCommand: function(t) {
                            if (Vs.hasOwnProperty(t)) return Vs[t].call(null, this)
                        },
                        triggerElectric: gr(function(t) {
                            ta(this, t)
                        }),
                        findPosH: function(t, e, n, r) {
                            var i = this,
                                o = 1;
                            e < 0 && (o = -1, e = -e);
                            for (var a = W(this.doc, t), s = 0; s < e && (a = ia(i.doc, a, o, n, r), !a.hitSide); ++s);
                            return a
                        },
                        moveH: gr(function(t, e) {
                            var n = this;
                            this.extendSelectionsBy(function(r) {
                                return n.display.shift || n.doc.extend || r.empty() ? ia(n.doc, r.head, t, e, n.options.rtlMoveVisually) : t < 0 ? r.from() : r.to()
                            }, $a)
                        }),
                        deleteH: gr(function(t, e) {
                            var n = this.doc.sel,
                                r = this.doc;
                            n.somethingSelected() ? r.replaceSelection("", null, "+delete") : ho(this, function(n) {
                                var i = ia(r, n.head, t, e, !1);
                                return t < 0 ? {
                                    from: i,
                                    to: n.head
                                } : {
                                    from: n.head,
                                    to: i
                                }
                            })
                        }),
                        findPosV: function(t, e, n, r) {
                            var i = this,
                                o = 1,
                                a = r;
                            e < 0 && (o = -1, e = -e);
                            for (var s = W(this.doc, t), l = 0; l < e; ++l) {
                                var c = un(i, s, "div");
                                if (null == a ? a = c.left : c.left = a, s = oa(i, c, o, n), s.hitSide) break
                            }
                            return s
                        },
                        moveV: gr(function(t, e) {
                            var n = this,
                                r = this.doc,
                                i = [],
                                o = !this.display.shift && !r.extend && r.sel.somethingSelected();
                            if (r.extendSelectionsBy(function(a) {
                                    if (o) return t < 0 ? a.from() : a.to();
                                    var s = un(n, a.head, "div");
                                    null != a.goalColumn && (s.left = a.goalColumn), i.push(s.left);
                                    var l = oa(n, s, t, e);
                                    return "page" == e && a == r.sel.primary() && $n(n, cn(n, l, "div").top - s.top), l
                                }, $a), i.length)
                                for (var a = 0; a < r.sel.ranges.length; a++) r.sel.ranges[a].goalColumn = i[a]
                        }),
                        findWordAt: function(t) {
                            var e = this.doc,
                                n = T(e, t.line).text,
                                r = t.ch,
                                i = t.ch;
                            if (n) {
                                var o = this.getHelper(t, "wordChars");
                                "before" != t.sticky && i != n.length || !r ? ++i : --r;
                                for (var a = n.charAt(r), s = w(a, o) ? function(t) {
                                        return w(t, o)
                                    } : /\s/.test(a) ? function(t) {
                                        return /\s/.test(t)
                                    } : function(t) {
                                        return !/\s/.test(t) && !w(t)
                                    }; r > 0 && s(n.charAt(r - 1));) --r;
                                for (; i < n.length && s(n.charAt(i));) ++i
                            }
                            return new Ls(D(t.line, r), D(t.line, i))
                        },
                        toggleOverwrite: function(t) {
                            null != t && t == this.state.overwrite || ((this.state.overwrite = !this.state.overwrite) ? s(this.display.cursorDiv, "CodeMirror-overwrite") : za(this.display.cursorDiv, "CodeMirror-overwrite"), Tt(this, "overwriteToggle", this, this.state.overwrite))
                        },
                        hasFocus: function() {
                            return this.display.input.getField() == a()
                        },
                        isReadOnly: function() {
                            return !(!this.options.readOnly && !this.doc.cantEdit)
                        },
                        scrollTo: gr(function(t, e) {
                            Yn(this, t, e)
                        }),
                        getScrollInfo: function() {
                            var t = this.display.scroller;
                            return {
                                left: t.scrollLeft,
                                top: t.scrollTop,
                                height: t.scrollHeight - Be(this) - this.display.barHeight,
                                width: t.scrollWidth - Be(this) - this.display.barWidth,
                                clientHeight: We(this),
                                clientWidth: He(this)
                            }
                        },
                        scrollIntoView: gr(function(t, e) {
                            null == t ? (t = {
                                from: this.doc.sel.primary().head,
                                to: null
                            }, null == e && (e = this.options.cursorScrollMargin)) : "number" == typeof t ? t = {
                                from: D(t, 0),
                                to: null
                            } : null == t.from && (t = {
                                from: t,
                                to: null
                            }), t.to || (t.to = t.from), t.margin = e || 0, null != t.from.line ? Jn(this, t) : Zn(this, t.from, t.to, t.margin)
                        }),
                        setSize: gr(function(t, e) {
                            var n = this,
                                r = function(t) {
                                    return "number" == typeof t || /^\d+$/.test(String(t)) ? t + "px" : t
                                };
                            null != t && (this.display.wrapper.style.width = r(t)), null != e && (this.display.wrapper.style.height = r(e)), this.options.lineWrapping && en(this);
                            var i = this.display.viewFrom;
                            this.doc.iter(i, this.display.viewTo, function(t) {
                                if (t.widgets)
                                    for (var e = 0; e < t.widgets.length; e++)
                                        if (t.widgets[e].noHScroll) {
                                            br(n, i, "widget");
                                            break
                                        }++i
                            }), this.curOp.forceUpdate = !0, Tt(this, "refresh", this)
                        }),
                        operation: function(t) {
                            return pr(this, t)
                        },
                        startOperation: function() {
                            return ar(this)
                        },
                        endOperation: function() {
                            return sr(this)
                        },
                        refresh: gr(function() {
                            var t = this.display.cachedTextHeight;
                            yr(this), this.curOp.forceUpdate = !0, nn(this), Yn(this, this.doc.scrollLeft, this.doc.scrollTop), Pr(this), (null == t || Math.abs(t - xn(this.display)) > .5) && Sn(this), Tt(this, "refresh", this)
                        }),
                        swapDoc: gr(function(t) {
                            var e = this.doc;
                            return e.cm = null, Zr(this, t), nn(this), this.display.input.reset(), Yn(this, t.scrollLeft, t.scrollTop), this.curOp.forceScroll = !0, we(this, "swapDoc", this, e), e
                        }),
                        phrase: function(t) {
                            var e = this.options.phrases;
                            return e && Object.prototype.hasOwnProperty.call(e, t) ? e[t] : t
                        },
                        getInputField: function() {
                            return this.display.input.getField()
                        },
                        getWrapperElement: function() {
                            return this.display.wrapper
                        },
                        getScrollerElement: function() {
                            return this.display.scroller
                        },
                        getGutterElement: function() {
                            return this.display.gutters
                        }
                    }, At(t), t.registerHelper = function(e, r, i) {
                        n.hasOwnProperty(e) || (n[e] = t[e] = {
                            _global: []
                        }), n[e][r] = i
                    }, t.registerGlobalHelper = function(e, r, i, o) {
                        t.registerHelper(e, r, o), n[e]._global.push({
                            pred: i,
                            val: o
                        })
                    }
                }(qo);
            var rl = "iter insert remove copy getEditor constructor".split(" ");
            for (var il in Fs.prototype) Fs.prototype.hasOwnProperty(il) && d(rl, il) < 0 && (qo.prototype[il] = function(t) {
                return function() {
                    return t.apply(this.doc, arguments)
                }
            }(Fs.prototype[il]));
            return At(Fs), qo.inputStyles = {
                    textarea: nl,
                    contenteditable: el
                }, qo.defineMode = function(t) {
                    qo.defaults.mode || "null" == t || (qo.defaults.mode = t), Ht.apply(this, arguments)
                }, qo.defineMIME = Wt, qo.defineMode("null", function() {
                    return {
                        token: function(t) {
                            return t.skipToEnd()
                        }
                    }
                }), qo.defineMIME("text/plain", "null"), qo.defineExtension = function(t, e) {
                    qo.prototype[t] = e
                }, qo.defineDocExtension = function(t, e) {
                    Fs.prototype[t] = e
                }, qo.fromTextArea = da,
                function(t) {
                    t.off = Et, t.on = ns, t.wheelEventPixels = zr, t.Doc = Fs, t.splitLines = is, t.countColumn = f, t.findColumn = h, t.isWordChar = x, t.Pass = Va, t.signal = Tt, t.Line = ms, t.changeEnd = Wr, t.scrollbarModel = _s, t.Pos = D, t.cmpPos = F, t.modes = ls, t.mimeModes = cs, t.resolveMode = Ut, t.getMode = Vt, t.modeExtensions = us, t.extendMode = Gt, t.copyState = Kt, t.startState = qt, t.innerMode = $t, t.commands = Vs, t.keyMap = Us, t.keyName = uo, t.isModifierKey = lo, t.lookupKey = so, t.normalizeKeyMap = ao, t.StringStream = fs, t.SharedTextMarker = Is, t.TextMarker = Ps, t.LineWidget = As, t.e_preventDefault = Nt, t.e_stopPropagation = Pt, t.e_stop = Dt, t.addClass = s, t.contains = o, t.rmClass = za, t.keyNames = js
                }(qo), qo.version = "5.40.0", qo
        })
    }, function(t, e, n) {
        "use strict";
        var r = n(6);
        t.exports = function(t) {
            if (!r(t)) throw new TypeError("Cannot use null or undefined");
            return t
        }
    }, function(t, e, n) {
        "use strict";
        t.exports = function(t) {
            if ("function" != typeof t) throw new TypeError(t + " is not a function");
            return t
        }
    }, function(t, e, n) {
        "use strict";
        t.exports = n(67)() ? Symbol : n(68)
    }, function(t, e, n) {
        "use strict";
        var r, i = n(15),
            o = n(24),
            a = n(64),
            s = n(16);
        r = t.exports = function(t, e) {
            var n, r, a, l, c;
            return arguments.length < 2 || "string" != typeof t ? (l = e, e = t, t = null) : l = arguments[2], null == t ? (n = a = !0, r = !1) : (n = s.call(t, "c"), r = s.call(t, "e"), a = s.call(t, "w")), c = {
                value: e,
                configurable: n,
                enumerable: r,
                writable: a
            }, l ? i(o(l), c) : c
        }, r.gs = function(t, e, n) {
            var r, l, c, u;
            return "string" != typeof t ? (c = n, n = e, e = t, t = null) : c = arguments[3], null == e ? e = void 0 : a(e) ? null == n ? n = void 0 : a(n) || (c = n, n = void 0) : (c = e, e = n = void 0), null == t ? (r = !0, l = !1) : (r = s.call(t, "c"), l = s.call(t, "e")), u = {
                get: e,
                set: n,
                configurable: r,
                enumerable: l
            }, c ? i(o(c), u) : u
        }
    }, function(t, e, n) {
        function r(t) {
            return i(t).replace(/[\W_]+(.|$)/g, function(t, e) {
                return e ? " " + e : ""
            }).trim()
        }
        var i = n(18);
        t.exports = r
    }, function(t, e, n) {
        "use strict";
        var r = n(19)();
        t.exports = function(t) {
            return t !== r && null !== t
        }
    }, function(t, e, n) {
        "use strict";
        t.exports = n(22)() ? Object.setPrototypeOf : n(23)
    }, function(t, e) {
        function n(t, e) {
            var n = t[1] || "",
                i = t[3];
            if (!i) return n;
            if (e && "function" == typeof btoa) {
                var o = r(i);
                return [n].concat(i.sources.map(function(t) {
                    return "/*# sourceURL=" + i.sourceRoot + t + " */"
                })).concat([o]).join("\n")
            }
            return [n].join("\n")
        }

        function r(t) {
            return "/*# sourceMappingURL=data:application/json;charset=utf-8;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(t)))) + " */"
        }
        t.exports = function(t) {
            var e = [];
            return e.toString = function() {
                return this.map(function(e) {
                    var r = n(e, t);
                    return e[2] ? "@media " + e[2] + "{" + r + "}" : r
                }).join("")
            }, e.i = function(t, n) {
                "string" == typeof t && (t = [
                    [null, t, ""]
                ]);
                for (var r = {}, i = 0; i < this.length; i++) {
                    var o = this[i][0];
                    "number" == typeof o && (r[o] = !0)
                }
                for (i = 0; i < t.length; i++) {
                    var a = t[i];
                    "number" == typeof a[0] && r[a[0]] || (n && !a[2] ? a[2] = n : n && (a[2] = "(" + a[2] + ") and (" + n + ")"), e.push(a))
                }
            }, e
        }
    }, function(t, e, n) {
        "use strict";
        var r = Object.prototype.toString,
            i = r.call(function() {
                return arguments
            }());
        t.exports = function(t) {
            return r.call(t) === i
        }
    }, function(t, e, n) {
        "use strict";
        var r = Object.prototype.toString,
            i = r.call("");
        t.exports = function(t) {
            return "string" == typeof t || t && "object" == typeof t && (t instanceof String || r.call(t) === i) || !1
        }
    }, function(t, e, n) {
        "use strict";
        var r, i = n(14),
            o = n(15),
            a = n(2),
            s = n(1),
            l = n(4),
            c = n(74),
            u = n(3),
            f = Object.defineProperty,
            d = Object.defineProperties;
        t.exports = r = function(t, e) {
            if (!(this instanceof r)) throw new TypeError("Constructor requires 'new'");
            d(this, {
                __list__: l("w", s(t)),
                __context__: l("w", e),
                __nextIndex__: l("w", 0)
            }), e && (a(e.on), e.on("_add", this._onAdd), e.on("_delete", this._onDelete), e.on("_clear", this._onClear))
        }, delete r.prototype.constructor, d(r.prototype, o({
            _next: l(function() {
                var t;
                if (this.__list__) return this.__redo__ && void 0 !== (t = this.__redo__.shift()) ? t : this.__nextIndex__ < this.__list__.length ? this.__nextIndex__++ : void this._unBind()
            }),
            next: l(function() {
                return this._createResult(this._next())
            }),
            _createResult: l(function(t) {
                return void 0 === t ? {
                    done: !0,
                    value: void 0
                } : {
                    done: !1,
                    value: this._resolve(t)
                }
            }),
            _resolve: l(function(t) {
                return this.__list__[t]
            }),
            _unBind: l(function() {
                this.__list__ = null, delete this.__redo__, this.__context__ && (this.__context__.off("_add", this._onAdd), this.__context__.off("_delete", this._onDelete), this.__context__.off("_clear", this._onClear), this.__context__ = null)
            }),
            toString: l(function() {
                return "[object " + (this[u.toStringTag] || "Object") + "]"
            })
        }, c({
            _onAdd: l(function(t) {
                if (!(t >= this.__nextIndex__)) {
                    if (++this.__nextIndex__, !this.__redo__) return void f(this, "__redo__", l("c", [t]));
                    this.__redo__.forEach(function(e, n) {
                        e >= t && (this.__redo__[n] = ++e)
                    }, this), this.__redo__.push(t)
                }
            }),
            _onDelete: l(function(t) {
                var e;
                t >= this.__nextIndex__ || (--this.__nextIndex__, this.__redo__ && (e = this.__redo__.indexOf(t), -1 !== e && this.__redo__.splice(e, 1), this.__redo__.forEach(function(e, n) {
                    e > t && (this.__redo__[n] = --e)
                }, this)))
            }),
            _onClear: l(function() {
                this.__redo__ && i.call(this.__redo__), this.__nextIndex__ = 0
            })
        }))), f(r.prototype, u.iterator, l(function() {
            return this
        }))
    }, function(t, e, n) {
        ! function(e) {
            function n() {
                this.parent = null, this.nested = [], this.nodes = [], this.filters = null, this.directives = null, this.context = null, this.unbind = null, this.onRender = null, this.onUpdate = null, this.onRemove = null, this.noCache = !1
            }

            function r() {
                this.items = Object.create(null), this.length = 0, this.next = 0
            }

            function i(t, e, n, r) {
                if (r) {
                    var i = {
                        __index__: n
                    };
                    return i[r.value] = t[n], r.key && (i[r.key] = n), i
                }
                return t[n]
            }

            function o(t, e, n, r) {
                if (r) {
                    var i = {
                        __index__: n
                    };
                    return i[r.value] = t[e[n]], r.key && (i[r.key] = e[n]), i
                }
                return t[e[n]]
            }
            n.render = function(t, e, n) {
                var r;
                return r = n && n.noCache ? new t : t.pool.pop() || new t, 8 == e.nodeType ? r.insertBefore(e) : r.appendTo(e), n && (n.parent && (r.parent = n.parent), n.context && (r.context = n.context), n.filters && (r.filters = n.filters), n.directives && (r.directives = n.directives), n.noCache && (r.noCache = n.noCache)), r.onRender && r.onRender(), r
            }, n.prerender = function(t, e) {
                for (; e--;) t.pool.push(new t)
            }, n.loop = function(t, e, r, a, s, l) {
                var c, u, f, d, h, p, m = r.length;
                Array.isArray(s) ? (h = i, p = s.length) : (h = o, d = Object.keys(s), p = d.length), f = m - p;
                for (c in r.items) {
                    if (!(f-- > 0)) break;
                    r.items[c].remove()
                }
                u = 0;
                for (c in r.items) r.items[c].__state__ = h(s, d, u, l), u++;
                for (u = m, f = p; u < f; u++) {
                    var g = n.render(a, e, {
                        parent: t,
                        context: t.context,
                        filters: t.filters,
                        directives: t.directives,
                        noCache: t.noCache
                    });
                    t.nested.push(g), c = r.push(g), g.unbind = function(t) {
                        return function() {
                            r.remove(t)
                        }
                    }(c), g.__state__ = h(s, d, u, l)
                }
            }, n.cond = function(t, e, r, i, o) {
                if (r.ref) o || r.ref.remove();
                else if (o) {
                    var a = n.render(i, e, {
                        parent: t,
                        context: t.context,
                        filters: t.filters,
                        directives: t.directives,
                        noCache: t.noCache
                    });
                    t.nested.push(a), r.ref = a, a.unbind = function() {
                        r.ref = null
                    }
                }
                return o
            }, n.insert = function(t, e, r, i, o) {
                if (r.ref) r.ref.update(o);
                else {
                    var a = n.render(i, e, {
                        parent: t,
                        context: t.context,
                        filters: t.filters,
                        directives: t.directives,
                        noCache: t.noCache
                    });
                    t.nested.push(a), r.ref = a, a.unbind = function() {
                        r.ref = null
                    }, a.update(o)
                }
            }, n.prototype.remove = function() {
                for (var t = this.nodes.length; t--;) this.nodes[t].parentNode.removeChild(this.nodes[t]);
                for (this.unbind && this.unbind(), t = this.nested.length; t--;) this.nested[t].remove();
                this.parent && (t = this.parent.nested.indexOf(this), this.parent.nested.splice(t, 1), this.parent = null), this.onRemove && this.onRemove(), this.noCache || this.constructor.pool.push(this)
            }, n.prototype.appendTo = function(t) {
                for (var e = 0, n = this.nodes.length; e < n; e++) t.appendChild(this.nodes[e])
            }, n.prototype.insertBefore = function(t) {
                if (!t.parentNode) throw new Error("Can not insert child view into parent node. You need append your view first and then update.");
                for (var e = 0, n = this.nodes.length; e < n; e++) t.parentNode.insertBefore(this.nodes[e], t)
            }, n.prototype.createDocument = function() {
                if (1 == this.nodes.length) return this.nodes[0];
                for (var t = e.createDocumentFragment(), n = 0, r = this.nodes.length; n < r; n++) t.appendChild(this.nodes[n]);
                return t
            }, n.prototype.querySelector = function(t) {
                for (var e = 0; e < this.nodes.length; e++) {
                    if (this.nodes[e].matches && this.nodes[e].matches(t)) return this.nodes[e];
                    if (8 === this.nodes[e].nodeType) throw new Error("Can not use querySelector with non-element nodes on first level.");
                    if (this.nodes[e].querySelector) {
                        var n = this.nodes[e].querySelector(t);
                        if (n) return n
                    }
                }
                return null
            }, r.prototype.push = function(t) {
                return this.items[this.next] = t, this.length += 1, this.next += 1, this.next - 1
            }, r.prototype.remove = function(t) {
                if (!(t in this.items)) throw new Error('You are trying to delete not existing element "' + t + '" form map.');
                delete this.items[t], this.length -= 1
            }, r.prototype.forEach = function(t) {
                for (var e in this.items) t(this.items[e])
            }, n.Map = r, t.exports = n
        }(window.document)
    }, function(t, e, n) {
        "use strict";

        function r(t) {
            return !!t && "object" == typeof t
        }

        function i(t) {
            var e = Object.prototype.toString.call(t);
            return "[object RegExp]" === e || "[object Date]" === e || o(t)
        }

        function o(t) {
            return t.$$typeof === h
        }

        function a(t) {
            return Array.isArray(t) ? [] : {}
        }

        function s(t, e) {
            return e && !0 === e.clone && f(t) ? u(a(t), t, e) : t
        }

        function l(t, e, n) {
            var r = t.slice();
            return e.forEach(function(e, i) {
                void 0 === r[i] ? r[i] = s(e, n) : f(e) ? r[i] = u(t[i], e, n) : -1 === t.indexOf(e) && r.push(s(e, n))
            }), r
        }

        function c(t, e, n) {
            var r = {};
            return f(t) && Object.keys(t).forEach(function(e) {
                r[e] = s(t[e], n)
            }), Object.keys(e).forEach(function(i) {
                f(e[i]) && t[i] ? r[i] = u(t[i], e[i], n) : r[i] = s(e[i], n)
            }), r
        }

        function u(t, e, n) {
            var r = Array.isArray(e),
                i = Array.isArray(t),
                o = n || {
                    arrayMerge: l
                };
            if (r === i) return r ? (o.arrayMerge || l)(t, e, n) : c(t, e, n);
            return s(e, n)
        }
        var f = function(t) {
                return r(t) && !i(t)
            },
            d = "function" == typeof Symbol && Symbol.for,
            h = d ? Symbol.for("react.element") : 60103;
        u.all = function(t, e) {
            if (!Array.isArray(t) || t.length < 2) throw new Error("first argument should be an array with at least two elements");
            return t.reduce(function(t, n) {
                return u(t, n, e)
            })
        };
        var p = u;
        t.exports = p
    }, function(t, e, n) {
        "use strict";
        var r = n(1);
        t.exports = function() {
            return r(this).length = 0, this
        }
    }, function(t, e, n) {
        "use strict";
        t.exports = n(59)() ? Object.assign : n(60)
    }, function(t, e, n) {
        "use strict";
        t.exports = n(65)() ? String.prototype.contains : n(66)
    }, function(t, e, n) {
        "use strict";
        var r = n(71);
        t.exports = function(t) {
            if (!r(t)) throw new TypeError(t + " is not iterable");
            return t
        }
    }, function(t, e) {
        function n(t) {
            return o.test(t) ? t.toLowerCase() : a.test(t) ? (r(t) || t).toLowerCase() : s.test(t) ? i(t).toLowerCase() : t.toLowerCase()
        }

        function r(t) {
            return t.replace(l, function(t, e) {
                return e ? " " + e : ""
            })
        }

        function i(t) {
            return t.replace(c, function(t, e, n) {
                return e + " " + n.toLowerCase().split("").join(" ")
            })
        }
        t.exports = n;
        var o = /\s/,
            a = /(_|-|\.|:)/,
            s = /([a-z][A-Z]|[A-Z][a-z])/,
            l = /[\W_]+(.|$)/g,
            c = /(.)([A-Z]+)/g
    }, function(t, e, n) {
        "use strict";
        t.exports = function() {}
    }, function(t, e, n) {
        "use strict";
        var r = n(50),
            i = n(21),
            o = n(1),
            a = Array.prototype.indexOf,
            s = Object.prototype.hasOwnProperty,
            l = Math.abs,
            c = Math.floor;
        t.exports = function(t) {
            var e, n, u, f;
            if (!r(t)) return a.apply(this, arguments);
            for (n = i(o(this).length), u = arguments[1], u = isNaN(u) ? 0 : u >= 0 ? c(u) : i(this.length) - c(l(u)), e = u; e < n; ++e)
                if (s.call(this, e) && (f = this[e], r(f))) return e;
            return -1
        }
    }, function(t, e, n) {
        "use strict";
        var r = n(53),
            i = Math.max;
        t.exports = function(t) {
            return i(0, r(t))
        }
    }, function(t, e, n) {
        "use strict";
        var r = Object.create,
            i = Object.getPrototypeOf,
            o = {};
        t.exports = function() {
            var t = Object.setPrototypeOf,
                e = arguments[0] || r;
            return "function" == typeof t && i(t(e(null), o)) === o
        }
    }, function(t, e, n) {
        "use strict";
        var r, i = n(57),
            o = n(1),
            a = Object.prototype.isPrototypeOf,
            s = Object.defineProperty,
            l = {
                configurable: !0,
                enumerable: !1,
                writable: !0,
                value: void 0
            };
        r = function(t, e) {
            if (o(t), null === e || i(e)) return t;
            throw new TypeError("Prototype must be null or an object")
        }, t.exports = function(t) {
            var e, n;
            return t ? (2 === t.level ? t.set ? (n = t.set, e = function(t, e) {
                return n.call(r(t, e), e), t
            }) : e = function(t, e) {
                return r(t, e).__proto__ = e, t
            } : e = function t(e, n) {
                var i;
                return r(e, n), i = a.call(t.nullPolyfill, e), i && delete t.nullPolyfill.__proto__, null === n && (n = t.nullPolyfill), e.__proto__ = n, i && s(t.nullPolyfill, "__proto__", l), e
            }, Object.defineProperty(e, "level", {
                configurable: !1,
                enumerable: !1,
                writable: !1,
                value: t.level
            })) : null
        }(function() {
            var t, e = Object.create(null),
                n = {},
                r = Object.getOwnPropertyDescriptor(Object.prototype, "__proto__");
            if (r) {
                try {
                    t = r.set, t.call(e, n)
                } catch (t) {}
                if (Object.getPrototypeOf(e) === n) return {
                    set: t,
                    level: 2
                }
            }
            return e.__proto__ = n, Object.getPrototypeOf(e) === n ? {
                level: 2
            } : (e = {}, e.__proto__ = n, Object.getPrototypeOf(e) === n && {
                level: 1
            })
        }()), n(58)
    }, function(t, e, n) {
        "use strict";
        var r = n(6),
            i = Array.prototype.forEach,
            o = Object.create,
            a = function(t, e) {
                var n;
                for (n in t) e[n] = t[n]
            };
        t.exports = function(t) {
            var e = o(null);
            return i.call(arguments, function(t) {
                r(t) && a(Object(t), e)
            }), e
        }
    }, function(t, e, n) {
        "use strict";
        var r, i, o, a, s, l, c, u = n(4),
            f = n(2),
            d = Function.prototype.apply,
            h = Function.prototype.call,
            p = Object.create,
            m = Object.defineProperty,
            g = Object.defineProperties,
            v = Object.prototype.hasOwnProperty,
            y = {
                configurable: !0,
                enumerable: !1,
                writable: !0
            };
        r = function(t, e) {
            var n;
            return f(e), v.call(this, "__ee__") ? n = this.__ee__ : (n = y.value = p(null), m(this, "__ee__", y), y.value = null), n[t] ? "object" == typeof n[t] ? n[t].push(e) : n[t] = [n[t], e] : n[t] = e, this
        }, i = function(t, e) {
            var n, i;
            return f(e), i = this, r.call(this, t, n = function() {
                o.call(i, t, n), d.call(e, this, arguments)
            }), n.__eeOnceListener__ = e, this
        }, o = function(t, e) {
            var n, r, i, o;
            if (f(e), !v.call(this, "__ee__")) return this;
            if (n = this.__ee__, !n[t]) return this;
            if ("object" == typeof(r = n[t]))
                for (o = 0; i = r[o]; ++o) i !== e && i.__eeOnceListener__ !== e || (2 === r.length ? n[t] = r[o ? 0 : 1] : r.splice(o, 1));
            else r !== e && r.__eeOnceListener__ !== e || delete n[t];
            return this
        }, a = function(t) {
            var e, n, r, i, o;
            if (v.call(this, "__ee__") && (i = this.__ee__[t]))
                if ("object" == typeof i) {
                    for (n = arguments.length, o = new Array(n - 1), e = 1; e < n; ++e) o[e - 1] = arguments[e];
                    for (i = i.slice(), e = 0; r = i[e]; ++e) d.call(r, this, o)
                } else switch (arguments.length) {
                    case 1:
                        h.call(i, this);
                        break;
                    case 2:
                        h.call(i, this, arguments[1]);
                        break;
                    case 3:
                        h.call(i, this, arguments[1], arguments[2]);
                        break;
                    default:
                        for (n = arguments.length, o = new Array(n - 1), e = 1; e < n; ++e) o[e - 1] = arguments[e];
                        d.call(i, this, o)
                }
        }, s = {
            on: r,
            once: i,
            off: o,
            emit: a
        }, l = {
            on: u(r),
            once: u(i),
            off: u(o),
            emit: u(a)
        }, c = g({}, l), t.exports = e = function(t) {
            return null == t ? p(c) : g(Object(t), l)
        }, e.methods = s
    }, function(t, e, n) {
        "use strict";
        var r = n(9),
            i = n(2),
            o = n(10),
            a = n(72),
            s = Array.isArray,
            l = Function.prototype.call,
            c = Array.prototype.some;
        t.exports = function(t, e) {
            var n, u, f, d, h, p, m, g, v = arguments[2];
            if (s(t) || r(t) ? n = "array" : o(t) ? n = "string" : t = a(t), i(e), f = function() {
                    d = !0
                }, "array" === n) return void c.call(t, function(t) {
                return l.call(e, v, t, f), d
            });
            if ("string" !== n)
                for (u = t.next(); !u.done;) {
                    if (l.call(e, v, u.value, f), d) return;
                    u = t.next()
                } else
                    for (p = t.length, h = 0; h < p && (m = t[h], h + 1 < p && (g = m.charCodeAt(0)) >= 55296 && g <= 56319 && (m += t[++h]), l.call(e, v, m, f), !d); ++h);
        }
    }, function(t, e, n) {
        function r(t) {
            return i(t).replace(/\s/g, "_")
        }
        var i = n(5);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return i(t).replace(/[a-z]/i, function(t) {
                return t.toUpperCase()
            }).trim()
        }
        var i = n(18);
        t.exports = r
    }, function(t, e, n) {
        function r(t, e) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n],
                    i = p[r.id];
                if (i) {
                    i.refs++;
                    for (var o = 0; o < i.parts.length; o++) i.parts[o](r.parts[o]);
                    for (; o < r.parts.length; o++) i.parts.push(u(r.parts[o], e))
                } else {
                    for (var a = [], o = 0; o < r.parts.length; o++) a.push(u(r.parts[o], e));
                    p[r.id] = {
                        id: r.id,
                        refs: 1,
                        parts: a
                    }
                }
            }
        }

        function i(t, e) {
            for (var n = [], r = {}, i = 0; i < t.length; i++) {
                var o = t[i],
                    a = e.base ? o[0] + e.base : o[0],
                    s = o[1],
                    l = o[2],
                    c = o[3],
                    u = {
                        css: s,
                        media: l,
                        sourceMap: c
                    };
                r[a] ? r[a].parts.push(u) : n.push(r[a] = {
                    id: a,
                    parts: [u]
                })
            }
            return n
        }

        function o(t, e) {
            var n = g(t.insertInto);
            if (!n) throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
            var r = b[b.length - 1];
            if ("top" === t.insertAt) r ? r.nextSibling ? n.insertBefore(e, r.nextSibling) : n.appendChild(e) : n.insertBefore(e, n.firstChild), b.push(e);
            else {
                if ("bottom" !== t.insertAt) throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
                n.appendChild(e)
            }
        }

        function a(t) {
            if (null === t.parentNode) return !1;
            t.parentNode.removeChild(t);
            var e = b.indexOf(t);
            e >= 0 && b.splice(e, 1)
        }

        function s(t) {
            var e = document.createElement("style");
            return t.attrs.type = "text/css", c(e, t.attrs), o(t, e), e
        }

        function l(t) {
            var e = document.createElement("link");
            return t.attrs.type = "text/css", t.attrs.rel = "stylesheet", c(e, t.attrs), o(t, e), e
        }

        function c(t, e) {
            Object.keys(e).forEach(function(n) {
                t.setAttribute(n, e[n])
            })
        }

        function u(t, e) {
            var n, r, i, o;
            if (e.transform && t.css) {
                if (!(o = e.transform(t.css))) return function() {};
                t.css = o
            }
            if (e.singleton) {
                var c = y++;
                n = v || (v = s(e)), r = f.bind(null, n, c, !1), i = f.bind(null, n, c, !0)
            } else t.sourceMap && "function" == typeof URL && "function" == typeof URL.createObjectURL && "function" == typeof URL.revokeObjectURL && "function" == typeof Blob && "function" == typeof btoa ? (n = l(e), r = h.bind(null, n, e), i = function() {
                a(n), n.href && URL.revokeObjectURL(n.href)
            }) : (n = s(e), r = d.bind(null, n), i = function() {
                a(n)
            });
            return r(t),
                function(e) {
                    if (e) {
                        if (e.css === t.css && e.media === t.media && e.sourceMap === t.sourceMap) return;
                        r(t = e)
                    } else i()
                }
        }

        function f(t, e, n, r) {
            var i = n ? "" : r.css;
            if (t.styleSheet) t.styleSheet.cssText = w(e, i);
            else {
                var o = document.createTextNode(i),
                    a = t.childNodes;
                a[e] && t.removeChild(a[e]), a.length ? t.insertBefore(o, a[e]) : t.appendChild(o)
            }
        }

        function d(t, e) {
            var n = e.css,
                r = e.media;
            if (r && t.setAttribute("media", r), t.styleSheet) t.styleSheet.cssText = n;
            else {
                for (; t.firstChild;) t.removeChild(t.firstChild);
                t.appendChild(document.createTextNode(n))
            }
        }

        function h(t, e, n) {
            var r = n.css,
                i = n.sourceMap,
                o = void 0 === e.convertToAbsoluteUrls && i;
            (e.convertToAbsoluteUrls || o) && (r = x(r)), i && (r += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(i)))) + " */");
            var a = new Blob([r], {
                    type: "text/css"
                }),
                s = t.href;
            t.href = URL.createObjectURL(a), s && URL.revokeObjectURL(s)
        }
        var p = {},
            m = function(t) {
                var e;
                return function() {
                    return void 0 === e && (e = t.apply(this, arguments)), e
                }
            }(function() {
                return window && document && document.all && !window.atob
            }),
            g = function(t) {
                var e = {};
                return function(n) {
                    return void 0 === e[n] && (e[n] = t.call(this, n)), e[n]
                }
            }(function(t) {
                return document.querySelector(t)
            }),
            v = null,
            y = 0,
            b = [],
            x = n(114);
        t.exports = function(t, e) {
            if ("undefined" != typeof DEBUG && DEBUG && "object" != typeof document) throw new Error("The style-loader cannot be used in a non-browser environment");
            e = e || {}, e.attrs = "object" == typeof e.attrs ? e.attrs : {}, e.singleton || (e.singleton = m()), e.insertInto || (e.insertInto = "head"), e.insertAt || (e.insertAt = "bottom");
            var n = i(t, e);
            return r(n, e),
                function(t) {
                    for (var o = [], a = 0; a < n.length; a++) {
                        var s = n[a],
                            l = p[s.id];
                        l.refs--, o.push(l)
                    }
                    if (t) {
                        r(i(t, e), e)
                    }
                    for (var a = 0; a < o.length; a++) {
                        var l = o[a];
                        if (0 === l.refs) {
                            for (var c = 0; c < l.parts.length; c++) l.parts[c]();
                            delete p[l.id]
                        }
                    }
                }
        };
        var w = function() {
            var t = [];
            return function(e, n) {
                return t[e] = n, t.filter(Boolean).join("\n")
            }
        }()
    }, function(t, e) {
        t.exports = function(t, e, n) {
            function r() {
                var c = Date.now() - s;
                c < e && c >= 0 ? i = setTimeout(r, e - c) : (i = null, n || (l = t.apply(a, o), a = o = null))
            }
            var i, o, a, s, l;
            null == e && (e = 100);
            var c = function() {
                a = this, o = arguments, s = Date.now();
                var c = n && !i;
                return i || (i = setTimeout(r, e)), c && (l = t.apply(a, o), a = o = null), l
            };
            return c.clear = function() {
                i && (clearTimeout(i), i = null)
            }, c.flush = function() {
                i && (l = t.apply(a, o), a = o = null, clearTimeout(i), i = null)
            }, c
        }
    }, function(t, e) {
        t.exports = "\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='16' height='16' viewBox='0 0 16 16'%3E %3Ctitle%3Eicons_all_sprite%3C/title%3E %3Cg id='Layer_4' data-name='Layer 4'%3E %3Cpolygon points='13.5 12.125 9.375 8 13.5 3.875 12.125 2.5 8 6.625 3.875 2.5 2.5 3.875 6.625 8 2.5 12.125 3.875 13.5 8 9.375 12.125 13.5 13.5 12.125' fill='%23ffffff' opacity='0.9'/%3E %3C/g%3E %3C/svg%3E\""
    }, function(t, e, n) {
        "use strict";

        function r(t) {
            return Array.prototype.slice.call(t, 0)
        }

        function i(t) {
            var e = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
            if (!t.attributes) return {};
            var n = r(t.attributes).map(function(t) {
                return {
                    name: t.name,
                    value: t.value
                }
            }).filter(function(t) {
                return -1 !== t.name.indexOf("data-")
            }).reduce(function(t, e) {
                return t[U.a.camel(e.name.replace("data-", ""))] = e.value, t
            }, {});
            return e ? F.a.all([H, n || {}]) : n
        }

        function o() {
            var t = document.getElementsByTagName("script");
            return t[t.length - 1] || null
        }

        function a(t, e) {
            e.parentNode.insertBefore(t, e.nextSibling)
        }

        function s(t) {
            var e = {
                    "&lt;": "<",
                    "&gt;": ">"
                },
                n = t;
            return Object.keys(e).forEach(function(t) {
                n = n.replace(new RegExp(e[t], "g"), t)
            }), n
        }

        function l(t) {
            var e = {
                    "<": "&amp;lt;",
                    ">": "&amp;gt;",
                    "&": "&amp;",
                    " ": "%20"
                },
                n = t;
            return Object.keys(e).forEach(function(t) {
                n = n.replace(new RegExp(e[t], "g"), t)
            }), n
        }

        function c(t) {
            var e = {
                    "&lt;": "&amp;lt;",
                    "&gt;": "&amp;gt;"
                },
                n = t;
            return Object.keys(e).forEach(function(t) {
                n = n.replace(new RegExp(e[t], "g"), t)
            }), n
        }

        function u(t) {
            return (t.match(/\n/g) || []).length
        }

        function f(t) {
            return t.replace(/^\s+|\s+$/g, "")
        }

        function d(t, e) {
            var n = setInterval(function() {
                var r = document.querySelector(t);
                r && (clearInterval(n), e(r))
            }, 100)
        }

        function h(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
        }

        function p(t, e) {
            return s(t).replace(ut, ft).replace(st + "outStream" + lt, '<span class="standard-output ' + e + '">').replace(st + "/outStream" + lt, "</span>").replace(st + "errStream" + lt, '<span class="error-output ' + e + '">').replace(st + "/errStream" + lt, "</span>")
        }

        function m(t, e) {
            var n = "",
                i = 0,
                o = !0;
            if (tt()(t)) return at;
            for (var a in t) {
                n += r(t[a]).reduce(function(t, e) {
                    switch (i += e.executionTime / 1e3, e.status !== ct.ERROR.value && e.status !== ct.FAIL.value || (o = !1), e.status) {
                        case ct.FAIL.value:
                            return t + '<span class="console-icon fail"></span><div class="test-fail">' + ct.FAIL.text + ": " + e.methodName + ": " + c(e.comparisonFailure.message) + "</div>";
                        case ct.ERROR.value:
                            return t + '<span class="console-icon fail"></span><div class="test-fail">' + ct.ERROR.text + ": " + e.methodName + ": " + c(e.exception.message) + "</div>";
                        case ct.PASSED.value:
                            return t + '<span class="console-icon ok"></span><div class="test-output">' + ct.PASSED.text + ": " + e.methodName + "</div>"
                    }
                }, "")
            }
            return o && e && e(), '<div class="test-time">Total test time: ' + i + "s</div>" + n
        }

        function g(t, e) {
            return t.reduce(function(t, n) {
                return t + '<span class="console-icon attention"></span><div class="test-fail ' + e + '">' + nt()(n.message) + "</div>"
            }, "")
        }

        function v(t) {
            for (var e = t; null != e;) {
                if (e.fullName === rt) return x(e);
                e = e.cause
            }
            return t
        }

        function y(t) {
            return void 0 !== t.cause && null != t.cause ? [t.cause].concat(y(t.cause)) : []
        }

        function b(t) {
            if (null != t.stack) {
                var e = t.stack.toString().substr(0, t.stack.toString().indexOf("at eval (<anonymous>)"));
                return ot + ": " + t.message + " \n " + e
            }
            return ot
        }

        function x(t) {
            return null != t.stackTrace && (null != t.message ? t.message = it + ": \n " + t.message : t.message = it, t.stackTrace = t.stackTrace.slice(t.stackTrace.length - 1)), t
        }

        function w(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
        }

        function C(t, e, n, r, i, o, a) {
            var s = [k(e, pt)].concat(o.map(function(t, e) {
                    return k(t, "hiddenDependency" + e + ".kt")
                })),
                l = JSON.stringify({
                    id: "",
                    name: "",
                    args: i,
                    compilerVersion: n,
                    confType: r.id,
                    originUrl: null,
                    files: s,
                    readOnlyFileNames: []
                }),
                c = new $.a;
            if (c.set("filename", pt), c.set("project", l), void 0 !== a)
                for (var u in a) c.set(u, a[u]);
            return fetch(t + r.id, {
                method: "POST",
                body: c.toString(),
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
                }
            }).then(function(t) {
                return t.json()
            })
        }

        function k(t, e) {
            return {
                name: e,
                text: t,
                publicId: ""
            }
        }

        function _(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
        }

        function S(t, e, n, r) {
            var i = void 0;
            return r === J.CANVAS ? new Mt(t, e, n, r) : (Et.has(t) ? i = Et.get(t) : (i = new Mt(t, e, n, r), Et.set(t, i)), i)
        }

        function E(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
        }

        function T(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
        }

        function M(t, e) {
            if (!t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !e || "object" != typeof e && "function" != typeof e ? t : e
        }

        function O(t, e) {
            if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function, not " + typeof e);
            t.prototype = Object.create(e && e.prototype, {
                constructor: {
                    value: t,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), e && (Object.setPrototypeOf ? Object.setPrototypeOf(t, e) : t.__proto__ = e)
        }

        function L(t) {
            if (Array.isArray(t)) {
                for (var e = 0, n = Array(t.length); e < t.length; e++) n[e] = t[e];
                return n
            }
            return Array.from(t)
        }

        function A(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
        }

        function N(t, e) {
            return Yt.create(t, e)
        }
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var P = n(0),
            I = n.n(P),
            D = (n(33), n(34), n(35), n(36), n(37), n(39), n(40), n(41), n(42), n(43), n(44), n(45), n(46), n(47), n(48), n(13)),
            F = n.n(D),
            R = n(49),
            z = n.n(R),
            j = "https://try.kotlinlang.org",
            B = {
                COMPILE: j + "/kotlinServer?type=run&runConf=",
                HIGHLIGHT: j + "/kotlinServer?type=highlight&runConf=",
                COMPLETE: j + "/kotlinServer?type=complete&runConf=",
                VERSIONS: j + "/kotlinServer?type=getKotlinVersions",
                JQUERY: j + "/static/lib/jquery/dist/jquery.min.js",
                KOTLIN_JS: j + "/static/kotlin/"
            },
            H = {
                selector: "code",
                compilerVersion: void 0
            },
            W = n(86),
            U = n.n(W),
            V = {
                DARCULA: "darcula",
                IDEA: "idea",
                DEFAULT: "default"
            },
            G = "nocursor",
            K = (n(97), n(98)),
            $ = n.n(K),
            q = function() {
                function t(t, e) {
                    for (var n = 0; n < e.length; n++) {
                        var r = e[n];
                        r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(t, r.key, r)
                    }
                }
                return function(e, n, r) {
                    return n && t(e.prototype, n), r && t(e, r), e
                }
            }(),
            Y = function() {
                function t(e, n) {
                    h(this, t), this.id = e, this.printableName = n
                }
                return q(t, null, [{
                    key: "getById",
                    value: function(e) {
                        switch (e) {
                            case "js":
                                return t.JS;
                            case "junit":
                                return t.JUNIT;
                            case "canvas":
                                return t.CANVAS;
                            default:
                                return t.JAVA
                        }
                    }
                }]), t
            }();
        Y.JS = new Y("js", "JavaScript"), Y.JAVA = new Y("java", "JVM"), Y.JUNIT = new Y("junit", "JUnit"), Y.CANVAS = new Y("canvas", "JavaScript(canvas)");
        var J = Y,
            X = n(100),
            Z = n.n(X),
            Q = n(101),
            tt = n.n(Q),
            et = n(102),
            nt = n.n(et),
            rt = "java.security.AccessControlException",
            it = "Access control exception due to security reasons in web playground",
            ot = "Unhandled JavaScript exception",
            at = "No tests methods are found",
            st = "&lt;",
            lt = "&gt;",
            ct = {
                FAIL: {
                    value: "FAIL",
                    text: "Fail"
                },
                ERROR: {
                    value: "ERROR",
                    text: "Error"
                },
                PASSED: {
                    value: "OK",
                    text: "Passed"
                }
            },
            ut = st + "errStream" + lt + "BUG" + st + "/errStream" + lt,
            ft = st + "errStream" + lt + "Hey! It seems you just found a bug! 🐞\nPlease click <a href=https://youtrack.jetbrains.com/newIssue?draftId=25-2077811 target=_blank>here<a> to submit it to the issue tracker and one day we fix it, hopefully 😉\n✅ Don't forget to attach code to the issue" + st + "/errStream" + lt + "\n",
            dt = function() {
                function t(t, e) {
                    for (var n = 0; n < e.length; n++) {
                        var r = e[n];
                        r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(t, r.key, r)
                    }
                }
                return function(e, n, r) {
                    return n && t(e.prototype, n), r && t(e, r), e
                }
            }(),
            ht = {},
            pt = "File.kt",
            mt = function() {
                function t() {
                    w(this, t)
                }
                return dt(t, null, [{
                    key: "getCompilerVersions",
                    value: function() {
                        return "compilerVersions" in ht ? Promise.resolve(ht.compilerVersions) : fetch(B.VERSIONS).then(function(t) {
                            return t.json()
                        }).then(function(t) {
                            return ht.compilerVersions = t, t
                        })
                    }
                }, {
                    key: "translateKotlinToJs",
                    value: function(t, e, n, r, i) {
                        return C(B.COMPILE, t, e, n, r, i).then(function(t) {
                            return {
                                output: "",
                                errors: Z()(Object.values(t.errors)),
                                jsCode: t.jsCode
                            }
                        })
                    }
                }, {
                    key: "executeKotlinCode",
                    value: function(t, e, n, r, i, o, a) {
                        return C(B.COMPILE, t, e, n, r, o).then(function(t) {
                            var e = "",
                                r = Z()(Object.values(t.errors)),
                                o = r.filter(function(t) {
                                    return "ERROR" === t.severity
                                });
                            if (o.length > 0) e = g(o, i);
                            else switch (n) {
                                case J.JAVA:
                                    t.text && (e = p(t.text, i));
                                    break;
                                case J.JUNIT:
                                    e = t.testResults ? m(t.testResults, a) : p(t.text, i)
                            }
                            var s = null;
                            return null != t.exception && (s = v(t.exception), s.causes = y(s), s.cause = void 0), {
                                errors: r,
                                output: e,
                                exception: s
                            }
                        })
                    }
                }, {
                    key: "getAutoCompletion",
                    value: function(t, e, n, r, i, o) {
                        var a = {
                            line: e.line,
                            ch: e.ch
                        };
                        C(B.COMPLETE, t, n, r, "", i, a).then(function(t) {
                            o(t)
                        })
                    }
                }, {
                    key: "getHighlight",
                    value: function(t, e, n, r) {
                        return C(B.HIGHLIGHT, t, e, n, "", r).then(function(t) {
                            return t[pt]
                        })
                    }
                }]), t
            }(),
            gt = mt,
            vt = n(12),
            yt = n.n(vt),
            bt = n(103),
            xt = n.n(bt),
            wt = (n(107), n(110)),
            Ct = n.n(wt),
            kt = (n(112), n(115)),
            _t = n.n(kt),
            St = function() {
                function t(t, e) {
                    for (var n = 0; n < e.length; n++) {
                        var r = e[n];
                        r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(t, r.key, r)
                    }
                }
                return function(e, n, r) {
                    return n && t(e.prototype, n), r && t(e, r), e
                }
            }(),
            Et = new _t.a,
            Tt = "if(kotlin.BufferedOutput!==undefined){kotlin.out = new kotlin.BufferedOutput()}else{kotlin.kotlin.io.output = new kotlin.kotlin.io.BufferedOutput()}",
            Mt = function() {
                function t(e, n, r, i) {
                    _(this, t), this.kotlinVersion = e, i === J.JS && this.reloadIframeScripts(n, r)
                }
                return St(t, [{
                    key: "executeJsCode",
                    value: function(t, e, n, r, i) {
                        n === J.CANVAS && (this.iframe.style.display = "block", i && (this.iframe.style.height = i + "px"));
                        var o = this.iframe.contentWindow.eval(t);
                        return n === J.JS && this.reloadIframeScripts(e, r), o
                    }
                }, {
                    key: "_initializeKotlin",
                    value: function() {
                        var t = this;
                        setTimeout(function() {
                            try {
                                t.iframe.contentWindow.eval(Tt)
                            } catch (e) {
                                t._initializeKotlin()
                            }
                        }, 3e3)
                    }
                }, {
                    key: "reloadIframeScripts",
                    value: function(t, e) {
                        void 0 !== this.iframe && e.removeChild(this.iframe);
                        var n = document.createElement("iframe");
                        n.className = "k2js-iframe", e.appendChild(n), this.iframe = n;
                        var r = this.iframe.contentDocument || this.iframe.document,
                            i = B.KOTLIN_JS + (this.kotlinVersion + "/kotlin.js");
                        r.write("<script src='" + i + "'><\/script>");
                        var o = !0,
                            a = !1,
                            s = void 0;
                        try {
                            for (var l, c = t[Symbol.iterator](); !(o = (l = c.next()).done); o = !0) {
                                var u = l.value;
                                r.write("<script src='" + u + "'><\/script>")
                            }
                        } catch (t) {
                            a = !0, s = t
                        } finally {
                            try {
                                !o && c.return && c.return()
                            } finally {
                                if (a) throw s
                            }
                        }
                        r.write("<script>" + Tt + "<\/script>"), n.contentWindow.document.write('<body style="margin: 0; overflow: hidden;"></body>'), this._initializeKotlin()
                    }
                }]), t
            }(),
            Ot = S,
            Lt = n(30),
            At = n.n(Lt),
            Nt = n(120),
            Pt = n.n(Nt),
            It = function() {
                function t(t, e) {
                    for (var n = 0; n < e.length; n++) {
                        var r = e[n];
                        r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(t, r.key, r)
                    }
                }
                return function(e, n, r) {
                    return n && t(e.prototype, n), r && t(e, r), e
                }
            }(),
            Dt = function() {
                function t(e) {
                    E(this, t), this.completion = e
                }
                return It(t, [{
                    key: "render",
                    value: function(t, e, n) {
                        var r = document.createElement("div"),
                            i = document.createElement("div"),
                            o = document.createElement("div");
                        r.setAttribute("class", "icon " + this.completion.icon), i.setAttribute("class", "name"), o.setAttribute("class", "tail"), i.textContent = this.completion.displayText, o.textContent = this.completion.tail, t.appendChild(r), t.appendChild(i), t.appendChild(o)
                    }
                }, {
                    key: "hint",
                    value: function(t, e, n) {
                        var r = t.getCursor(),
                            i = t.getTokenAt(r),
                            o = {
                                line: r.line,
                                ch: i.start
                            },
                            a = {
                                line: r.line,
                                ch: i.end
                            };
                        if ("." === i.string || " " === i.string || "(" === i.string) t.replaceRange(this.completion.text, a);
                        else {
                            var s = r.ch - i.start,
                                l = i.string.substring(0, s).lastIndexOf("$"),
                                c = i.string.substring(0, l + 1),
                                u = c + this.completion.text + i.string.substring(s, i.string.length);
                            t.replaceRange(u, o, a), t.setCursor(r.line, i.start + l + this.completion.text.length + 1), u.endsWith("(") && (t.replaceRange(")", {
                                line: r.line,
                                ch: i.start + this.completion.text.length
                            }), t.execCommand("goCharLeft"))
                        }
                    }
                }]), t
            }(),
            Ft = Dt,
            Rt = function() {
                function t(t, e) {
                    for (var n = 0; n < e.length; n++) {
                        var r = e[n];
                        r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(t, r.key, r)
                    }
                }
                return function(e, n, r) {
                    return n && t(e.prototype, n), r && t(e, r), e
                }
            }(),
            zt = function t(e, n, r) {
                null === e && (e = Function.prototype);
                var i = Object.getOwnPropertyDescriptor(e, n);
                if (void 0 === i) {
                    var o = Object.getPrototypeOf(e);
                    return null === o ? void 0 : t(o, n, r)
                }
                if ("value" in i) return i.value;
                var a = i.get;
                if (void 0 !== a) return a.call(r)
            },
            jt = {
                CANVAS_PLACEHOLDER_OUTPUT: ".js-code-output-canvas-placeholder",
                FOLD_BUTTON: ".fold-button",
                UNMODIFIABLE_LINE_DARK: "unmodifiable-line-dark",
                UNMODIFIABLE_LINE: "unmodifiable-line",
                MARK_PLACEHOLDER: "markPlaceholder",
                MARK_PLACEHOLDER_START: "markPlaceholder-start",
                MARK_PLACEHOLDER_END: "markPlaceholder-end",
                GUTTER: "gutter",
                FOLD_GUTTER: "CodeMirror-foldgutter",
                ERROR_GUTTER: "ERRORgutter",
                ERROR_AND_WARNING_GUTTER: "errors-and-warnings-gutter",
                BACKGROUND: "background"
            },
            Bt = function(t) {
                function e() {
                    return T(this, e), M(this, (e.__proto__ || Object.getPrototypeOf(e)).apply(this, arguments))
                }
                return O(e, t), Rt(e, [{
                    key: "update",
                    value: function(t) {
                        var n = this,
                            r = void 0,
                            i = !1,
                            o = t.targetPlatform;
                        if (!t.compilerVersion || o !== J.JS && o !== J.CANVAS || (this.jsExecutor = Ot(t.compilerVersion, t.jsLibs, this.getNodeForMountIframe(o), o)), t.code) {
                            var a = t.code;
                            if (t.from && t.to && t.to >= t.from && t.from > 0 && t.to > 0) {
                                var s = a.split("\n");
                                s.splice(t.from - 1, 0, "//sampleStart"), s.splice(t.to + 1, 0, "//sampleEnd"), a = s.join("\n")
                            }
                            var l = a.indexOf("//sampleStart"),
                                c = a.indexOf("//sampleEnd");
                            i = !t.noneMarkers && l > -1 && c > -1, this.prefix = "", this.suffix = "", r = a, i && (this.prefix = a.substring(0, l), this.suffix = a.substring(c + "//sampleEnd".length), r = a.substring(l + "//sampleStart".length + 1, c - 1)), this.suffix.endsWith("\n") && (this.suffix = this.suffix.substr(0, this.suffix.length - 1))
                        } else if (this.state.folded) r = this.codemirror.getValue();
                        else {
                            var f = this.codemirror.getValue();
                            r = f.substring(this.prefix.length, f.length - this.suffix.length)
                        }
                        if (this.state = F.a.all([this.state, t, {
                                isShouldBeFolded: this.isShouldBeFolded && t.isFoldedButton
                            }]), zt(e.prototype.__proto__ || Object.getPrototypeOf(e.prototype), "update", this).call(this, this.state), this.initialized) {
                            if (this.showDiagnostics(t.errors), void 0 === t.folded) return
                        } else this.initializeCodeMirror(t), this.initialized = !0;
                        if (this.state.folded) this.codemirror.setOption("lineNumbers", t.lines && !i), this.codemirror.setValue(r), this.markPlaceHolders();
                        else {
                            this.codemirror.setOption("lineNumbers", !0), this.codemirror.setValue(this.prefix + r + this.suffix), this.codemirror.markText({
                                line: 0,
                                ch: 0
                            }, {
                                line: u(this.prefix),
                                ch: 0
                            }, {
                                readOnly: !0,
                                inclusiveLeft: !0,
                                inclusiveRight: !1
                            }), this.codemirror.markText({
                                line: this.codemirror.lineCount() - u(this.suffix) - 1,
                                ch: null
                            }, {
                                line: this.codemirror.lineCount() - 1,
                                ch: null
                            }, {
                                readOnly: !0,
                                inclusiveLeft: !1,
                                inclusiveRight: !0
                            });
                            var d = this.state.theme === V.DARCULA ? jt.UNMODIFIABLE_LINE_DARK : jt.UNMODIFIABLE_LINE;
                            this.codemirror.operation(function() {
                                for (var t = 0; t < u(n.prefix); t++) n.codemirror.addLineClass(t, jt.BACKGROUND, d);
                                for (var e = n.codemirror.lineCount() - u(n.suffix); e < n.codemirror.lineCount(); e++) n.codemirror.addLineClass(e, jt.BACKGROUND, d)
                            })
                        }
                        if (this.state.autoIndent || this.prefix && this.suffix)
                            for (var h = 0; h < this.codemirror.lineCount(); h++) this.codemirror.indentLine(h)
                    }
                }, {
                    key: "markPlaceHolders",
                    value: function() {
                        var t = this,
                            e = this.getTaskRanges();
                        this.codemirror.setValue(this.codemirror.getValue().replace(new RegExp(Pt()("[mark]"), "g"), "").replace(new RegExp(Pt()("[/mark]"), "g"), "")), e.forEach(function(e) {
                            t.codemirror.markText({
                                line: e.line,
                                ch: e.ch
                            }, {
                                line: e.line,
                                ch: e.ch + e.length
                            }, {
                                className: jt.MARK_PLACEHOLDER,
                                startStyle: jt.MARK_PLACEHOLDER_START,
                                endStyle: jt.MARK_PLACEHOLDER_END,
                                handleMouseEvents: !0
                            })
                        })
                    }
                }, {
                    key: "getTaskRanges",
                    value: function() {
                        for (var t = [], e = this.codemirror.getValue().split("\n"), n = 0; n < e.length; n++)
                            for (var r = e[n]; r.includes("[mark]");) {
                                var i = r.indexOf("[mark]");
                                r = r.replace("[mark]", "");
                                var o = r.indexOf("[/mark]");
                                r = r.replace("[/mark]", ""), t.push({
                                    line: n,
                                    ch: i,
                                    length: o - i
                                })
                            }
                        return t
                    }
                }, {
                    key: "onFoldButtonMouseEnter",
                    value: function() {
                        this.state.foldButtonHover || this.update({
                            foldButtonHover: !0
                        })
                    }
                }, {
                    key: "onFoldButtonMouseLeave",
                    value: function() {
                        this.state.foldButtonHover && this.update({
                            foldButtonHover: !1
                        })
                    }
                }, {
                    key: "onConsoleCloseButtonEnter",
                    value: function() {
                        var t = this.state,
                            e = t.targetPlatform,
                            n = t.jsLibs,
                            r = t.onCloseConsole;
                        e === J.CANVAS && this.jsExecutor.reloadIframeScripts(n, this.getNodeForMountIframe(J.CANVAS)), this.update({
                            output: "",
                            openConsole: !1
                        }), r && r()
                    }
                }, {
                    key: "execute",
                    value: function() {
                        var t = this,
                            e = this.state,
                            n = e.onOpenConsole,
                            r = e.targetPlatform,
                            i = e.waitingForOutput,
                            o = e.compilerVersion,
                            a = e.args,
                            s = e.theme,
                            l = e.hiddenDependencies,
                            c = e.onTestPassed,
                            u = e.onCloseConsole,
                            f = e.jsLibs,
                            d = e.outputHeight;
                        i || (this.update({
                            waitingForOutput: !0,
                            openConsole: !1
                        }), n && n(), r === J.JAVA || r === J.JUNIT ? gt.executeKotlinCode(this.getCode(), o, r, a, s, l, c).then(function(e) {
                            e.waitingForOutput = !1, e.output ? e.openConsole = !0 : u && u(), t.update(e)
                        }, function() {
                            return t.update({
                                waitingForOutput: !1
                            })
                        }) : (r === J.CANVAS && this.jsExecutor.reloadIframeScripts(f, this.getNodeForMountIframe(r)), gt.translateKotlinToJs(this.getCode(), o, r, a, l).then(function(e) {
                            e.waitingForOutput = !1;
                            var i = e.jsCode;
                            delete e.jsCode;
                            try {
                                var o = e.errors.filter(function(t) {
                                    return "ERROR" === t.severity
                                });
                                if (o.length > 0) e.output = g(o);
                                else {
                                    var a = t.jsExecutor.executeJsCode(i, f, r, t.getNodeForMountIframe(r), d);
                                    a ? (e.openConsole = !0, e.output = '<span class="standard-output ' + s + '">' + a + "</span>") : (e.output = "", u && u()), r === J.CANVAS && (n && n(), e.openConsole = !0)
                                }
                            } catch (t) {
                                var l = b(t);
                                e.output = '<span class="error-output">' + l + "</span>", console.error(t)
                            }
                            e.exception = null, t.update(e)
                        }, function() {
                            return t.update({
                                waitingForOutput: !1
                            })
                        })))
                    }
                }, {
                    key: "getNodeForMountIframe",
                    value: function(t) {
                        return t === J.JS ? document.body : this.nodes[0].querySelector(jt.CANVAS_PLACEHOLDER_OUTPUT)
                    }
                }, {
                    key: "getCode",
                    value: function() {
                        return this.state.folded ? this.prefix + this.codemirror.getValue() + this.suffix : this.codemirror.getValue()
                    }
                }, {
                    key: "recalculatePosition",
                    value: function(t) {
                        var e = {
                            line: t.line,
                            ch: t.ch
                        };
                        if (!this.state.folded) return e;
                        var n = (this.prefix.match(/\n/g) || []).length;
                        return e.line = t.line - n, e.line < 0 ? (e.line = 0, e.ch = 0) : e.line >= this.codemirror.lineCount() && (e.line = this.codemirror.lineCount() - 1, e.ch = null), e
                    }
                }, {
                    key: "showDiagnostics",
                    value: function(t) {
                        var e = this;
                        this.removeStyles(), void 0 !== t && t.forEach(function(t) {
                            var n = t.interval;
                            n.start = e.recalculatePosition(n.start), n.end = e.recalculatePosition(n.end);
                            var r = l(t.message),
                                i = t.severity;
                            if (e.arrayClasses.push(e.codemirror.markText(n.start, n.end, {
                                    className: "cm__" + t.className,
                                    title: r
                                })), null != e.codemirror.lineInfo(n.start.line) && null == e.codemirror.lineInfo(n.start.line).gutterMarkers) {
                                var o = document.createElement("div");
                                o.className = i + jt.GUTTER, o.title = r, e.codemirror.setGutterMarker(n.start.line, jt.ERROR_AND_WARNING_GUTTER, o)
                            } else {
                                var a = e.codemirror.lineInfo(n.start.line).gutterMarkers[jt.ERROR_AND_WARNING_GUTTER];
                                a.title += "\n" + r, -1 === a.className.indexOf(jt.ERROR_GUTTER) && (a.className = i + jt.GUTTER)
                            }
                        })
                    }
                }, {
                    key: "removeStyles",
                    value: function() {
                        this.arrayClasses.forEach(function(t) {
                            return t.clear()
                        }), this.codemirror.clearGutter(jt.ERROR_AND_WARNING_GUTTER)
                    }
                }, {
                    key: "initializeCodeMirror",
                    value: function() {
                        var t = this,
                            e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
                            n = this.nodes[0].getElementsByTagName("textarea")[0],
                            r = e.highlightOnly,
                            i = {
                                readOnly: r,
                                lineNumbers: !1,
                                mode: e.mode,
                                theme: e.theme,
                                matchBrackets: e.matchBrackets,
                                scrollbarStyle: "overlay",
                                continueComments: !0,
                                autoCloseBrackets: !0,
                                indentUnit: e.indent,
                                viewportMargin: 1 / 0,
                                foldGutter: !0,
                                gutters: [jt.ERROR_AND_WARNING_GUTTER, jt.FOLD_GUTTER]
                            };
                        r && (i.cursorBlinkRate = -1), I.a.registerHelper("hint", "kotlin", function(e, n) {
                            function r(t) {
                                var r = e.findWordAt({
                                        line: i.line,
                                        ch: i.ch
                                    }).anchor.ch,
                                    a = e.findWordAt({
                                        line: i.line,
                                        ch: i.ch
                                    }).head.ch,
                                    s = e.getRange({
                                        line: i.line,
                                        ch: r
                                    }, {
                                        line: i.line,
                                        ch: a
                                    });
                                0 === t.length && /^[a-zA-Z]+$/.test(s) ? I.a.showHint(e, I.a.hint.default, {
                                    completeSingle: !1
                                }) : n({
                                    list: t.map(function(t) {
                                        return new Ft(t)
                                    }),
                                    from: {
                                        line: i.line,
                                        ch: o.start
                                    },
                                    to: {
                                        line: i.line,
                                        ch: o.end
                                    }
                                })
                            }
                            var i = e.getCursor(),
                                o = e.getTokenAt(i);
                            gt.getAutoCompletion(e.getValue(), i, t.state.compilerVersion, t.state.targetPlatform, t.state.hiddenDependencies, r)
                        }), I.a.hint.kotlin.async = !0, I.a.commands.autocomplete = function(t) {
                            I.a.showHint(t, I.a.hint.kotlin)
                        }, this.codemirror = I.a.fromTextArea(n, i), -1 !== window.navigator.appVersion.indexOf("Mac") ? this.codemirror.setOption("extraKeys", {
                            "Cmd-Alt-L": "indentAuto",
                            "Shift-Tab": "indentLess",
                            "Ctrl-/": "toggleComment",
                            "Cmd-[": !1,
                            "Cmd-]": !1,
                            "Ctrl-Space": "autocomplete"
                        }) : this.codemirror.setOption("extraKeys", {
                            "Ctrl-Alt-L": "indentAuto",
                            "Shift-Tab": "indentLess",
                            "Ctrl-/": "toggleComment",
                            "Ctrl-[": !1,
                            "Ctrl-]": !1,
                            "Ctrl-Space": "autocomplete"
                        }), this.codemirror.on("change", At()(function(e) {
                            var n = t.state,
                                r = n.onChange,
                                i = n.onFlyHighLight,
                                o = n.compilerVersion,
                                a = n.targetPlatform,
                                s = n.hiddenDependencies;
                            r && r(e.getValue()), t.removeStyles(), i && gt.getHighlight(t.getCode(), o, a, s).then(function(e) {
                                return t.showDiagnostics(e)
                            })
                        }, 500)), this.codemirror.on("keypress", At()(function(e) {
                            t.state.autoComplete && I.a.showHint(e, I.a.hint.kotlin, {
                                completeSingle: !1
                            })
                        }, 500)), this.codemirror.on("mousedown", function(t, e) {
                            var n = t.coordsChar({
                                left: e.pageX,
                                top: e.pageY
                            });
                            if (0 !== n.line || 0 !== n.ch) {
                                var r = t.findMarksAt(n),
                                    i = r.find(function(t) {
                                        return t.className === jt.MARK_PLACEHOLDER
                                    });
                                if (null != i) {
                                    var o = i.find();
                                    t.setSelection(o.from, o.to), t.focus(), e.preventDefault()
                                }
                            }
                        })
                    }
                }, {
                    key: "destroy",
                    value: function() {
                        this.arrayClasses = null, this.initialized = !1, this.jsExecutor = !1, this.state = null, this.codemirror.toTextArea(), this.remove()
                    }
                }, {
                    key: "isShouldBeFolded",
                    get: function() {
                        return "" !== this.prefix.trim() || "" !== this.suffix.trim()
                    }
                }], [{
                    key: "render",
                    value: function(t) {
                        var n = (arguments.length > 1 && void 0 !== arguments[1] && arguments[1], yt.a.render(e, t, {
                            directives: xt.a
                        }));
                        return n.arrayClasses = [], n.initialized = !1, n.state = {
                            theme: "",
                            code: "",
                            foldButtonHover: !1,
                            folded: !0,
                            output: null
                        }, n.codemirror = new I.a, n.on("click", jt.FOLD_BUTTON, function() {
                            n.update({
                                folded: !n.state.folded
                            })
                        }), n.on("keyup", function(t) {
                            120 === t.keyCode && t.ctrlKey && n.execute()
                        }), n
                    }
                }]), e
            }(Ct.a),
            Ht = Bt,
            Wt = (n(121), "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
                return typeof t
            } : function(t) {
                return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
            }),
            Ut = function() {
                function t(t, e) {
                    for (var n = 0; n < e.length; n++) {
                        var r = e[n];
                        r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(t, r.key, r)
                    }
                }
                return function(e, n, r) {
                    return n && t(e.prototype, n), r && t(e, r), e
                }
            }(),
            Vt = "data-kotlin-playground-initialized",
            Gt = 4,
            Kt = {
                HIDDEN_DEPENDENCY: "hidden-dependency",
                INDENT: "indent",
                HIGHLIGHT_ONLY: "data-highlight-only",
                STYLE: "style",
                FROM: "from",
                TO: "to",
                NONE_MARKERS: "none-markers",
                THEME: "theme",
                MODE: "mode",
                MATCH_BRACKETS: "match-brackets",
                OUTPUT_HEIGHT: "data-output-height",
                COMPLETE: "autocomplete",
                ON_FLY_HIGHLIGHT: "highlight-on-fly",
                PLATFORM: "data-target-platform",
                JS_LIBS: "data-js-libs",
                FOLDED_BUTTON: "folded-button",
                ARGUMENTS: "args",
                LINES: "lines",
                AUTO_INDENT: "auto-indent"
            },
            $t = {
                JAVA: "text/x-java",
                KOTLIN: "text/x-kotlin",
                JS: "text/javascript",
                GROOVY: "text/x-groovy",
                XML: "text/html",
                C: "text/x-c",
                OBJ_C: "text/x-objectivec",
                SWIFT: "text/x-swift",
                SHELL: "text/x-sh"
            },
            qt = function() {
                function t(e) {
                    var n = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {},
                        r = arguments[2];
                    A(this, t);
                    var i = "string" == typeof e ? document.querySelector(e) : e,
                        o = i.getAttribute(Kt.HIGHLIGHT_ONLY) === G ? i.getAttribute(Kt.HIGHLIGHT_ONLY) : i.hasAttribute(Kt.HIGHLIGHT_ONLY),
                        s = i.hasAttribute(Kt.NONE_MARKERS),
                        l = i.hasAttribute(Kt.INDENT) ? parseInt(i.getAttribute(Kt.INDENT)) : Gt,
                        c = i.hasAttribute(Kt.FROM) ? parseInt(i.getAttribute(Kt.FROM)) : null,
                        u = i.hasAttribute(Kt.TO) ? parseInt(i.getAttribute(Kt.TO)) : null,
                        d = this.getTheme(i),
                        h = i.hasAttribute(Kt.ARGUMENTS) ? i.getAttribute(Kt.ARGUMENTS) : "",
                        p = this.getHiddenDependencies(i),
                        m = i.getAttribute(Kt.OUTPUT_HEIGHT) || null,
                        g = J.getById(i.getAttribute(Kt.PLATFORM)),
                        v = i.getAttribute(Kt.STYLE),
                        y = this.getJsLibraries(i, g),
                        b = "false" !== i.getAttribute(Kt.FOLDED_BUTTON),
                        x = "true" === i.getAttribute(Kt.LINES),
                        w = "true" === i.getAttribute(Kt.ON_FLY_HIGHLIGHT),
                        C = "true" === i.getAttribute(Kt.COMPLETE),
                        k = "true" === i.getAttribute(Kt.MATCH_BRACKETS),
                        _ = "true" === i.getAttribute(Kt.AUTO_INDENT),
                        S = this.getMode(i),
                        E = f(i.textContent),
                        T = F()(H, n);
                    S !== $t.KOTLIN && o !== G && (o = !0), i.style.display = "none", i.setAttribute(Vt, "true");
                    var M = document.createElement("div");
                    a(M, i);
                    var O = Ht.render(M, {
                        highlightOnly: o
                    });
                    O.update(Object.assign({
                        code: E,
                        lines: x,
                        theme: d,
                        indent: l,
                        args: h,
                        mode: S,
                        matchBrackets: k,
                        from: c,
                        to: u,
                        autoComplete: C,
                        hiddenDependencies: p,
                        compilerVersion: T.compilerVersion,
                        noneMarkers: s,
                        onFlyHighLight: w,
                        autoIndent: _,
                        highlightOnly: o,
                        targetPlatform: g,
                        jsLibs: y,
                        isFoldedButton: b,
                        outputHeight: m
                    }, r)), this.config = T, this.node = M, this.targetNode = i, this.targetNodeStyle = v, this.view = O, i.KotlinPlayground = this, r && r.callback && r.callback(i, M)
                }
                return Ut(t, [{
                    key: "getHiddenDependencies",
                    value: function(t) {
                        return r(t.getElementsByClassName(Kt.HIDDEN_DEPENDENCY)).reduce(function(t, e) {
                            return e.parentNode.removeChild(e), [].concat(L(t), [f(e.textContent)])
                        }, [])
                    }
                }, {
                    key: "getJsLibraries",
                    value: function(t, e) {
                        if (e === J.JS || e === J.CANVAS) {
                            var n = t.getAttribute(Kt.JS_LIBS),
                                r = new z.a(B.JQUERY.split());
                            if (n) {
                                var i = new RegExp("https?://.+.js$");
                                n.replace(" ", "").split(",").filter(function(t) {
                                    return i.test(t)
                                }).forEach(function(t) {
                                    return r.add(t)
                                })
                            }
                            return r
                        }
                    }
                }, {
                    key: "getTheme",
                    value: function(t) {
                        switch (t.getAttribute(Kt.THEME)) {
                            case V.DARCULA:
                                return V.DARCULA;
                            case V.IDEA:
                                return V.IDEA;
                            default:
                                return V.DEFAULT
                        }
                    }
                }, {
                    key: "getMode",
                    value: function(t) {
                        switch (t.getAttribute(Kt.MODE)) {
                            case "java":
                                return $t.JAVA;
                            case "c":
                                return $t.C;
                            case "js":
                                return $t.JS;
                            case "groovy":
                                return $t.GROOVY;
                            case "xml":
                                return $t.XML;
                            case "shell":
                                return $t.SHELL;
                            case "obj-c":
                                return $t.OBJ_C;
                            case "swift":
                                return $t.SWIFT;
                            default:
                                return $t.KOTLIN
                        }
                    }
                }, {
                    key: "destroy",
                    value: function() {
                        this.config = null, this.node = null, this.view.destroy();
                        var t = this.targetNode;
                        null !== this.targetNodeStyle ? t.style = this.targetNodeStyle : t.style = "", t.removeAttribute(Vt), delete t.KotlinPlayground
                    }
                }, {
                    key: "isInited",
                    value: function() {
                        var t = this.targetNode,
                            e = t && t.getAttribute(Vt);
                        return e && "true" === e
                    }
                }], [{
                    key: "create",
                    value: function(e, n) {
                        var o = void 0;
                        if ("string" == typeof e) o = r(document.querySelectorAll(e));
                        else if (e instanceof Node) o = [e];
                        else if (e instanceof NodeList == !1) throw new Error("'target' type should be string|Node|NodeList, " + (void 0 === e ? "undefined" : Wt(e)) + " given");
                        return 0 === o.length ? Promise.resolve([]) : gt.getCompilerVersions().then(function(e) {
                            var r = [];
                            return o.forEach(function(o) {
                                var a = i(o, !0),
                                    s = a.minCompilerVersion,
                                    l = null,
                                    c = null;
                                e.map(function(t) {
                                    return t.version
                                }).includes(a.version) ? c = a.version : (e.forEach(function(t) {
                                    t.latestStable && (l = t.version)
                                }), c = l), s && (c = s > l ? e[e.length - 1].version : l), "" !== o.textContent.trim() && "true" !== o.getAttribute(Vt) && r.push(new t(o, {
                                    compilerVersion: c
                                }, n))
                            }), r
                        })
                    }
                }]), t
            }(),
            Yt = qt,
            Jt = {
                PREVIEW_PANEL: ".d-editor-preview",
                PREVIEW_TEXTAREA: ".d-editor-input.ember-text-area",
                KOTLIN_CODE_BLOCK: ".lang-run-kotlin"
            },
            Xt = function() {
                var t = window.KotlinPlayground,
                    e = document.querySelector(Jt.PREVIEW_TEXTAREA),
                    n = document.querySelector(Jt.PREVIEW_PANEL);
                e && n && e.addEventListener("keydown", At()(function() {
                    r(n.querySelectorAll(Jt.KOTLIN_CODE_BLOCK)).forEach(function(e) {
                        var n = e.KotlinPlayground;
                        n && n.destroy(), t(e)
                    })
                }, 300))
            };
        e.default = N, N.default = N, N.discourse = function(t) {
            return Xt(), N(t)
        };
        var Zt = o(),
            Qt = i(Zt),
            te = Qt.selector,
            ee = Qt.discourseSelector;
        (te || ee) && document.addEventListener("DOMContentLoaded", function() {
            ee ? (N.discourse(ee), d(Jt.PREVIEW_PANEL, function() {
                return Xt()
            })) : N(te)
        })
    }, function(t, e, n) {
        ! function(t) {
            t(n(0))
        }(function(t) {
            "use strict";

            function e(t, e) {
                this.cm = t, this.options = e, this.widget = null, this.debounce = 0, this.tick = 0, this.startPos = this.cm.getCursor("start"), this.startLen = this.cm.getLine(this.startPos.line).length - this.cm.getSelection().length;
                var n = this;
                t.on("cursorActivity", this.activityFunc = function() {
                    n.cursorActivity()
                })
            }

            function n(t, e, n) {
                var r = t.options.hintOptions,
                    i = {};
                for (var o in p) i[o] = p[o];
                if (r)
                    for (var o in r) void 0 !== r[o] && (i[o] = r[o]);
                if (n)
                    for (var o in n) void 0 !== n[o] && (i[o] = n[o]);
                return i.hint.resolve && (i.hint = i.hint.resolve(t, e)), i
            }

            function r(t) {
                return "string" == typeof t ? t : t.text
            }

            function i(t, e) {
                function n(t, n) {
                    var i;
                    i = "string" != typeof n ? function(t) {
                        return n(t, e)
                    } : r.hasOwnProperty(n) ? r[n] : n, o[t] = i
                }
                var r = {
                        Up: function() {
                            e.moveFocus(-1)
                        },
                        Down: function() {
                            e.moveFocus(1)
                        },
                        PageUp: function() {
                            e.moveFocus(1 - e.menuSize(), !0)
                        },
                        PageDown: function() {
                            e.moveFocus(e.menuSize() - 1, !0)
                        },
                        Home: function() {
                            e.setFocus(0)
                        },
                        End: function() {
                            e.setFocus(e.length - 1)
                        },
                        Enter: e.pick,
                        Tab: e.pick,
                        Esc: e.close
                    },
                    i = t.options.customKeys,
                    o = i ? {} : r;
                if (i)
                    for (var a in i) i.hasOwnProperty(a) && n(a, i[a]);
                var s = t.options.extraKeys;
                if (s)
                    for (var a in s) s.hasOwnProperty(a) && n(a, s[a]);
                return o
            }

            function o(t, e) {
                for (; e && e != t;) {
                    if ("LI" === e.nodeName.toUpperCase() && e.parentNode == t) return e;
                    e = e.parentNode
                }
            }

            function a(e, n) {
                this.completion = e, this.data = n, this.picked = !1;
                var a = this,
                    s = e.cm,
                    l = this.hints = document.createElement("ul"),
                    c = e.cm.options.theme;
                l.className = "CodeMirror-hints " + c, this.selectedHint = n.selectedHint || 0;
                for (var d = n.list, h = 0; h < d.length; ++h) {
                    var p = l.appendChild(document.createElement("li")),
                        m = d[h],
                        g = u + (h != this.selectedHint ? "" : " " + f);
                    null != m.className && (g = m.className + " " + g), p.className = g, m.render ? m.render(p, n, m) : p.appendChild(document.createTextNode(m.displayText || r(m))), p.hintId = h
                }
                var v = s.cursorCoords(e.options.alignWithWord ? n.from : null),
                    y = v.left,
                    b = v.bottom,
                    x = !0;
                l.style.left = y + "px", l.style.top = b + "px";
                var w = window.innerWidth || Math.max(document.body.offsetWidth, document.documentElement.offsetWidth),
                    C = window.innerHeight || Math.max(document.body.offsetHeight, document.documentElement.offsetHeight);
                (e.options.container || document.body).appendChild(l);
                var k = l.getBoundingClientRect(),
                    _ = k.bottom - C,
                    S = l.scrollHeight > l.clientHeight + 1,
                    E = s.getScrollInfo();
                if (_ > 0) {
                    var T = k.bottom - k.top;
                    if (v.top - (v.bottom - k.top) - T > 0) l.style.top = (b = v.top - T) + "px", x = !1;
                    else if (T > C) {
                        l.style.height = C - 5 + "px", l.style.top = (b = v.bottom - k.top) + "px";
                        var M = s.getCursor();
                        n.from.ch != M.ch && (v = s.cursorCoords(M), l.style.left = (y = v.left) + "px", k = l.getBoundingClientRect())
                    }
                }
                var O = k.right - w;
                if (O > 0 && (k.right - k.left > w && (l.style.width = w - 5 + "px", O -= k.right - k.left - w), l.style.left = (y = v.left - O) + "px"), S)
                    for (var L = l.firstChild; L; L = L.nextSibling) L.style.paddingRight = s.display.nativeBarWidth + "px";
                if (s.addKeyMap(this.keyMap = i(e, {
                        moveFocus: function(t, e) {
                            a.changeActive(a.selectedHint + t, e)
                        },
                        setFocus: function(t) {
                            a.changeActive(t)
                        },
                        menuSize: function() {
                            return a.screenAmount()
                        },
                        length: d.length,
                        close: function() {
                            e.close()
                        },
                        pick: function() {
                            a.pick()
                        },
                        data: n
                    })), e.options.closeOnUnfocus) {
                    var A;
                    s.on("blur", this.onBlur = function() {
                        A = setTimeout(function() {
                            e.close()
                        }, 100)
                    }), s.on("focus", this.onFocus = function() {
                        clearTimeout(A)
                    })
                }
                return s.on("scroll", this.onScroll = function() {
                    var t = s.getScrollInfo(),
                        n = s.getWrapperElement().getBoundingClientRect(),
                        r = b + E.top - t.top,
                        i = r - (window.pageYOffset || (document.documentElement || document.body).scrollTop);
                    if (x || (i += l.offsetHeight), i <= n.top || i >= n.bottom) return e.close();
                    l.style.top = r + "px", l.style.left = y + E.left - t.left + "px"
                }), t.on(l, "dblclick", function(t) {
                    var e = o(l, t.target || t.srcElement);
                    e && null != e.hintId && (a.changeActive(e.hintId), a.pick())
                }), t.on(l, "click", function(t) {
                    var n = o(l, t.target || t.srcElement);
                    n && null != n.hintId && (a.changeActive(n.hintId), e.options.completeOnSingleClick && a.pick())
                }), t.on(l, "mousedown", function() {
                    setTimeout(function() {
                        s.focus()
                    }, 20)
                }), t.signal(n, "select", d[this.selectedHint], l.childNodes[this.selectedHint]), !0
            }

            function s(t, e) {
                if (!t.somethingSelected()) return e;
                for (var n = [], r = 0; r < e.length; r++) e[r].supportsSelection && n.push(e[r]);
                return n
            }

            function l(t, e, n, r) {
                if (t.async) t(e, r, n);
                else {
                    var i = t(e, n);
                    i && i.then ? i.then(r) : r(i)
                }
            }

            function c(e, n) {
                var r, i = e.getHelpers(n, "hint");
                if (i.length) {
                    var o = function(t, e, n) {
                        function r(i) {
                            if (i == o.length) return e(null);
                            l(o[i], t, n, function(t) {
                                t && t.list.length > 0 ? e(t) : r(i + 1)
                            })
                        }
                        var o = s(t, i);
                        r(0)
                    };
                    return o.async = !0, o.supportsSelection = !0, o
                }
                return (r = e.getHelper(e.getCursor(), "hintWords")) ? function(e) {
                    return t.hint.fromList(e, {
                        words: r
                    })
                } : t.hint.anyword ? function(e, n) {
                    return t.hint.anyword(e, n)
                } : function() {}
            }
            var u = "CodeMirror-hint",
                f = "CodeMirror-hint-active";
            t.showHint = function(t, e, n) {
                if (!e) return t.showHint(n);
                n && n.async && (e.async = !0);
                var r = {
                    hint: e
                };
                if (n)
                    for (var i in n) r[i] = n[i];
                return t.showHint(r)
            }, t.defineExtension("showHint", function(r) {
                r = n(this, this.getCursor("start"), r);
                var i = this.listSelections();
                if (!(i.length > 1)) {
                    if (this.somethingSelected()) {
                        if (!r.hint.supportsSelection) return;
                        for (var o = 0; o < i.length; o++)
                            if (i[o].head.line != i[o].anchor.line) return
                    }
                    this.state.completionActive && this.state.completionActive.close();
                    var a = this.state.completionActive = new e(this, r);
                    a.options.hint && (t.signal(this, "startCompletion", this), a.update(!0))
                }
            });
            var d = window.requestAnimationFrame || function(t) {
                    return setTimeout(t, 1e3 / 60)
                },
                h = window.cancelAnimationFrame || clearTimeout;
            e.prototype = {
                close: function() {
                    this.active() && (this.cm.state.completionActive = null, this.tick = null, this.cm.off("cursorActivity", this.activityFunc), this.widget && this.data && t.signal(this.data, "close"), this.widget && this.widget.close(), t.signal(this.cm, "endCompletion", this.cm))
                },
                active: function() {
                    return this.cm.state.completionActive == this
                },
                pick: function(e, n) {
                    var i = e.list[n];
                    i.hint ? i.hint(this.cm, e, i) : this.cm.replaceRange(r(i), i.from || e.from, i.to || e.to, "complete"), t.signal(e, "pick", i), this.close()
                },
                cursorActivity: function() {
                    this.debounce && (h(this.debounce), this.debounce = 0);
                    var t = this.cm.getCursor(),
                        e = this.cm.getLine(t.line);
                    if (t.line != this.startPos.line || e.length - t.ch != this.startLen - this.startPos.ch || t.ch < this.startPos.ch || this.cm.somethingSelected() || !t.ch || this.options.closeCharacters.test(e.charAt(t.ch - 1))) this.close();
                    else {
                        var n = this;
                        this.debounce = d(function() {
                            n.update()
                        }), this.widget && this.widget.disable()
                    }
                },
                update: function(t) {
                    if (null != this.tick) {
                        var e = this,
                            n = ++this.tick;
                        l(this.options.hint, this.cm, this.options, function(r) {
                            e.tick == n && e.finishUpdate(r, t)
                        })
                    }
                },
                finishUpdate: function(e, n) {
                    this.data && t.signal(this.data, "update");
                    var r = this.widget && this.widget.picked || n && this.options.completeSingle;
                    this.widget && this.widget.close(), this.data = e, e && e.list.length && (r && 1 == e.list.length ? this.pick(e, 0) : (this.widget = new a(this, e), t.signal(e, "shown")))
                }
            }, a.prototype = {
                close: function() {
                    if (this.completion.widget == this) {
                        this.completion.widget = null, this.hints.parentNode.removeChild(this.hints), this.completion.cm.removeKeyMap(this.keyMap);
                        var t = this.completion.cm;
                        this.completion.options.closeOnUnfocus && (t.off("blur", this.onBlur), t.off("focus", this.onFocus)), t.off("scroll", this.onScroll)
                    }
                },
                disable: function() {
                    this.completion.cm.removeKeyMap(this.keyMap);
                    var t = this;
                    this.keyMap = {
                        Enter: function() {
                            t.picked = !0
                        }
                    }, this.completion.cm.addKeyMap(this.keyMap)
                },
                pick: function() {
                    this.completion.pick(this.data, this.selectedHint)
                },
                changeActive: function(e, n) {
                    if (e >= this.data.list.length ? e = n ? this.data.list.length - 1 : 0 : e < 0 && (e = n ? 0 : this.data.list.length - 1), this.selectedHint != e) {
                        var r = this.hints.childNodes[this.selectedHint];
                        r && (r.className = r.className.replace(" " + f, "")), r = this.hints.childNodes[this.selectedHint = e], r.className += " " + f, r.offsetTop < this.hints.scrollTop ? this.hints.scrollTop = r.offsetTop - 3 : r.offsetTop + r.offsetHeight > this.hints.scrollTop + this.hints.clientHeight && (this.hints.scrollTop = r.offsetTop + r.offsetHeight - this.hints.clientHeight + 3), t.signal(this.data, "select", this.data.list[this.selectedHint], r)
                    }
                },
                screenAmount: function() {
                    return Math.floor(this.hints.clientHeight / this.hints.firstChild.offsetHeight) || 1
                }
            }, t.registerHelper("hint", "auto", {
                resolve: c
            }), t.registerHelper("hint", "fromList", function(e, n) {
                var r, i = e.getCursor(),
                    o = e.getTokenAt(i),
                    a = t.Pos(i.line, o.start),
                    s = i;
                o.start < i.ch && /\w/.test(o.string.charAt(i.ch - o.start - 1)) ? r = o.string.substr(0, i.ch - o.start) : (r = "", a = i);
                for (var l = [], c = 0; c < n.words.length; c++) {
                    var u = n.words[c];
                    u.slice(0, r.length) == r && l.push(u)
                }
                if (l.length) return {
                    list: l,
                    from: a,
                    to: s
                }
            }), t.commands.autocomplete = t.showHint;
            var p = {
                hint: t.hint.auto,
                completeSingle: !0,
                alignWithWord: !0,
                closeCharacters: /[\s()\[\]{};:>,]/,
                closeOnUnfocus: !0,
                completeOnSingleClick: !0,
                container: null,
                customKeys: null,
                extraKeys: null
            };
            t.defineOption("hintOptions", null)
        })
    }, function(t, e, n) {
        ! function(t) {
            t(n(0))
        }(function(t) {
            "use strict";
            var e = /[\w$]+/;
            t.registerHelper("hint", "anyword", function(n, r) {
                for (var i = r && r.word || e, o = r && r.range || 500, a = n.getCursor(), s = n.getLine(a.line), l = a.ch, c = l; c && i.test(s.charAt(c - 1));) --c;
                for (var u = c != l && s.slice(c, l), f = r && r.list || [], d = {}, h = new RegExp(i.source, "g"), p = -1; p <= 1; p += 2)
                    for (var m = a.line, g = Math.min(Math.max(m + p * o, n.firstLine()), n.lastLine()) + p; m != g; m += p)
                        for (var v, y = n.getLine(m); v = h.exec(y);) m == a.line && v[0] === u || u && 0 != v[0].lastIndexOf(u, 0) || Object.prototype.hasOwnProperty.call(d, v[0]) || (d[v[0]] = !0, f.push(v[0]));
                return {
                    list: f,
                    from: t.Pos(a.line, c),
                    to: t.Pos(a.line, l)
                }
            })
        })
    }, function(t, e, n) {
        ! function(t) {
            t(n(0))
        }(function(t) {
            "use strict";

            function e(e, n, r) {
                function i(e) {
                    var n = t.wheelEventPixels(e)["horizontal" == o.orientation ? "x" : "y"],
                        r = o.pos;
                    o.moveTo(o.pos + n), o.pos != r && t.e_preventDefault(e)
                }
                this.orientation = n, this.scroll = r, this.screen = this.total = this.size = 1, this.pos = 0, this.node = document.createElement("div"), this.node.className = e + "-" + n, this.inner = this.node.appendChild(document.createElement("div"));
                var o = this;
                t.on(this.inner, "mousedown", function(e) {
                    function n() {
                        t.off(document, "mousemove", r), t.off(document, "mouseup", n)
                    }

                    function r(t) {
                        if (1 != t.which) return n();
                        o.moveTo(s + (t[i] - a) * (o.total / o.size))
                    }
                    if (1 == e.which) {
                        t.e_preventDefault(e);
                        var i = "horizontal" == o.orientation ? "pageX" : "pageY",
                            a = e[i],
                            s = o.pos;
                        t.on(document, "mousemove", r), t.on(document, "mouseup", n)
                    }
                }), t.on(this.node, "click", function(e) {
                    t.e_preventDefault(e);
                    var n, r = o.inner.getBoundingClientRect();
                    n = "horizontal" == o.orientation ? e.clientX < r.left ? -1 : e.clientX > r.right ? 1 : 0 : e.clientY < r.top ? -1 : e.clientY > r.bottom ? 1 : 0, o.moveTo(o.pos + n * o.screen)
                }), t.on(this.node, "mousewheel", i), t.on(this.node, "DOMMouseScroll", i)
            }

            function n(t, n, r) {
                this.addClass = t, this.horiz = new e(t, "horizontal", r), n(this.horiz.node), this.vert = new e(t, "vertical", r), n(this.vert.node), this.width = null
            }
            e.prototype.setPos = function(t, e) {
                return t < 0 && (t = 0), t > this.total - this.screen && (t = this.total - this.screen), !(!e && t == this.pos) && (this.pos = t, this.inner.style["horizontal" == this.orientation ? "left" : "top"] = t * (this.size / this.total) + "px", !0)
            }, e.prototype.moveTo = function(t) {
                this.setPos(t) && this.scroll(t, this.orientation)
            };
            e.prototype.update = function(t, e, n) {
                var r = this.screen != e || this.total != t || this.size != n;
                r && (this.screen = e, this.total = t, this.size = n);
                var i = this.screen * (this.size / this.total);
                i < 10 && (this.size -= 10 - i, i = 10), this.inner.style["horizontal" == this.orientation ? "width" : "height"] = i + "px", this.setPos(this.pos, r)
            }, n.prototype.update = function(t) {
                if (null == this.width) {
                    var e = window.getComputedStyle ? window.getComputedStyle(this.horiz.node) : this.horiz.node.currentStyle;
                    e && (this.width = parseInt(e.height))
                }
                var n = this.width || 0,
                    r = t.scrollWidth > t.clientWidth + 1,
                    i = t.scrollHeight > t.clientHeight + 1;
                return this.vert.node.style.display = i ? "block" : "none", this.horiz.node.style.display = r ? "block" : "none", i && (this.vert.update(t.scrollHeight, t.clientHeight, t.viewHeight - (r ? n : 0)), this.vert.node.style.bottom = r ? n + "px" : "0"), r && (this.horiz.update(t.scrollWidth, t.clientWidth, t.viewWidth - (i ? n : 0) - t.barLeft), this.horiz.node.style.right = i ? n + "px" : "0", this.horiz.node.style.left = t.barLeft + "px"), {
                    right: i ? n : 0,
                    bottom: r ? n : 0
                }
            }, n.prototype.setScrollTop = function(t) {
                this.vert.setPos(t)
            }, n.prototype.setScrollLeft = function(t) {
                this.horiz.setPos(t)
            }, n.prototype.clear = function() {
                var t = this.horiz.node.parentNode;
                t.removeChild(this.horiz.node), t.removeChild(this.vert.node)
            }, t.scrollbarModel.simple = function(t, e) {
                return new n("CodeMirror-simplescroll", t, e)
            }, t.scrollbarModel.overlay = function(t, e) {
                return new n("CodeMirror-overlayscroll", t, e)
            }
        })
    }, function(t, e, n) {
        ! function(t) {
            t(n(0))
        }(function(t) {
            "use strict";
            t.defineMode("smalltalk", function(t) {
                var e = /[+\-\/\\*~<>=@%|&?!.,:;^]/,
                    n = /true|false|nil|self|super|thisContext/,
                    r = function(t, e) {
                        this.next = t, this.parent = e
                    },
                    i = function(t, e, n) {
                        this.name = t, this.context = e, this.eos = n
                    },
                    o = function() {
                        this.context = new r(a, null), this.expectVariable = !0, this.indentation = 0, this.userIndentationDelta = 0
                    };
                o.prototype.userIndent = function(e) {
                    this.userIndentationDelta = e > 0 ? e / t.indentUnit - this.indentation : 0
                };
                var a = function(t, o, a) {
                        var f = new i(null, o, !1),
                            d = t.next();
                        return '"' === d ? f = s(t, new r(s, o)) : "'" === d ? f = l(t, new r(l, o)) : "#" === d ? "'" === t.peek() ? (t.next(), f = c(t, new r(c, o))) : t.eatWhile(/[^\s.{}\[\]()]/) ? f.name = "string-2" : f.name = "meta" : "$" === d ? ("<" === t.next() && (t.eatWhile(/[^\s>]/), t.next()), f.name = "string-2") : "|" === d && a.expectVariable ? f.context = new r(u, o) : /[\[\]{}()]/.test(d) ? (f.name = "bracket", f.eos = /[\[{(]/.test(d), "[" === d ? a.indentation++ : "]" === d && (a.indentation = Math.max(0, a.indentation - 1))) : e.test(d) ? (t.eatWhile(e), f.name = "operator", f.eos = ";" !== d) : /\d/.test(d) ? (t.eatWhile(/[\w\d]/), f.name = "number") : /[\w_]/.test(d) ? (t.eatWhile(/[\w\d_]/), f.name = a.expectVariable ? n.test(t.current()) ? "keyword" : "variable" : null) : f.eos = a.expectVariable, f
                    },
                    s = function(t, e) {
                        return t.eatWhile(/[^"]/), new i("comment", t.eat('"') ? e.parent : e, !0)
                    },
                    l = function(t, e) {
                        return t.eatWhile(/[^']/), new i("string", t.eat("'") ? e.parent : e, !1)
                    },
                    c = function(t, e) {
                        return t.eatWhile(/[^']/), new i("string-2", t.eat("'") ? e.parent : e, !1)
                    },
                    u = function(t, e) {
                        var n = new i(null, e, !1);
                        return "|" === t.next() ? (n.context = e.parent, n.eos = !0) : (t.eatWhile(/[^|]/), n.name = "variable"), n
                    };
                return {
                    startState: function() {
                        return new o
                    },
                    token: function(t, e) {
                        if (e.userIndent(t.indentation()), t.eatSpace()) return null;
                        var n = e.context.next(t, e.context, e);
                        return e.context = n.context, e.expectVariable = n.eos, n.name
                    },
                    blankLine: function(t) {
                        t.userIndent(0)
                    },
                    indent: function(e, n) {
                        var r = e.context.next === a && n && "]" === n.charAt(0) ? -1 : e.userIndentationDelta;
                        return (e.indentation + r) * t.indentUnit
                    },
                    electricChars: "]"
                }
            }), t.defineMIME("text/x-stsrc", {
                name: "smalltalk"
            })
        })
    }, function(t, e, n) {
        ! function(t) {
            t(n(0), n(38))
        }(function(t) {
            "use strict";

            function e(t, r) {
                if (3 == t.nodeType) return r.push(t.nodeValue);
                for (var i = t.firstChild; i; i = i.nextSibling) e(i, r), n.test(t.nodeType) && r.push("\n")
            }
            var n = /^(p|li|div|h\\d|pre|blockquote|td)$/;
            t.colorize = function(n, r) {
                n || (n = document.body.getElementsByTagName("pre"));
                for (var i = 0; i < n.length; ++i) {
                    var o = n[i],
                        a = o.getAttribute("data-lang") || r;
                    if (a) {
                        var s = [];
                        e(o, s), o.innerHTML = "", t.runMode(s.join(""), a, o), o.className += " cm-s-default"
                    }
                }
            }
        })
    }, function(t, e, n) {
        ! function(t) {
            t(n(0))
        }(function(t) {
            "use strict";
            t.runMode = function(e, n, r, i) {
                var o = t.getMode(t.defaults, n),
                    a = /MSIE \d/.test(navigator.userAgent),
                    s = a && (null == document.documentMode || document.documentMode < 9);
                if (r.appendChild) {
                    var l = i && i.tabSize || t.defaults.tabSize,
                        c = r,
                        u = 0;
                    c.innerHTML = "", r = function(t, e) {
                        if ("\n" == t) return c.appendChild(document.createTextNode(s ? "\r" : t)), void(u = 0);
                        for (var n = "", r = 0;;) {
                            var i = t.indexOf("\t", r);
                            if (-1 == i) {
                                n += t.slice(r), u += t.length - r;
                                break
                            }
                            u += i - r, n += t.slice(r, i);
                            var o = l - u % l;
                            u += o;
                            for (var a = 0; a < o; ++a) n += " ";
                            r = i + 1
                        }
                        if (e) {
                            var f = c.appendChild(document.createElement("span"));
                            f.className = "cm-" + e.replace(/ +/g, " cm-"), f.appendChild(document.createTextNode(n))
                        } else c.appendChild(document.createTextNode(n))
                    }
                }
                for (var f = t.splitLines(e), d = i && i.state || t.startState(o), h = 0, p = f.length; h < p; ++h) {
                    h && r("\n");
                    var m = new t.StringStream(f[h]);
                    for (!m.string && o.blankLine && o.blankLine(d); !m.eol();) {
                        var g = o.token(m, d);
                        r(m.current(), g, h, m.start, d), m.start = m.pos
                    }
                }
            }
        })
    }, function(t, e, n) {
        ! function(t) {
            t(n(0))
        }(function(t) {
            "use strict";

            function e(t, e, n, r, i, o) {
                this.indented = t, this.column = e, this.type = n, this.info = r, this.align = i, this.prev = o
            }

            function n(t, n, r, i) {
                var o = t.indented;
                return t.context && "statement" == t.context.type && "statement" != r && (o = t.context.indented), t.context = new e(o, n, r, i, null, t.context)
            }

            function r(t) {
                var e = t.context.type;
                return ")" != e && "]" != e && "}" != e || (t.indented = t.context.indented), t.context = t.context.prev
            }

            function i(t, e, n) {
                return "variable" == e.prevToken || "type" == e.prevToken || (!!/\S(?:[^- ]>|[*\]])\s*$|\*$/.test(t.string.slice(0, n)) || (!(!e.typeAtEndOfLine || t.column() != t.indentation()) || void 0))
            }

            function o(t) {
                for (;;) {
                    if (!t || "top" == t.type) return !0;
                    if ("}" == t.type && "namespace" != t.prev.info) return !1;
                    t = t.prev
                }
            }

            function a(t) {
                for (var e = {}, n = t.split(" "), r = 0; r < n.length; ++r) e[n[r]] = !0;
                return e
            }

            function s(t, e) {
                return "function" == typeof t ? t(e) : t.propertyIsEnumerable(e)
            }

            function l(t, e) {
                if (!e.startOfLine) return !1;
                for (var n, r = null; n = t.peek();) {
                    if ("\\" == n && t.match(/^.$/)) {
                        r = l;
                        break
                    }
                    if ("/" == n && t.match(/^\/[\/\*]/, !1)) break;
                    t.next()
                }
                return e.tokenize = r, "meta"
            }

            function c(t, e) {
                return "type" == e.prevToken && "type"
            }

            function u(t) {
                return t.eatWhile(/[\w\.']/), "number"
            }

            function f(t, e) {
                if (t.backUp(1), t.match(/(R|u8R|uR|UR|LR)/)) {
                    var n = t.match(/"([^\s\\()]{0,16})\(/);
                    return !!n && (e.cpp11RawStringDelim = n[1], e.tokenize = p, p(t, e))
                }
                return t.match(/(u8|u|U|L)/) ? !!t.match(/["']/, !1) && "string" : (t.next(), !1)
            }

            function d(t) {
                var e = /(\w+)::~?(\w+)$/.exec(t);
                return e && e[1] == e[2]
            }

            function h(t, e) {
                for (var n; null != (n = t.next());)
                    if ('"' == n && !t.eat('"')) {
                        e.tokenize = null;
                        break
                    }
                return "string"
            }

            function p(t, e) {
                var n = e.cpp11RawStringDelim.replace(/[^\w\s]/g, "\\$&");
                return t.match(new RegExp(".*?\\)" + n + '"')) ? e.tokenize = null : t.skipToEnd(), "string"
            }

            function m(e, n) {
                function r(t) {
                    if (t)
                        for (var e in t) t.hasOwnProperty(e) && i.push(e)
                }
                "string" == typeof e && (e = [e]);
                var i = [];
                r(n.keywords), r(n.types), r(n.builtin), r(n.atoms), i.length && (n.helperType = e[0], t.registerHelper("hintWords", e[0], i));
                for (var o = 0; o < e.length; ++o) t.defineMIME(e[o], n)
            }

            function g(t, e) {
                for (var n = !1; !t.eol();) {
                    if (!n && t.match('"""')) {
                        e.tokenize = null;
                        break
                    }
                    n = "\\" == t.next() && !n
                }
                return "string"
            }

            function v(t) {
                return function(e, n) {
                    for (var r; r = e.next();) {
                        if ("*" == r && e.eat("/")) {
                            if (1 == t) {
                                n.tokenize = null;
                                break
                            }
                            return n.tokenize = v(t - 1), n.tokenize(e, n)
                        }
                        if ("/" == r && e.eat("*")) return n.tokenize = v(t + 1), n.tokenize(e, n)
                    }
                    return "comment"
                }
            }

            function y(t) {
                return function(e, n) {
                    for (var r, i = !1, o = !1; !e.eol();) {
                        if (!t && !i && e.match('"')) {
                            o = !0;
                            break
                        }
                        if (t && e.match('"""')) {
                            o = !0;
                            break
                        }
                        r = e.next(), !i && "$" == r && e.match("{") && e.skipTo("}"), i = !i && "\\" == r && !t
                    }
                    return !o && t || (n.tokenize = null), "string"
                }
            }

            function b(t) {
                return function(e, n) {
                    for (var r, i = !1, o = !1; !e.eol();) {
                        if (!i && e.match('"') && ("single" == t || e.match('""'))) {
                            o = !0;
                            break
                        }
                        if (!i && e.match("``")) {
                            C = b(t), o = !0;
                            break
                        }
                        r = e.next(), i = "single" == t && !i && "\\" == r
                    }
                    return o && (n.tokenize = null), "string"
                }
            }
            t.defineMode("clike", function(a, l) {
                function c(t, e) {
                    var n = t.next();
                    if (_[n]) {
                        var r = _[n](t, e);
                        if (!1 !== r) return r
                    }
                    if ('"' == n || "'" == n) return e.tokenize = u(n), e.tokenize(t, e);
                    if (O.test(n)) return h = n, null;
                    if (L.test(n)) {
                        if (t.backUp(1), t.match(A)) return "number";
                        t.next()
                    }
                    if ("/" == n) {
                        if (t.eat("*")) return e.tokenize = f, f(t, e);
                        if (t.eat("/")) return t.skipToEnd(), "comment"
                    }
                    if (N.test(n)) {
                        for (; !t.match(/^\/[\/*]/, !1) && t.eat(N););
                        return "operator"
                    }
                    if (t.eatWhile(P), M)
                        for (; t.match(M);) t.eatWhile(P);
                    var i = t.current();
                    return s(y, i) ? (s(w, i) && (h = "newstatement"), s(C, i) && (p = !0), "keyword") : s(b, i) ? "type" : s(x, i) ? (s(w, i) && (h = "newstatement"), "builtin") : s(k, i) ? "atom" : "variable"
                }

                function u(t) {
                    return function(e, n) {
                        for (var r, i = !1, o = !1; null != (r = e.next());) {
                            if (r == t && !i) {
                                o = !0;
                                break
                            }
                            i = !i && "\\" == r
                        }
                        return (o || !i && !S) && (n.tokenize = null), "string"
                    }
                }

                function f(t, e) {
                    for (var n, r = !1; n = t.next();) {
                        if ("/" == n && r) {
                            e.tokenize = null;
                            break
                        }
                        r = "*" == n
                    }
                    return "comment"
                }

                function d(t, e) {
                    l.typeFirstDefinitions && t.eol() && o(e.context) && (e.typeAtEndOfLine = i(t, e, t.pos))
                }
                var h, p, m = a.indentUnit,
                    g = l.statementIndentUnit || m,
                    v = l.dontAlignCalls,
                    y = l.keywords || {},
                    b = l.types || {},
                    x = l.builtin || {},
                    w = l.blockKeywords || {},
                    C = l.defKeywords || {},
                    k = l.atoms || {},
                    _ = l.hooks || {},
                    S = l.multiLineStrings,
                    E = !1 !== l.indentStatements,
                    T = !1 !== l.indentSwitch,
                    M = l.namespaceSeparator,
                    O = l.isPunctuationChar || /[\[\]{}\(\),;\:\.]/,
                    L = l.numberStart || /[\d\.]/,
                    A = l.number || /^(?:0x[a-f\d]+|0b[01]+|(?:\d+\.?\d*|\.\d+)(?:e[-+]?\d+)?)(u|ll?|l|f)?/i,
                    N = l.isOperatorChar || /[+\-*&%=<>!?|\/]/,
                    P = l.isIdentifierChar || /[\w\$_\xa1-\uffff]/;
                return {
                    startState: function(t) {
                        return {
                            tokenize: null,
                            context: new e((t || 0) - m, 0, "top", null, !1),
                            indented: 0,
                            startOfLine: !0,
                            prevToken: null
                        }
                    },
                    token: function(t, e) {
                        var a = e.context;
                        if (t.sol() && (null == a.align && (a.align = !1), e.indented = t.indentation(), e.startOfLine = !0), t.eatSpace()) return d(t, e), null;
                        h = p = null;
                        var s = (e.tokenize || c)(t, e);
                        if ("comment" == s || "meta" == s) return s;
                        if (null == a.align && (a.align = !0), ";" == h || ":" == h || "," == h && t.match(/^\s*(?:\/\/.*)?$/, !1))
                            for (;
                                "statement" == e.context.type;) r(e);
                        else if ("{" == h) n(e, t.column(), "}");
                        else if ("[" == h) n(e, t.column(), "]");
                        else if ("(" == h) n(e, t.column(), ")");
                        else if ("}" == h) {
                            for (;
                                "statement" == a.type;) a = r(e);
                            for ("}" == a.type && (a = r(e));
                                "statement" == a.type;) a = r(e)
                        } else h == a.type ? r(e) : E && (("}" == a.type || "top" == a.type) && ";" != h || "statement" == a.type && "newstatement" == h) && n(e, t.column(), "statement", t.current());
                        if ("variable" == s && ("def" == e.prevToken || l.typeFirstDefinitions && i(t, e, t.start) && o(e.context) && t.match(/^\s*\(/, !1)) && (s = "def"), _.token) {
                            var u = _.token(t, e, s);
                            void 0 !== u && (s = u)
                        }
                        return "def" == s && !1 === l.styleDefs && (s = "variable"), e.startOfLine = !1, e.prevToken = p ? "def" : s || h, d(t, e), s
                    },
                    indent: function(e, n) {
                        if (e.tokenize != c && null != e.tokenize || e.typeAtEndOfLine) return t.Pass;
                        var r = e.context,
                            i = n && n.charAt(0),
                            o = i == r.type;
                        if ("statement" == r.type && "}" == i && (r = r.prev), l.dontIndentStatements)
                            for (;
                                "statement" == r.type && l.dontIndentStatements.test(r.info);) r = r.prev;
                        if (_.indent) {
                            var a = _.indent(e, r, n, m);
                            if ("number" == typeof a) return a
                        }
                        var s = r.prev && "switch" == r.prev.info;
                        if (l.allmanIndentation && /[{(]/.test(i)) {
                            for (;
                                "top" != r.type && "}" != r.type;) r = r.prev;
                            return r.indented
                        }
                        return "statement" == r.type ? r.indented + ("{" == i ? 0 : g) : !r.align || v && ")" == r.type ? ")" != r.type || o ? r.indented + (o ? 0 : m) + (o || !s || /^(?:case|default)\b/.test(n) ? 0 : m) : r.indented + g : r.column + (o ? 0 : 1)
                    },
                    electricInput: T ? /^\s*(?:case .*?:|default:|\{\}?|\})$/ : /^\s*[{}]$/,
                    blockCommentStart: "/*",
                    blockCommentEnd: "*/",
                    blockCommentContinue: " * ",
                    lineComment: "//",
                    fold: "brace"
                }
            });
            var x = "auto if break case register continue return default do sizeof static else struct switch extern typedef union for goto while enum const volatile",
                w = "int long char short double float unsigned signed void size_t ptrdiff_t";
            m(["text/x-csrc", "text/x-c", "text/x-chdr"], {
                name: "clike",
                keywords: a(x),
                types: a(w + " bool _Complex _Bool float_t double_t intptr_t intmax_t int8_t int16_t int32_t int64_t uintptr_t uintmax_t uint8_t uint16_t uint32_t uint64_t"),
                blockKeywords: a("case do else for if switch while struct"),
                defKeywords: a("struct"),
                typeFirstDefinitions: !0,
                atoms: a("NULL true false"),
                hooks: {
                    "#": l,
                    "*": c
                },
                modeProps: {
                    fold: ["brace", "include"]
                }
            }), m(["text/x-c++src", "text/x-c++hdr"], {
                name: "clike",
                keywords: a(x + " asm dynamic_cast namespace reinterpret_cast try explicit new static_cast typeid catch operator template typename class friend private this using const_cast inline public throw virtual delete mutable protected alignas alignof constexpr decltype nullptr noexcept thread_local final static_assert override"),
                types: a(w + " bool wchar_t"),
                blockKeywords: a("catch class do else finally for if struct switch try while"),
                defKeywords: a("class namespace struct enum union"),
                typeFirstDefinitions: !0,
                atoms: a("true false NULL"),
                dontIndentStatements: /^template$/,
                isIdentifierChar: /[\w\$_~\xa1-\uffff]/,
                hooks: {
                    "#": l,
                    "*": c,
                    u: f,
                    U: f,
                    L: f,
                    R: f,
                    0: u,
                    1: u,
                    2: u,
                    3: u,
                    4: u,
                    5: u,
                    6: u,
                    7: u,
                    8: u,
                    9: u,
                    token: function(t, e, n) {
                        if ("variable" == n && "(" == t.peek() && (";" == e.prevToken || null == e.prevToken || "}" == e.prevToken) && d(t.current())) return "def"
                    }
                },
                namespaceSeparator: "::",
                modeProps: {
                    fold: ["brace", "include"]
                }
            }), m("text/x-java", {
                name: "clike",
                keywords: a("abstract assert break case catch class const continue default do else enum extends final finally float for goto if implements import instanceof interface native new package private protected public return static strictfp super switch synchronized this throw throws transient try volatile while @interface"),
                types: a("byte short int long float double boolean char void Boolean Byte Character Double Float Integer Long Number Object Short String StringBuffer StringBuilder Void"),
                blockKeywords: a("catch class do else finally for if switch try while"),
                defKeywords: a("class interface enum @interface"),
                typeFirstDefinitions: !0,
                atoms: a("true false null"),
                number: /^(?:0x[a-f\d_]+|0b[01_]+|(?:[\d_]+\.?\d*|\.\d+)(?:e[-+]?[\d_]+)?)(u|ll?|l|f)?/i,
                hooks: {
                    "@": function(t) {
                        return !t.match("interface", !1) && (t.eatWhile(/[\w\$_]/), "meta")
                    }
                },
                modeProps: {
                    fold: ["brace", "import"]
                }
            }), m("text/x-csharp", {
                name: "clike",
                keywords: a("abstract as async await base break case catch checked class const continue default delegate do else enum event explicit extern finally fixed for foreach goto if implicit in interface internal is lock namespace new operator out override params private protected public readonly ref return sealed sizeof stackalloc static struct switch this throw try typeof unchecked unsafe using virtual void volatile while add alias ascending descending dynamic from get global group into join let orderby partial remove select set value var yield"),
                types: a("Action Boolean Byte Char DateTime DateTimeOffset Decimal Double Func Guid Int16 Int32 Int64 Object SByte Single String Task TimeSpan UInt16 UInt32 UInt64 bool byte char decimal double short int long object sbyte float string ushort uint ulong"),
                blockKeywords: a("catch class do else finally for foreach if struct switch try while"),
                defKeywords: a("class interface namespace struct var"),
                typeFirstDefinitions: !0,
                atoms: a("true false null"),
                hooks: {
                    "@": function(t, e) {
                        return t.eat('"') ? (e.tokenize = h, h(t, e)) : (t.eatWhile(/[\w\$_]/), "meta")
                    }
                }
            }), m("text/x-scala", {
                name: "clike",
                keywords: a("abstract case catch class def do else extends final finally for forSome if implicit import lazy match new null object override package private protected return sealed super this throw trait try type val var while with yield _ assert assume require print println printf readLine readBoolean readByte readShort readChar readInt readLong readFloat readDouble"),
                types: a("AnyVal App Application Array BufferedIterator BigDecimal BigInt Char Console Either Enumeration Equiv Error Exception Fractional Function IndexedSeq Int Integral Iterable Iterator List Map Numeric Nil NotNull Option Ordered Ordering PartialFunction PartialOrdering Product Proxy Range Responder Seq Serializable Set Specializable Stream StringBuilder StringContext Symbol Throwable Traversable TraversableOnce Tuple Unit Vector Boolean Byte Character CharSequence Class ClassLoader Cloneable Comparable Compiler Double Exception Float Integer Long Math Number Object Package Pair Process Runtime Runnable SecurityManager Short StackTraceElement StrictMath String StringBuffer System Thread ThreadGroup ThreadLocal Throwable Triple Void"),
                multiLineStrings: !0,
                blockKeywords: a("catch class enum do else finally for forSome if match switch try while"),
                defKeywords: a("class enum def object package trait type val var"),
                atoms: a("true false null"),
                indentStatements: !1,
                indentSwitch: !1,
                isOperatorChar: /[+\-*&%=<>!?|\/#:@]/,
                hooks: {
                    "@": function(t) {
                        return t.eatWhile(/[\w\$_]/), "meta"
                    },
                    '"': function(t, e) {
                        return !!t.match('""') && (e.tokenize = g, e.tokenize(t, e))
                    },
                    "'": function(t) {
                        return t.eatWhile(/[\w\$_\xa1-\uffff]/), "atom"
                    },
                    "=": function(t, n) {
                        var r = n.context;
                        return !("}" != r.type || !r.align || !t.eat(">")) && (n.context = new e(r.indented, r.column, r.type, r.info, null, r.prev), "operator")
                    },
                    "/": function(t, e) {
                        return !!t.eat("*") && (e.tokenize = v(1), e.tokenize(t, e))
                    }
                },
                modeProps: {
                    closeBrackets: {
                        triples: '"'
                    }
                }
            }), m("text/x-kotlin", {
                name: "clike",
                keywords: a("package as typealias class interface this super val operator var fun for is in This throw return annotation break continue object if else while do try when !in !is as? file import where by get set abstract enum open inner override private public internal protected catch finally out final vararg reified dynamic companion constructor init sealed field property receiver param sparam lateinit data inline noinline tailrec external annotation crossinline const operator infix suspend actual expect setparam"),
                types: a("Boolean Byte Character CharSequence Class ClassLoader Cloneable Comparable Compiler Double Exception Float Integer Long Math Number Object Package Pair Process Runtime Runnable SecurityManager Short StackTraceElement StrictMath String StringBuffer System Thread ThreadGroup ThreadLocal Throwable Triple Void Annotation Any BooleanArray ByteArray Char CharArray DeprecationLevel DoubleArray Enum FloatArray Function Int IntArray Lazy LazyThreadSafetyMode LongArray Nothing ShortArray Unit"),
                intendSwitch: !1,
                indentStatements: !1,
                multiLineStrings: !0,
                number: /^(?:0x[a-f\d_]+|0b[01_]+|(?:[\d_]+(\.\d+)?|\.\d+)(?:e[-+]?[\d_]+)?)(u|ll?|l|f)?/i,
                blockKeywords: a("catch class do else finally for if where try while enum"),
                defKeywords: a("class val var object interface fun"),
                atoms: a("true false null this"),
                hooks: {
                    "@": function(t) {
                        return t.eatWhile(/[\w\$_]/), "meta"
                    },
                    '"': function(t, e) {
                        return e.tokenize = y(t.match('""')), e.tokenize(t, e)
                    },
                    indent: function(t, e, n, r) {
                        var i = n && n.charAt(0);
                        return "}" != t.prevToken && ")" != t.prevToken || "" != n ? "operator" == t.prevToken && "}" != n || "variable" == t.prevToken && "." == i || ("}" == t.prevToken || ")" == t.prevToken) && "." == i ? 2 * r + e.indented : e.align && "}" == e.type ? e.indented + (t.context.type == (n || "").charAt(0) ? 0 : r) : void 0 : t.indented
                    }
                },
                modeProps: {
                    closeBrackets: {
                        triples: '"'
                    }
                }
            }), m(["x-shader/x-vertex", "x-shader/x-fragment"], {
                name: "clike",
                keywords: a("sampler1D sampler2D sampler3D samplerCube sampler1DShadow sampler2DShadow const attribute uniform varying break continue discard return for while do if else struct in out inout"),
                types: a("float int bool void vec2 vec3 vec4 ivec2 ivec3 ivec4 bvec2 bvec3 bvec4 mat2 mat3 mat4"),
                blockKeywords: a("for while do if else struct"),
                builtin: a("radians degrees sin cos tan asin acos atan pow exp log exp2 sqrt inversesqrt abs sign floor ceil fract mod min max clamp mix step smoothstep length distance dot cross normalize ftransform faceforward reflect refract matrixCompMult lessThan lessThanEqual greaterThan greaterThanEqual equal notEqual any all not texture1D texture1DProj texture1DLod texture1DProjLod texture2D texture2DProj texture2DLod texture2DProjLod texture3D texture3DProj texture3DLod texture3DProjLod textureCube textureCubeLod shadow1D shadow2D shadow1DProj shadow2DProj shadow1DLod shadow2DLod shadow1DProjLod shadow2DProjLod dFdx dFdy fwidth noise1 noise2 noise3 noise4"),
                atoms: a("true false gl_FragColor gl_SecondaryColor gl_Normal gl_Vertex gl_MultiTexCoord0 gl_MultiTexCoord1 gl_MultiTexCoord2 gl_MultiTexCoord3 gl_MultiTexCoord4 gl_MultiTexCoord5 gl_MultiTexCoord6 gl_MultiTexCoord7 gl_FogCoord gl_PointCoord gl_Position gl_PointSize gl_ClipVertex gl_FrontColor gl_BackColor gl_FrontSecondaryColor gl_BackSecondaryColor gl_TexCoord gl_FogFragCoord gl_FragCoord gl_FrontFacing gl_FragData gl_FragDepth gl_ModelViewMatrix gl_ProjectionMatrix gl_ModelViewProjectionMatrix gl_TextureMatrix gl_NormalMatrix gl_ModelViewMatrixInverse gl_ProjectionMatrixInverse gl_ModelViewProjectionMatrixInverse gl_TexureMatrixTranspose gl_ModelViewMatrixInverseTranspose gl_ProjectionMatrixInverseTranspose gl_ModelViewProjectionMatrixInverseTranspose gl_TextureMatrixInverseTranspose gl_NormalScale gl_DepthRange gl_ClipPlane gl_Point gl_FrontMaterial gl_BackMaterial gl_LightSource gl_LightModel gl_FrontLightModelProduct gl_BackLightModelProduct gl_TextureColor gl_EyePlaneS gl_EyePlaneT gl_EyePlaneR gl_EyePlaneQ gl_FogParameters gl_MaxLights gl_MaxClipPlanes gl_MaxTextureUnits gl_MaxTextureCoords gl_MaxVertexAttribs gl_MaxVertexUniformComponents gl_MaxVaryingFloats gl_MaxVertexTextureImageUnits gl_MaxTextureImageUnits gl_MaxFragmentUniformComponents gl_MaxCombineTextureImageUnits gl_MaxDrawBuffers"),
                indentSwitch: !1,
                hooks: {
                    "#": l
                },
                modeProps: {
                    fold: ["brace", "include"]
                }
            }), m("text/x-nesc", {
                name: "clike",
                keywords: a(x + "as atomic async call command component components configuration event generic implementation includes interface module new norace nx_struct nx_union post provides signal task uses abstract extends"),
                types: a(w),
                blockKeywords: a("case do else for if switch while struct"),
                atoms: a("null true false"),
                hooks: {
                    "#": l
                },
                modeProps: {
                    fold: ["brace", "include"]
                }
            }), m("text/x-objectivec", {
                name: "clike",
                keywords: a(x + "inline restrict _Bool _Complex _Imaginary BOOL Class bycopy byref id IMP in inout nil oneway out Protocol SEL self super atomic nonatomic retain copy readwrite readonly"),
                types: a(w),
                atoms: a("YES NO NULL NILL ON OFF true false"),
                hooks: {
                    "@": function(t) {
                        return t.eatWhile(/[\w\$]/), "keyword"
                    },
                    "#": l,
                    indent: function(t, e, n) {
                        if ("statement" == e.type && /^@\w/.test(n)) return e.indented
                    }
                },
                modeProps: {
                    fold: "brace"
                }
            }), m("text/x-squirrel", {
                name: "clike",
                keywords: a("base break clone continue const default delete enum extends function in class foreach local resume return this throw typeof yield constructor instanceof static"),
                types: a(w),
                blockKeywords: a("case catch class else for foreach if switch try while"),
                defKeywords: a("function local class"),
                typeFirstDefinitions: !0,
                atoms: a("true false null"),
                hooks: {
                    "#": l
                },
                modeProps: {
                    fold: ["brace", "include"]
                }
            });
            var C = null;
            m("text/x-ceylon", {
                name: "clike",
                keywords: a("abstracts alias assembly assert assign break case catch class continue dynamic else exists extends finally for function given if import in interface is let module new nonempty object of out outer package return satisfies super switch then this throw try value void while"),
                types: function(t) {
                    var e = t.charAt(0);
                    return e === e.toUpperCase() && e !== e.toLowerCase()
                },
                blockKeywords: a("case catch class dynamic else finally for function if interface module new object switch try while"),
                defKeywords: a("class dynamic function interface module object package value"),
                builtin: a("abstract actual aliased annotation by default deprecated doc final formal late license native optional sealed see serializable shared suppressWarnings tagged throws variable"),
                isPunctuationChar: /[\[\]{}\(\),;\:\.`]/,
                isOperatorChar: /[+\-*&%=<>!?|^~:\/]/,
                numberStart: /[\d#$]/,
                number: /^(?:#[\da-fA-F_]+|\$[01_]+|[\d_]+[kMGTPmunpf]?|[\d_]+\.[\d_]+(?:[eE][-+]?\d+|[kMGTPmunpf]|)|)/i,
                multiLineStrings: !0,
                typeFirstDefinitions: !0,
                atoms: a("true false null larger smaller equal empty finished"),
                indentSwitch: !1,
                styleDefs: !1,
                hooks: {
                    "@": function(t) {
                        return t.eatWhile(/[\w\$_]/), "meta"
                    },
                    '"': function(t, e) {
                        return e.tokenize = b(t.match('""') ? "triple" : "single"), e.tokenize(t, e)
                    },
                    "`": function(t, e) {
                        return !(!C || !t.match("`")) && (e.tokenize = C, C = null, e.tokenize(t, e))
                    },
                    "'": function(t) {
                        return t.eatWhile(/[\w\$_\xa1-\uffff]/), "atom"
                    },
                    token: function(t, e, n) {
                        if (("variable" == n || "type" == n) && "." == e.prevToken) return "variable-2"
                    }
                },
                modeProps: {
                    fold: ["brace", "import"],
                    closeBrackets: {
                        triples: '"'
                    }
                }
            })
        })
    }, function(t, e, n) {
        ! function(t) {
            t(n(0))
        }(function(t) {
            "use strict";
            t.defineMode("groovy", function(e) {
                function n(t) {
                    for (var e = {}, n = t.split(" "), r = 0; r < n.length; ++r) e[n[r]] = !0;
                    return e
                }

                function r(t, e) {
                    var n = t.next();
                    if ('"' == n || "'" == n) return i(n, t, e);
                    if (/[\[\]{}\(\),;\:\.]/.test(n)) return f = n, null;
                    if (/\d/.test(n)) return t.eatWhile(/[\w\.]/), t.eat(/eE/) && (t.eat(/\+\-/), t.eatWhile(/\d/)), "number";
                    if ("/" == n) {
                        if (t.eat("*")) return e.tokenize.push(a), a(t, e);
                        if (t.eat("/")) return t.skipToEnd(), "comment";
                        if (s(e.lastToken, !1)) return i(n, t, e)
                    }
                    if ("-" == n && t.eat(">")) return f = "->", null;
                    if (/[+\-*&%=<>!?|\/~]/.test(n)) return t.eatWhile(/[+\-*&%=<>|~]/), "operator";
                    if (t.eatWhile(/[\w\$_]/), "@" == n) return t.eatWhile(/[\w\$_\.]/), "meta";
                    if ("." == e.lastToken) return "property";
                    if (t.eat(":")) return f = "proplabel", "property";
                    var r = t.current();
                    return m.propertyIsEnumerable(r) ? "atom" : d.propertyIsEnumerable(r) ? (h.propertyIsEnumerable(r) ? f = "newstatement" : p.propertyIsEnumerable(r) && (f = "standalone"), "keyword") : "variable"
                }

                function i(t, e, n) {
                    function r(e, n) {
                        for (var r, a = !1, s = !i; null != (r = e.next());) {
                            if (r == t && !a) {
                                if (!i) break;
                                if (e.match(t + t)) {
                                    s = !0;
                                    break
                                }
                            }
                            if ('"' == t && "$" == r && !a && e.eat("{")) return n.tokenize.push(o()), "string";
                            a = !a && "\\" == r
                        }
                        return s && n.tokenize.pop(), "string"
                    }
                    var i = !1;
                    if ("/" != t && e.eat(t)) {
                        if (!e.eat(t)) return "string";
                        i = !0
                    }
                    return n.tokenize.push(r), r(e, n)
                }

                function o() {
                    function t(t, n) {
                        if ("}" == t.peek()) {
                            if (0 == --e) return n.tokenize.pop(), n.tokenize[n.tokenize.length - 1](t, n)
                        } else "{" == t.peek() && e++;
                        return r(t, n)
                    }
                    var e = 1;
                    return t.isBase = !0, t
                }

                function a(t, e) {
                    for (var n, r = !1; n = t.next();) {
                        if ("/" == n && r) {
                            e.tokenize.pop();
                            break
                        }
                        r = "*" == n
                    }
                    return "comment"
                }

                function s(t, e) {
                    return !t || "operator" == t || "->" == t || /[\.\[\{\(,;:]/.test(t) || "newstatement" == t || "keyword" == t || "proplabel" == t || "standalone" == t && !e
                }

                function l(t, e, n, r, i) {
                    this.indented = t, this.column = e, this.type = n, this.align = r, this.prev = i
                }

                function c(t, e, n) {
                    return t.context = new l(t.indented, e, n, null, t.context)
                }

                function u(t) {
                    var e = t.context.type;
                    return ")" != e && "]" != e && "}" != e || (t.indented = t.context.indented), t.context = t.context.prev
                }
                var f, d = n("abstract as assert boolean break byte case catch char class const continue def default do double else enum extends final finally float for goto if implements import in instanceof int interface long native new package private protected public return short static strictfp super switch synchronized threadsafe throw throws trait transient try void volatile while"),
                    h = n("catch class def do else enum finally for if interface switch trait try while"),
                    p = n("return break continue"),
                    m = n("null true false this");
                return r.isBase = !0, {
                    startState: function(t) {
                        return {
                            tokenize: [r],
                            context: new l((t || 0) - e.indentUnit, 0, "top", !1),
                            indented: 0,
                            startOfLine: !0,
                            lastToken: null
                        }
                    },
                    token: function(t, e) {
                        var n = e.context;
                        if (t.sol() && (null == n.align && (n.align = !1), e.indented = t.indentation(), e.startOfLine = !0, "statement" != n.type || s(e.lastToken, !0) || (u(e), n = e.context)), t.eatSpace()) return null;
                        f = null;
                        var r = e.tokenize[e.tokenize.length - 1](t, e);
                        if ("comment" == r) return r;
                        if (null == n.align && (n.align = !0), ";" != f && ":" != f || "statement" != n.type)
                            if ("->" == f && "statement" == n.type && "}" == n.prev.type) u(e), e.context.align = !1;
                            else if ("{" == f) c(e, t.column(), "}");
                        else if ("[" == f) c(e, t.column(), "]");
                        else if ("(" == f) c(e, t.column(), ")");
                        else if ("}" == f) {
                            for (;
                                "statement" == n.type;) n = u(e);
                            for ("}" == n.type && (n = u(e));
                                "statement" == n.type;) n = u(e)
                        } else f == n.type ? u(e) : ("}" == n.type || "top" == n.type || "statement" == n.type && "newstatement" == f) && c(e, t.column(), "statement");
                        else u(e);
                        return e.startOfLine = !1, e.lastToken = f || r, r
                    },
                    indent: function(n, r) {
                        if (!n.tokenize[n.tokenize.length - 1].isBase) return t.Pass;
                        var i = r && r.charAt(0),
                            o = n.context;
                        "statement" != o.type || s(n.lastToken, !0) || (o = o.prev);
                        var a = i == o.type;
                        return "statement" == o.type ? o.indented + ("{" == i ? 0 : e.indentUnit) : o.align ? o.column + (a ? 0 : 1) : o.indented + (a ? 0 : e.indentUnit)
                    },
                    electricChars: "{}",
                    closeBrackets: {
                        triples: "'\""
                    },
                    fold: "brace"
                }
            }), t.defineMIME("text/x-groovy", "groovy")
        })
    }, function(t, e, n) {
        ! function(t) {
            t(n(0))
        }(function(t) {
            "use strict";
            var e = {
                    autoSelfClosers: {
                        area: !0,
                        base: !0,
                        br: !0,
                        col: !0,
                        command: !0,
                        embed: !0,
                        frame: !0,
                        hr: !0,
                        img: !0,
                        input: !0,
                        keygen: !0,
                        link: !0,
                        meta: !0,
                        param: !0,
                        source: !0,
                        track: !0,
                        wbr: !0,
                        menuitem: !0
                    },
                    implicitlyClosed: {
                        dd: !0,
                        li: !0,
                        optgroup: !0,
                        option: !0,
                        p: !0,
                        rp: !0,
                        rt: !0,
                        tbody: !0,
                        td: !0,
                        tfoot: !0,
                        th: !0,
                        tr: !0
                    },
                    contextGrabbers: {
                        dd: {
                            dd: !0,
                            dt: !0
                        },
                        dt: {
                            dd: !0,
                            dt: !0
                        },
                        li: {
                            li: !0
                        },
                        option: {
                            option: !0,
                            optgroup: !0
                        },
                        optgroup: {
                            optgroup: !0
                        },
                        p: {
                            address: !0,
                            article: !0,
                            aside: !0,
                            blockquote: !0,
                            dir: !0,
                            div: !0,
                            dl: !0,
                            fieldset: !0,
                            footer: !0,
                            form: !0,
                            h1: !0,
                            h2: !0,
                            h3: !0,
                            h4: !0,
                            h5: !0,
                            h6: !0,
                            header: !0,
                            hgroup: !0,
                            hr: !0,
                            menu: !0,
                            nav: !0,
                            ol: !0,
                            p: !0,
                            pre: !0,
                            section: !0,
                            table: !0,
                            ul: !0
                        },
                        rp: {
                            rp: !0,
                            rt: !0
                        },
                        rt: {
                            rp: !0,
                            rt: !0
                        },
                        tbody: {
                            tbody: !0,
                            tfoot: !0
                        },
                        td: {
                            td: !0,
                            th: !0
                        },
                        tfoot: {
                            tbody: !0
                        },
                        th: {
                            td: !0,
                            th: !0
                        },
                        thead: {
                            tbody: !0,
                            tfoot: !0
                        },
                        tr: {
                            tr: !0
                        }
                    },
                    doNotIndent: {
                        pre: !0
                    },
                    allowUnquoted: !0,
                    allowMissing: !0,
                    caseFold: !0
                },
                n = {
                    autoSelfClosers: {},
                    implicitlyClosed: {},
                    contextGrabbers: {},
                    doNotIndent: {},
                    allowUnquoted: !1,
                    allowMissing: !1,
                    allowMissingTagName: !1,
                    caseFold: !1
                };
            t.defineMode("xml", function(r, i) {
                function o(t, e) {
                    function n(n) {
                        return e.tokenize = n, n(t, e)
                    }
                    var r = t.next();
                    if ("<" == r) return t.eat("!") ? t.eat("[") ? t.match("CDATA[") ? n(l("atom", "]]>")) : null : t.match("--") ? n(l("comment", "--\x3e")) : t.match("DOCTYPE", !0, !0) ? (t.eatWhile(/[\w\._\-]/), n(c(1))) : null : t.eat("?") ? (t.eatWhile(/[\w\._\-]/), e.tokenize = l("meta", "?>"), "meta") : (E = t.eat("/") ? "closeTag" : "openTag", e.tokenize = a, "tag bracket");
                    if ("&" == r) {
                        var i;
                        return i = t.eat("#") ? t.eat("x") ? t.eatWhile(/[a-fA-F\d]/) && t.eat(";") : t.eatWhile(/[\d]/) && t.eat(";") : t.eatWhile(/[\w\.\-:]/) && t.eat(";"), i ? "atom" : "error"
                    }
                    return t.eatWhile(/[^&<]/), null
                }

                function a(t, e) {
                    var n = t.next();
                    if (">" == n || "/" == n && t.eat(">")) return e.tokenize = o, E = ">" == n ? "endTag" : "selfcloseTag", "tag bracket";
                    if ("=" == n) return E = "equals", null;
                    if ("<" == n) {
                        e.tokenize = o, e.state = h, e.tagName = e.tagStart = null;
                        var r = e.tokenize(t, e);
                        return r ? r + " tag error" : "tag error"
                    }
                    return /[\'\"]/.test(n) ? (e.tokenize = s(n), e.stringStartCol = t.column(), e.tokenize(t, e)) : (t.match(/^[^\s\u00a0=<>\"\']*[^\s\u00a0=<>\"\'\/]/), "word")
                }

                function s(t) {
                    var e = function(e, n) {
                        for (; !e.eol();)
                            if (e.next() == t) {
                                n.tokenize = a;
                                break
                            }
                        return "string"
                    };
                    return e.isInAttribute = !0, e
                }

                function l(t, e) {
                    return function(n, r) {
                        for (; !n.eol();) {
                            if (n.match(e)) {
                                r.tokenize = o;
                                break
                            }
                            n.next()
                        }
                        return t
                    }
                }

                function c(t) {
                    return function(e, n) {
                        for (var r; null != (r = e.next());) {
                            if ("<" == r) return n.tokenize = c(t + 1), n.tokenize(e, n);
                            if (">" == r) {
                                if (1 == t) {
                                    n.tokenize = o;
                                    break
                                }
                                return n.tokenize = c(t - 1), n.tokenize(e, n)
                            }
                        }
                        return "meta"
                    }
                }

                function u(t, e, n) {
                    this.prev = t.context, this.tagName = e, this.indent = t.indented, this.startOfLine = n, (k.doNotIndent.hasOwnProperty(e) || t.context && t.context.noIndent) && (this.noIndent = !0)
                }

                function f(t) {
                    t.context && (t.context = t.context.prev)
                }

                function d(t, e) {
                    for (var n;;) {
                        if (!t.context) return;
                        if (n = t.context.tagName, !k.contextGrabbers.hasOwnProperty(n) || !k.contextGrabbers[n].hasOwnProperty(e)) return;
                        f(t)
                    }
                }

                function h(t, e, n) {
                    return "openTag" == t ? (n.tagStart = e.column(), p) : "closeTag" == t ? m : h
                }

                function p(t, e, n) {
                    return "word" == t ? (n.tagName = e.current(), T = "tag", y) : k.allowMissingTagName && "endTag" == t ? (T = "tag bracket", y(t, e, n)) : (T = "error", p)
                }

                function m(t, e, n) {
                    if ("word" == t) {
                        var r = e.current();
                        return n.context && n.context.tagName != r && k.implicitlyClosed.hasOwnProperty(n.context.tagName) && f(n), n.context && n.context.tagName == r || !1 === k.matchClosing ? (T = "tag", g) : (T = "tag error", v)
                    }
                    return k.allowMissingTagName && "endTag" == t ? (T = "tag bracket", g(t, e, n)) : (T = "error", v)
                }

                function g(t, e, n) {
                    return "endTag" != t ? (T = "error", g) : (f(n), h)
                }

                function v(t, e, n) {
                    return T = "error", g(t, e, n)
                }

                function y(t, e, n) {
                    if ("word" == t) return T = "attribute", b;
                    if ("endTag" == t || "selfcloseTag" == t) {
                        var r = n.tagName,
                            i = n.tagStart;
                        return n.tagName = n.tagStart = null, "selfcloseTag" == t || k.autoSelfClosers.hasOwnProperty(r) ? d(n, r) : (d(n, r), n.context = new u(n, r, i == n.indented)), h
                    }
                    return T = "error", y
                }

                function b(t, e, n) {
                    return "equals" == t ? x : (k.allowMissing || (T = "error"), y(t, e, n))
                }

                function x(t, e, n) {
                    return "string" == t ? w : "word" == t && k.allowUnquoted ? (T = "string", y) : (T = "error", y(t, e, n))
                }

                function w(t, e, n) {
                    return "string" == t ? w : y(t, e, n)
                }
                var C = r.indentUnit,
                    k = {},
                    _ = i.htmlMode ? e : n;
                for (var S in _) k[S] = _[S];
                for (var S in i) k[S] = i[S];
                var E, T;
                return o.isInText = !0, {
                    startState: function(t) {
                        var e = {
                            tokenize: o,
                            state: h,
                            indented: t || 0,
                            tagName: null,
                            tagStart: null,
                            context: null
                        };
                        return null != t && (e.baseIndent = t), e
                    },
                    token: function(t, e) {
                        if (!e.tagName && t.sol() && (e.indented = t.indentation()), t.eatSpace()) return null;
                        E = null;
                        var n = e.tokenize(t, e);
                        return (n || E) && "comment" != n && (T = null, e.state = e.state(E || n, t, e), T && (n = "error" == T ? n + " error" : T)), n
                    },
                    indent: function(e, n, r) {
                        var i = e.context;
                        if (e.tokenize.isInAttribute) return e.tagStart == e.indented ? e.stringStartCol + 1 : e.indented + C;
                        if (i && i.noIndent) return t.Pass;
                        if (e.tokenize != a && e.tokenize != o) return r ? r.match(/^(\s*)/)[0].length : 0;
                        if (e.tagName) return !1 !== k.multilineTagIndentPastTag ? e.tagStart + e.tagName.length + 2 : e.tagStart + C * (k.multilineTagIndentFactor || 1);
                        if (k.alignCDATA && /<!\[CDATA\[/.test(n)) return 0;
                        var s = n && /^<(\/)?([\w_:\.-]*)/.exec(n);
                        if (s && s[1])
                            for (; i;) {
                                if (i.tagName == s[2]) {
                                    i = i.prev;
                                    break
                                }
                                if (!k.implicitlyClosed.hasOwnProperty(i.tagName)) break;
                                i = i.prev
                            } else if (s)
                                for (; i;) {
                                    var l = k.contextGrabbers[i.tagName];
                                    if (!l || !l.hasOwnProperty(s[2])) break;
                                    i = i.prev
                                }
                            for (; i && i.prev && !i.startOfLine;) i = i.prev;
                        return i ? i.indent + C : e.baseIndent || 0
                    },
                    electricInput: /<\/[\s\w:]+>$/,
                    blockCommentStart: "\x3c!--",
                    blockCommentEnd: "--\x3e",
                    configuration: k.htmlMode ? "html" : "xml",
                    helperType: k.htmlMode ? "html" : "xml",
                    skipAttribute: function(t) {
                        t.state == x && (t.state = y)
                    }
                }
            }), t.defineMIME("text/xml", "xml"), t.defineMIME("application/xml", "xml"), t.mimeModes.hasOwnProperty("text/html") || t.defineMIME("text/html", {
                name: "xml",
                htmlMode: !0
            })
        })
    }, function(t, e, n) {
        ! function(t) {
            t(n(0))
        }(function(t) {
            function e(t, e, r) {
                var i = t.getLineHandle(e.line),
                    o = e.ch - 1,
                    l = r && r.afterCursor;
                null == l && (l = /(^| )cm-fat-cursor($| )/.test(t.getWrapperElement().className));
                var c = !l && o >= 0 && s[i.text.charAt(o)] || s[i.text.charAt(++o)];
                if (!c) return null;
                var u = ">" == c.charAt(1) ? 1 : -1;
                if (r && r.strict && u > 0 != (o == e.ch)) return null;
                var f = t.getTokenTypeAt(a(e.line, o + 1)),
                    d = n(t, a(e.line, o + (u > 0 ? 1 : 0)), u, f || null, r);
                return null == d ? null : {
                    from: a(e.line, o),
                    to: d && d.pos,
                    match: d && d.ch == c.charAt(0),
                    forward: u > 0
                }
            }

            function n(t, e, n, r, i) {
                for (var o = i && i.maxScanLineLength || 1e4, l = i && i.maxScanLines || 1e3, c = [], u = i && i.bracketRegex ? i.bracketRegex : /[(){}[\]]/, f = n > 0 ? Math.min(e.line + l, t.lastLine() + 1) : Math.max(t.firstLine() - 1, e.line - l), d = e.line; d != f; d += n) {
                    var h = t.getLine(d);
                    if (h) {
                        var p = n > 0 ? 0 : h.length - 1,
                            m = n > 0 ? h.length : -1;
                        if (!(h.length > o))
                            for (d == e.line && (p = e.ch - (n < 0 ? 1 : 0)); p != m; p += n) {
                                var g = h.charAt(p);
                                if (u.test(g) && (void 0 === r || t.getTokenTypeAt(a(d, p + 1)) == r)) {
                                    var v = s[g];
                                    if (">" == v.charAt(1) == n > 0) c.push(g);
                                    else {
                                        if (!c.length) return {
                                            pos: a(d, p),
                                            ch: g
                                        };
                                        c.pop()
                                    }
                                }
                            }
                    }
                }
                return d - n != (n > 0 ? t.lastLine() : t.firstLine()) && null
            }

            function r(t, n, r) {
                for (var i = t.state.matchBrackets.maxHighlightLineLength || 1e3, s = [], l = t.listSelections(), c = 0; c < l.length; c++) {
                    var u = l[c].empty() && e(t, l[c].head, r);
                    if (u && t.getLine(u.from.line).length <= i) {
                        var f = u.match ? "CodeMirror-matchingbracket" : "CodeMirror-nonmatchingbracket";
                        s.push(t.markText(u.from, a(u.from.line, u.from.ch + 1), {
                            className: f
                        })), u.to && t.getLine(u.to.line).length <= i && s.push(t.markText(u.to, a(u.to.line, u.to.ch + 1), {
                            className: f
                        }))
                    }
                }
                if (s.length) {
                    o && t.state.focused && t.focus();
                    var d = function() {
                        t.operation(function() {
                            for (var t = 0; t < s.length; t++) s[t].clear()
                        })
                    };
                    if (!n) return d;
                    setTimeout(d, 800)
                }
            }

            function i(t) {
                t.operation(function() {
                    t.state.matchBrackets.currentlyHighlighted && (t.state.matchBrackets.currentlyHighlighted(), t.state.matchBrackets.currentlyHighlighted = null), t.state.matchBrackets.currentlyHighlighted = r(t, !1, t.state.matchBrackets)
                })
            }
            var o = /MSIE \d/.test(navigator.userAgent) && (null == document.documentMode || document.documentMode < 8),
                a = t.Pos,
                s = {
                    "(": ")>",
                    ")": "(<",
                    "[": "]>",
                    "]": "[<",
                    "{": "}>",
                    "}": "{<"
                };
            t.defineOption("matchBrackets", !1, function(e, n, r) {
                r && r != t.Init && (e.off("cursorActivity", i), e.state.matchBrackets && e.state.matchBrackets.currentlyHighlighted && (e.state.matchBrackets.currentlyHighlighted(), e.state.matchBrackets.currentlyHighlighted = null)), n && (e.state.matchBrackets = "object" == typeof n ? n : {}, e.on("cursorActivity", i))
            }), t.defineExtension("matchBrackets", function() {
                r(this, !0)
            }), t.defineExtension("findMatchingBracket", function(t, n, r) {
                return (r || "boolean" == typeof n) && (r ? (r.strict = n, n = r) : n = n ? {
                    strict: !0
                } : null), e(this, t, n)
            }), t.defineExtension("scanForBracket", function(t, e, r, i) {
                return n(this, t, e, r, i)
            })
        })
    }, function(t, e, n) {
        ! function(t) {
            t(n(0))
        }(function(t) {
            function e(t, e) {
                return "pairs" == e && "string" == typeof t ? t : "object" == typeof t && null != t[e] ? t[e] : f[e]
            }

            function n(t) {
                for (var e = 0; e < t.length; e++) {
                    var n = t.charAt(e),
                        i = "'" + n + "'";
                    h[i] || (h[i] = r(n))
                }
            }

            function r(t) {
                return function(e) {
                    return l(e, t)
                }
            }

            function i(t) {
                var e = t.state.closeBrackets;
                return !e || e.override ? e : t.getModeAt(t.getCursor()).closeBrackets || e
            }

            function o(n) {
                var r = i(n);
                if (!r || n.getOption("disableInput")) return t.Pass;
                for (var o = e(r, "pairs"), a = n.listSelections(), s = 0; s < a.length; s++) {
                    if (!a[s].empty()) return t.Pass;
                    var l = c(n, a[s].head);
                    if (!l || o.indexOf(l) % 2 != 0) return t.Pass
                }
                for (var s = a.length - 1; s >= 0; s--) {
                    var u = a[s].head;
                    n.replaceRange("", d(u.line, u.ch - 1), d(u.line, u.ch + 1), "+delete")
                }
            }

            function a(n) {
                var r = i(n),
                    o = r && e(r, "explode");
                if (!o || n.getOption("disableInput")) return t.Pass;
                for (var a = n.listSelections(), s = 0; s < a.length; s++) {
                    if (!a[s].empty()) return t.Pass;
                    var l = c(n, a[s].head);
                    if (!l || o.indexOf(l) % 2 != 0) return t.Pass
                }
                n.operation(function() {
                    var t = n.lineSeparator() || "\n";
                    n.replaceSelection(t + t, null), n.execCommand("goCharLeft"), a = n.listSelections();
                    for (var e = 0; e < a.length; e++) {
                        var r = a[e].head.line;
                        n.indentLine(r, null, !0), n.indentLine(r + 1, null, !0)
                    }
                })
            }

            function s(e) {
                var n = t.cmpPos(e.anchor, e.head) > 0;
                return {
                    anchor: new d(e.anchor.line, e.anchor.ch + (n ? -1 : 1)),
                    head: new d(e.head.line, e.head.ch + (n ? 1 : -1))
                }
            }

            function l(n, r) {
                var o = i(n);
                if (!o || n.getOption("disableInput")) return t.Pass;
                var a = e(o, "pairs"),
                    l = a.indexOf(r);
                if (-1 == l) return t.Pass;
                for (var c, f = e(o, "triples"), h = a.charAt(l + 1) == r, p = n.listSelections(), m = l % 2 == 0, g = 0; g < p.length; g++) {
                    var v, y = p[g],
                        b = y.head,
                        x = n.getRange(b, d(b.line, b.ch + 1));
                    if (m && !y.empty()) v = "surround";
                    else if (!h && m || x != r)
                        if (h && b.ch > 1 && f.indexOf(r) >= 0 && n.getRange(d(b.line, b.ch - 2), b) == r + r) {
                            if (b.ch > 2 && /\bstring/.test(n.getTokenTypeAt(d(b.line, b.ch - 2)))) return t.Pass;
                            v = "addFour"
                        } else if (h) {
                        var w = 0 == b.ch ? " " : n.getRange(d(b.line, b.ch - 1), b);
                        if (t.isWordChar(x) || w == r || t.isWordChar(w)) return t.Pass;
                        v = "both"
                    } else {
                        if (!m) return t.Pass;
                        v = "both"
                    } else v = h && u(n, b) ? "both" : f.indexOf(r) >= 0 && n.getRange(b, d(b.line, b.ch + 3)) == r + r + r ? "skipThree" : "skip";
                    if (c) {
                        if (c != v) return t.Pass
                    } else c = v
                }
                var C = l % 2 ? a.charAt(l - 1) : r,
                    k = l % 2 ? r : a.charAt(l + 1);
                n.operation(function() {
                    if ("skip" == c) n.execCommand("goCharRight");
                    else if ("skipThree" == c)
                        for (var t = 0; t < 3; t++) n.execCommand("goCharRight");
                    else if ("surround" == c) {
                        for (var e = n.getSelections(), t = 0; t < e.length; t++) e[t] = C + e[t] + k;
                        n.replaceSelections(e, "around"), e = n.listSelections().slice();
                        for (var t = 0; t < e.length; t++) e[t] = s(e[t]);
                        n.setSelections(e)
                    } else "both" == c ? (n.replaceSelection(C + k, null), n.triggerElectric(C + k), n.execCommand("goCharLeft")) : "addFour" == c && (n.replaceSelection(C + C + C + C, "before"), n.execCommand("goCharRight"))
                })
            }

            function c(t, e) {
                var n = t.getRange(d(e.line, e.ch - 1), d(e.line, e.ch + 1));
                return 2 == n.length ? n : null
            }

            function u(t, e) {
                var n = t.getTokenAt(d(e.line, e.ch + 1));
                return /\bstring/.test(n.type) && n.start == e.ch && (0 == e.ch || !/\bstring/.test(t.getTokenTypeAt(e)))
            }
            var f = {
                    pairs: "()[]{}''\"\"",
                    triples: "",
                    explode: "[]{}"
                },
                d = t.Pos;
            t.defineOption("autoCloseBrackets", !1, function(r, i, o) {
                o && o != t.Init && (r.removeKeyMap(h), r.state.closeBrackets = null), i && (n(e(i, "pairs")), r.state.closeBrackets = i, r.addKeyMap(h))
            });
            var h = {
                Backspace: o,
                Enter: a
            };
            n(f.pairs + "`")
        })
    }, function(t, e, n) {
        ! function(t) {
            t(n(0))
        }(function(t) {
            "use strict";

            function e(t) {
                var e = t.search(o);
                return -1 == e ? 0 : e
            }

            function n(t, e, n) {
                return /\bstring\b/.test(t.getTokenTypeAt(a(e.line, 0))) && !/^[\'\"\`]/.test(n)
            }

            function r(t, e) {
                var n = t.getMode();
                return !1 !== n.useInnerComments && n.innerMode ? t.getModeAt(e) : n
            }
            var i = {},
                o = /[^\s\u00a0]/,
                a = t.Pos;
            t.commands.toggleComment = function(t) {
                t.toggleComment()
            }, t.defineExtension("toggleComment", function(t) {
                t || (t = i);
                for (var e = this, n = 1 / 0, r = this.listSelections(), o = null, s = r.length - 1; s >= 0; s--) {
                    var l = r[s].from(),
                        c = r[s].to();
                    l.line >= n || (c.line >= n && (c = a(n, 0)), n = l.line, null == o ? e.uncomment(l, c, t) ? o = "un" : (e.lineComment(l, c, t), o = "line") : "un" == o ? e.uncomment(l, c, t) : e.lineComment(l, c, t))
                }
            }), t.defineExtension("lineComment", function(t, s, l) {
                l || (l = i);
                var c = this,
                    u = r(c, t),
                    f = c.getLine(t.line);
                if (null != f && !n(c, t, f)) {
                    var d = l.lineComment || u.lineComment;
                    if (!d) return void((l.blockCommentStart || u.blockCommentStart) && (l.fullLines = !0, c.blockComment(t, s, l)));
                    var h = Math.min(0 != s.ch || s.line == t.line ? s.line + 1 : s.line, c.lastLine() + 1),
                        p = null == l.padding ? " " : l.padding,
                        m = l.commentBlankLines || t.line == s.line;
                    c.operation(function() {
                        if (l.indent) {
                            for (var n = null, r = t.line; r < h; ++r) {
                                var i = c.getLine(r),
                                    s = i.slice(0, e(i));
                                (null == n || n.length > s.length) && (n = s)
                            }
                            for (var r = t.line; r < h; ++r) {
                                var i = c.getLine(r),
                                    u = n.length;
                                (m || o.test(i)) && (i.slice(0, u) != n && (u = e(i)), c.replaceRange(n + d + p, a(r, 0), a(r, u)))
                            }
                        } else
                            for (var r = t.line; r < h; ++r)(m || o.test(c.getLine(r))) && c.replaceRange(d + p, a(r, 0))
                    })
                }
            }), t.defineExtension("blockComment", function(t, e, n) {
                n || (n = i);
                var s = this,
                    l = r(s, t),
                    c = n.blockCommentStart || l.blockCommentStart,
                    u = n.blockCommentEnd || l.blockCommentEnd;
                if (!c || !u) return void((n.lineComment || l.lineComment) && 0 != n.fullLines && s.lineComment(t, e, n));
                if (!/\bcomment\b/.test(s.getTokenTypeAt(a(t.line, 0)))) {
                    var f = Math.min(e.line, s.lastLine());
                    f != t.line && 0 == e.ch && o.test(s.getLine(f)) && --f;
                    var d = null == n.padding ? " " : n.padding;
                    t.line > f || s.operation(function() {
                        if (0 != n.fullLines) {
                            var r = o.test(s.getLine(f));
                            s.replaceRange(d + u, a(f)), s.replaceRange(c + d, a(t.line, 0));
                            var i = n.blockCommentLead || l.blockCommentLead;
                            if (null != i)
                                for (var h = t.line + 1; h <= f; ++h)(h != f || r) && s.replaceRange(i + d, a(h, 0))
                        } else s.replaceRange(u, e), s.replaceRange(c, t)
                    })
                }
            }), t.defineExtension("uncomment", function(t, e, n) {
                n || (n = i);
                var s, l = this,
                    c = r(l, t),
                    u = Math.min(0 != e.ch || e.line == t.line ? e.line : e.line - 1, l.lastLine()),
                    f = Math.min(t.line, u),
                    d = n.lineComment || c.lineComment,
                    h = [],
                    p = null == n.padding ? " " : n.padding;
                t: if (d) {
                    for (var m = f; m <= u; ++m) {
                        var g = l.getLine(m),
                            v = g.indexOf(d);
                        if (v > -1 && !/comment/.test(l.getTokenTypeAt(a(m, v + 1))) && (v = -1), -1 == v && o.test(g)) break t;
                        if (v > -1 && o.test(g.slice(0, v))) break t;
                        h.push(g)
                    }
                    if (l.operation(function() {
                            for (var t = f; t <= u; ++t) {
                                var e = h[t - f],
                                    n = e.indexOf(d),
                                    r = n + d.length;
                                n < 0 || (e.slice(r, r + p.length) == p && (r += p.length), s = !0, l.replaceRange("", a(t, n), a(t, r)))
                            }
                        }), s) return !0
                }
                var y = n.blockCommentStart || c.blockCommentStart,
                    b = n.blockCommentEnd || c.blockCommentEnd;
                if (!y || !b) return !1;
                var x = n.blockCommentLead || c.blockCommentLead,
                    w = l.getLine(f),
                    C = w.indexOf(y);
                if (-1 == C) return !1;
                var k = u == f ? w : l.getLine(u),
                    _ = k.indexOf(b, u == f ? C + y.length : 0),
                    S = a(f, C + 1),
                    E = a(u, _ + 1);
                if (-1 == _ || !/comment/.test(l.getTokenTypeAt(S)) || !/comment/.test(l.getTokenTypeAt(E)) || l.getRange(S, E, "\n").indexOf(b) > -1) return !1;
                var T = w.lastIndexOf(y, t.ch),
                    M = -1 == T ? -1 : w.slice(0, t.ch).indexOf(b, T + y.length);
                if (-1 != T && -1 != M && M + b.length != t.ch) return !1;
                M = k.indexOf(b, e.ch);
                var O = k.slice(e.ch).lastIndexOf(y, M - e.ch);
                return T = -1 == M || -1 == O ? -1 : e.ch + O, (-1 == M || -1 == T || T == e.ch) && (l.operation(function() {
                    l.replaceRange("", a(u, _ - (p && k.slice(_ - p.length, _) == p ? p.length : 0)), a(u, _ + b.length));
                    var t = C + y.length;
                    if (p && w.slice(t, t + p.length) == p && (t += p.length), l.replaceRange("", a(f, C), a(f, t)), x)
                        for (var e = f + 1; e <= u; ++e) {
                            var n = l.getLine(e),
                                r = n.indexOf(x);
                            if (-1 != r && !o.test(n.slice(0, r))) {
                                var i = r + x.length;
                                p && n.slice(i, i + p.length) == p && (i += p.length), l.replaceRange("", a(e, r), a(e, i))
                            }
                        }
                }), !0)
            })
        })
    }, function(t, e, n) {
        ! function(t) {
            t(n(0))
        }(function(t) {
            function e(e) {
                if (e.getOption("disableInput")) return t.Pass;
                for (var r, i = e.listSelections(), o = [], a = 0; a < i.length; a++) {
                    var s = i[a].head;
                    if (!/\bcomment\b/.test(e.getTokenTypeAt(s))) return t.Pass;
                    var l = e.getModeAt(s);
                    if (r) {
                        if (r != l) return t.Pass
                    } else r = l;
                    var c = null;
                    if (r.blockCommentStart && r.blockCommentContinue) {
                        var u, f = e.getLine(s.line).slice(0, s.ch),
                            d = f.lastIndexOf(r.blockCommentEnd);
                        if (-1 != d && d == s.ch - r.blockCommentEnd.length);
                        else if ((u = f.lastIndexOf(r.blockCommentStart)) > -1 && u > d) {
                            if (c = f.slice(0, u), /\S/.test(c)) {
                                c = "";
                                for (var h = 0; h < u; ++h) c += " "
                            }
                        } else(u = f.indexOf(r.blockCommentContinue)) > -1 && !/\S/.test(f.slice(0, u)) && (c = f.slice(0, u));
                        null != c && (c += r.blockCommentContinue)
                    }
                    if (null == c && r.lineComment && n(e)) {
                        var f = e.getLine(s.line),
                            u = f.indexOf(r.lineComment);
                        u > -1 && (c = f.slice(0, u), /\S/.test(c) ? c = null : c += r.lineComment + f.slice(u + r.lineComment.length).match(/^\s*/)[0])
                    }
                    if (null == c) return t.Pass;
                    o[a] = "\n" + c
                }
                e.operation(function() {
                    for (var t = i.length - 1; t >= 0; t--) e.replaceRange(o[t], i[t].from(), i[t].to(), "+insert")
                })
            }

            function n(t) {
                var e = t.getOption("continueComments");
                return !e || "object" != typeof e || !1 !== e.continueLineComment
            }
            t.defineOption("continueComments", null, function(n, r, i) {
                if (i && i != t.Init && n.removeKeyMap("continueComment"), r) {
                    var o = "Enter";
                    "string" == typeof r ? o = r : "object" == typeof r && r.key && (o = r.key);
                    var a = {
                        name: "continueComment"
                    };
                    a[o] = e, n.addKeyMap(a)
                }
            })
        })
    }, function(t, e, n) {
        ! function(t) {
            t(n(0))
        }(function(t) {
            "use strict";
            t.defineMode("javascript", function(e, n) {
                function r(t) {
                    for (var e, n = !1, r = !1; null != (e = t.next());) {
                        if (!n) {
                            if ("/" == e && !r) return;
                            "[" == e ? r = !0 : r && "]" == e && (r = !1)
                        }
                        n = !n && "\\" == e
                    }
                }

                function i(t, e, n) {
                    return zt = t, jt = n, e
                }

                function o(t, e) {
                    var n = t.next();
                    if ('"' == n || "'" == n) return e.tokenize = a(n), e.tokenize(t, e);
                    if ("." == n && t.match(/^\d+(?:[eE][+\-]?\d+)?/)) return i("number", "number");
                    if ("." == n && t.match("..")) return i("spread", "meta");
                    if (/[\[\]{}\(\),;\:\.]/.test(n)) return i(n);
                    if ("=" == n && t.eat(">")) return i("=>", "operator");
                    if ("0" == n && t.match(/^(?:x[\da-f]+|o[0-7]+|b[01]+)n?/i)) return i("number", "number");
                    if (/\d/.test(n)) return t.match(/^\d*(?:n|(?:\.\d*)?(?:[eE][+\-]?\d+)?)?/), i("number", "number");
                    if ("/" == n) return t.eat("*") ? (e.tokenize = s, s(t, e)) : t.eat("/") ? (t.skipToEnd(), i("comment", "comment")) : Rt(t, e, 1) ? (r(t), t.match(/^\b(([gimyus])(?![gimyus]*\2))+\b/), i("regexp", "string-2")) : (t.eat("="), i("operator", "operator", t.current()));
                    if ("`" == n) return e.tokenize = l, l(t, e);
                    if ("#" == n) return t.skipToEnd(), i("error", "error");
                    if ($t.test(n)) return ">" == n && e.lexical && ">" == e.lexical.type || (t.eat("=") ? "!" != n && "=" != n || t.eat("=") : /[<>*+\-]/.test(n) && (t.eat(n), ">" == n && t.eat(n))), i("operator", "operator", t.current());
                    if (Gt.test(n)) {
                        t.eatWhile(Gt);
                        var o = t.current();
                        if ("." != e.lastType) {
                            if (Kt.propertyIsEnumerable(o)) {
                                var c = Kt[o];
                                return i(c.type, c.style, o)
                            }
                            if ("async" == o && t.match(/^(\s|\/\*.*?\*\/)*[\[\(\w]/, !1)) return i("async", "keyword", o)
                        }
                        return i("variable", "variable", o)
                    }
                }

                function a(t) {
                    return function(e, n) {
                        var r, a = !1;
                        if (Wt && "@" == e.peek() && e.match(qt)) return n.tokenize = o, i("jsonld-keyword", "meta");
                        for (; null != (r = e.next()) && (r != t || a);) a = !a && "\\" == r;
                        return a || (n.tokenize = o), i("string", "string")
                    }
                }

                function s(t, e) {
                    for (var n, r = !1; n = t.next();) {
                        if ("/" == n && r) {
                            e.tokenize = o;
                            break
                        }
                        r = "*" == n
                    }
                    return i("comment", "comment")
                }

                function l(t, e) {
                    for (var n, r = !1; null != (n = t.next());) {
                        if (!r && ("`" == n || "$" == n && t.eat("{"))) {
                            e.tokenize = o;
                            break
                        }
                        r = !r && "\\" == n
                    }
                    return i("quasi", "string-2", t.current())
                }

                function c(t, e) {
                    e.fatArrowAt && (e.fatArrowAt = null);
                    var n = t.string.indexOf("=>", t.start);
                    if (!(n < 0)) {
                        if (Vt) {
                            var r = /:\s*(?:\w+(?:<[^>]*>|\[\])?|\{[^}]*\})\s*$/.exec(t.string.slice(t.start, n));
                            r && (n = r.index)
                        }
                        for (var i = 0, o = !1, a = n - 1; a >= 0; --a) {
                            var s = t.string.charAt(a),
                                l = Yt.indexOf(s);
                            if (l >= 0 && l < 3) {
                                if (!i) {
                                    ++a;
                                    break
                                }
                                if (0 == --i) {
                                    "(" == s && (o = !0);
                                    break
                                }
                            } else if (l >= 3 && l < 6) ++i;
                            else if (Gt.test(s)) o = !0;
                            else {
                                if (/["'\/]/.test(s)) return;
                                if (o && !i) {
                                    ++a;
                                    break
                                }
                            }
                        }
                        o && !i && (e.fatArrowAt = a)
                    }
                }

                function u(t, e, n, r, i, o) {
                    this.indented = t, this.column = e, this.type = n, this.prev = i, this.info = o, null != r && (this.align = r)
                }

                function f(t, e) {
                    for (var n = t.localVars; n; n = n.next)
                        if (n.name == e) return !0;
                    for (var r = t.context; r; r = r.prev)
                        for (var n = r.vars; n; n = n.next)
                            if (n.name == e) return !0
                }

                function d(t, e, n, r, i) {
                    var o = t.cc;
                    for (Xt.state = t, Xt.stream = i, Xt.marked = null, Xt.cc = o, Xt.style = e, t.lexical.hasOwnProperty("align") || (t.lexical.align = !0);;) {
                        if ((o.length ? o.pop() : Ut ? O : T)(n, r)) {
                            for (; o.length && o[o.length - 1].lex;) o.pop()();
                            return Xt.marked ? Xt.marked : "variable" == n && f(t, r) ? "variable-2" : e
                        }
                    }
                }

                function h() {
                    for (var t = arguments.length - 1; t >= 0; t--) Xt.cc.push(arguments[t])
                }

                function p() {
                    return h.apply(null, arguments), !0
                }

                function m(t, e) {
                    for (var n = e; n; n = n.next)
                        if (n.name == t) return !0;
                    return !1
                }

                function g(t) {
                    var e = Xt.state;
                    if (Xt.marked = "def", e.context)
                        if ("var" == e.lexical.info && e.context && e.context.block) {
                            var r = v(t, e.context);
                            if (null != r) return void(e.context = r)
                        } else if (!m(t, e.localVars)) return void(e.localVars = new x(t, e.localVars));
                    n.globalVars && !m(t, e.globalVars) && (e.globalVars = new x(t, e.globalVars))
                }

                function v(t, e) {
                    if (e) {
                        if (e.block) {
                            var n = v(t, e.prev);
                            return n ? n == e.prev ? e : new b(n, e.vars, !0) : null
                        }
                        return m(t, e.vars) ? e : new b(e.prev, new x(t, e.vars), !1)
                    }
                    return null
                }

                function y(t) {
                    return "public" == t || "private" == t || "protected" == t || "abstract" == t || "readonly" == t
                }

                function b(t, e, n) {
                    this.prev = t, this.vars = e, this.block = n
                }

                function x(t, e) {
                    this.name = t, this.next = e
                }

                function w() {
                    Xt.state.context = new b(Xt.state.context, Xt.state.localVars, !1), Xt.state.localVars = Zt
                }

                function C() {
                    Xt.state.context = new b(Xt.state.context, Xt.state.localVars, !0), Xt.state.localVars = null
                }

                function k() {
                    Xt.state.localVars = Xt.state.context.vars, Xt.state.context = Xt.state.context.prev
                }

                function _(t, e) {
                    var n = function() {
                        var n = Xt.state,
                            r = n.indented;
                        if ("stat" == n.lexical.type) r = n.lexical.indented;
                        else
                            for (var i = n.lexical; i && ")" == i.type && i.align; i = i.prev) r = i.indented;
                        n.lexical = new u(r, Xt.stream.column(), t, null, n.lexical, e)
                    };
                    return n.lex = !0, n
                }

                function S() {
                    var t = Xt.state;
                    t.lexical.prev && (")" == t.lexical.type && (t.indented = t.lexical.indented), t.lexical = t.lexical.prev)
                }

                function E(t) {
                    function e(n) {
                        return n == t ? p() : ";" == t || "}" == n || ")" == n || "]" == n ? h() : p(e)
                    }
                    return e
                }

                function T(t, e) {
                    return "var" == t ? p(_("vardef", e), lt, E(";"), S) : "keyword a" == t ? p(_("form"), A, T, S) : "keyword b" == t ? p(_("form"), T, S) : "keyword d" == t ? Xt.stream.match(/^\s*$/, !1) ? p() : p(_("stat"), P, E(";"), S) : "debugger" == t ? p(E(";")) : "{" == t ? p(_("}"), C, J, S, k) : ";" == t ? p() : "if" == t ? ("else" == Xt.state.lexical.info && Xt.state.cc[Xt.state.cc.length - 1] == S && Xt.state.cc.pop()(), p(_("form"), A, T, S, ht)) : "function" == t ? p(bt) : "for" == t ? p(_("form"), pt, T, S) : "class" == t || Vt && "interface" == e ? (Xt.marked = "keyword", p(_("form"), Ct, S)) : "variable" == t ? Vt && "declare" == e ? (Xt.marked = "keyword", p(T)) : Vt && ("module" == e || "enum" == e || "type" == e) && Xt.stream.match(/^\s*\w/, !1) ? (Xt.marked = "keyword", "enum" == e ? p(It) : "type" == e ? p(tt, E("operator"), tt, E(";")) : p(_("form"), ct, E("{"), _("}"), J, S, S)) : Vt && "namespace" == e ? (Xt.marked = "keyword", p(_("form"), O, J, S)) : Vt && "abstract" == e ? (Xt.marked = "keyword", p(T)) : p(_("stat"), U) : "switch" == t ? p(_("form"), A, E("{"), _("}", "switch"), C, J, S, S, k) : "case" == t ? p(O, E(":")) : "default" == t ? p(E(":")) : "catch" == t ? p(_("form"), w, M, T, S, k) : "export" == t ? p(_("stat"), Et, S) : "import" == t ? p(_("stat"), Mt, S) : "async" == t ? p(T) : "@" == e ? p(O, T) : h(_("stat"), O, E(";"), S)
                }

                function M(t) {
                    if ("(" == t) return p(xt, E(")"))
                }

                function O(t, e) {
                    return N(t, e, !1)
                }

                function L(t, e) {
                    return N(t, e, !0)
                }

                function A(t) {
                    return "(" != t ? h() : p(_(")"), O, E(")"), S)
                }

                function N(t, e, n) {
                    if (Xt.state.fatArrowAt == Xt.stream.start) {
                        var r = n ? j : z;
                        if ("(" == t) return p(w, _(")"), q(xt, ")"), S, E("=>"), r, k);
                        if ("variable" == t) return h(w, ct, E("=>"), r, k)
                    }
                    var i = n ? D : I;
                    return Jt.hasOwnProperty(t) ? p(i) : "function" == t ? p(bt, i) : "class" == t || Vt && "interface" == e ? (Xt.marked = "keyword", p(_("form"), wt, S)) : "keyword c" == t || "async" == t ? p(n ? L : O) : "(" == t ? p(_(")"), P, E(")"), S, i) : "operator" == t || "spread" == t ? p(n ? L : O) : "[" == t ? p(_("]"), Pt, S, i) : "{" == t ? Y(G, "}", null, i) : "quasi" == t ? h(F, i) : "new" == t ? p(B(n)) : "import" == t ? p(O) : p()
                }

                function P(t) {
                    return t.match(/[;\}\)\],]/) ? h() : h(O)
                }

                function I(t, e) {
                    return "," == t ? p(O) : D(t, e, !1)
                }

                function D(t, e, n) {
                    var r = 0 == n ? I : D,
                        i = 0 == n ? O : L;
                    return "=>" == t ? p(w, n ? j : z, k) : "operator" == t ? /\+\+|--/.test(e) || Vt && "!" == e ? p(r) : Vt && "<" == e && Xt.stream.match(/^([^>]|<.*?>)*>\s*\(/, !1) ? p(_(">"), q(tt, ">"), S, r) : "?" == e ? p(O, E(":"), i) : p(i) : "quasi" == t ? h(F, r) : ";" != t ? "(" == t ? Y(L, ")", "call", r) : "." == t ? p(V, r) : "[" == t ? p(_("]"), P, E("]"), S, r) : Vt && "as" == e ? (Xt.marked = "keyword", p(tt, r)) : "regexp" == t ? (Xt.state.lastType = Xt.marked = "operator", Xt.stream.backUp(Xt.stream.pos - Xt.stream.start - 1), p(i)) : void 0 : void 0
                }

                function F(t, e) {
                    return "quasi" != t ? h() : "${" != e.slice(e.length - 2) ? p(F) : p(O, R)
                }

                function R(t) {
                    if ("}" == t) return Xt.marked = "string-2", Xt.state.tokenize = l, p(F)
                }

                function z(t) {
                    return c(Xt.stream, Xt.state), h("{" == t ? T : O)
                }

                function j(t) {
                    return c(Xt.stream, Xt.state), h("{" == t ? T : L)
                }

                function B(t) {
                    return function(e) {
                        return "." == e ? p(t ? W : H) : "variable" == e && Vt ? p(ot, t ? D : I) : h(t ? L : O)
                    }
                }

                function H(t, e) {
                    if ("target" == e) return Xt.marked = "keyword", p(I)
                }

                function W(t, e) {
                    if ("target" == e) return Xt.marked = "keyword", p(D)
                }

                function U(t) {
                    return ":" == t ? p(S, T) : h(I, E(";"), S)
                }

                function V(t) {
                    if ("variable" == t) return Xt.marked = "property", p()
                }

                function G(t, e) {
                    if ("async" == t) return Xt.marked = "property", p(G);
                    if ("variable" == t || "keyword" == Xt.style) {
                        if (Xt.marked = "property", "get" == e || "set" == e) return p(K);
                        var n;
                        return Vt && Xt.state.fatArrowAt == Xt.stream.start && (n = Xt.stream.match(/^\s*:\s*/, !1)) && (Xt.state.fatArrowAt = Xt.stream.pos + n[0].length), p($)
                    }
                    return "number" == t || "string" == t ? (Xt.marked = Wt ? "property" : Xt.style + " property", p($)) : "jsonld-keyword" == t ? p($) : Vt && y(e) ? (Xt.marked = "keyword", p(G)) : "[" == t ? p(O, X, E("]"), $) : "spread" == t ? p(L, $) : "*" == e ? (Xt.marked = "keyword", p(G)) : ":" == t ? h($) : void 0
                }

                function K(t) {
                    return "variable" != t ? h($) : (Xt.marked = "property", p(bt))
                }

                function $(t) {
                    return ":" == t ? p(L) : "(" == t ? h(bt) : void 0
                }

                function q(t, e, n) {
                    function r(i, o) {
                        if (n ? n.indexOf(i) > -1 : "," == i) {
                            var a = Xt.state.lexical;
                            return "call" == a.info && (a.pos = (a.pos || 0) + 1), p(function(n, r) {
                                return n == e || r == e ? h() : h(t)
                            }, r)
                        }
                        return i == e || o == e ? p() : p(E(e))
                    }
                    return function(n, i) {
                        return n == e || i == e ? p() : h(t, r)
                    }
                }

                function Y(t, e, n) {
                    for (var r = 3; r < arguments.length; r++) Xt.cc.push(arguments[r]);
                    return p(_(e, n), q(t, e), S)
                }

                function J(t) {
                    return "}" == t ? p() : h(T, J)
                }

                function X(t, e) {
                    if (Vt) {
                        if (":" == t) return p(tt);
                        if ("?" == e) return p(X)
                    }
                }

                function Z(t) {
                    if (Vt && ":" == t) return Xt.stream.match(/^\s*\w+\s+is\b/, !1) ? p(O, Q, tt) : p(tt)
                }

                function Q(t, e) {
                    if ("is" == e) return Xt.marked = "keyword", p()
                }

                function tt(t, e) {
                    return "keyof" == e || "typeof" == e ? (Xt.marked = "keyword", p("keyof" == e ? tt : L)) : "variable" == t || "void" == e ? (Xt.marked = "type", p(it)) : "string" == t || "number" == t || "atom" == t ? p(it) : "[" == t ? p(_("]"), q(tt, "]", ","), S, it) : "{" == t ? p(_("}"), q(nt, "}", ",;"), S, it) : "(" == t ? p(q(rt, ")"), et) : "<" == t ? p(q(tt, ">"), tt) : void 0
                }

                function et(t) {
                    if ("=>" == t) return p(tt)
                }

                function nt(t, e) {
                    return "variable" == t || "keyword" == Xt.style ? (Xt.marked = "property", p(nt)) : "?" == e ? p(nt) : ":" == t ? p(tt) : "[" == t ? p(O, X, E("]"), nt) : void 0
                }

                function rt(t, e) {
                    return "variable" == t && Xt.stream.match(/^\s*[?:]/, !1) || "?" == e ? p(rt) : ":" == t ? p(tt) : h(tt)
                }

                function it(t, e) {
                    return "<" == e ? p(_(">"), q(tt, ">"), S, it) : "|" == e || "." == t || "&" == e ? p(tt) : "[" == t ? p(E("]"), it) : "extends" == e || "implements" == e ? (Xt.marked = "keyword", p(tt)) : void 0
                }

                function ot(t, e) {
                    if ("<" == e) return p(_(">"), q(tt, ">"), S, it)
                }

                function at() {
                    return h(tt, st)
                }

                function st(t, e) {
                    if ("=" == e) return p(tt)
                }

                function lt(t, e) {
                    return "enum" == e ? (Xt.marked = "keyword", p(It)) : h(ct, X, ft, dt)
                }

                function ct(t, e) {
                    return Vt && y(e) ? (Xt.marked = "keyword", p(ct)) : "variable" == t ? (g(e), p()) : "spread" == t ? p(ct) : "[" == t ? Y(ct, "]") : "{" == t ? Y(ut, "}") : void 0
                }

                function ut(t, e) {
                    return "variable" != t || Xt.stream.match(/^\s*:/, !1) ? ("variable" == t && (Xt.marked = "property"), "spread" == t ? p(ct) : "}" == t ? h() : p(E(":"), ct, ft)) : (g(e), p(ft))
                }

                function ft(t, e) {
                    if ("=" == e) return p(L)
                }

                function dt(t) {
                    if ("," == t) return p(lt)
                }

                function ht(t, e) {
                    if ("keyword b" == t && "else" == e) return p(_("form", "else"), T, S)
                }

                function pt(t, e) {
                    return "await" == e ? p(pt) : "(" == t ? p(_(")"), mt, E(")"), S) : void 0
                }

                function mt(t) {
                    return "var" == t ? p(lt, E(";"), vt) : ";" == t ? p(vt) : "variable" == t ? p(gt) : h(O, E(";"), vt)
                }

                function gt(t, e) {
                    return "in" == e || "of" == e ? (Xt.marked = "keyword", p(O)) : p(I, vt)
                }

                function vt(t, e) {
                    return ";" == t ? p(yt) : "in" == e || "of" == e ? (Xt.marked = "keyword", p(O)) : h(O, E(";"), yt)
                }

                function yt(t) {
                    ")" != t && p(O)
                }

                function bt(t, e) {
                    return "*" == e ? (Xt.marked = "keyword", p(bt)) : "variable" == t ? (g(e), p(bt)) : "(" == t ? p(w, _(")"), q(xt, ")"), S, Z, T, k) : Vt && "<" == e ? p(_(">"), q(at, ">"), S, bt) : void 0
                }

                function xt(t, e) {
                    return "@" == e && p(O, xt), "spread" == t ? p(xt) : Vt && y(e) ? (Xt.marked = "keyword", p(xt)) : h(ct, X, ft)
                }

                function wt(t, e) {
                    return "variable" == t ? Ct(t, e) : kt(t, e)
                }

                function Ct(t, e) {
                    if ("variable" == t) return g(e), p(kt)
                }

                function kt(t, e) {
                    return "<" == e ? p(_(">"), q(at, ">"), S, kt) : "extends" == e || "implements" == e || Vt && "," == t ? ("implements" == e && (Xt.marked = "keyword"), p(Vt ? tt : O, kt)) : "{" == t ? p(_("}"), _t, S) : void 0
                }

                function _t(t, e) {
                    return "async" == t || "variable" == t && ("static" == e || "get" == e || "set" == e || Vt && y(e)) && Xt.stream.match(/^\s+[\w$\xa1-\uffff]/, !1) ? (Xt.marked = "keyword", p(_t)) : "variable" == t || "keyword" == Xt.style ? (Xt.marked = "property", p(Vt ? St : bt, _t)) : "[" == t ? p(O, X, E("]"), Vt ? St : bt, _t) : "*" == e ? (Xt.marked = "keyword", p(_t)) : ";" == t ? p(_t) : "}" == t ? p() : "@" == e ? p(O, _t) : void 0
                }

                function St(t, e) {
                    return "?" == e ? p(St) : ":" == t ? p(tt, ft) : "=" == e ? p(L) : h(bt)
                }

                function Et(t, e) {
                    return "*" == e ? (Xt.marked = "keyword", p(Nt, E(";"))) : "default" == e ? (Xt.marked = "keyword", p(O, E(";"))) : "{" == t ? p(q(Tt, "}"), Nt, E(";")) : h(T)
                }

                function Tt(t, e) {
                    return "as" == e ? (Xt.marked = "keyword", p(E("variable"))) : "variable" == t ? h(L, Tt) : void 0
                }

                function Mt(t) {
                    return "string" == t ? p() : "(" == t ? h(O) : h(Ot, Lt, Nt)
                }

                function Ot(t, e) {
                    return "{" == t ? Y(Ot, "}") : ("variable" == t && g(e), "*" == e && (Xt.marked = "keyword"), p(At))
                }

                function Lt(t) {
                    if ("," == t) return p(Ot, Lt)
                }

                function At(t, e) {
                    if ("as" == e) return Xt.marked = "keyword", p(Ot)
                }

                function Nt(t, e) {
                    if ("from" == e) return Xt.marked = "keyword", p(O)
                }

                function Pt(t) {
                    return "]" == t ? p() : h(q(L, "]"))
                }

                function It() {
                    return h(_("form"), ct, E("{"), _("}"), q(Dt, "}"), S, S)
                }

                function Dt() {
                    return h(ct, ft)
                }

                function Ft(t, e) {
                    return "operator" == t.lastType || "," == t.lastType || $t.test(e.charAt(0)) || /[,.]/.test(e.charAt(0))
                }

                function Rt(t, e, n) {
                    return e.tokenize == o && /^(?:operator|sof|keyword [bcd]|case|new|export|default|spread|[\[{}\(,;:]|=>)$/.test(e.lastType) || "quasi" == e.lastType && /\{\s*$/.test(t.string.slice(0, t.pos - (n || 0)))
                }
                var zt, jt, Bt = e.indentUnit,
                    Ht = n.statementIndent,
                    Wt = n.jsonld,
                    Ut = n.json || Wt,
                    Vt = n.typescript,
                    Gt = n.wordCharacters || /[\w$\xa1-\uffff]/,
                    Kt = function() {
                        function t(t) {
                            return {
                                type: t,
                                style: "keyword"
                            }
                        }
                        var e = t("keyword a"),
                            n = t("keyword b"),
                            r = t("keyword c"),
                            i = t("keyword d"),
                            o = t("operator"),
                            a = {
                                type: "atom",
                                style: "atom"
                            };
                        return {
                            if: t("if"),
                            while: e,
                            with: e,
                            else: n,
                            do: n,
                            try: n,
                            finally: n,
                            return: i,
                            break: i,
                            continue: i,
                            new: t("new"),
                            delete: r,
                            void: r,
                            throw: r,
                            debugger: t("debugger"),
                            var: t("var"),
                            const: t("var"),
                            let: t("var"),
                            function: t("function"),
                            catch: t("catch"),
                            for: t("for"),
                            switch: t("switch"),
                            case: t("case"),
                            default: t("default"),
                            in : o,
                            typeof: o,
                            instanceof: o,
                            true: a,
                            false: a,
                            null: a,
                            undefined: a,
                            NaN: a,
                            Infinity: a,
                            this: t("this"),
                            class: t("class"),
                            super: t("atom"),
                            yield: r,
                            export: t("export"),
                            import: t("import"),
                            extends: r,
                            await: r
                        }
                    }(),
                    $t = /[+\-*&%=<>!?|~^@]/,
                    qt = /^@(context|id|value|language|type|container|list|set|reverse|index|base|vocab|graph)"/,
                    Yt = "([{}])",
                    Jt = {
                        atom: !0,
                        number: !0,
                        variable: !0,
                        string: !0,
                        regexp: !0,
                        this: !0,
                        "jsonld-keyword": !0
                    },
                    Xt = {
                        state: null,
                        column: null,
                        marked: null,
                        cc: null
                    },
                    Zt = new x("this", new x("arguments", null));
                return k.lex = !0, S.lex = !0, {
                    startState: function(t) {
                        var e = {
                            tokenize: o,
                            lastType: "sof",
                            cc: [],
                            lexical: new u((t || 0) - Bt, 0, "block", !1),
                            localVars: n.localVars,
                            context: n.localVars && new b(null, null, !1),
                            indented: t || 0
                        };
                        return n.globalVars && "object" == typeof n.globalVars && (e.globalVars = n.globalVars), e
                    },
                    token: function(t, e) {
                        if (t.sol() && (e.lexical.hasOwnProperty("align") || (e.lexical.align = !1), e.indented = t.indentation(), c(t, e)), e.tokenize != s && t.eatSpace()) return null;
                        var n = e.tokenize(t, e);
                        return "comment" == zt ? n : (e.lastType = "operator" != zt || "++" != jt && "--" != jt ? zt : "incdec", d(e, n, zt, jt, t))
                    },
                    indent: function(e, r) {
                        if (e.tokenize == s) return t.Pass;
                        if (e.tokenize != o) return 0;
                        var i, a = r && r.charAt(0),
                            l = e.lexical;
                        if (!/^\s*else\b/.test(r))
                            for (var c = e.cc.length - 1; c >= 0; --c) {
                                var u = e.cc[c];
                                if (u == S) l = l.prev;
                                else if (u != ht) break
                            }
                        for (;
                            ("stat" == l.type || "form" == l.type) && ("}" == a || (i = e.cc[e.cc.length - 1]) && (i == I || i == D) && !/^[,\.=+\-*:?[\(]/.test(r));) l = l.prev;
                        Ht && ")" == l.type && "stat" == l.prev.type && (l = l.prev);
                        var f = l.type,
                            d = a == f;
                        return "vardef" == f ? l.indented + ("operator" == e.lastType || "," == e.lastType ? l.info.length + 1 : 0) : "form" == f && "{" == a ? l.indented : "form" == f ? l.indented + Bt : "stat" == f ? l.indented + (Ft(e, r) ? Ht || Bt : 0) : "switch" != l.info || d || 0 == n.doubleIndentSwitch ? l.align ? l.column + (d ? 0 : 1) : l.indented + (d ? 0 : Bt) : l.indented + (/^(?:case|default)\b/.test(r) ? Bt : 2 * Bt)
                    },
                    electricInput: /^\s*(?:case .*?:|default:|\{|\})$/,
                    blockCommentStart: Ut ? null : "/*",
                    blockCommentEnd: Ut ? null : "*/",
                    blockCommentContinue: Ut ? null : " * ",
                    lineComment: Ut ? null : "//",
                    fold: "brace",
                    closeBrackets: "()[]{}''\"\"``",
                    helperType: Ut ? "json" : "javascript",
                    jsonldMode: Wt,
                    jsonMode: Ut,
                    expressionAllowed: Rt,
                    skipExpression: function(t) {
                        var e = t.cc[t.cc.length - 1];
                        e != O && e != L || t.cc.pop()
                    }
                }
            }), t.registerHelper("wordChars", "javascript", /[\w$]/), t.defineMIME("text/javascript", "javascript"), t.defineMIME("text/ecmascript", "javascript"), t.defineMIME("application/javascript", "javascript"), t.defineMIME("application/x-javascript", "javascript"), t.defineMIME("application/ecmascript", "javascript"), t.defineMIME("application/json", {
                name: "javascript",
                json: !0
            }), t.defineMIME("application/x-json", {
                name: "javascript",
                json: !0
            }), t.defineMIME("application/ld+json", {
                name: "javascript",
                jsonld: !0
            }), t.defineMIME("text/typescript", {
                name: "javascript",
                typescript: !0
            }), t.defineMIME("application/typescript", {
                name: "javascript",
                typescript: !0
            })
        })
    }, function(t, e, n) {
        ! function(t) {
            t(n(0))
        }(function(t) {
            "use strict";
            t.defineMode("shell", function() {
                function t(t, e) {
                    for (var n = e.split(" "), r = 0; r < n.length; r++) o[n[r]] = t
                }

                function e(t, e) {
                    if (t.eatSpace()) return null;
                    var r = t.sol(),
                        s = t.next();
                    if ("\\" === s) return t.next(), null;
                    if ("'" === s || '"' === s || "`" === s) return e.tokens.unshift(n(s, "`" === s ? "quote" : "string")), i(t, e);
                    if ("#" === s) return r && t.eat("!") ? (t.skipToEnd(), "meta") : (t.skipToEnd(), "comment");
                    if ("$" === s) return e.tokens.unshift(a), i(t, e);
                    if ("+" === s || "=" === s) return "operator";
                    if ("-" === s) return t.eat("-"), t.eatWhile(/\w/), "attribute";
                    if (/\d/.test(s) && (t.eatWhile(/\d/), t.eol() || !/\w/.test(t.peek()))) return "number";
                    t.eatWhile(/[\w-]/);
                    var l = t.current();
                    return "=" === t.peek() && /\w+/.test(l) ? "def" : o.hasOwnProperty(l) ? o[l] : null
                }

                function n(t, e) {
                    var o = "(" == t ? ")" : "{" == t ? "}" : t;
                    return function(s, l) {
                        for (var c, u = !1; null != (c = s.next());) {
                            if (c === o && !u) {
                                l.tokens.shift();
                                break
                            }
                            if ("$" === c && !u && "'" !== t && s.peek() != o) {
                                u = !0, s.backUp(1), l.tokens.unshift(a);
                                break
                            }
                            if (!u && t !== o && c === t) return l.tokens.unshift(n(t, e)), i(s, l);
                            if (!u && /['"]/.test(c) && !/['"]/.test(t)) {
                                l.tokens.unshift(r(c, "string")), s.backUp(1);
                                break
                            }
                            u = !u && "\\" === c
                        }
                        return e
                    }
                }

                function r(t, e) {
                    return function(r, o) {
                        return o.tokens[0] = n(t, e), r.next(), i(r, o)
                    }
                }

                function i(t, n) {
                    return (n.tokens[0] || e)(t, n)
                }
                var o = {};
                t("atom", "true false"), t("keyword", "if then do else elif while until for in esac fi fin fil done exit set unset export function"), t("builtin", "ab awk bash beep cat cc cd chown chmod chroot clear cp curl cut diff echo find gawk gcc get git grep hg kill killall ln ls make mkdir openssl mv nc nl node npm ping ps restart rm rmdir sed service sh shopt shred source sort sleep ssh start stop su sudo svn tee telnet top touch vi vim wall wc wget who write yes zsh");
                var a = function(t, e) {
                    e.tokens.length > 1 && t.eat("$");
                    var r = t.next();
                    return /['"({]/.test(r) ? (e.tokens[0] = n(r, "(" == r ? "quote" : "{" == r ? "def" : "string"), i(t, e)) : (/\d/.test(r) || t.eatWhile(/\w/), e.tokens.shift(), "def")
                };
                return {
                    startState: function() {
                        return {
                            tokens: []
                        }
                    },
                    token: function(t, e) {
                        return i(t, e)
                    },
                    closeBrackets: "()[]{}''\"\"``",
                    lineComment: "#",
                    fold: "brace"
                }
            }), t.defineMIME("text/x-sh", "shell"), t.defineMIME("application/x-sh", "shell")
        })
    }, function(t, e, n) {
        ! function(t) {
            t(n(0))
        }(function(t) {
            "use strict";

            function e(t) {
                for (var e = {}, n = 0; n < t.length; n++) e[t[n]] = !0;
                return e
            }

            function n(t, e, n) {
                if (t.sol() && (e.indented = t.indentation()), t.eatSpace()) return null;
                var r = t.peek();
                if ("/" == r) {
                    if (t.match("//")) return t.skipToEnd(), "comment";
                    if (t.match("/*")) return e.tokenize.push(o), o(t, e)
                }
                if (t.match(w)) return "builtin";
                if (t.match(C)) return "attribute";
                if (t.match(m)) return "number";
                if (t.match(g)) return "number";
                if (t.match(v)) return "number";
                if (t.match(y)) return "number";
                if (t.match(x)) return "property";
                if (h.indexOf(r) > -1) return t.next(), "operator";
                if (p.indexOf(r) > -1) return t.next(), t.match(".."), "punctuation";
                if ('"' == r || "'" == r) {
                    t.next();
                    var a = i(r);
                    return e.tokenize.push(a), a(t, e)
                }
                if (t.match(b)) {
                    var s = t.current();
                    return d.hasOwnProperty(s) ? "variable-2" : f.hasOwnProperty(s) ? "atom" : c.hasOwnProperty(s) ? (u.hasOwnProperty(s) && (e.prev = "define"), "keyword") : "define" == n ? "def" : "variable"
                }
                return t.next(), null
            }

            function r() {
                var t = 0;
                return function(e, r, i) {
                    var o = n(e, r, i);
                    if ("punctuation" == o)
                        if ("(" == e.current()) ++t;
                        else if (")" == e.current()) {
                        if (0 == t) return e.backUp(1), r.tokenize.pop(), r.tokenize[r.tokenize.length - 1](e, r);
                        --t
                    }
                    return o
                }
            }

            function i(t) {
                return function(e, n) {
                    for (var i, o = !1; i = e.next();)
                        if (o) {
                            if ("(" == i) return n.tokenize.push(r()), "string";
                            o = !1
                        } else {
                            if (i == t) break;
                            o = "\\" == i
                        }
                    return n.tokenize.pop(), "string"
                }
            }

            function o(t, e) {
                for (var n;;) {
                    if (t.match(/^[^\/*]+/, !0), !(n = t.next())) break;
                    "/" === n && t.eat("*") ? e.tokenize.push(o) : "*" === n && t.eat("/") && e.tokenize.pop()
                }
                return "comment"
            }

            function a(t, e, n) {
                this.prev = t, this.align = e, this.indented = n
            }

            function s(t, e) {
                var n = e.match(/^\s*($|\/[\/\*])/, !1) ? null : e.column() + 1;
                t.context = new a(t.context, n, t.indented)
            }

            function l(t) {
                t.context && (t.indented = t.context.indented, t.context = t.context.prev)
            }
            var c = e(["_", "var", "let", "class", "enum", "extension", "import", "protocol", "struct", "func", "typealias", "associatedtype", "open", "public", "internal", "fileprivate", "private", "deinit", "init", "new", "override", "self", "subscript", "super", "convenience", "dynamic", "final", "indirect", "lazy", "required", "static", "unowned", "unowned(safe)", "unowned(unsafe)", "weak", "as", "is", "break", "case", "continue", "default", "else", "fallthrough", "for", "guard", "if", "in", "repeat", "switch", "where", "while", "defer", "return", "inout", "mutating", "nonmutating", "catch", "do", "rethrows", "throw", "throws", "try", "didSet", "get", "set", "willSet", "assignment", "associativity", "infix", "left", "none", "operator", "postfix", "precedence", "precedencegroup", "prefix", "right", "Any", "AnyObject", "Type", "dynamicType", "Self", "Protocol", "__COLUMN__", "__FILE__", "__FUNCTION__", "__LINE__"]),
                u = e(["var", "let", "class", "enum", "extension", "import", "protocol", "struct", "func", "typealias", "associatedtype", "for"]),
                f = e(["true", "false", "nil", "self", "super", "_"]),
                d = e(["Array", "Bool", "Character", "Dictionary", "Double", "Float", "Int", "Int8", "Int16", "Int32", "Int64", "Never", "Optional", "Set", "String", "UInt8", "UInt16", "UInt32", "UInt64", "Void"]),
                h = "+-/*%=|&<>~^?!",
                p = ":;,.(){}[]",
                m = /^\-?0b[01][01_]*/,
                g = /^\-?0o[0-7][0-7_]*/,
                v = /^\-?0x[\dA-Fa-f][\dA-Fa-f_]*(?:(?:\.[\dA-Fa-f][\dA-Fa-f_]*)?[Pp]\-?\d[\d_]*)?/,
                y = /^\-?\d[\d_]*(?:\.\d[\d_]*)?(?:[Ee]\-?\d[\d_]*)?/,
                b = /^\$\d+|(`?)[_A-Za-z][_A-Za-z$0-9]*\1/,
                x = /^\.(?:\$\d+|(`?)[_A-Za-z][_A-Za-z$0-9]*\1)/,
                w = /^\#[A-Za-z]+/,
                C = /^@(?:\$\d+|(`?)[_A-Za-z][_A-Za-z$0-9]*\1)/;
            t.defineMode("swift", function(t) {
                return {
                    startState: function() {
                        return {
                            prev: null,
                            context: null,
                            indented: 0,
                            tokenize: []
                        }
                    },
                    token: function(t, e) {
                        var r = e.prev;
                        e.prev = null;
                        var i = e.tokenize[e.tokenize.length - 1] || n,
                            o = i(t, e, r);
                        if (o && "comment" != o ? e.prev || (e.prev = o) : e.prev = r, "punctuation" == o) {
                            var a = /[\(\[\{]|([\]\)\}])/.exec(t.current());
                            a && (a[1] ? l : s)(e, t)
                        }
                        return o
                    },
                    indent: function(e, n) {
                        var r = e.context;
                        if (!r) return 0;
                        var i = /^[\]\}\)]/.test(n);
                        return null != r.align ? r.align - (i ? 1 : 0) : r.indented + (i ? 0 : t.indentUnit)
                    },
                    electricInput: /^\s*[\)\}\]]$/,
                    lineComment: "//",
                    blockCommentStart: "/*",
                    blockCommentEnd: "*/",
                    fold: "brace",
                    closeBrackets: "()[]{}''\"\"``"
                }
            }), t.defineMIME("text/x-swift", "swift")
        })
    }, function(t, e, n) {
        "use strict";
        var r, i, o, a = n(14),
            s = n(20),
            l = n(7),
            c = n(2),
            u = n(4),
            f = n(25),
            d = n(3),
            h = n(17),
            p = n(26),
            m = n(84),
            g = n(85),
            v = Function.prototype.call,
            y = Object.defineProperty,
            b = Object.getPrototypeOf;
        g && (o = Set), t.exports = r = function() {
            var t, e = arguments[0];
            if (!(this instanceof r)) throw new TypeError("Constructor requires 'new'");
            return t = g && l ? l(new o, b(this)) : this, null != e && h(e), y(t, "__setData__", u("c", [])), e ? (p(e, function(t) {
                -1 === s.call(this, t) && this.push(t)
            }, t.__setData__), t) : t
        }, g && (l && l(r, o), r.prototype = Object.create(o.prototype, {
            constructor: u(r)
        })), f(Object.defineProperties(r.prototype, {
            add: u(function(t) {
                return this.has(t) ? this : (this.emit("_add", this.__setData__.push(t) - 1, t), this)
            }),
            clear: u(function() {
                this.__setData__.length && (a.call(this.__setData__), this.emit("_clear"))
            }),
            delete: u(function(t) {
                var e = s.call(this.__setData__, t);
                return -1 !== e && (this.__setData__.splice(e, 1), this.emit("_delete", e, t), !0)
            }),
            entries: u(function() {
                return new m(this, "key+value")
            }),
            forEach: u(function(t) {
                var e, n, r, i = arguments[1];
                for (c(t), e = this.values(), n = e._next(); void 0 !== n;) r = e._resolve(n), v.call(t, i, r, r, this), n = e._next()
            }),
            has: u(function(t) {
                return -1 !== s.call(this.__setData__, t)
            }),
            keys: u(i = function() {
                return this.values()
            }),
            size: u.gs(function() {
                return this.__setData__.length
            }),
            values: u(function() {
                return new m(this)
            }),
            toString: u(function() {
                return "[object Set]"
            })
        })), y(r.prototype, d.iterator, u(i)), y(r.prototype, d.toStringTag, u("c", "Set"))
    }, function(t, e, n) {
        "use strict";
        t.exports = n(51)() ? Number.isNaN : n(52)
    }, function(t, e, n) {
        "use strict";
        t.exports = function() {
            var t = Number.isNaN;
            return "function" == typeof t && (!t({}) && t(NaN) && !t(34))
        }
    }, function(t, e, n) {
        "use strict";
        t.exports = function(t) {
            return t !== t
        }
    }, function(t, e, n) {
        "use strict";
        var r = n(54),
            i = Math.abs,
            o = Math.floor;
        t.exports = function(t) {
            return isNaN(t) ? 0 : (t = Number(t), 0 !== t && isFinite(t) ? r(t) * o(i(t)) : t)
        }
    }, function(t, e, n) {
        "use strict";
        t.exports = n(55)() ? Math.sign : n(56)
    }, function(t, e, n) {
        "use strict";
        t.exports = function() {
            var t = Math.sign;
            return "function" == typeof t && (1 === t(10) && -1 === t(-20))
        }
    }, function(t, e, n) {
        "use strict";
        t.exports = function(t) {
            return t = Number(t), isNaN(t) || 0 === t ? t : t > 0 ? 1 : -1
        }
    }, function(t, e, n) {
        "use strict";
        var r = n(6),
            i = {
                function: !0,
                object: !0
            };
        t.exports = function(t) {
            return r(t) && i[typeof t] || !1
        }
    }, function(t, e, n) {
        "use strict";
        var r, i = Object.create;
        n(22)() || (r = n(23)), t.exports = function() {
            var t, e, n;
            return r ? 1 !== r.level ? i : (t = {}, e = {}, n = {
                configurable: !1,
                enumerable: !1,
                writable: !0,
                value: void 0
            }, Object.getOwnPropertyNames(Object.prototype).forEach(function(t) {
                if ("__proto__" === t) return void(e[t] = {
                    configurable: !0,
                    enumerable: !1,
                    writable: !0,
                    value: void 0
                });
                e[t] = n
            }), Object.defineProperties(t, e), Object.defineProperty(r, "nullPolyfill", {
                configurable: !1,
                enumerable: !1,
                writable: !1,
                value: t
            }), function(e, n) {
                return i(null === e ? t : e, n)
            }) : i
        }()
    }, function(t, e, n) {
        "use strict";
        t.exports = function() {
            var t, e = Object.assign;
            return "function" == typeof e && (t = {
                foo: "raz"
            }, e(t, {
                bar: "dwa"
            }, {
                trzy: "trzy"
            }), t.foo + t.bar + t.trzy === "razdwatrzy")
        }
    }, function(t, e, n) {
        "use strict";
        var r = n(61),
            i = n(1),
            o = Math.max;
        t.exports = function(t, e) {
            var n, a, s, l = o(arguments.length, 2);
            for (t = Object(i(t)), s = function(r) {
                    try {
                        t[r] = e[r]
                    } catch (t) {
                        n || (n = t)
                    }
                }, a = 1; a < l; ++a) e = arguments[a], r(e).forEach(s);
            if (void 0 !== n) throw n;
            return t
        }
    }, function(t, e, n) {
        "use strict";
        t.exports = n(62)() ? Object.keys : n(63)
    }, function(t, e, n) {
        "use strict";
        t.exports = function() {
            try {
                return Object.keys("primitive"), !0
            } catch (t) {
                return !1
            }
        }
    }, function(t, e, n) {
        "use strict";
        var r = n(6),
            i = Object.keys;
        t.exports = function(t) {
            return i(r(t) ? Object(t) : t)
        }
    }, function(t, e, n) {
        "use strict";
        t.exports = function(t) {
            return "function" == typeof t
        }
    }, function(t, e, n) {
        "use strict";
        var r = "razdwatrzy";
        t.exports = function() {
            return "function" == typeof r.contains && (!0 === r.contains("dwa") && !1 === r.contains("foo"))
        }
    }, function(t, e, n) {
        "use strict";
        var r = String.prototype.indexOf;
        t.exports = function(t) {
            return r.call(this, t, arguments[1]) > -1
        }
    }, function(t, e, n) {
        "use strict";
        var r = {
            object: !0,
            symbol: !0
        };
        t.exports = function() {
            var t;
            if ("function" != typeof Symbol) return !1;
            t = Symbol("test symbol");
            try {
                String(t)
            } catch (t) {
                return !1
            }
            return !!r[typeof Symbol.iterator] && (!!r[typeof Symbol.toPrimitive] && !!r[typeof Symbol.toStringTag])
        }
    }, function(t, e, n) {
        "use strict";
        var r, i, o, a, s = n(4),
            l = n(69),
            c = Object.create,
            u = Object.defineProperties,
            f = Object.defineProperty,
            d = Object.prototype,
            h = c(null);
        if ("function" == typeof Symbol) {
            r = Symbol;
            try {
                String(r()), a = !0
            } catch (t) {}
        }
        var p = function() {
            var t = c(null);
            return function(e) {
                for (var n, r, i = 0; t[e + (i || "")];) ++i;
                return e += i || "", t[e] = !0, n = "@@" + e, f(d, n, s.gs(null, function(t) {
                    r || (r = !0, f(this, n, s(t)), r = !1)
                })), n
            }
        }();
        o = function(t) {
            if (this instanceof o) throw new TypeError("Symbol is not a constructor");
            return i(t)
        }, t.exports = i = function t(e) {
            var n;
            if (this instanceof t) throw new TypeError("Symbol is not a constructor");
            return a ? r(e) : (n = c(o.prototype), e = void 0 === e ? "" : String(e), u(n, {
                __description__: s("", e),
                __name__: s("", p(e))
            }))
        }, u(i, {
            for: s(function(t) {
                return h[t] ? h[t] : h[t] = i(String(t))
            }),
            keyFor: s(function(t) {
                var e;
                l(t);
                for (e in h)
                    if (h[e] === t) return e
            }),
            hasInstance: s("", r && r.hasInstance || i("hasInstance")),
            isConcatSpreadable: s("", r && r.isConcatSpreadable || i("isConcatSpreadable")),
            iterator: s("", r && r.iterator || i("iterator")),
            match: s("", r && r.match || i("match")),
            replace: s("", r && r.replace || i("replace")),
            search: s("", r && r.search || i("search")),
            species: s("", r && r.species || i("species")),
            split: s("", r && r.split || i("split")),
            toPrimitive: s("", r && r.toPrimitive || i("toPrimitive")),
            toStringTag: s("", r && r.toStringTag || i("toStringTag")),
            unscopables: s("", r && r.unscopables || i("unscopables"))
        }), u(o.prototype, {
            constructor: s(i),
            toString: s("", function() {
                return this.__name__
            })
        }), u(i.prototype, {
            toString: s(function() {
                return "Symbol (" + l(this).__description__ + ")"
            }),
            valueOf: s(function() {
                return l(this)
            })
        }), f(i.prototype, i.toPrimitive, s("", function() {
            var t = l(this);
            return "symbol" == typeof t ? t : t.toString()
        })), f(i.prototype, i.toStringTag, s("c", "Symbol")), f(o.prototype, i.toStringTag, s("c", i.prototype[i.toStringTag])), f(o.prototype, i.toPrimitive, s("c", i.prototype[i.toPrimitive]))
    }, function(t, e, n) {
        "use strict";
        var r = n(70);
        t.exports = function(t) {
            if (!r(t)) throw new TypeError(t + " is not a symbol");
            return t
        }
    }, function(t, e, n) {
        "use strict";
        t.exports = function(t) {
            return !!t && ("symbol" == typeof t || !!t.constructor && ("Symbol" === t.constructor.name && "Symbol" === t[t.constructor.toStringTag]))
        }
    }, function(t, e, n) {
        "use strict";
        var r = n(9),
            i = n(6),
            o = n(10),
            a = n(3).iterator,
            s = Array.isArray;
        t.exports = function(t) {
            return !!i(t) && (!!s(t) || (!!o(t) || (!!r(t) || "function" == typeof t[a])))
        }
    }, function(t, e, n) {
        "use strict";
        var r = n(9),
            i = n(10),
            o = n(73),
            a = n(83),
            s = n(17),
            l = n(3).iterator;
        t.exports = function(t) {
            return "function" == typeof s(t)[l] ? t[l]() : r(t) ? new o(t) : i(t) ? new a(t) : new o(t)
        }
    }, function(t, e, n) {
        "use strict";
        var r, i = n(7),
            o = n(16),
            a = n(4),
            s = n(3),
            l = n(11),
            c = Object.defineProperty;
        r = t.exports = function(t, e) {
            if (!(this instanceof r)) throw new TypeError("Constructor requires 'new'");
            l.call(this, t), e = e ? o.call(e, "key+value") ? "key+value" : o.call(e, "key") ? "key" : "value" : "value", c(this, "__kind__", a("", e))
        }, i && i(r, l), delete r.prototype.constructor, r.prototype = Object.create(l.prototype, {
            _resolve: a(function(t) {
                return "value" === this.__kind__ ? this.__list__[t] : "key+value" === this.__kind__ ? [t, this.__list__[t]] : t
            })
        }), c(r.prototype, s.toStringTag, a("c", "Array Iterator"))
    }, function(t, e, n) {
        "use strict";
        var r, i = n(75),
            o = n(24),
            a = n(2),
            s = n(80),
            l = n(2),
            c = n(1),
            u = Function.prototype.bind,
            f = Object.defineProperty,
            d = Object.prototype.hasOwnProperty;
        r = function(t, e, n) {
            var r, o = c(e) && l(e.value);
            return r = i(e), delete r.writable, delete r.value, r.get = function() {
                return !n.overwriteDefinition && d.call(this, t) ? o : (e.value = u.call(o, n.resolveContext ? n.resolveContext(this) : this), f(this, t, e), this[t])
            }, r
        }, t.exports = function(t) {
            var e = o(arguments[1]);
            return null != e.resolveContext && a(e.resolveContext), s(t, function(t, n) {
                return r(n, t, e)
            })
        }
    }, function(t, e, n) {
        "use strict";
        var r = n(76),
            i = n(15),
            o = n(1);
        t.exports = function(t) {
            var e = Object(o(t)),
                n = arguments[1],
                a = Object(arguments[2]);
            if (e !== t && !n) return e;
            var s = {};
            return n ? r(n, function(e) {
                (a.ensure || e in t) && (s[e] = t[e])
            }) : i(s, t), s
        }
    }, function(t, e, n) {
        "use strict";
        t.exports = n(77)() ? Array.from : n(78)
    }, function(t, e, n) {
        "use strict";
        t.exports = function() {
            var t, e, n = Array.from;
            return "function" == typeof n && (t = ["raz", "dwa"], e = n(t), Boolean(e && e !== t && "dwa" === e[1]))
        }
    }, function(t, e, n) {
        "use strict";
        var r = n(3).iterator,
            i = n(9),
            o = n(79),
            a = n(21),
            s = n(2),
            l = n(1),
            c = n(6),
            u = n(10),
            f = Array.isArray,
            d = Function.prototype.call,
            h = {
                configurable: !0,
                enumerable: !0,
                writable: !0,
                value: null
            },
            p = Object.defineProperty;
        t.exports = function(t) {
            var e, n, m, g, v, y, b, x, w, C, k = arguments[1],
                _ = arguments[2];
            if (t = Object(l(t)), c(k) && s(k), this && this !== Array && o(this)) e = this;
            else {
                if (!k) {
                    if (i(t)) return 1 !== (v = t.length) ? Array.apply(null, t) : (g = new Array(1), g[0] = t[0], g);
                    if (f(t)) {
                        for (g = new Array(v = t.length), n = 0; n < v; ++n) g[n] = t[n];
                        return g
                    }
                }
                g = []
            }
            if (!f(t))
                if (void 0 !== (w = t[r])) {
                    for (b = s(w).call(t), e && (g = new e), x = b.next(), n = 0; !x.done;) C = k ? d.call(k, _, x.value, n) : x.value, e ? (h.value = C, p(g, n, h)) : g[n] = C, x = b.next(), ++n;
                    v = n
                } else if (u(t)) {
                for (v = t.length, e && (g = new e), n = 0, m = 0; n < v; ++n) C = t[n], n + 1 < v && (y = C.charCodeAt(0)) >= 55296 && y <= 56319 && (C += t[++n]), C = k ? d.call(k, _, C, m) : C, e ? (h.value = C, p(g, m, h)) : g[m] = C, ++m;
                v = m
            }
            if (void 0 === v)
                for (v = a(t.length), e && (g = new e(v)), n = 0; n < v; ++n) C = k ? d.call(k, _, t[n], n) : t[n], e ? (h.value = C, p(g, n, h)) : g[n] = C;
            return e && (h.value = null, g.length = v), g
        }
    }, function(t, e, n) {
        "use strict";
        var r = Object.prototype.toString,
            i = r.call(n(19));
        t.exports = function(t) {
            return "function" == typeof t && r.call(t) === i
        }
    }, function(t, e, n) {
        "use strict";
        var r = n(2),
            i = n(81),
            o = Function.prototype.call;
        t.exports = function(t, e) {
            var n = {},
                a = arguments[2];
            return r(e), i(t, function(t, r, i, s) {
                n[r] = o.call(e, a, t, r, i, s)
            }), n
        }
    }, function(t, e, n) {
        "use strict";
        t.exports = n(82)("forEach")
    }, function(t, e, n) {
        "use strict";
        var r = n(2),
            i = n(1),
            o = Function.prototype.bind,
            a = Function.prototype.call,
            s = Object.keys,
            l = Object.prototype.propertyIsEnumerable;
        t.exports = function(t, e) {
            return function(n, c) {
                var u, f = arguments[2],
                    d = arguments[3];
                return n = Object(i(n)), r(c), u = s(n), d && u.sort("function" == typeof d ? o.call(d, n) : void 0), "function" != typeof t && (t = u[t]), a.call(t, u, function(t, r) {
                    return l.call(n, t) ? a.call(c, f, n[t], t, n, r) : e
                })
            }
        }
    }, function(t, e, n) {
        "use strict";
        var r, i = n(7),
            o = n(4),
            a = n(3),
            s = n(11),
            l = Object.defineProperty;
        r = t.exports = function(t) {
            if (!(this instanceof r)) throw new TypeError("Constructor requires 'new'");
            t = String(t), s.call(this, t), l(this, "__length__", o("", t.length))
        }, i && i(r, s), delete r.prototype.constructor, r.prototype = Object.create(s.prototype, {
            _next: o(function() {
                if (this.__list__) return this.__nextIndex__ < this.__length__ ? this.__nextIndex__++ : void this._unBind()
            }),
            _resolve: o(function(t) {
                var e, n = this.__list__[t];
                return this.__nextIndex__ === this.__length__ ? n : (e = n.charCodeAt(0), e >= 55296 && e <= 56319 ? n + this.__list__[this.__nextIndex__++] : n)
            })
        }), l(r.prototype, a.toStringTag, o("c", "String Iterator"))
    }, function(t, e, n) {
        "use strict";
        var r, i = n(7),
            o = n(16),
            a = n(4),
            s = n(11),
            l = n(3).toStringTag,
            c = Object.defineProperty;
        r = t.exports = function(t, e) {
            if (!(this instanceof r)) return new r(t, e);
            s.call(this, t.__setData__, t), e = e && o.call(e, "key+value") ? "key+value" : "value", c(this, "__kind__", a("", e))
        }, i && i(r, s), r.prototype = Object.create(s.prototype, {
            constructor: a(r),
            _resolve: a(function(t) {
                return "value" === this.__kind__ ? this.__list__[t] : [this.__list__[t], this.__list__[t]]
            }),
            toString: a(function() {
                return "[object Set Iterator]"
            })
        }), c(r.prototype, l, a("c", "Set Iterator"))
    }, function(t, e, n) {
        "use strict";
        t.exports = function() {
            return "undefined" != typeof Set && "[object Set]" === Object.prototype.toString.call(Set.prototype)
        }()
    }, function(t, e, n) {
        function r(t) {
            for (var e in i)
                if ("none" != e) {
                    var n = i[e];
                    if (n(t) == t) return e
                }
            return null
        }
        var i = n(87);
        t.exports = e = r, e.cases = i, e.add = function(t, n) {
            e[t] = i[t] = n
        };
        for (var o in i) e.add(o, i[o])
    }, function(t, e, n) {
        var r = n(88),
            i = n(89),
            o = n(90),
            a = n(91),
            s = n(18),
            l = n(92),
            c = n(28),
            u = n(93),
            f = n(27),
            d = n(5),
            h = n(94);
        e.camel = r, e.pascal = l, e.dot = a, e.slug = u, e.snake = f, e.space = d, e.constant = o, e.capital = i, e.title = h, e.sentence = c, e.lower = function(t) {
            return s(t).toLowerCase()
        }, e.upper = function(t) {
            return s(t).toUpperCase()
        }, e.inverse = function(t) {
            for (var e, n = t.split(""), r = 0; e = n[r]; r++)
                if (/[a-z]/i.test(e)) {
                    var i = e.toUpperCase(),
                        o = e.toLowerCase();
                    n[r] = e == i ? o : i
                }
            return n.join("")
        }, e.none = s
    }, function(t, e, n) {
        function r(t) {
            return i(t).replace(/\s(\w)/g, function(t, e) {
                return e.toUpperCase()
            })
        }
        var i = n(5);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return i(t).replace(/(^|\s)(\w)/g, function(t, e, n) {
                return e + n.toUpperCase()
            })
        }
        var i = n(5);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return i(t).toUpperCase()
        }
        var i = n(27);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return i(t).replace(/\s/g, ".")
        }
        var i = n(5);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return i(t).replace(/(?:^|\s)(\w)/g, function(t, e) {
                return e.toUpperCase()
            })
        }
        var i = n(5);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return i(t).replace(/\s/g, "-")
        }
        var i = n(5);
        t.exports = r
    }, function(t, e, n) {
        function r(t) {
            return i(t).replace(/(^|\s)(\w)/g, function(t, e, n) {
                return e + n.toUpperCase()
            }).replace(l, function(t) {
                return t.toLowerCase()
            }).replace(c, function(t) {
                return t.toUpperCase()
            })
        }
        var i = n(28),
            o = n(95),
            a = n(96);
        t.exports = r;
        var s = a.map(o),
            l = new RegExp("[^^]\\b(" + s.join("|") + ")\\b", "ig"),
            c = /:\s*(\w)/g
    }, function(t, e) {
        t.exports = function(t) {
            return String(t).replace(/([.*+?=^!:${}()|[\]\/\\])/g, "\\$1")
        }
    }, function(t, e) {
        t.exports = ["a", "an", "and", "as", "at", "but", "by", "en", "for", "from", "how", "if", "in", "neither", "nor", "of", "on", "only", "onto", "out", "or", "per", "so", "than", "that", "the", "to", "until", "up", "upon", "v", "v.", "versus", "vs", "vs.", "via", "when", "with", "without", "yet"]
    }, function(t, e) {
        ! function(t) {
            "use strict";

            function e(t) {
                if ("string" != typeof t && (t = String(t)), /[^a-z0-9\-#$%&'*+.\^_`|~]/i.test(t)) throw new TypeError("Invalid character in header field name");
                return t.toLowerCase()
            }

            function n(t) {
                return "string" != typeof t && (t = String(t)), t
            }

            function r(t) {
                var e = {
                    next: function() {
                        var e = t.shift();
                        return {
                            done: void 0 === e,
                            value: e
                        }
                    }
                };
                return v.iterable && (e[Symbol.iterator] = function() {
                    return e
                }), e
            }

            function i(t) {
                this.map = {}, t instanceof i ? t.forEach(function(t, e) {
                    this.append(e, t)
                }, this) : Array.isArray(t) ? t.forEach(function(t) {
                    this.append(t[0], t[1])
                }, this) : t && Object.getOwnPropertyNames(t).forEach(function(e) {
                    this.append(e, t[e])
                }, this)
            }

            function o(t) {
                if (t.bodyUsed) return Promise.reject(new TypeError("Already read"));
                t.bodyUsed = !0
            }

            function a(t) {
                return new Promise(function(e, n) {
                    t.onload = function() {
                        e(t.result)
                    }, t.onerror = function() {
                        n(t.error)
                    }
                })
            }

            function s(t) {
                var e = new FileReader,
                    n = a(e);
                return e.readAsArrayBuffer(t), n
            }

            function l(t) {
                var e = new FileReader,
                    n = a(e);
                return e.readAsText(t), n
            }

            function c(t) {
                for (var e = new Uint8Array(t), n = new Array(e.length), r = 0; r < e.length; r++) n[r] = String.fromCharCode(e[r]);
                return n.join("")
            }

            function u(t) {
                if (t.slice) return t.slice(0);
                var e = new Uint8Array(t.byteLength);
                return e.set(new Uint8Array(t)), e.buffer
            }

            function f() {
                return this.bodyUsed = !1, this._initBody = function(t) {
                    if (this._bodyInit = t, t)
                        if ("string" == typeof t) this._bodyText = t;
                        else if (v.blob && Blob.prototype.isPrototypeOf(t)) this._bodyBlob = t;
                    else if (v.formData && FormData.prototype.isPrototypeOf(t)) this._bodyFormData = t;
                    else if (v.searchParams && URLSearchParams.prototype.isPrototypeOf(t)) this._bodyText = t.toString();
                    else if (v.arrayBuffer && v.blob && b(t)) this._bodyArrayBuffer = u(t.buffer), this._bodyInit = new Blob([this._bodyArrayBuffer]);
                    else {
                        if (!v.arrayBuffer || !ArrayBuffer.prototype.isPrototypeOf(t) && !x(t)) throw new Error("unsupported BodyInit type");
                        this._bodyArrayBuffer = u(t)
                    } else this._bodyText = "";
                    this.headers.get("content-type") || ("string" == typeof t ? this.headers.set("content-type", "text/plain;charset=UTF-8") : this._bodyBlob && this._bodyBlob.type ? this.headers.set("content-type", this._bodyBlob.type) : v.searchParams && URLSearchParams.prototype.isPrototypeOf(t) && this.headers.set("content-type", "application/x-www-form-urlencoded;charset=UTF-8"))
                }, v.blob && (this.blob = function() {
                    var t = o(this);
                    if (t) return t;
                    if (this._bodyBlob) return Promise.resolve(this._bodyBlob);
                    if (this._bodyArrayBuffer) return Promise.resolve(new Blob([this._bodyArrayBuffer]));
                    if (this._bodyFormData) throw new Error("could not read FormData body as blob");
                    return Promise.resolve(new Blob([this._bodyText]))
                }, this.arrayBuffer = function() {
                    return this._bodyArrayBuffer ? o(this) || Promise.resolve(this._bodyArrayBuffer) : this.blob().then(s)
                }), this.text = function() {
                    var t = o(this);
                    if (t) return t;
                    if (this._bodyBlob) return l(this._bodyBlob);
                    if (this._bodyArrayBuffer) return Promise.resolve(c(this._bodyArrayBuffer));
                    if (this._bodyFormData) throw new Error("could not read FormData body as text");
                    return Promise.resolve(this._bodyText)
                }, v.formData && (this.formData = function() {
                    return this.text().then(p)
                }), this.json = function() {
                    return this.text().then(JSON.parse)
                }, this
            }

            function d(t) {
                var e = t.toUpperCase();
                return w.indexOf(e) > -1 ? e : t
            }

            function h(t, e) {
                e = e || {};
                var n = e.body;
                if (t instanceof h) {
                    if (t.bodyUsed) throw new TypeError("Already read");
                    this.url = t.url, this.credentials = t.credentials, e.headers || (this.headers = new i(t.headers)), this.method = t.method, this.mode = t.mode, n || null == t._bodyInit || (n = t._bodyInit, t.bodyUsed = !0)
                } else this.url = String(t);
                if (this.credentials = e.credentials || this.credentials || "omit", !e.headers && this.headers || (this.headers = new i(e.headers)), this.method = d(e.method || this.method || "GET"), this.mode = e.mode || this.mode || null, this.referrer = null, ("GET" === this.method || "HEAD" === this.method) && n) throw new TypeError("Body not allowed for GET or HEAD requests");
                this._initBody(n)
            }

            function p(t) {
                var e = new FormData;
                return t.trim().split("&").forEach(function(t) {
                    if (t) {
                        var n = t.split("="),
                            r = n.shift().replace(/\+/g, " "),
                            i = n.join("=").replace(/\+/g, " ");
                        e.append(decodeURIComponent(r), decodeURIComponent(i))
                    }
                }), e
            }

            function m(t) {
                var e = new i;
                return t.replace(/\r?\n[\t ]+/g, " ").split(/\r?\n/).forEach(function(t) {
                    var n = t.split(":"),
                        r = n.shift().trim();
                    if (r) {
                        var i = n.join(":").trim();
                        e.append(r, i)
                    }
                }), e
            }

            function g(t, e) {
                e || (e = {}), this.type = "default", this.status = void 0 === e.status ? 200 : e.status, this.ok = this.status >= 200 && this.status < 300, this.statusText = "statusText" in e ? e.statusText : "OK", this.headers = new i(e.headers), this.url = e.url || "", this._initBody(t)
            }
            if (!t.fetch) {
                var v = {
                    searchParams: "URLSearchParams" in t,
                    iterable: "Symbol" in t && "iterator" in Symbol,
                    blob: "FileReader" in t && "Blob" in t && function() {
                        try {
                            return new Blob, !0
                        } catch (t) {
                            return !1
                        }
                    }(),
                    formData: "FormData" in t,
                    arrayBuffer: "ArrayBuffer" in t
                };
                if (v.arrayBuffer) var y = ["[object Int8Array]", "[object Uint8Array]", "[object Uint8ClampedArray]", "[object Int16Array]", "[object Uint16Array]", "[object Int32Array]", "[object Uint32Array]", "[object Float32Array]", "[object Float64Array]"],
                    b = function(t) {
                        return t && DataView.prototype.isPrototypeOf(t)
                    },
                    x = ArrayBuffer.isView || function(t) {
                        return t && y.indexOf(Object.prototype.toString.call(t)) > -1
                    };
                i.prototype.append = function(t, r) {
                    t = e(t), r = n(r);
                    var i = this.map[t];
                    this.map[t] = i ? i + "," + r : r
                }, i.prototype.delete = function(t) {
                    delete this.map[e(t)]
                }, i.prototype.get = function(t) {
                    return t = e(t), this.has(t) ? this.map[t] : null
                }, i.prototype.has = function(t) {
                    return this.map.hasOwnProperty(e(t))
                }, i.prototype.set = function(t, r) {
                    this.map[e(t)] = n(r)
                }, i.prototype.forEach = function(t, e) {
                    for (var n in this.map) this.map.hasOwnProperty(n) && t.call(e, this.map[n], n, this)
                }, i.prototype.keys = function() {
                    var t = [];
                    return this.forEach(function(e, n) {
                        t.push(n)
                    }), r(t)
                }, i.prototype.values = function() {
                    var t = [];
                    return this.forEach(function(e) {
                        t.push(e)
                    }), r(t)
                }, i.prototype.entries = function() {
                    var t = [];
                    return this.forEach(function(e, n) {
                        t.push([n, e])
                    }), r(t)
                }, v.iterable && (i.prototype[Symbol.iterator] = i.prototype.entries);
                var w = ["DELETE", "GET", "HEAD", "OPTIONS", "POST", "PUT"];
                h.prototype.clone = function() {
                    return new h(this, {
                        body: this._bodyInit
                    })
                }, f.call(h.prototype), f.call(g.prototype), g.prototype.clone = function() {
                    return new g(this._bodyInit, {
                        status: this.status,
                        statusText: this.statusText,
                        headers: new i(this.headers),
                        url: this.url
                    })
                }, g.error = function() {
                    var t = new g(null, {
                        status: 0,
                        statusText: ""
                    });
                    return t.type = "error", t
                };
                var C = [301, 302, 303, 307, 308];
                g.redirect = function(t, e) {
                    if (-1 === C.indexOf(e)) throw new RangeError("Invalid status code");
                    return new g(null, {
                        status: e,
                        headers: {
                            location: t
                        }
                    })
                }, t.Headers = i, t.Request = h, t.Response = g, t.fetch = function(t, e) {
                    return new Promise(function(n, r) {
                        var i = new h(t, e),
                            o = new XMLHttpRequest;
                        o.onload = function() {
                            var t = {
                                status: o.status,
                                statusText: o.statusText,
                                headers: m(o.getAllResponseHeaders() || "")
                            };
                            t.url = "responseURL" in o ? o.responseURL : t.headers.get("X-Request-URL");
                            var e = "response" in o ? o.response : o.responseText;
                            n(new g(e, t))
                        }, o.onerror = function() {
                            r(new TypeError("Network request failed"))
                        }, o.ontimeout = function() {
                            r(new TypeError("Network request failed"))
                        }, o.open(i.method, i.url, !0), "include" === i.credentials ? o.withCredentials = !0 : "omit" === i.credentials && (o.withCredentials = !1), "responseType" in o && v.blob && (o.responseType = "blob"), i.headers.forEach(function(t, e) {
                            o.setRequestHeader(e, t)
                        }), o.send(void 0 === i._bodyInit ? null : i._bodyInit)
                    })
                }, t.fetch.polyfill = !0
            }
        }("undefined" != typeof self ? self : this)
    }, function(t, e, n) {
        "use strict";
        (function(e) {
            /*!
            Copyright (C) 2015 by WebReflection

            Permission is hereby granted, free of charge, to any person obtaining a copy
            of this software and associated documentation files (the "Software"), to deal
            in the Software without restriction, including without limitation the rights
            to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
            copies of the Software, and to permit persons to whom the Software is
            furnished to do so, subject to the following conditions:

            The above copyright notice and this permission notice shall be included in
            all copies or substantial portions of the Software.

            THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
            IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
            FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
            AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
            LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
            OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
            THE SOFTWARE.

            */
            function n(t) {
                var e, n, o, s, l, c, u = Object.create(null);
                if (this[h] = u, t)
                    if ("string" == typeof t)
                        for ("?" === t.charAt(0) && (t = t.slice(1)), s = t.split("&"), l = 0, c = s.length; l < c; l++) o = s[l], e = o.indexOf("="), -1 < e ? r(u, i(o.slice(0, e)), i(o.slice(e + 1))) : o.length && r(u, i(o), "");
                    else if (a(t))
                    for (l = 0, c = t.length; l < c; l++) o = t[l], r(u, o[0], o[1]);
                else
                    for (n in t) r(u, n, t[n])
            }

            function r(t, e, n) {
                e in t ? t[e].push("" + n) : t[e] = a(n) ? n : ["" + n]
            }

            function i(t) {
                return decodeURIComponent(t.replace(c, " "))
            }

            function o(t) {
                return encodeURIComponent(t).replace(l, f)
            }
            var a = Array.isArray,
                s = n.prototype,
                l = /[!'\(\)~]|%20|%00/g,
                c = /\+/g,
                u = {
                    "!": "%21",
                    "'": "%27",
                    "(": "%28",
                    ")": "%29",
                    "~": "%7E",
                    "%20": "+",
                    "%00": "\0"
                },
                f = function(t) {
                    return u[t]
                },
                d = function() {
                    try {
                        return !!Symbol.iterator
                    } catch (t) {
                        return !1
                    }
                }(),
                h = "__URLSearchParams__:" + Math.random();
            s.append = function(t, e) {
                r(this[h], t, e)
            }, s.delete = function(t) {
                delete this[h][t]
            }, s.get = function(t) {
                var e = this[h];
                return t in e ? e[t][0] : null
            }, s.getAll = function(t) {
                var e = this[h];
                return t in e ? e[t].slice(0) : []
            }, s.has = function(t) {
                return t in this[h]
            }, s.set = function(t, e) {
                this[h][t] = ["" + e]
            }, s.forEach = function(t, e) {
                var n = this[h];
                Object.getOwnPropertyNames(n).forEach(function(r) {
                    n[r].forEach(function(n) {
                        t.call(e, n, r, this)
                    }, this)
                }, this)
            }, s.keys = function() {
                var t = [];
                this.forEach(function(e, n) {
                    t.push(n)
                });
                var e = {
                    next: function() {
                        var e = t.shift();
                        return {
                            done: void 0 === e,
                            value: e
                        }
                    }
                };
                return d && (e[Symbol.iterator] = function() {
                    return e
                }), e
            }, s.values = function() {
                var t = [];
                this.forEach(function(e) {
                    t.push(e)
                });
                var e = {
                    next: function() {
                        var e = t.shift();
                        return {
                            done: void 0 === e,
                            value: e
                        }
                    }
                };
                return d && (e[Symbol.iterator] = function() {
                    return e
                }), e
            }, s.entries = function() {
                var t = [];
                this.forEach(function(e, n) {
                    t.push([n, e])
                });
                var e = {
                    next: function() {
                        var e = t.shift();
                        return {
                            done: void 0 === e,
                            value: e
                        }
                    }
                };
                return d && (e[Symbol.iterator] = function() {
                    return e
                }), e
            }, d && (s[Symbol.iterator] = s.entries), s.toJSON = function() {
                return {}
            }, s.toString = function() {
                var t, e, n, r, i = this[h],
                    a = [];
                for (e in i)
                    for (n = o(e), t = 0, r = i[e]; t < r.length; t++) a.push(n + "=" + o(r[t]));
                return a.join("&")
            }, t.exports = e.URLSearchParams || n
        }).call(e, n(99))
    }, function(t, e) {
        var n;
        n = function() {
            return this
        }();
        try {
            n = n || Function("return this")() || (0, eval)("this")
        } catch (t) {
            "object" == typeof window && (n = window)
        }
        t.exports = n
    }, function(t, e) {
        t.exports = function(t, e) {
            function n(t, r) {
                return t.reduce(function(t, i) {
                    return Array.isArray(i) && r < e ? t.concat(n(i, r + 1)) : t.concat(i)
                }, [])
            }
            return e = "number" == typeof e ? e : 1 / 0, e ? n(t, 1) : Array.isArray(t) ? t.map(function(t) {
                return t
            }) : t
        }
    }, function(t, e) {
        function n(t) {
            return !(!t || "object" != typeof t || r(t)) && !Object.keys(t).length
        }
        var r = Array.isArray;
        t.exports = n
    }, function(t, e, n) {
        "use strict";

        function r(t) {
            var e = "" + t,
                n = i.exec(e);
            if (!n) return e;
            var r, o = "",
                a = 0,
                s = 0;
            for (a = n.index; a < e.length; a++) {
                switch (e.charCodeAt(a)) {
                    case 34:
                        r = "&quot;";
                        break;
                    case 38:
                        r = "&amp;";
                        break;
                    case 39:
                        r = "&#39;";
                        break;
                    case 60:
                        r = "&lt;";
                        break;
                    case 62:
                        r = "&gt;";
                        break;
                    default:
                        continue
                }
                s !== a && (o += e.substring(s, a)), s = a + 1, o += r
            }
            return s !== a ? o + e.substring(s, a) : o
        }
        /*!
         * escape-html
         * Copyright(c) 2012-2013 TJ Holowaychuk
         * Copyright(c) 2015 Andreas Lubbe
         * Copyright(c) 2015 Tiancheng "Timothy" Gu
         * MIT Licensed
         */
        var i = /["'&<>]/;
        t.exports = r
    }, function(t, e, n) {
        "use strict";

        function r(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
        }

        function i(t, e) {
            if (!t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !e || "object" != typeof e && "function" != typeof e ? t : e
        }

        function o(t, e) {
            if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function, not " + typeof e);
            t.prototype = Object.create(e && e.prototype, {
                constructor: {
                    value: t,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), e && (Object.setPrototypeOf ? Object.setPrototypeOf(t, e) : t.__proto__ = e)
        }
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var a = n(104),
            s = n(105),
            l = n(106),
            c = function(t) {
                return t && t.__esModule ? t : {
                    default: t
                }
            }(l);
        e.default = {
            show: a.Show,
            hide: a.Hide,
            fadeIn: s.FadeIn,
            onclick: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "click"))
                }
                return o(e, t), e
            }(c.default),
            oncontextmenu: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "contextmenu"))
                }
                return o(e, t), e
            }(c.default),
            ondblclick: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "dblclick"))
                }
                return o(e, t), e
            }(c.default),
            onmousedown: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "mousedown"))
                }
                return o(e, t), e
            }(c.default),
            onmouseenter: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "mouseenter"))
                }
                return o(e, t), e
            }(c.default),
            onmouseleave: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "mouseleave"))
                }
                return o(e, t), e
            }(c.default),
            onmousemove: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "mousemove"))
                }
                return o(e, t), e
            }(c.default),
            onmouseover: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "mouseover"))
                }
                return o(e, t), e
            }(c.default),
            onmouseout: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "mouseout"))
                }
                return o(e, t), e
            }(c.default),
            onmouseup: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "mouseup"))
                }
                return o(e, t), e
            }(c.default),
            onkeydown: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "keydown"))
                }
                return o(e, t), e
            }(c.default),
            onkeypress: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "keypress"))
                }
                return o(e, t), e
            }(c.default),
            onkeyup: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "keyup"))
                }
                return o(e, t), e
            }(c.default),
            onload: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "load"))
                }
                return o(e, t), e
            }(c.default),
            onresize: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "resize"))
                }
                return o(e, t), e
            }(c.default),
            onscroll: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "scroll"))
                }
                return o(e, t), e
            }(c.default),
            onblur: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "blur"))
                }
                return o(e, t), e
            }(c.default),
            onchange: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "change"))
                }
                return o(e, t), e
            }(c.default),
            onfocus: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "focus"))
                }
                return o(e, t), e
            }(c.default),
            onfocusin: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "focusin"))
                }
                return o(e, t), e
            }(c.default),
            onfocusout: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "focusout"))
                }
                return o(e, t), e
            }(c.default),
            oninput: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "input"))
                }
                return o(e, t), e
            }(c.default),
            onreset: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "reset"))
                }
                return o(e, t), e
            }(c.default),
            onselect: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "select"))
                }
                return o(e, t), e
            }(c.default),
            onsubmit: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "submit"))
                }
                return o(e, t), e
            }(c.default),
            ondrag: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "drag"))
                }
                return o(e, t), e
            }(c.default),
            ondragend: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "dragend"))
                }
                return o(e, t), e
            }(c.default),
            ondragenter: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "dragenter"))
                }
                return o(e, t), e
            }(c.default),
            ondragleave: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "dragleave"))
                }
                return o(e, t), e
            }(c.default),
            ondragover: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "dragover"))
                }
                return o(e, t), e
            }(c.default),
            ondragstart: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "dragstart"))
                }
                return o(e, t), e
            }(c.default),
            ondrop: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "drop"))
                }
                return o(e, t), e
            }(c.default),
            animationend: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "animationend"))
                }
                return o(e, t), e
            }(c.default),
            animationiteration: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "animationiteration"))
                }
                return o(e, t), e
            }(c.default),
            animationstart: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "animationstart"))
                }
                return o(e, t), e
            }(c.default),
            transitionend: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "webkitTransitionEnd"))
                }
                return o(e, t), e
            }(c.default),
            onmessage: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "message"))
                }
                return o(e, t), e
            }(c.default),
            onopen: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "open"))
                }
                return o(e, t), e
            }(c.default),
            onwheel: function(t) {
                function e() {
                    return r(this, e), i(this, Object.getPrototypeOf(e).call(this, "wheel"))
                }
                return o(e, t), e
            }(c.default)
        }
    }, function(t, e, n) {
        "use strict";

        function r(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
        }
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i = function() {
            function t(t, e) {
                for (var n = 0; n < e.length; n++) {
                    var r = e[n];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(t, r.key, r)
                }
            }
            return function(e, n, r) {
                return n && t(e.prototype, n), r && t(e, r), e
            }
        }();
        e.Show = function() {
            function t() {
                r(this, t), this.node = null
            }
            return i(t, [{
                key: "bind",
                value: function(t) {
                    this.node = t
                }
            }, {
                key: "unbind",
                value: function(t) {
                    this.node = null
                }
            }, {
                key: "update",
                value: function(t) {
                    this.node.style.display = t ? "" : "none"
                }
            }]), t
        }(), e.Hide = function() {
            function t() {
                r(this, t), this.node = null
            }
            return i(t, [{
                key: "bind",
                value: function(t) {
                    this.node = t
                }
            }, {
                key: "unbind",
                value: function(t) {
                    this.node = null
                }
            }, {
                key: "update",
                value: function(t) {
                    this.node.style.display = t ? "none" : ""
                }
            }]), t
        }()
    }, function(t, e, n) {
        "use strict";

        function r(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
        }
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i = function() {
            function t(t, e) {
                for (var n = 0; n < e.length; n++) {
                    var r = e[n];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(t, r.key, r)
                }
            }
            return function(e, n, r) {
                return n && t(e.prototype, n), r && t(e, r), e
            }
        }();
        e.FadeIn = function() {
            function t() {
                r(this, t), this.node = null
            }
            return i(t, [{
                key: "bind",
                value: function(t) {
                    this.node = t
                }
            }, {
                key: "unbind",
                value: function(t) {
                    this.node = null
                }
            }, {
                key: "update",
                value: function() {
                    var t = this,
                        e = arguments.length <= 0 || void 0 === arguments[0] ? 300 : arguments[0];
                    this.node.style.opacity = 0, this.node.style.transition = "opacity " + e + "ms", window.requestAnimationFrame(function() {
                        return t.node.style.opacity = 1
                    })
                }
            }]), t
        }()
    }, function(t, e, n) {
        "use strict";

        function r(t, e) {
            if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
        }
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var i = function() {
                function t(t, e) {
                    for (var n = 0; n < e.length; n++) {
                        var r = e[n];
                        r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(t, r.key, r)
                    }
                }
                return function(e, n, r) {
                    return n && t(e.prototype, n), r && t(e, r), e
                }
            }(),
            o = function() {
                function t(e) {
                    r(this, t), this.type = e, this.handler = null, this.callback = this.callback.bind(this)
                }
                return i(t, [{
                    key: "callback",
                    value: function(t) {
                        this.handler(t)
                    }
                }, {
                    key: "bind",
                    value: function(t) {
                        t.addEventListener(this.type, this.callback)
                    }
                }, {
                    key: "unbind",
                    value: function(t) {
                        t.removeEventListener(this.type, this.callback)
                    }
                }, {
                    key: "update",
                    value: function(t) {
                        this.handler = t
                    }
                }]), t
            }();
        e.default = o
    }, function(t, e, n) {
        "use strict";
        var r = n(12),
            i = function(t) {
                return t && t.__esModule ? t : {
                    default: t
                }
            }(r),
            o = n(108);
        i.default.prototype.on = function(t, e, n) {
            var r = this,
                i = arguments.length <= 3 || void 0 === arguments[3] ? void 0 : arguments[3];
            this.delegates || (this.delegates = [], this.nodes.forEach(function(t, e) {
                if (8 === t.nodeType) throw "Can not use event delegating with non-element nodes on first level.";
                r.delegates[e] = new o.Delegate(t)
            })), this.delegates.forEach(function(r) {
                return r.on(t, e, n, i)
            })
        }, i.default.prototype.off = function() {
            var t = arguments.length <= 0 || void 0 === arguments[0] ? void 0 : arguments[0],
                e = arguments.length <= 1 || void 0 === arguments[1] ? void 0 : arguments[1],
                n = arguments.length <= 2 || void 0 === arguments[2] ? void 0 : arguments[2],
                r = arguments.length <= 3 || void 0 === arguments[3] ? void 0 : arguments[3];
            this.delegates.forEach(function(i) {
                return i.off(t, e, n, r)
            })
        }
    }, function(t, e, n) {
        "use strict";
        /**
         * @preserve Create and manage a DOM event delegator.
         *
         * @version 0.3.0
         * @codingstandard ftlabs-jsv2
         * @copyright The Financial Times Limited [All Rights Reserved]
         * @license MIT License (see LICENSE.txt)
         */
        var r = n(109);
        t.exports = function(t) {
            return new r(t)
        }, t.exports.Delegate = r
    }, function(t, e, n) {
        "use strict";

        function r(t) {
            this.listenerMap = [{}, {}], t && this.root(t), this.handle = r.prototype.handle.bind(this)
        }

        function i(t, e) {
            return t.toLowerCase() === e.tagName.toLowerCase()
        }

        function o(t, e) {
            return this.rootElement === window ? e === document : this.rootElement === e
        }

        function a(t, e) {
            return t === e.id
        }
        t.exports = r, r.prototype.root = function(t) {
            var e, n = this.listenerMap;
            if (this.rootElement) {
                for (e in n[1]) n[1].hasOwnProperty(e) && this.rootElement.removeEventListener(e, this.handle, !0);
                for (e in n[0]) n[0].hasOwnProperty(e) && this.rootElement.removeEventListener(e, this.handle, !1)
            }
            if (!t || !t.addEventListener) return this.rootElement && delete this.rootElement, this;
            this.rootElement = t;
            for (e in n[1]) n[1].hasOwnProperty(e) && this.rootElement.addEventListener(e, this.handle, !0);
            for (e in n[0]) n[0].hasOwnProperty(e) && this.rootElement.addEventListener(e, this.handle, !1);
            return this
        }, r.prototype.captureForType = function(t) {
            return -1 !== ["blur", "error", "focus", "load", "resize", "scroll"].indexOf(t)
        }, r.prototype.on = function(t, e, n, r) {
            var l, c, u, f;
            if (!t) throw new TypeError("Invalid event type: " + t);
            if ("function" == typeof e && (r = n, n = e, e = null), void 0 === r && (r = this.captureForType(t)), "function" != typeof n) throw new TypeError("Handler must be a type of Function");
            return l = this.rootElement, c = this.listenerMap[r ? 1 : 0], c[t] || (l && l.addEventListener(t, this.handle, r), c[t] = []), e ? /^[a-z]+$/i.test(e) ? (f = e, u = i) : /^#[a-z0-9\-_]+$/i.test(e) ? (f = e.slice(1), u = a) : (f = e, u = s) : (f = null, u = o.bind(this)), c[t].push({
                selector: e,
                handler: n,
                matcher: u,
                matcherParam: f
            }), this
        }, r.prototype.off = function(t, e, n, r) {
            var i, o, a, s, l;
            if ("function" == typeof e && (r = n, n = e, e = null), void 0 === r) return this.off(t, e, n, !0), this.off(t, e, n, !1), this;
            if (a = this.listenerMap[r ? 1 : 0], !t) {
                for (l in a) a.hasOwnProperty(l) && this.off(l, e, n);
                return this
            }
            if (!(s = a[t]) || !s.length) return this;
            for (i = s.length - 1; i >= 0; i--) o = s[i], e && e !== o.selector || n && n !== o.handler || s.splice(i, 1);
            return s.length || (delete a[t], this.rootElement && this.rootElement.removeEventListener(t, this.handle, r)), this
        }, r.prototype.handle = function(t) {
            var e, n, r, i, o, a, s = t.type,
                l = [];
            if (!0 !== t.ftLabsDelegateIgnore) {
                switch (a = t.target, 3 === a.nodeType && (a = a.parentNode), r = this.rootElement, t.eventPhase || (t.target !== t.currentTarget ? 3 : 2)) {
                    case 1:
                        l = this.listenerMap[1][s];
                        break;
                    case 2:
                        this.listenerMap[0] && this.listenerMap[0][s] && (l = l.concat(this.listenerMap[0][s])), this.listenerMap[1] && this.listenerMap[1][s] && (l = l.concat(this.listenerMap[1][s]));
                        break;
                    case 3:
                        l = this.listenerMap[0][s]
                }
                for (n = l.length; a && n;) {
                    for (e = 0; e < n && (i = l[e]); e++)
                        if (i.matcher.call(a, i.matcherParam, a) && (o = this.fire(t, a, i)), !1 === o) return t.ftLabsDelegateIgnore = !0, void t.preventDefault();
                    if (a === r) break;
                    n = l.length, a = a.parentElement
                }
            }
        }, r.prototype.fire = function(t, e, n) {
            return n.handler.call(e, t, e)
        };
        var s = function(t) {
            if (t) {
                var e = t.prototype;
                return e.matches || e.matchesSelector || e.webkitMatchesSelector || e.mozMatchesSelector || e.msMatchesSelector || e.oMatchesSelector
            }
        }(Element);
        r.prototype.destroy = function() {
            this.off(), this.root()
        }
    }, function(t, e, n) {
        function r() {
            d.call(this);
            var t = this,
                e = document.createElement("div"),
                n = document.createElement("div"),
                r = document.createComment("if"),
                c = {},
                u = document.createElement("div"),
                h = document.createComment("if"),
                p = {},
                m = document.createElement("textarea"),
                g = document.createComment("if"),
                v = {},
                y = document.createElement("div"),
                b = {},
                x = {},
                w = document.createComment("if"),
                C = {};
            u.appendChild(h), u.appendChild(m), u.setAttribute("class", "code-area "), y.setAttribute("class", "js-code-output-canvas-placeholder"), n.appendChild(r), n.appendChild(u), n.appendChild(g), n.appendChild(y), n.setAttribute("class", "executable-fragment "), e.appendChild(n), e.appendChild(w), e.setAttribute("class", "executable-fragment-wrapper"), this.__update__ = {
                highlightOnly: function(e) {
                    d.cond(t, r, c, i, !e), d.cond(t, w, C, f, !e)
                },
                isShouldBeFolded: function(e) {
                    d.cond(t, h, p, o, e)
                },
                folded: function(t) {
                    u.setAttribute("class", "code-area " + (t ? "_folded" : "_unfolded"))
                },
                openConsole: function(e) {
                    d.cond(t, g, v, a, e)
                },
                waitingForOutput: function(e) {
                    var n;
                    n = d.cond(t, y, b, s, e), d.cond(t, y, x, l, !n)
                },
                theme: function(t) {
                    n.setAttribute("class", "executable-fragment " + t)
                }
            }, this.onUpdate = function(t) {
                c.ref && c.ref.update(t), p.ref && p.ref.update(t), v.ref && v.ref.update(t), b.ref && b.ref.update(t), x.ref && x.ref.update(t), C.ref && C.ref.update(t)
            }, this.nodes = [e]
        }

        function i() {
            d.call(this);
            var t = this,
                e = document.createElement("div");
            e.setAttribute("class", "run-button ");
            var n;
            this.__update__ = {
                waitingForOutput: function(t) {
                    e.setAttribute("class", "run-button " + (t ? "_disabled" : ""))
                }
            }, this.onRender = function() {
                void 0 === n && (n = new t.directives.onclick), n.bind(e), n.update(t.parent.execute.bind(t.parent))
            }, this.onRemove = function(t) {
                n.unbind(e)
            }, this.nodes = [e]
        }

        function o() {
            d.call(this), this.__cache__ = {};
            var t = this,
                e = document.createElement("div");
            e.setAttribute("class", "fold-button  ");
            var n, r;
            this.__update__ = {
                foldButtonHover_theme: function(t, n) {
                    e.setAttribute("class", "fold-button " + (t ? "_hover" : "") + " " + n)
                }
            }, this.onRender = function() {
                void 0 === n && (n = new t.directives.onmouseenter), n.bind(e), n.update(t.parent.onFoldButtonMouseEnter.bind(t.parent)), void 0 === r && (r = new t.directives.onmouseleave), r.bind(e), r.update(t.parent.onFoldButtonMouseLeave.bind(t.parent))
            }, this.onRemove = function(t) {
                n.unbind(e), r.unbind(e)
            }, this.nodes = [e]
        }

        function a() {
            d.call(this);
            var t = this,
                e = document.createElement("div");
            e.setAttribute("class", "console-close ");
            var n;
            this.__update__ = {
                theme: function(t) {
                    e.setAttribute("class", "console-close " + t)
                }
            }, this.onRender = function() {
                void 0 === n && (n = new t.directives.onclick), n.bind(e), n.update(t.parent.onConsoleCloseButtonEnter.bind(t.parent))
            }, this.onRemove = function(t) {
                n.unbind(e)
            }, this.nodes = [e]
        }

        function s() {
            d.call(this);
            var t = document.createElement("div"),
                e = document.createElement("div");
            e.setAttribute("class", "loader "), t.appendChild(e), t.setAttribute("class", "output-wrapper "), this.__update__ = {
                theme: function(n) {
                    e.setAttribute("class", "loader " + n), t.setAttribute("class", "output-wrapper " + n)
                }
            }, this.nodes = [t]
        }

        function l() {
            d.call(this), this.__cache__ = {};
            var t = this,
                e = document.createComment("if"),
                n = {};
            this.__update__ = {
                exception_output: function(r, i) {
                    d.cond(t, e, n, c, i && "" != i || r)
                }
            }, this.onUpdate = function(t) {
                n.ref && n.ref.update(t)
            }, this.nodes = [e]
        }

        function c() {
            d.call(this);
            var t = this,
                e = document.createElement("div"),
                n = document.createElement("div"),
                r = document.createComment("unsafe"),
                i = [],
                o = document.createComment("if"),
                a = {};
            n.appendChild(r), n.appendChild(o), n.setAttribute("class", "code-output"), e.appendChild(n), e.setAttribute("class", "output-wrapper "), this.__update__ = {
                output: function(t) {
                    p(r, i, t)
                },
                exception: function(e) {
                    d.cond(t, o, a, u, e)
                },
                theme: function(t) {
                    e.setAttribute("class", "output-wrapper " + t)
                }
            }, this.onUpdate = function(t) {
                a.ref && a.ref.update(t)
            }, this.nodes = [e]
        }

        function u() {
            d.call(this);
            var t = this,
                e = document.createComment("Exception"),
                n = {};
            this.__update__ = {
                exception: function(r) {
                    d.insert(t, e, n, h, r)
                }
            }, this.onRender = function() {
                d.insert(t, e, n, h, {
                    originalException: !0
                })
            }, this.nodes = [e]
        }

        function f() {
            d.call(this);
            var t = document.createElement("div"),
                e = document.createElement("span"),
                n = document.createTextNode(""),
                r = document.createElement("span"),
                i = document.createTextNode("");
            e.appendChild(document.createTextNode("Target platform: ")), e.appendChild(n), r.appendChild(document.createTextNode("Running on kotlin v. ")), r.appendChild(i), t.appendChild(e), t.appendChild(r), t.setAttribute("class", "compiler-info"), this.__update__ = {
                targetPlatform: function(t) {
                    n.textContent = t.printableName
                },
                compilerVersion: function(t) {
                    i.textContent = t
                }
            }, this.nodes = [t]
        }
        var d = n(12),
            h = function(t) {
                return t && t.__esModule ? t.default : t
            }(n(111)),
            p = function(t, e, n) {
                var r, i = e.length,
                    o = document.createElement("div");
                for (o.innerHTML = n; i-- > 0;) e[i].parentNode.removeChild(e.pop());
                for (i = r = o.childNodes.length - 1; r >= 0; r--) e.push(o.childNodes[r]);
                if (++i, 8 == t.nodeType) {
                    if (!t.parentNode) throw "Can not insert child view into parent node. You need append your view first and then update.";
                    for (; i-- > 0;) t.parentNode.insertBefore(e[i], t)
                } else
                    for (; i-- > 0;) t.appendChild(e[i])
            };
        r.prototype = Object.create(d.prototype), r.prototype.constructor = r, r.pool = [], r.prototype.update = function(t) {
            void 0 !== t.highlightOnly && this.__update__.highlightOnly(t.highlightOnly), void 0 !== t.isShouldBeFolded && this.__update__.isShouldBeFolded(t.isShouldBeFolded), void 0 !== t.folded && this.__update__.folded(t.folded), void 0 !== t.openConsole && this.__update__.openConsole(t.openConsole), void 0 !== t.waitingForOutput && this.__update__.waitingForOutput(t.waitingForOutput), void 0 !== t.theme && this.__update__.theme(t.theme), this.onUpdate(t)
        }, i.prototype = Object.create(d.prototype), i.prototype.constructor = i, i.pool = [], i.prototype.update = function(t) {
            void 0 !== t.waitingForOutput && this.__update__.waitingForOutput(t.waitingForOutput)
        }, o.prototype = Object.create(d.prototype), o.prototype.constructor = o, o.pool = [], o.prototype.update = function(t) {
            void 0 !== t.foldButtonHover && (this.__cache__.foldButtonHover = t.foldButtonHover), void 0 !== t.theme && (this.__cache__.theme = t.theme), void 0 !== this.__cache__.foldButtonHover && void 0 !== this.__cache__.theme && this.__update__.foldButtonHover_theme(this.__cache__.foldButtonHover, this.__cache__.theme)
        }, a.prototype = Object.create(d.prototype), a.prototype.constructor = a, a.pool = [], a.prototype.update = function(t) {
            void 0 !== t.theme && this.__update__.theme(t.theme)
        }, s.prototype = Object.create(d.prototype), s.prototype.constructor = s, s.pool = [], s.prototype.update = function(t) {
            void 0 !== t.theme && this.__update__.theme(t.theme)
        }, l.prototype = Object.create(d.prototype), l.prototype.constructor = l, l.pool = [], l.prototype.update = function(t) {
            void 0 !== t.exception && (this.__cache__.exception = t.exception), void 0 !== t.output && (this.__cache__.output = t.output), void 0 !== this.__cache__.exception && void 0 !== this.__cache__.output && this.__update__.exception_output(this.__cache__.exception, this.__cache__.output), this.onUpdate(t)
        }, c.prototype = Object.create(d.prototype), c.prototype.constructor = c, c.pool = [], c.prototype.update = function(t) {
            void 0 !== t.output && this.__update__.output(t.output), void 0 !== t.exception && this.__update__.exception(t.exception), void 0 !== t.theme && this.__update__.theme(t.theme), this.onUpdate(t)
        }, u.prototype = Object.create(d.prototype), u.prototype.constructor = u, u.pool = [], u.prototype.update = function(t) {
            void 0 !== t.exception && this.__update__.exception(t.exception)
        }, f.prototype = Object.create(d.prototype), f.prototype.constructor = f, f.pool = [], f.prototype.update = function(t) {
            void 0 !== t.targetPlatform && this.__update__.targetPlatform(t.targetPlatform), void 0 !== t.compilerVersion && this.__update__.compilerVersion(t.compilerVersion)
        }, t.exports = r
    }, function(t, e, n) {
        function r() {
            c.call(this);
            var t = this,
                e = document.createElement("span"),
                n = document.createTextNode(""),
                r = document.createComment("if"),
                s = {},
                l = document.createElement("br"),
                u = document.createComment("for"),
                f = new c.Map,
                d = document.createComment("for"),
                h = new c.Map;
            e.appendChild(document.createTextNode('Exception in thread "main" ')), e.appendChild(n), e.appendChild(r), e.setAttribute("class", "error-output"), this.__update__ = {
                fullName: function(t) {
                    n.textContent = t
                },
                message: function(e) {
                    c.cond(t, r, s, i, e)
                },
                stackTrace: function(e) {
                    c.loop(t, u, f, o, e, {
                        value: "stacktraceElement"
                    })
                },
                causes: function(e) {
                    c.loop(t, d, h, a, e, {
                        value: "cause"
                    })
                }
            }, this.onUpdate = function(t) {
                s.ref && s.ref.update(t), f.forEach(function(e) {
                    e.update(e.__state__), e.update(t), e.update(e.__state__)
                }), h.forEach(function(e) {
                    e.update(e.__state__), e.update(t), e.update(e.__state__)
                })
            }, this.nodes = [e, l, u, d]
        }

        function i() {
            c.call(this);
            var t = document.createTextNode("");
            this.__update__ = {
                message: function(e) {
                    t.textContent = e
                }
            }, this.nodes = [document.createTextNode(": "), t]
        }

        function o() {
            c.call(this), this.__state__ = {};
            var t = document.createElement("span"),
                e = document.createTextNode(""),
                n = document.createTextNode(""),
                r = document.createTextNode(""),
                i = document.createTextNode(""),
                o = document.createElement("br");
            t.appendChild(document.createTextNode(" at ")), t.appendChild(e), t.appendChild(document.createTextNode(".")), t.appendChild(n), t.appendChild(document.createTextNode("(")), t.appendChild(r), t.appendChild(document.createTextNode(":")), t.appendChild(i), t.appendChild(document.createTextNode(")")), t.setAttribute("class", "stacktrace-element error-output"), this.__update__ = {
                stacktraceElement: function(t) {
                    e.textContent = t.className, n.textContent = t.methodName, r.textContent = t.fileName, i.textContent = t.lineNumber
                }
            }, this.nodes = [t, o]
        }

        function a() {
            c.call(this), this.__state__ = {};
            var t = this,
                e = document.createElement("span"),
                n = document.createTextNode(""),
                r = document.createComment("if"),
                i = {},
                o = document.createElement("br"),
                a = document.createComment("for"),
                u = new c.Map;
            e.appendChild(document.createTextNode("Caused by: ")), e.appendChild(n), e.appendChild(r), e.setAttribute("class", "error-output"), this.__update__ = {
                fullName: function(t) {
                    n.textContent = t
                },
                message: function(e) {
                    c.cond(t, r, i, s, e)
                },
                stackTrace: function(e) {
                    c.loop(t, a, u, l, e, {
                        value: "stacktraceElement"
                    })
                }
            }, this.onUpdate = function(t) {
                i.ref && i.ref.update(t), u.forEach(function(e) {
                    e.update(e.__state__), e.update(t), e.update(e.__state__)
                })
            }, this.nodes = [e, o, a]
        }

        function s() {
            c.call(this);
            var t = document.createTextNode("");
            this.__update__ = {
                message: function(e) {
                    t.textContent = e
                }
            }, this.nodes = [document.createTextNode(": "), t]
        }

        function l() {
            c.call(this), this.__state__ = {};
            var t = document.createElement("span"),
                e = document.createTextNode(""),
                n = document.createTextNode(""),
                r = document.createTextNode(""),
                i = document.createTextNode(""),
                o = document.createElement("br");
            t.appendChild(document.createTextNode("at ")), t.appendChild(e), t.appendChild(document.createTextNode(".")), t.appendChild(n), t.appendChild(document.createTextNode("(")), t.appendChild(r), t.appendChild(document.createTextNode(":")), t.appendChild(i), t.appendChild(document.createTextNode(")")), t.setAttribute("class", "stacktrace-element error-output"), this.__update__ = {
                stacktraceElement: function(t) {
                    e.textContent = t.className, n.textContent = t.methodName, r.textContent = t.fileName, i.textContent = t.lineNumber
                }
            }, this.nodes = [t, o]
        }
        var c = n(12);
        r.prototype = Object.create(c.prototype), r.prototype.constructor = r, r.pool = [], r.prototype.update = function(t) {
            void 0 !== t.fullName && this.__update__.fullName(t.fullName), void 0 !== t.message && this.__update__.message(t.message), void 0 !== t.stackTrace && this.__update__.stackTrace(t.stackTrace), void 0 !== t.causes && this.__update__.causes(t.causes), this.onUpdate(t)
        }, i.prototype = Object.create(c.prototype), i.prototype.constructor = i, i.pool = [], i.prototype.update = function(t) {
            void 0 !== t.message && this.__update__.message(t.message)
        }, o.prototype = Object.create(c.prototype), o.prototype.constructor = o, o.pool = [], o.prototype.update = function(t) {
            void 0 !== t.stacktraceElement && void 0 !== t.__index__ && this.__update__.stacktraceElement(t.stacktraceElement)
        }, a.prototype = Object.create(c.prototype), a.prototype.constructor = a, a.pool = [], a.prototype.update = function(t) {
            void 0 !== t.fullName && this.__update__.fullName(t.fullName), void 0 !== t.message && this.__update__.message(t.message), void 0 !== t.stackTrace && this.__update__.stackTrace(t.stackTrace), void 0 !== t.cause && t.__index__, this.onUpdate(t)
        }, s.prototype = Object.create(c.prototype), s.prototype.constructor = s, s.pool = [], s.prototype.update = function(t) {
            void 0 !== t.message && this.__update__.message(t.message)
        }, l.prototype = Object.create(c.prototype), l.prototype.constructor = l, l.pool = [], l.prototype.update = function(t) {
            void 0 !== t.stacktraceElement && void 0 !== t.__index__ && this.__update__.stacktraceElement(t.stacktraceElement)
        }, t.exports = r
    }, function(t, e, n) {
        var r = n(113);
        "string" == typeof r && (r = [
            [t.i, r, ""]
        ]);
        var i = {};
        i.transform = void 0;
        n(29)(r, i);
        r.locals && (t.exports = r.locals)
    }, function(t, e, n) {
        e = t.exports = n(8)(void 0), e.push([t.i, ".k2js-iframe{display:none;padding-top:2px}iframe{border:none;background:#fff;width:100%;z-index:10}", ""])
    }, function(t, e) {
        t.exports = function(t) {
            var e = "undefined" != typeof window && window.location;
            if (!e) throw new Error("fixUrls requires window.location");
            if (!t || "string" != typeof t) return t;
            var n = e.protocol + "//" + e.host,
                r = n + e.pathname.replace(/\/[^\/]*$/, "/");
            return t.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(t, e) {
                var i = e.trim().replace(/^"(.*)"$/, function(t, e) {
                    return e
                }).replace(/^'(.*)'$/, function(t, e) {
                    return e
                });
                if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/)/i.test(i)) return t;
                var o;
                return o = 0 === i.indexOf("//") ? i : 0 === i.indexOf("/") ? n + i : r + i.replace(/^\.\//, ""), "url(" + JSON.stringify(o) + ")"
            })
        }
    }, function(t, e, n) {
        "use strict";
        var r, i = n(14),
            o = n(20),
            a = n(7),
            s = n(2),
            l = n(1),
            c = n(4),
            u = n(25),
            f = n(3),
            d = n(17),
            h = n(26),
            p = n(116),
            m = n(119),
            g = Function.prototype.call,
            v = Object.defineProperties,
            y = Object.getPrototypeOf;
        t.exports = r = function() {
            var t, e, n, i = arguments[0];
            if (!(this instanceof r)) throw new TypeError("Constructor requires 'new'");
            return n = m && a && Map !== r ? a(new Map, y(this)) : this, null != i && d(i), v(n, {
                __mapKeysData__: c("c", t = []),
                __mapValuesData__: c("c", e = [])
            }), i ? (h(i, function(n) {
                var r = l(n)[0];
                n = n[1], -1 === o.call(t, r) && (t.push(r), e.push(n))
            }, n), n) : n
        }, m && (a && a(r, Map), r.prototype = Object.create(Map.prototype, {
            constructor: c(r)
        })), u(v(r.prototype, {
            clear: c(function() {
                this.__mapKeysData__.length && (i.call(this.__mapKeysData__), i.call(this.__mapValuesData__), this.emit("_clear"))
            }),
            delete: c(function(t) {
                var e = o.call(this.__mapKeysData__, t);
                return -1 !== e && (this.__mapKeysData__.splice(e, 1), this.__mapValuesData__.splice(e, 1), this.emit("_delete", e, t), !0)
            }),
            entries: c(function() {
                return new p(this, "key+value")
            }),
            forEach: c(function(t) {
                var e, n, r = arguments[1];
                for (s(t), e = this.entries(), n = e._next(); void 0 !== n;) g.call(t, r, this.__mapValuesData__[n], this.__mapKeysData__[n], this), n = e._next()
            }),
            get: c(function(t) {
                var e = o.call(this.__mapKeysData__, t);
                if (-1 !== e) return this.__mapValuesData__[e]
            }),
            has: c(function(t) {
                return -1 !== o.call(this.__mapKeysData__, t)
            }),
            keys: c(function() {
                return new p(this, "key")
            }),
            set: c(function(t, e) {
                var n, r = o.call(this.__mapKeysData__, t);
                return -1 === r && (r = this.__mapKeysData__.push(t) - 1, n = !0), this.__mapValuesData__[r] = e, n && this.emit("_add", r, t), this
            }),
            size: c.gs(function() {
                return this.__mapKeysData__.length
            }),
            values: c(function() {
                return new p(this, "value")
            }),
            toString: c(function() {
                return "[object Map]"
            })
        })), Object.defineProperty(r.prototype, f.iterator, c(function() {
            return this.entries()
        })), Object.defineProperty(r.prototype, f.toStringTag, c("c", "Map"))
    }, function(t, e, n) {
        "use strict";
        var r, i = n(7),
            o = n(4),
            a = n(11),
            s = n(3).toStringTag,
            l = n(117),
            c = Object.defineProperties,
            u = a.prototype._unBind;
        r = t.exports = function(t, e) {
            if (!(this instanceof r)) return new r(t, e);
            a.call(this, t.__mapKeysData__, t), e && l[e] || (e = "key+value"), c(this, {
                __kind__: o("", e),
                __values__: o("w", t.__mapValuesData__)
            })
        }, i && i(r, a), r.prototype = Object.create(a.prototype, {
            constructor: o(r),
            _resolve: o(function(t) {
                return "value" === this.__kind__ ? this.__values__[t] : "key" === this.__kind__ ? this.__list__[t] : [this.__list__[t], this.__values__[t]]
            }),
            _unBind: o(function() {
                this.__values__ = null, u.call(this)
            }),
            toString: o(function() {
                return "[object Map Iterator]"
            })
        }), Object.defineProperty(r.prototype, s, o("c", "Map Iterator"))
    }, function(t, e, n) {
        "use strict";
        t.exports = n(118)("key", "value", "key+value")
    }, function(t, e, n) {
        "use strict";
        var r = Array.prototype.forEach,
            i = Object.create;
        t.exports = function(t) {
            var e = i(null);
            return r.call(arguments, function(t) {
                e[t] = !0
            }), e
        }
    }, function(t, e, n) {
        "use strict";
        t.exports = function() {
            return "undefined" != typeof Map && "[object Map]" === Object.prototype.toString.call(new Map)
        }()
    }, function(t, e, n) {
        "use strict";
        var r = /[|\\{}()[\]^$+*?.]/g;
        t.exports = function(t) {
            if ("string" != typeof t) throw new TypeError("Expected a string");
            return t.replace(r, "\\$&")
        }
    }, function(t, e, n) {
        var r = n(122);
        "string" == typeof r && (r = [
            [t.i, r, ""]
        ]);
        var i = {};
        i.transform = void 0;
        n(29)(r, i);
        r.locals && (t.exports = r.locals)
    }, function(t, e, n) {
        e = t.exports = n(8)(void 0), e.i(n(123), ""), e.i(n(124), ""), e.i(n(125), ""), e.i(n(126), ""), e.push([t.i, ".executable-fragment-wrapper{margin-bottom:20px;position:relative}.executable-fragment{border:1px solid #eaeaec}.hidden-dependency{display:none}.executable-fragment.darcula{background-color:#313336}.CodeMirror{height:auto}.CodeMirror pre{line-height:16.8px;margin-bottom:0!important}.CodeMirror .CodeMirror-overlayscroll-horizontal div,.CodeMirror .CodeMirror-overlayscroll-vertical div{display:none}.CodeMirror:hover .CodeMirror-overlayscroll-horizontal div,.CodeMirror:hover .CodeMirror-overlayscroll-vertical div{display:block;background:#afb1b3}.CodeMirror-lines{padding:0;margin:12px 0}.CodeMirror-gutter{height:auto}.CodeMirror{line-height:1.4;font-family:menlo,consolas,monospace;font-size:12px}.CodeMirror-linenumber{min-width:0;text-align:center}.CodeMirror-linebackground.unmodifiable-line{background-color:#eaeaec}.CodeMirror-linebackground.unmodifiable-line-dark{background-color:#3c3f43}.markPlaceholder{border-top:1px solid #167dff;border-bottom:1px solid #167dff}.markPlaceholder-start{border-left:1px solid #167dff}.markPlaceholder-end{border-right:1px solid #167dff}.run-button{position:absolute;z-index:10;right:5px;top:5px;height:20px;width:16px;cursor:pointer;background:url(" + n(127) + ');background-size:cover;background-repeat:no-repeat}.run-button._disabled{cursor:default;opacity:.5}.loader{position:relative;width:15px;height:15px;margin:0 auto;border-radius:50%;text-indent:-9999em;color:#161616;font-size:8px;opacity:.7;animation:loader 1s infinite ease-in-out;animation-delay:.16s}.loader.darcula{opacity:1;color:#696969}.loader:before{left:-3.5em;animation-delay:0s;animation:loader 1s infinite ease-in-out}.loader:after,.loader:before{position:absolute;width:15px;border-radius:50%;height:15px;content:""}.loader:after{left:3.5em;animation-delay:.32s;animation:loader 1s infinite ease-in-out}@keyframes loader{to{box-shadow:0 2.5em 0 -1.3em}80%{box-shadow:0 2.5em 0 -1.3em}0%{box-shadow:0 2.5em 0 -1.3em}40%{box-shadow:0 2.5em 0 0}}.cm__ERROR{color:#ec5424!important}.errors-and-warnings-gutter{width:16px}.ERRORgutter{background:url("https://try.kotlinlang.org/static/images/icons_all_sprite.svg") no-repeat -150px -500px}.ERRORgutter,.WARNINGgutter{height:13px;width:13px;margin-top:2px;margin-left:2px}.WARNINGgutter{background:url("https://try.kotlinlang.org/static/images/icons_all_sprite.svg") no-repeat -150px -600px}.cm__red_wavy_line{background:url(http://try.kotlinlang.org/static/images/wavyline-red.gif) repeat-x 100% 100%!important;padding-bottom:2px}.cm__green_wavy_line{background:url(http://try.kotlinlang.org/static/images/wavyline-green.gif) repeat-x 100% 100%!important;padding-bottom:2px}.output-wrapper{border-top:1px solid #eaeaec;min-height:60px;font-size:14px;background-color:#fff}.output-wrapper.darcula{background-color:#313336;border-top:1px solid grey;color:#afb1b3}.code-output{white-space:pre-wrap;font-family:Liberation Mono,Consolas,Menlo,Courier,monospace;overflow:auto;padding-left:10px;padding-top:15px}.standard-output.darcula{color:#afb1b3}.error-output{white-space:pre-wrap;color:#ec5424;min-height:1.4em;margin:0;vertical-align:top}.error-output.darcula{color:#ec5424}.standard-output{color:#000;margin:0}.standard-output,.test-output{white-space:pre;min-height:1.4em;vertical-align:top}.test-output{color:#4dbb5f;margin-left:20px}.console-close{position:absolute;right:1px;margin-top:1px;width:16px;height:16px;background:#afb1b3 url(' + n(31) + ")}.console-close.darcula{background:url(" + n(31) + ")}.console-close:hover{background-color:dimgray}.console-close.darcula:hover{background-color:#ec5424}.test-fail{min-height:1.4em;margin-left:20px;vertical-align:top}.test-fail,.test-fail.darcula{color:#ec5424}.console-icon{margin-top:2px;width:15px;height:15px;position:absolute}.console-icon.attention{background-image:url(" + n(128) + ")}.console-icon.ok{background-image:url(" + n(129) + ")}.console-icon.fail{background-image:url(" + n(130) + ")}.test-time{float:right;font-size:10px;color:#afb1b3;margin-right:20px;margin-top:-14px}.stacktrace-element{margin-left:20px}.CodeMirror-foldgutter{position:absolute;width:100%}.CodeMirror-foldgutter-folded{width:100%;background:#fff}.fold-button{position:absolute;height:19px;width:31px;top:0;left:50%;transform:translate(-50%,-50%);z-index:10;cursor:pointer;background:transparent url(" + n(131) + ") no-repeat}.fold-button._hover{background-image:url(" + n(132) + ")}.fold-button.darcula{background:url(" + n(133) + ") no-repeat}.fold-button.darcula._hover{background-image:url(" + n(134) + ")}._unfolded .fold-button{background-image:url(" + n(135) + ")}._unfolded .fold-button._hover{background-image:url(" + n(136) + ")}._unfolded .fold-button.darcula{background:url(" + n(137) + ") no-repeat}._unfolded .fold-button.darcula._hover{background-image:url(" + n(138) + ')}.code-area{position:relative}.compiler-info{position:absolute;right:0;font-size:10px;color:#afb1b3}.compiler-info span{margin-left:15px}.CodeMirror-hints{padding-left:0!important;border:1px solid #afb1b3;border-radius:4px;position:absolute;background-color:#f7f7f7;overflow-y:hidden;z-index:10;max-height:20em;box-shadow:2px 3px 5px rgba(0,0,0,.2)}.CodeMirror-hint{white-space:pre;cursor:pointer;border-radius:0;margin:0;padding-right:0!important;line-height:18px}li.CodeMirror-hint-active{background-color:#d8d8d8;color:#000}.CodeMirror-hint .name{float:left}.CodeMirror-hint .tail{float:right;padding-left:20px;margin-right:5px}.CodeMirror-hint .icon{margin-top:1px;margin-left:2px;height:16px;width:16px;float:left;margin-right:10px}.icon{background:url("https://try.kotlinlang.org/static/images/icons_all_sprite.svg")}.icon.class{background:url("https://try.kotlinlang.org/static/images/completion_class.svg")}.icon.package{background:url("https://try.kotlinlang.org/static/images/completion_package.svg")}.icon.method{background:url("https://try.kotlinlang.org/static/images/completion_method.svg")}.icon.genericValue{background:url("https://try.kotlinlang.org/static/images/completion_generic.svg")}.icon.property{background:url("https://try.kotlinlang.org/static/images/completion_property.svg")}', ""])
    }, function(t, e, n) {
        e = t.exports = n(8)(void 0), e.push([t.i, '.CodeMirror{font-family:monospace;height:300px;color:#000;direction:ltr}.CodeMirror-lines{padding:4px 0}.CodeMirror pre{padding:0 4px}.CodeMirror-gutter-filler,.CodeMirror-scrollbar-filler{background-color:#fff}.CodeMirror-gutters{border-right:1px solid #ddd;background-color:#f7f7f7;white-space:nowrap}.CodeMirror-linenumber{padding:0 3px 0 5px;min-width:20px;text-align:right;color:#999;white-space:nowrap}.CodeMirror-guttermarker{color:#000}.CodeMirror-guttermarker-subtle{color:#999}.CodeMirror-cursor{border-left:1px solid #000;border-right:none;width:0}.CodeMirror div.CodeMirror-secondarycursor{border-left:1px solid silver}.cm-fat-cursor .CodeMirror-cursor{width:auto;border:0!important;background:#7e7}.cm-fat-cursor div.CodeMirror-cursors{z-index:1}.cm-fat-cursor-mark{background-color:rgba(20,255,20,.5)}.cm-animate-fat-cursor,.cm-fat-cursor-mark{-webkit-animation:blink 1.06s steps(1) infinite;-moz-animation:blink 1.06s steps(1) infinite;animation:blink 1.06s steps(1) infinite}.cm-animate-fat-cursor{width:auto;border:0;background-color:#7e7}@-moz-keyframes blink{50%{background-color:transparent}}@-webkit-keyframes blink{50%{background-color:transparent}}@keyframes blink{50%{background-color:transparent}}.cm-tab{display:inline-block;text-decoration:inherit}.CodeMirror-rulers{position:absolute;left:0;right:0;top:-50px;bottom:-20px;overflow:hidden}.CodeMirror-ruler{border-left:1px solid #ccc;top:0;bottom:0;position:absolute}.cm-s-default .cm-header{color:blue}.cm-s-default .cm-quote{color:#090}.cm-negative{color:#d44}.cm-positive{color:#292}.cm-header,.cm-strong{font-weight:700}.cm-em{font-style:italic}.cm-link{text-decoration:underline}.cm-strikethrough{text-decoration:line-through}.cm-s-default .cm-keyword{color:#708}.cm-s-default .cm-atom{color:#219}.cm-s-default .cm-number{color:#164}.cm-s-default .cm-def{color:#00f}.cm-s-default .cm-variable-2{color:#05a}.cm-s-default .cm-type,.cm-s-default .cm-variable-3{color:#085}.cm-s-default .cm-comment{color:#a50}.cm-s-default .cm-string{color:#a11}.cm-s-default .cm-string-2{color:#f50}.cm-s-default .cm-meta,.cm-s-default .cm-qualifier{color:#555}.cm-s-default .cm-builtin{color:#30a}.cm-s-default .cm-bracket{color:#997}.cm-s-default .cm-tag{color:#170}.cm-s-default .cm-attribute{color:#00c}.cm-s-default .cm-hr{color:#999}.cm-s-default .cm-link{color:#00c}.cm-invalidchar,.cm-s-default .cm-error{color:red}.CodeMirror-composing{border-bottom:2px solid}div.CodeMirror span.CodeMirror-matchingbracket{color:#0b0}div.CodeMirror span.CodeMirror-nonmatchingbracket{color:#a22}.CodeMirror-matchingtag{background:rgba(255,150,0,.3)}.CodeMirror-activeline-background{background:#e8f2ff}.CodeMirror{position:relative;overflow:hidden;background:#fff}.CodeMirror-scroll{overflow:scroll!important;margin-bottom:-30px;margin-right:-30px;padding-bottom:30px;height:100%;outline:none;position:relative}.CodeMirror-sizer{position:relative;border-right:30px solid transparent}.CodeMirror-gutter-filler,.CodeMirror-hscrollbar,.CodeMirror-scrollbar-filler,.CodeMirror-vscrollbar{position:absolute;z-index:6;display:none}.CodeMirror-vscrollbar{right:0;top:0;overflow-x:hidden;overflow-y:scroll}.CodeMirror-hscrollbar{bottom:0;left:0;overflow-y:hidden;overflow-x:scroll}.CodeMirror-scrollbar-filler{right:0;bottom:0}.CodeMirror-gutter-filler{left:0;bottom:0}.CodeMirror-gutters{position:absolute;left:0;top:0;min-height:100%;z-index:3}.CodeMirror-gutter{white-space:normal;height:100%;display:inline-block;vertical-align:top;margin-bottom:-30px}.CodeMirror-gutter-wrapper{position:absolute;z-index:4;background:none!important;border:none!important}.CodeMirror-gutter-background{position:absolute;top:0;bottom:0;z-index:4}.CodeMirror-gutter-elt{position:absolute;cursor:default;z-index:4}.CodeMirror-gutter-wrapper ::selection{background-color:transparent}.CodeMirror-gutter-wrapper ::-moz-selection{background-color:transparent}.CodeMirror-lines{cursor:text;min-height:1px}.CodeMirror pre{-moz-border-radius:0;-webkit-border-radius:0;border-radius:0;border-width:0;background:transparent;font-family:inherit;font-size:inherit;margin:0;white-space:pre;word-wrap:normal;line-height:inherit;color:inherit;z-index:2;position:relative;overflow:visible;-webkit-tap-highlight-color:transparent;-webkit-font-variant-ligatures:contextual;font-variant-ligatures:contextual}.CodeMirror-wrap pre{word-wrap:break-word;white-space:pre-wrap;word-break:normal}.CodeMirror-linebackground{position:absolute;left:0;right:0;top:0;bottom:0;z-index:0}.CodeMirror-linewidget{position:relative;z-index:2;padding:.1px}.CodeMirror-rtl pre{direction:rtl}.CodeMirror-code{outline:none}.CodeMirror-gutter,.CodeMirror-gutters,.CodeMirror-linenumber,.CodeMirror-scroll,.CodeMirror-sizer{-moz-box-sizing:content-box;box-sizing:content-box}.CodeMirror-measure{position:absolute;width:100%;height:0;overflow:hidden;visibility:hidden}.CodeMirror-cursor{position:absolute;pointer-events:none}.CodeMirror-measure pre{position:static}div.CodeMirror-cursors{visibility:hidden;position:relative;z-index:3}.CodeMirror-focused div.CodeMirror-cursors,div.CodeMirror-dragcursors{visibility:visible}.CodeMirror-selected{background:#d9d9d9}.CodeMirror-focused .CodeMirror-selected{background:#d7d4f0}.CodeMirror-crosshair{cursor:crosshair}.CodeMirror-line::selection,.CodeMirror-line>span::selection,.CodeMirror-line>span>span::selection{background:#d7d4f0}.CodeMirror-line::-moz-selection,.CodeMirror-line>span::-moz-selection,.CodeMirror-line>span>span::-moz-selection{background:#d7d4f0}.cm-searching{background-color:#ffa;background-color:rgba(255,255,0,.4)}.cm-force-border{padding-right:.1px}@media print{.CodeMirror div.CodeMirror-cursors{visibility:hidden}}.cm-tab-wrap-hack:after{content:""}span.CodeMirror-selectedtext{background:none}', ""])
    }, function(t, e, n) {
        e = t.exports = n(8)(void 0), e.push([t.i, ".cm-s-idea span.cm-meta{color:olive}.cm-s-idea span.cm-number{color:#00f}.cm-s-idea span.cm-keyword{line-height:1em;font-weight:700;color:navy}.cm-s-idea span.cm-atom{font-weight:700;color:navy}.cm-s-idea span.cm-def,.cm-s-idea span.cm-operator,.cm-s-idea span.cm-property,.cm-s-idea span.cm-type,.cm-s-idea span.cm-variable,.cm-s-idea span.cm-variable-2,.cm-s-idea span.cm-variable-3{color:#000}.cm-s-idea span.cm-comment{color:gray}.cm-s-idea span.cm-string,.cm-s-idea span.cm-string-2{color:green}.cm-s-idea span.cm-qualifier{color:#555}.cm-s-idea span.cm-error{color:red}.cm-s-idea span.cm-attribute{color:#00f}.cm-s-idea span.cm-tag{color:navy}.cm-s-idea span.cm-link{color:#00f}.cm-s-idea .CodeMirror-activeline-background{background:#fffae3}.cm-s-idea span.cm-builtin{color:#30a}.cm-s-idea span.cm-bracket{color:#cc7}.cm-s-idea{font-family:Consolas,Menlo,Monaco,Lucida Console,Liberation Mono,DejaVu Sans Mono,Bitstream Vera Sans Mono,Courier New,monospace,serif}.cm-s-idea .CodeMirror-matchingbracket{outline:1px solid grey;color:#000!important}.CodeMirror-hints.idea{font-family:Menlo,Monaco,Consolas,Courier New,monospace;color:#616569;background-color:#ebf3fd!important}.CodeMirror-hints.idea .CodeMirror-hint-active{background-color:#a2b8c9!important;color:#5c6065!important}", ""])
    }, function(t, e, n) {
        e = t.exports = n(8)(void 0), e.push([t.i, ".cm-s-darcula span.cm-meta{color:#bbb529}.cm-s-darcula span.cm-number{color:#6897bb}.cm-s-darcula span.cm-keyword{line-height:1em;font-weight:700;color:#cc7832}.cm-s-darcula span.cm-def{color:#ffc66d}.cm-s-darcula span.cm-operator,.cm-s-darcula span.cm-property,.cm-s-darcula span.cm-type,.cm-s-darcula span.cm-variable,.cm-s-darcula span.cm-variable-2,.cm-s-darcula span.cm-variable-3{color:#a9b7c6}.cm-s-darcula span.cm-string,.cm-s-darcula span.cm-string-2{color:#6a8759}.cm-s-darcula span.cm-comment{color:gray}.cm-s-darcula span.cm-link{color:#287bde}.cm-s-darcula span.cm-atom{font-weight:700;color:#cc7832}.cm-s-darcula span.cm-error{color:#bc3f3c}.cm-s-darcula span.cm-tag{color:#cc7832}.cm-s-darcula span.cm-attribute,.cm-s-darcula span.cm-qualifier{color:#6a8759}.cm-s-darcula span.cm-bracket{color:#a9b7c6}.cm-s-darcula.CodeMirror{background:#2b2b2b;color:#a9b7c6}.cm-s-darcula .CodeMirror-cursor{border-left:1px solid #ddd}.cm-s-darcula .CodeMirror-activeline-background{background:#3a3a3a}.cm-s-darcula div.CodeMirror-selected{background:#085a9c}.cm-s-darcula .CodeMirror-gutters{background:#484848;border-right:1px solid grey;color:#606366}.cm-s-darcula span.cm-builtin{color:#a9b7c6}.cm-s-darcula{font-family:Consolas,Menlo,Monaco,Lucida Console,Liberation Mono,DejaVu Sans Mono,Bitstream Vera Sans Mono,Courier New,monospace,serif}.cm-s-darcula .CodeMirror-matchingbracket{background-color:#3b514d;color:#ff0!important}.CodeMirror-hints.darcula{font-family:Menlo,Monaco,Consolas,Courier New,monospace;color:#9c9e9e;background-color:#3b3e3f!important}.CodeMirror-hints.darcula .CodeMirror-hint-active{background-color:#494d4e!important;color:#9c9e9e!important}", ""])
    }, function(t, e, n) {
        e = t.exports = n(8)(void 0), e.push([t.i, ".CodeMirror-simplescroll-horizontal div,.CodeMirror-simplescroll-vertical div{position:absolute;background:#ccc;-moz-box-sizing:border-box;box-sizing:border-box;border:1px solid #bbb;border-radius:2px}.CodeMirror-simplescroll-horizontal,.CodeMirror-simplescroll-vertical{position:absolute;z-index:6;background:#eee}.CodeMirror-simplescroll-horizontal{bottom:0;left:0;height:8px}.CodeMirror-simplescroll-horizontal div{bottom:0;height:100%}.CodeMirror-simplescroll-vertical{right:0;top:0;width:8px}.CodeMirror-simplescroll-vertical div{right:0;width:100%}.CodeMirror-overlayscroll .CodeMirror-gutter-filler,.CodeMirror-overlayscroll .CodeMirror-scrollbar-filler{display:none}.CodeMirror-overlayscroll-horizontal div,.CodeMirror-overlayscroll-vertical div{position:absolute;background:#bcd;border-radius:3px}.CodeMirror-overlayscroll-horizontal,.CodeMirror-overlayscroll-vertical{position:absolute;z-index:6}.CodeMirror-overlayscroll-horizontal{bottom:0;left:0;height:6px}.CodeMirror-overlayscroll-horizontal div{bottom:0;height:100%}.CodeMirror-overlayscroll-vertical{right:0;top:0;width:6px}.CodeMirror-overlayscroll-vertical div{right:0;width:100%}", ""])
    }, function(t, e) {
        t.exports = "\"data:image/svg+xml,%3C?xml version='1.0' encoding='utf-8'?%3E %3Csvg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' x='0px' y='0px' viewBox='0 0 16.996 20.999' enable-background='new 0 0 16.996 20.999' xml:space='preserve'%3E %3Cpolygon fill='%2338B058' points='16.996,10.499 0,20.999 0,0 '/%3E %3C/svg%3E\""
    }, function(t, e) {
        t.exports = "\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='16' height='16' viewBox='0 0 16 16'%3E %3Ctitle%3Eicons_all_sprite%3C/title%3E %3Cg id='Layer_4' data-name='Layer 4'%3E %3Cg%3E %3Ccircle cx='8' cy='8' r='6' fill='%23e05219' opacity='0.9' style='isolation: isolate'/%3E %3Cpath d='M8.99609,4.98828V6.48193a.4905.4905,0,0,1-.00976.09619L8.28174,9H7.71045L7.00586,6.57812A.44233.44233,0,0,1,7,6.521V5c0-.26465,0-.99023.47559-.99023h1.041C9,4.00977,8.99609,4.72363,8.99609,4.98828Zm-.10547,5.9329L8.42627,10.31a.47056.47056,0,0,0-.37354-.18943H7.94092a.47078.47078,0,0,0-.374.18943l-.46435.61117A.52557.52557,0,0,0,7,11.22882v.28184A.49088.49088,0,0,0,7.47607,12h1.041a.49312.49312,0,0,0,.479-.50638l.00049-.25447A.52333.52333,0,0,0,8.89062,10.92118Z' fill='%23fff'/%3E %3C/g%3E %3C/g%3E %3C/svg%3E\""
    }, function(t, e) {
        t.exports = "\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='16' height='16' viewBox='0 0 16 16'%3E %3Ctitle%3Eicons_all_sprite%3C/title%3E %3Cg id='Layer_4' data-name='Layer 4'%3E %3Ccircle cx='8' cy='8' r='7' fill='%234ca01f' opacity='0.9' style='isolation: isolate'/%3E %3Cpolygon points='12.808 6.205 11.5 5.025 7.486 9.625 4.346 6.077 3.125 7.182 7.5 12 12.808 6.205' fill='%23fff'/%3E %3C/g%3E %3C/svg%3E\""
    }, function(t, e) {
        t.exports = "\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='16' height='16' viewBox='0 0 16 16'%3E %3Ctitle%3Eicons_all_sprite%3C/title%3E %3Cg id='Layer_4' data-name='Layer 4'%3E %3Cg style='isolation: isolate'%3E %3Ccircle cx='7.5' cy='7.5' r='6.5' fill='%23e05219'/%3E %3C/g%3E %3Cpolygon points='10.328 9.621 8.207 7.5 10.328 5.379 9.621 4.672 7.5 6.793 5.379 4.672 4.672 5.379 6.793 7.5 4.672 9.621 5.379 10.328 7.5 8.207 9.621 10.328 10.328 9.621' fill='%23fff'/%3E %3C/g%3E %3C/svg%3E\""
    }, function(t, e) {
        t.exports = "\"data:image/svg+xml,%3C!-- Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E %3Csvg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 31 19' enable-background='new 0 0 31 19' xml:space='preserve'%3E %3Cg%3E %3Cpath fill='%23FFFFFF' d='M21.5,19h-12C4.253,19,0,14.747,0,9.5v0C0,4.253,4.253,0,9.5,0h12C26.747,0,31,4.253,31,9.5v0 C31,14.747,26.747,19,21.5,19z' /%3E %3Cpath class='data-fill' fill='%23d4d4d4' d='M21.5,1C26.187,1,30,4.813,30,9.5c0,4.687-3.813,8.5-8.5,8.5h-12C4.813,18,1,14.187,1,9.5 C1,4.813,4.813,1,9.5,1H21.5 M21.5,0h-12C4.253,0,0,4.253,0,9.5v0C0,14.747,4.253,19,9.5,19h12c5.247,0,9.5-4.253,9.5-9.5v0 C31,4.253,26.747,0,21.5,0L21.5,0z' /%3E %3C/g%3E %3Cg%3E %3Cg%3E %3Crect x='10' y='9' fill='%23FFFFFF' width='11' height='1' /%3E %3Cpolygon class='data-fill' fill='%23d4d4d4' points='21,9 10,9 10,10 21,10 21,9 ' /%3E %3C/g%3E %3Cg%3E %3Crect x='10' y='9' transform='matrix(-1.836970e-16 1 -1 -1.836970e-16 25 -6)' fill='%23FFFFFF' width='11' height='1' /%3E %3Cpolygon class='data-fill' fill='%23d4d4d4' points='16,4 15,4 15,15 16,15 16,4 ' /%3E %3C/g%3E %3C/g%3E %3C/svg%3E\""
    }, function(t, e) {
        t.exports = "\"data:image/svg+xml,%3C!-- Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E %3Csvg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 31 19' enable-background='new 0 0 31 19' xml:space='preserve'%3E %3Cg%3E %3Cpath fill='%23FFFFFF' d='M21.5,19h-12C4.253,19,0,14.747,0,9.5v0C0,4.253,4.253,0,9.5,0h12C26.747,0,31,4.253,31,9.5v0 C31,14.747,26.747,19,21.5,19z' /%3E %3Cpath class='data-fill' fill='%23f68322' d='M21.5,1C26.187,1,30,4.813,30,9.5c0,4.687-3.813,8.5-8.5,8.5h-12C4.813,18,1,14.187,1,9.5 C1,4.813,4.813,1,9.5,1H21.5 M21.5,0h-12C4.253,0,0,4.253,0,9.5v0C0,14.747,4.253,19,9.5,19h12c5.247,0,9.5-4.253,9.5-9.5v0 C31,4.253,26.747,0,21.5,0L21.5,0z' /%3E %3C/g%3E %3Cg%3E %3Cg%3E %3Crect x='10' y='9' fill='%23FFFFFF' width='11' height='1' /%3E %3Cpolygon class='data-fill' fill='%23f68322' points='21,9 10,9 10,10 21,10 21,9 ' /%3E %3C/g%3E %3Cg%3E %3Crect x='10' y='9' transform='matrix(-1.836970e-16 1 -1 -1.836970e-16 25 -6)' fill='%23FFFFFF' width='11' height='1' /%3E %3Cpolygon class='data-fill' fill='%23f68322' points='16,4 15,4 15,15 16,15 16,4 ' /%3E %3C/g%3E %3C/g%3E %3C/svg%3E\""
    }, function(t, e) {
        t.exports = "\"data:image/svg+xml,%3C?xml version='1.0' encoding='utf-8'?%3E %3C!-- Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E %3Csvg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' x='0px' y='0px' viewBox='0 0 31 19' enable-background='new 0 0 31 19' xml:space='preserve'%3E %3Cg%3E %3Cpath fill='%232B2B2B' d='M21.5,19h-12C4.253,19,0,14.747,0,9.5v0C0,4.253,4.253,0,9.5,0h12C26.747,0,31,4.253,31,9.5v0 C31,14.747,26.747,19,21.5,19z'/%3E %3Cpath class='data-fill' fill='%23ffffff' d='M21.5,1C26.187,1,30,4.813,30,9.5c0,4.687-3.813,8.5-8.5,8.5h-12C4.813,18,1,14.187,1,9.5 C1,4.813,4.813,1,9.5,1H21.5 M21.5,0h-12C4.253,0,0,4.253,0,9.5v0C0,14.747,4.253,19,9.5,19h12c5.247,0,9.5-4.253,9.5-9.5v0 C31,4.253,26.747,0,21.5,0L21.5,0z'/%3E %3C/g%3E %3Cg%3E %3Cg%3E %3Crect x='10' y='9' fill='%23EAEAEC' width='11' height='1'/%3E %3Cpolygon class='data-fill' fill='%23EAEAEC' points='21,9 10,9 10,10 21,10 21,9 '/%3E %3C/g%3E %3Cg%3E %3Crect x='10' y='9' transform='matrix(-1.836970e-16 1 -1 -1.836970e-16 25 -6)' fill='%23EAEAEC' width='11' height='1'/%3E %3Cpolygon class='data-fill' fill='%23EAEAEC' points='16,4 15,4 15,15 16,15 16,4 '/%3E %3C/g%3E %3C/g%3E %3C/svg%3E\""
    }, function(t, e) {
        t.exports = "\"data:image/svg+xml,%3C!-- Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E %3Csvg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' x='0px' y='0px' viewBox='0 0 31 19' enable-background='new 0 0 31 19' xml:space='preserve'%3E %3Cg%3E %3Cpath fill='%232B2B2B' d='M21.5,19h-12C4.253,19,0,14.747,0,9.5v0C0,4.253,4.253,0,9.5,0h12C26.747,0,31,4.253,31,9.5v0 C31,14.747,26.747,19,21.5,19z' /%3E %3Cpath class='data-fill' fill='%23f68322' d='M21.5,1C26.187,1,30,4.813,30,9.5c0,4.687-3.813,8.5-8.5,8.5h-12C4.813,18,1,14.187,1,9.5 C1,4.813,4.813,1,9.5,1H21.5 M21.5,0h-12C4.253,0,0,4.253,0,9.5v0C0,14.747,4.253,19,9.5,19h12c5.247,0,9.5-4.253,9.5-9.5v0 C31,4.253,26.747,0,21.5,0L21.5,0z' /%3E %3C/g%3E %3Cg%3E %3Cg%3E %3Crect x='10' y='9' fill='%23EAEAEC' width='11' height='1' /%3E %3Cpolygon class='data-fill' fill='%23f68322' points='21,9 10,9 10,10 21,10 21,9 ' /%3E %3C/g%3E %3Cg%3E %3Crect x='10' y='9' transform='matrix(-1.836970e-16 1 -1 -1.836970e-16 25 -6)' fill='%23EAEAEC' width='11' height='1' /%3E %3Cpolygon class='data-fill' fill='%23f68322' points='16,4 15,4 15,15 16,15 16,4 ' /%3E %3C/g%3E %3C/g%3E %3C/svg%3E\""
    }, function(t, e) {
        t.exports = "\"data:image/svg+xml,%3C!-- Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E %3Csvg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 31 19' enable-background='new 0 0 31 19' xml:space='preserve'%3E %3Cg%3E %3Cpath fill='%23FFFFFF' d='M21.5,19h-12C4.253,19,0,14.747,0,9.5v0C0,4.253,4.253,0,9.5,0h12C26.747,0,31,4.253,31,9.5v0 C31,14.747,26.747,19,21.5,19z' /%3E %3Cpath class='data-fill' fill='%23d4d4d4' d='M21.5,1C26.187,1,30,4.813,30,9.5c0,4.687-3.813,8.5-8.5,8.5h-12C4.813,18,1,14.187,1,9.5 C1,4.813,4.813,1,9.5,1H21.5 M21.5,0h-12C4.253,0,0,4.253,0,9.5v0C0,14.747,4.253,19,9.5,19h12c5.247,0,9.5-4.253,9.5-9.5v0 C31,4.253,26.747,0,21.5,0L21.5,0z' /%3E %3C/g%3E %3Cg%3E %3Cg%3E %3Crect x='10' y='9' transform='matrix(0.7071 0.7071 -0.7071 0.7071 11.2574 -8.1777)' fill='%23FFFFFF' width='11' height='1' /%3E %3Cpolygon class='data-fill' fill='%23d4d4d4' points='11.964,5.257 11.257,5.964 19.036,13.743 19.743,13.036 11.964,5.257 ' /%3E %3C/g%3E %3Cg%3E %3Crect x='10' y='9' transform='matrix(-0.7071 0.7071 -0.7071 -0.7071 33.1777 5.2574)' fill='%23FFFFFF' width='11' height='1' /%3E %3Cpolygon class='data-fill' fill='%23d4d4d4' points='19.036,5.257 11.257,13.036 11.964,13.743 19.743,5.964 19.036,5.257 ' /%3E %3C/g%3E %3C/g%3E %3C/svg%3E\""
    }, function(t, e) {
        t.exports = "\"data:image/svg+xml,%3C!-- Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E %3Csvg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 31 19' enable-background='new 0 0 31 19' xml:space='preserve'%3E %3Cg%3E %3Cpath fill='%23FFFFFF' d='M21.5,19h-12C4.253,19,0,14.747,0,9.5v0C0,4.253,4.253,0,9.5,0h12C26.747,0,31,4.253,31,9.5v0 C31,14.747,26.747,19,21.5,19z' /%3E %3Cpath class='data-fill' fill='%23f68322' d='M21.5,1C26.187,1,30,4.813,30,9.5c0,4.687-3.813,8.5-8.5,8.5h-12C4.813,18,1,14.187,1,9.5 C1,4.813,4.813,1,9.5,1H21.5 M21.5,0h-12C4.253,0,0,4.253,0,9.5v0C0,14.747,4.253,19,9.5,19h12c5.247,0,9.5-4.253,9.5-9.5v0 C31,4.253,26.747,0,21.5,0L21.5,0z' /%3E %3C/g%3E %3Cg%3E %3Cg%3E %3Crect x='10' y='9' transform='matrix(0.7071 0.7071 -0.7071 0.7071 11.2574 -8.1777)' fill='%23FFFFFF' width='11' height='1' /%3E %3Cpolygon class='data-fill' fill='%23f68322' points='11.964,5.257 11.257,5.964 19.036,13.743 19.743,13.036 11.964,5.257 ' /%3E %3C/g%3E %3Cg%3E %3Crect x='10' y='9' transform='matrix(-0.7071 0.7071 -0.7071 -0.7071 33.1777 5.2574)' fill='%23FFFFFF' width='11' height='1' /%3E %3Cpolygon class='data-fill' fill='%23f68322' points='19.036,5.257 11.257,13.036 11.964,13.743 19.743,5.964 19.036,5.257 ' /%3E %3C/g%3E %3C/g%3E %3C/svg%3E\""
    }, function(t, e) {
        t.exports = "\"data:image/svg+xml,%3C?xml version='1.0' encoding='utf-8'?%3E %3C!-- Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E %3Csvg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' x='0px' y='0px' viewBox='0 0 31 19' enable-background='new 0 0 31 19' xml:space='preserve'%3E %3Cg%3E %3Cpath fill='%232B2B2B' d='M21.5,19h-12C4.253,19,0,14.747,0,9.5v0C0,4.253,4.253,0,9.5,0h12C26.747,0,31,4.253,31,9.5v0 C31,14.747,26.747,19,21.5,19z'/%3E %3Cpath class='data-fill' fill='%23EAEAEC' d='M21.5,1C26.187,1,30,4.813,30,9.5c0,4.687-3.813,8.5-8.5,8.5h-12C4.813,18,1,14.187,1,9.5 C1,4.813,4.813,1,9.5,1H21.5 M21.5,0h-12C4.253,0,0,4.253,0,9.5v0C0,14.747,4.253,19,9.5,19h12c5.247,0,9.5-4.253,9.5-9.5v0 C31,4.253,26.747,0,21.5,0L21.5,0z'/%3E %3C/g%3E %3Cg%3E %3Cg%3E %3Crect x='10' y='9' transform='matrix(0.7071 0.7071 -0.7071 0.7071 11.2574 -8.1777)' fill='%23FFFFFF' width='11' height='1'/%3E %3Cpolygon class='data-fill' fill='%23EAEAEC' points='11.964,5.257 11.257,5.964 19.036,13.743 19.743,13.036 11.964,5.257 '/%3E %3C/g%3E %3Cg%3E %3Crect x='10' y='9' transform='matrix(-0.7071 0.7071 -0.7071 -0.7071 33.1777 5.2574)' fill='%23FFFFFF' width='11' height='1'/%3E %3Cpolygon class='data-fill' fill='%23EAEAEC' points='19.036,5.257 11.257,13.036 11.964,13.743 19.743,5.964 19.036,5.257 '/%3E %3C/g%3E %3C/g%3E %3C/svg%3E\""
    }, function(t, e) {
        t.exports = "\"data:image/svg+xml,%3C!-- Generator: Adobe Illustrator 21.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E %3Csvg version='1.1' id='Layer_1' xmlns='http://www.w3.org/2000/svg' x='0px' y='0px' viewBox='0 0 31 19' enable-background='new 0 0 31 19' xml:space='preserve'%3E %3Cg%3E %3Cpath fill='%232B2B2B' d='M21.5,19h-12C4.253,19,0,14.747,0,9.5v0C0,4.253,4.253,0,9.5,0h12C26.747,0,31,4.253,31,9.5v0 C31,14.747,26.747,19,21.5,19z' /%3E %3Cpath class='data-fill' fill='%23f68322' d='M21.5,1C26.187,1,30,4.813,30,9.5c0,4.687-3.813,8.5-8.5,8.5h-12C4.813,18,1,14.187,1,9.5 C1,4.813,4.813,1,9.5,1H21.5 M21.5,0h-12C4.253,0,0,4.253,0,9.5v0C0,14.747,4.253,19,9.5,19h12c5.247,0,9.5-4.253,9.5-9.5v0 C31,4.253,26.747,0,21.5,0L21.5,0z' /%3E %3C/g%3E %3Cg%3E %3Cg%3E %3Crect x='10' y='9' transform='matrix(0.7071 0.7071 -0.7071 0.7071 11.2574 -8.1777)' fill='%23FFFFFF' width='11' height='1' /%3E %3Cpolygon class='data-fill' fill='%23f68322' points='11.964,5.257 11.257,5.964 19.036,13.743 19.743,13.036 11.964,5.257 ' /%3E %3C/g%3E %3Cg%3E %3Crect x='10' y='9' transform='matrix(-0.7071 0.7071 -0.7071 -0.7071 33.1777 5.2574)' fill='%23FFFFFF' width='11' height='1' /%3E %3Cpolygon class='data-fill' fill='%23f68322' points='19.036,5.257 11.257,13.036 11.964,13.743 19.743,5.964 19.036,5.257 ' /%3E %3C/g%3E %3C/g%3E %3C/svg%3E\""
    }]).default
});
//# sourceMappingURL=playground.min.js.map